# Written by Andreas "Cold Evul" Brostrom

# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

#Iran supported Sudan during the Sudanese civil war
Iran_support_Bashir = {

	enable = {
		original_tag = PER
	}
	
	abort = {
		SUD = { 
			OR = { 
				war_with_us = yes 
				has_government = fascism
				has_government = democratic
				has_government = nationalist
			}
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "SUD"
		value = 50
	}
	
	ai_strategy = {
		type = influence
		id = "SUD"
		value = 50
	} 
	
	ai_strategy = {
		type = support
		id = "SUD"
		value = 100
	}
}

#To avoid volunteer forces from defeating ISIS alone
Iran_IRQ = {

	enable = {
		original_tag = PER
	}
	
	abort = {
		IRQ = { 
			OR = { 
				war_with_us = yes 
				is_bad_salafist = yes
			}
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "IRQ"
		value = 50
	}
	
	ai_strategy = {
		type = protect 
		id = "IRQ"
		value = 50
	}
	
	ai_strategy = {
		type = influence
		id = "IRQ"
		value = 50
	} 
	
	ai_strategy = {
		type = support
		id = "IRQ"
		value = 50
	}
}

Iraq_IRQ_dont_send_volunteers_against_ISIS = {

	enable = {
		original_tag = PER
	}
	
	abort = {				#Allow sending volunteers once ISIS is defeated, or Iraq is at war with someone else
		OR = {
			IRQ = { NOT = { has_war_with = ISI } }
			any_country = {
				NOT = { tag = IRQ }
				NOT = { tag = ISI }
				has_war_with = IRQ 
			}
		}
	}
	
	ai_strategy = {
		type = send_volunteers_desire
		id = "IRQ"
		value = -400
	}
}

#Iran support Shias
PER_support_shias = {

	reversed = yes
	
	enable = {
		NOT = { tag = PER }
		has_idea = shia
	}
	
	abort = {
		has_country_flag = Insulted_Iran
	}
	
	ai_strategy = {
		type = befriend
		id = "PER"
		value = 200
	}
	
	ai_strategy = {
		type = protect
		id = "PER"
		value = 200
	}
	
	ai_strategy = {
		type = support
		id = "PER"
		value = 200
	}
	
	ai_strategy = {
		type = send_volunteers_desire
		id = "PER"
		value = 200
	}
	
	ai_strategy = {
		type = influence
		id = "PER"
		value = 200
	}
	
}

#Strategic enemies
Iran_USA_Relations = {
	enable = {
		original_tag = PER
	}
	abort = {
		always = no
	}
	ai_strategy = {
		type = antagonize
		id = "USA"
		value = 50
	}
	ai_strategy = {
		type = alliance
		id = "USA"
		value = -200
	}
	ai_strategy = {
		type = support
		id = "USA"
		value = -200
	}
}

Iran_Saudi_Relations = {
	enable = {
		original_tag = PER
	}
	abort = {
		always = no
	}
	ai_strategy = {
		type = antagonize
		id = "SAU"
		value = 100
	}
	ai_strategy = {
		type = alliance
		id = "SAU"
		value = -200
	}
	ai_strategy = {
		type = support
		id = "SAU"
		value = -200
	}
	ai_strategy = {
		type = contain
		id = "SAU"
		value = 100
	}
}

Iran_Israel_Relations = {
	enable = {
		original_tag = PER
	}
	abort = {
		always = no
	}
	ai_strategy = {
		type = befriend 
		id = "ISR"
		value = -100
	}
	ai_strategy = {
		type = conquer 
		id = "ISR"
		value = 25
	}
	ai_strategy = {
		type = invade 
		id = "ISR"
		value = 25
	}
	ai_strategy = {
		type = antagonize
		id = "ISR"
		value = 100
	}
	ai_strategy = {
		type = alliance
		id = "ISR"
		value = -200
	}
	ai_strategy = {
		type = support
		id = "ISR"
		value = -200
	}
	ai_strategy = {
		type = contain
		id = "ISR"
		value = 100
	}
}

#Frenemies
Iran_Turkey_Relations = {
	enable = {
		original_tag = PER
	}
	abort = {
		always = no
	}
	ai_strategy = {
		type = befriend 
		id = "TUR"
		value = 50
	}
	ai_strategy = {
		type = protect 
		id = "TUR"
		value = -50
	}
	ai_strategy = {
		type = influence
		id = "TUR"
		value = 50
	} 
	ai_strategy = {
		type = antagonize
		id = "TUR"
		value = 25
	}
	ai_strategy = {
		type = alliance
		id = "TUR"
		value = -50
	}
	ai_strategy = {
		type = support
		id = "TUR"
		value = -50
	}
	ai_strategy = {
		type = contain
		id = "TUR"
		value = 25
	}
}

#Friends
Iran_India_Relations = {
	enable = {
		original_tag = PER
	}
	abort = {
		always = no
	}
	ai_strategy = {
		type = befriend 
		id = "RAJ"
		value = 50
	}
	ai_strategy = {
		type = protect 
		id = "RAJ"
		value = 25
	}
	ai_strategy = {
		type = influence
		id = "RAJ"
		value = 50
	} 
	ai_strategy = {
		type = antagonize
		id = "RAJ"
		value = -50
	}
	ai_strategy = {
		type = alliance
		id = "RAJ"
		value = 50
	}
	ai_strategy = {
		type = support
		id = "RAJ"
		value = 100
	}
	ai_strategy = {
		type = contain
		id = "RAJ"
		value = -50
	}
}

Iran_stop_sending_stuff = {
	enable = {
		original_tag = PER
	}
	abort = {
		always = no
	}
	ai_strategy = {
		type = send_volunteers_desire
		id = "HOR"
		value = -300
	}
	ai_strategy = {
		type = send_volunteers_desire
		id = "HOU"
		value = -300
	}
	ai_strategy = {
		type = send_volunteers_desire
		id = "SEL"
		value = -300
	}
}

Iran_area_priority = {
	enable = {
		original_tag = PER
	}
	ai_strategy = {
		type = area_priority
		id = europe
		value = -50
	}
	ai_strategy = {
		type = area_priority
		id = north_america
		value = -50
	}
	ai_strategy = {
		type = area_priority
		id = caribbean
		value = -50
	}
	ai_strategy = {
		type = area_priority
		id = south_america
		value = -25
	}
	ai_strategy = {
		type = area_priority
		id = asia
		value = 100
	}
	ai_strategy = {
		type = area_priority
		id = pacific
		value = -50
	}
	ai_strategy = {
		type = area_priority
		id = oceania
		value = -50
	}
	ai_strategy = {
		type = area_priority
		id = middle_east
		value = 200
	}
	ai_strategy = {
		type = area_priority
		id = africa
		value = -25
	}
}