# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

#Egypt is heavily trying to bring peace into Libya
Egypt_Intervention_Libya_HOR = {
	
	enable = {
		original_tag = EGY
	}
	
	abort = {
		HOR = { 
			OR = { 
				war_with_us = yes 
				is_bad_salafist = yes
				in_different_faction = yes
			}
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "HOR"
		value = 50
	}
	
	ai_strategy = {
		type = protect 
		id = "HOR"
		value = 50
	}
	
	#We want a peaceful solution
	ai_strategy = {
		type = contain
		id = "HOR"
		value = 50
	}
	
	ai_strategy = {
		type = influence
		id = "HOR"
		value = 25
	}
}
Egypt_Intervention_Libya_GNA = {
	
	enable = {
		original_tag = EGY
	}
	
	abort = {
		GNA = { 
			OR = { 
				war_with_us = yes 
				is_bad_salafist = yes
				in_different_faction = yes
			}
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "GNA"
		value = 50
	}
	
	ai_strategy = {
		type = protect 
		id = "GNA"
		value = 50
	}
	
	#We want a peaceful solution
	ai_strategy = {
		type = contain
		id = "GNA"
		value = 50
	}
	
	ai_strategy = {
		type = influence
		id = "GNA"
		value = 25
	}
}

#Egypt supports South Sudan to gain access to Nile
Egypt_befriend_south_sudan = {

	enable = {
		original_tag = EGY
	}
	
	abort = {
		SSU = { 
			OR = { 
				war_with_us = yes 
				is_bad_salafist = yes
				in_different_faction = yes
			}
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "SSU"
		value = 50
	}
	
	ai_strategy = {
		type = protect 
		id = "SSU"
		value = 50
	}
	
	ai_strategy = {
		type = influence
		id = "SSU"
		value = 50
	}
	
	ai_strategy = {
		type = support
		id = "SSU"
		value = 50
	}

}

#Egypt supports Yemen
EGY_YEM = {

	enable = {
		original_tag = EGY
	}
	
	abort = {
		YEM = { 
			OR = { 
				war_with_us = yes
				is_bad_salafist = yes
			}
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "YEM"
		value = 50
	}
	
	ai_strategy = {
		type = protect 
		id = "YEM"
		value = 50
	}
	
	ai_strategy = {
		type = influence
		id = "YEM"
		value = 50
	} 
	
	ai_strategy = {
		type = support
		id = "YEM"
		value = 100
	}
	
	ai_strategy = {
		type = send_volunteers_desire
		id = "YEM"
		value = 100
	}
}