# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

#Pakistan supports Yemen
PAK_YEM = {
	enable = {
		original_tag = PAK
	}
	abort = {
		YEM = {
			OR = {
				war_with_us = yes
			}
		}
	}
	ai_strategy = {
		type = support
		id = "YEM"
		value = 200
	}
}

#Hate India
Pakistan_India_Relations = {
	enable = {
		original_tag = PAK
	}
	abort = {
		always = no
	}
	ai_strategy = {
		type = conquer
		id = "RAJ"
		value = 200
	}
	ai_strategy = {
		type = antagonize
		id = "RAJ"
		value = 200
	}
	ai_strategy = {
		type = alliance
		id = "RAJ"
		value = -200
	}
	ai_strategy = {
		type = support
		id = "RAJ"
		value = -200
	}
	ai_strategy = {
		type = contain
		id = "RAJ"
		value = 200
	}
}

#Pakistan loves China
Pakistan_China_Relations = {
	enable = {
		original_tag = PAK
	}
	abort = {
		NOT = { PAK = { is_guaranteed_by = CHI } }
	}
	ai_strategy = {
		type = antagonize
		id = "CHI"
		value = -50
	}
	ai_strategy = {
		type = contain
		id = "CHI"
		value = -50
	}
	ai_strategy = {
		type = alliance
		id = "CHI"
		value = 50
	}
	ai_strategy = {
		type = support
		id = "CHI"
		value = 50
	}
	ai_strategy = {
		type = befriend
		id = "CHI"
		value = 150
	}
}

#Likes Saudi Arabia
Pakistan_Saudi_Relations = {
	enable = {
		original_tag = PAK
	}
	abort = {
		PAK = { is_guaranteed_by = PER } #reconsider if Iran support us
	}
	ai_strategy = {
		type = antagonize
		id = "SAU"
		value = -50
	}
	ai_strategy = {
		type = contain
		id = "SAU"
		value = -50
	}
	ai_strategy = {
		type = alliance
		id = "SAU"
		value = 25
	}
	ai_strategy = {
		type = support
		id = "SAU"
		value = 50
	}
	ai_strategy = {
		type = befriend
		id = "SAU"
		value = 100
	}
	ai_strategy = {
		type = send_volunteers_desire
		id = "SAU"
		value = 200
	}
}

#Likes Indonesia
Pakistan_Indonesia_Relations = {
	enable = {
		original_tag = PAK
	}
	abort = {
		RAJ = { is_guaranteed_by = IND }
	}
	ai_strategy = {
		type = antagonize
		id = "IND"
		value = -50
	}
	ai_strategy = {
		type = contain
		id = "IND"
		value = -50
	}
	ai_strategy = {
		type = alliance
		id = "IND"
		value = 25
	}
	ai_strategy = {
		type = support
		id = "IND"
		value = 50
	}
	ai_strategy = {
		type = befriend
		id = "IND"
		value = 50
	}
}

#Likes Malaysia
Pakistan_Malay_Relations = {
	enable = {
		original_tag = PAK
	}
	abort = {
		RAJ = { is_guaranteed_by = MAY }
	}
	ai_strategy = {
		type = antagonize
		id = "MAY"
		value = -50
	}
	ai_strategy = {
		type = contain
		id = "MAY"
		value = -50
	}
	ai_strategy = {
		type = alliance
		id = "MAY"
		value = 25
	}
	ai_strategy = {
		type = support
		id = "MAY"
		value = 50
	}
	ai_strategy = {
		type = befriend
		id = "MAY"
		value = 50
	}
}

#Pakistan likes USA if they help us
Pakistan_USA_Relations = {
	enable = {
		original_tag = PAK
		PAK = { is_guaranteed_by = USA }
	}
	abort = {
		NOT = { PAK = { is_guaranteed_by = USA } }
	}
	ai_strategy = {
		type = antagonize
		id = "USA"
		value = -25
	}
	ai_strategy = {
		type = contain
		id = "USA"
		value = -25
	}
	ai_strategy = {
		type = alliance
		id = "USA"
		value = 25
	}
	ai_strategy = {
		type = support
		id = "USA"
		value = 50
	}
	ai_strategy = {
		type = befriend
		id = "USA"
		value = 100
	}
}

#Distrusts Iran
Pakistan_Iran_Relations = {
	enable = {
		original_tag = PAK
	}
	abort = {
		PAK = { is_guaranteed_by = PER }
	}
	ai_strategy = {
		type = antagonize
		id = "PER"
		value = 25
	}
	ai_strategy = {
		type = contain
		id = "PER"
		value = 25
	}
	ai_strategy = {
		type = alliance
		id = "PER"
		value = -50
	}
	ai_strategy = {
		type = support
		id = "PER"
		value = -50
	}
	ai_strategy = {
		type = befriend
		id = "PER"
		value = -150
	}
}

#Dislikes Russia
Pakistan_Russia_Relations = {
	enable = {
		original_tag = PAK
	}
	abort = {
		always = no
	}
	ai_strategy = {
		type = antagonize
		id = "SOV"
		value = 25
	}
	ai_strategy = {
		type = contain
		id = "SOV"
		value = -25
	}
	ai_strategy = {
		type = support
		id = "SOV"
		value = -50
	}
	ai_strategy = {
		type = alliance
		id = "SOV"
		value = -50
	}
	ai_strategy = {
		type = befriend
		id = "SOV"
		value = -150
	}
}

Pakistan_Taliban_relations = {
	enable = {
		original_tag = PAK
	}
	abort = {
		always = no
	}
	ai_strategy = {
		type = antagonize
		id = "TTP"
		value = 200
	}
	ai_strategy = {
		type = invade
		id = "TTP"
		value = 200
	}
	ai_strategy = {
		type = conquer
		id = "TTP"
		value = 200
	}
	ai_strategy = {
		type = support
		id = "TTP"
		value = -200
	}
		ai_strategy = {
		type = befriend
		id = "TTP"
		value = -200
	}
		ai_strategy = {
		type = contain
		id = "TTP"
		value = 100
	}
}

#Afghanistan - Supports the Taliban
Pakistan_AFGTaliban_Relations = {
	enable = {
		original_tag = PAK
	}
	abort = {
		OR = {
			TAL = { NOT = { has_government = fascism } }
			AFG = { has_government = fascism }
		}
	}
	ai_strategy = {
		type = antagonize
		id = "TAL"
		value = -100
	}
	ai_strategy = {
		type = contain
		id = "TAL"
		value = -100
	}
	ai_strategy = {
		type = support
		id = "TAL"
		value = 200
	}
	ai_strategy = {
		type = befriend
		id = "TAL"
		value = 150
	}
	ai_strategy = {
		type = protect 
		id = "TAL"
		value = -50
	}
	ai_strategy = {
		type = send_volunteers_desire
		id = "TAL"
		value = -50
	}
	ai_strategy = {
		type = antagonize
		id = "AFG"
		value = 100
	}
	ai_strategy = {
		type = contain
		id = "AFG"
		value = 50
	}
	ai_strategy = {
		type = support
		id = "AFG"
		value = -200
	}
	ai_strategy = {
		type = befriend
		id = "AFG"
		value = -200
	}
	ai_strategy = {
		type = protect 
		id = "AFG"
		value = -50
	}
	ai_strategy = {
		type = send_volunteers_desire
		id = "AFG"
		value = -200
	}
}

Pakistan_AFG_Relations = {
	enable = {
		original_tag = PAK
		AFG = { has_government = fascism }
	}
	abort = {
	OR = {
		AFG = { NOT = { has_government = fascism } }
		}
	}
	ai_strategy = {
		type = antagonize
		id = "AFG"
		value = -100
	}
	ai_strategy = {
		type = contain
		id = "AFG"
		value = -100
	}
	ai_strategy = {
		type = support
		id = "AFG"
		value = 200
	}
	ai_strategy = {
		type = befriend
		id = "AFG"
		value = 200
	}
	ai_strategy = {
		type = protect 
		id = "AFG"
		value = -50
	}
	ai_strategy = {
		type = send_volunteers_desire
		id = "AFG"
		value = 50
	}
	ai_strategy = {
		type = antagonize
		id = "TAL"
		value = 100
	}
	ai_strategy = {
		type = contain
		id = "TAL"
		value = 100
	}
	ai_strategy = {
		type = support
		id = "TAL"
		value = -200
	}
	ai_strategy = {
		type = befriend
		id = "TAL"
		value = -200
	}
	ai_strategy = {
		type = protect 
		id = "TAL"
		value = -50
	}
	ai_strategy = {
		type = send_volunteers_desire
		id = "TAL"
		value = -200
	}
}

PAK_area_priority = {
	enable = {
		original_tag = PAK
	}
	ai_strategy = {
		type = area_priority
		id = europe
		value = -50
	}
	ai_strategy = {
		type = area_priority
		id = north_america
		value = -50
	}
	ai_strategy = {
		type = area_priority
		id = caribbean
		value = -50
	}
	ai_strategy = {
		type = area_priority
		id = south_america
		value = -50
	}
	ai_strategy = {
		type = area_priority
		id = asia
		value = 200
	}
	ai_strategy = {
		type = area_priority
		id = pacific
		value = -50
	}
	ai_strategy = {
		type = area_priority
		id = oceania
		value = -50
	}
	ai_strategy = {
		type = area_priority
		id = middle_east
		value = 25
	}
	ai_strategy = {
		type = area_priority
		id = africa
		value = -50
	}
}