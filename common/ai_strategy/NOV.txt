# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

Novorossiya_Ukraine_Relations = {
	enable = {
		original_tag = NOV
	}
	abort = {
		NOT = { country_exists = UKR }
	}
	
	ai_strategy = {
		type = conquer
		id = "UKR"
		value = 100
	}
	ai_strategy = {
		type = contain
		id = "UKR"
		value = 100
	}
	ai_strategy = {
		type = antagonize
		id = "UKR"
		value = 200
	}
}
Novorossiya_Russia_Relations = {
	enable = {
		original_tag = NOV
	}
	abort = {
		always = no
	}
	
	ai_strategy = {
		type = befriend
		id = "SOV"
		value = 50
	}
}