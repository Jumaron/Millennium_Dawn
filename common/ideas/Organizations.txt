ideas = {
	country = {
		
		#African Union
		AU_member = {
			
			allowed = {
				is_african_nation = yes
			}
			
			default = yes
	
			modifier = {
				# political_power_gain = 0.1
				# stability_factor = -0.05
			}
		}
		
		#European Union
		EU_member = {
			
			allowed = {
				OR = {
					original_tag = POR
					original_tag = SPR
					original_tag = IRE
					original_tag = ENG
					original_tag = FRA
					original_tag = BEL
					original_tag = ICE
					original_tag = HOL
					original_tag = GER
					original_tag = SWI
					original_tag = AUS
					original_tag = DEN
					original_tag = ITA
					original_tag = NOR
					original_tag = MLT
					#original_tag = CZE
					original_tag = POL
					original_tag = SWE
					original_tag = SLV
					original_tag = SLO
					original_tag = HUN
					original_tag = CRO
					original_tag = BOS
					#original_tag = YUG
					original_tag = SER
					#original_tag = MTN
					original_tag = ALB
					#original_tag = MCD
					original_tag = GRE
					original_tag = BUL
					original_tag = TUR
					original_tag = FIN
					original_tag = EST
					original_tag = LAT
					original_tag = LIT
					original_tag = BLR
					original_tag = UKR
					#original_tag = MOL
					original_tag = ROM
					original_tag = CYP

				}
			}
			
			default = no

			modifier = {
				political_power_factor = -0.1
				stability_factor = 0.03
			}
		}
		
		#Shanghai Cooperation Organization
		sco_member = {
		
			allowed = {
				OR = {
					original_tag = CHI
					original_tag = SOV
					original_tag = RAJ
					original_tag = PAK
					original_tag = KAZ
					original_tag = TAJ
					original_tag = KYR
					original_tag = UZB
				}
			}
			
			default = yes
	
			modifier = {
				political_power_gain = 0.2
				trade_opinion_factor = 0.2
			}
		}
		
		#North Atlanic Treaty Organization
		NATO_member = {
		
			picture = NATO_member
				
			allowed = {
				always = yes
			}
			
			allowed_civil_war = {
				has_government = democratic
			}

			rule = {
				can_create_factions = no
			}

			modifier = {
				ai_get_ally_desire_factor = -1000 #We are in NATO, and don't want other allies
			}
		}
		
		#The permanent members of the United Nations Security Council
		p5_member = {
			
			allowed = {
				OR = {
					tag = USA
					tag = ENG
					tag = SOV
					tag = CHI
					tag = FRA
					has_country_flag = reformed_council
				}
			}
			
			default = yes
	
			modifier = {
				political_power_gain = 0.5
			}
		}
		
		#Group of Seven
		g7_member = {
			
			allowed = {
				OR = {
					original_tag = USA
					original_tag = CAN
					original_tag = ENG
					original_tag = FRA
					original_tag = ITA
					original_tag = GER
					original_tag = JAP
				}
			}
			
			default = yes
	
			modifier = {
				political_power_gain = 0.1
				#stability_factor = -0.05
			}
		}
	}
}