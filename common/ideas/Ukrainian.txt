ideas = {

	country = {
	
		#### Kleptocracy
		
		UKR_kleptocracy = {

			allowed_civil_war = {
				always = yes
			}
			
			removal_cost = -1

			picture = kleptocracy
			
			modifier = {
				corruption_cost_factor = 0.25
				local_resources_factor = 0.025
			}
		}
		
		#### 450 Generals
		
		UKR_to_many_generals = {

			allowed_civil_war = {
				always = yes
			}
			
			removal_cost = -1

			picture = to_many_generals
			
			modifier = {
				max_planning = 0.05
				planning_speed = -0.50
			}
		}
		
		#### Budapest Memorandum
		
		UKR_budapest_memorandum = {

			allowed_civil_war = {
				always = yes
			}
			
			removal_cost = -1

			picture = budapest_memorandum
			
			modifier = {
				#will have it after nuclear weapon will be in mod
			}
		}
		
		######## Special mechanic for Ukraine of Pro-west/Pro-east rivalry
		
		#### Balanced
		
		UKR_west_east_rivalry_balanced = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}
			
			removal_cost = -1

			picture = west_east_rivalry_balanced
			
			modifier = {
				drift_defence_factor = -0.1
				democratic_drift = 0.01
				communism_drift = 0.01
			}
		}
		
		#### Towards west
		
		UKR_west_strengthened = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}
			
			removal_cost = -1

			picture = west_strengthened
			
			modifier = {
				stability_factor = -0.05
				democratic_drift = 0.05
				democratic_acceptance = 45
			}
		}

		UKR_west_dominant = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}
			
			removal_cost = -1

			picture = west_dominant
			
			modifier = {
				stability_factor = -0.10
				communism_drift = -0.05
				democratic_drift = 0.10
				democratic_acceptance = 85
			}
		}
		
		#### Towards east
		
		UKR_east_strengthened = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}
			
			removal_cost = -1

			picture = east_strengthened
			
			modifier = {
				stability_factor = -0.05
				communism_drift = 0.05
				communism_acceptance = 45
			}
		}

		UKR_east_dominant = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}
			
			removal_cost = -1

			picture = east_dominant
			
			modifier = {
				stability_factor = -0.10
				nationalist_drift = 0.02
				democratic_drift = -0.05
				communism_drift = 0.10
				communism_acceptance = 85
			}
		}
		
		#### Constitutional changes
		
		UKR_constitutional_changes = {

			removal_cost = -1
			
			picture = law_passed
			
			modifier = {
				political_power_gain = 0.15
				political_power_cost = -0.05
			}
		}
		
		#### Political support
		
		UKR_political_support = {

			removal_cost = -1
			
			picture = political_support
			
			modifier = {
				political_power_gain = -0.10
			}
		}
		
		#### Neglet of state interests
		
		UKR_neglect_of_state_interests = {

			removal_cost = -1
			
			picture = neglect_of_state_interests
			
			modifier = {
				stability_weekly = -0.001
			}
		}
		
		#### Pollution control program 
		
		UKR_eco_factories = {

			removal_cost = -1
			
			picture = eco_factories
			
			modifier = {
				production_factory_max_efficiency_factor = -0.1
			}
		}
		
		#### Ukrayinska Pravda
		
		UKR_anti_gov_news = {

			removal_cost = -1
			
			picture = anti_gov_news
			
			modifier = {
				political_power_gain = -0.05
				neutrality_drift = -0.01
			}
		}
		
		#### Anti-Kuchma's Protests
		
		UKR_ukraine_without_kuchma = {

			removal_cost = -1
			
			picture = ukraine_without_kuchma
			
			modifier = {
				political_power_gain = -0.25
				stability_weekly = -0.001
			}
		}
	}
}