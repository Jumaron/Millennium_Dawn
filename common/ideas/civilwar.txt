ideas = {
	country = {

		child_soldiers = {
			
			allowed = {
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				conscription = 0.02
				stability_factor = -0.10
				army_morale_factor = -0.15
				army_org_factor = -0.15
			}
		}
		
	}
}