ideas = {
	
	hidden_ideas = {
		
		usa_ind_investment = {
			
			allowed = {
				original_tag = USA
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				production_speed_industrial_complex_factor = 0.1
				production_speed_arms_factory_factor = 0.1
				
			}
		}
		usa_raj_court = {
			
			allowed = {
				original_tag = USA
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				democratic_drift = 0.05
			}
		}
		
		usa_united_europe = {
			
			allowed = {
				original_tag = USA
			}
			
			#default = yes
	
			modifier = {
				drift_defence_factor = 0.25
				production_speed_industrial_complex_factor = 0.05
				production_speed_arms_factory_factor = 0.05
			}
			
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
		}
		usa_isolationism = {
			
			allowed = {
				original_tag = USA
			}
			
			#default = yes
	
			modifier = {
				local_resources_factor = 0.10
			}
			
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
		}
		F_USA_350_ship = {
		
			allowed = {
				original_tag = USA
			}
			
			#default = yes
	
			equipment_bonus = {
				frigate = {
					build_cost_ic = -0.10 instant = yes
				}
				destroyer = {
					build_cost_ic = -0.10 instant = yes
				}
				attack_submarine = {
					build_cost_ic = -0.10 instant = yes
				}
				
			}
			
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
		}
		usa_air_dominance = {
		
			allowed = {
				original_tag = USA
			}
			
			#default = yes
	
			equipment_bonus = {
				MR_Fighter_equipment = {
					build_cost_ic = -0.10 instant = yes
				}
				# strategic_bomber_equipment = {
					# build_cost_ic = -0.10 instant = yes
				# }
			}
			
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
		}
		usa_expand_army = {
		
			allowed = {
				original_tag = USA
			}
			
			#default = yes
	
			equipment_bonus = {
				attack_helicopter_equipment = {
					build_cost_ic = -0.10 instant = yes
				}
				APC_Equipment = {
					build_cost_ic = -0.10 instant = yes
				}
				# transport_helicopter_equipment = {
					# build_cost_ic = -0.10 instant = yes
				# }
				
			}
			
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
		}
		usa_fund_marines = {
		
			allowed = {
				original_tag = USA
			}
			
			#default = yes
	
			equipment_bonus = {
				LPD = {
					build_cost_ic = -0.10 instant = yes
				}
				CV_MR_Fighter_equipment = {
					build_cost_ic = -0.10 instant = yes
				}
				
			}
			
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
		}
		usa_roads = {
		
			allowed = {
				original_tag = USA
			}
			
			#default = yes
	
			modifier = {
				production_speed_infrastructure_factor = 0.1
				
			}
			
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
		}
		usa_factories = {
		
			allowed = {
				original_tag = USA
			}
			
			#default = yes
	
			modifier = {
				production_speed_industrial_complex_factor = 0.1
				
			}
			
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
		}
		usa_ports = {
		
			allowed = {
				original_tag = USA
			}
			
			#default = yes
	
			modifier = {
				production_speed_naval_base_factor = 0.1
				
			}
			
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
		}
		
		usa_rejoin_tpp = {
		
			allowed = {
				original_tag = USA
			}
			
			#default = yes
	
			modifier = {
				#production_speed_naval_base_factor = 0.1
				
			}
			
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
		}
		
		usa_trump_budget = {
		
			allowed = {
				original_tag = USA
			}
			
			#default = yes
	
			modifier = {
				consumer_goods_factor = -0.01
			}
			
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
		}
		usa_pax_americana = {
		
			allowed = {
				original_tag = USA
			}
			
			#default = yes
	
			modifier = {
				justify_war_goal_time = -0.25
				invasion_preparation = -0.25
				experience_gain_army_factor = 0.1
				experience_gain_air_factor = 0.1
				experience_gain_navy_factor = 0.1
				
			}
			
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
		}
		usa_america_first = {
		
			allowed = {
				original_tag = USA
			}
			
			#default = yes
	
			modifier = {
				training_time_army_factor = -0.1
				min_export = 0.1
				consumer_goods_factor = -0.05
				
			}
			
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
		}
		usa_partners_allies = {
		
			allowed = {
				original_tag = USA
			}
			
			#default = yes
	
			modifier = {
				
				improve_relations_maintain_cost_factor = -0.15
				opinion_gain_monthly_same_ideology_factor = 0.2
			}
			
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
		}
		usa_shadow_war = {
		
			allowed = {
				original_tag = USA
			}
			
			#default = yes
	
			modifier = {
				send_volunteer_size = 3
			}
			equipment_bonus = {
				Air_UAV_equipment = {
					build_cost_ic = -0.10 instant = yes
				}
				
			}
			
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
		}
		usa_export_democracy = {
		
			allowed = {
				original_tag = USA
			}
			
			#default = yes
	
			modifier = {
				subversive_activites_upkeep = -0.15
				
			}
			
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
		}
		usa_arsenal_of_democracy = {
		
			allowed = {
				original_tag = USA
			}
			
			#default = yes
	
			modifier = {
				lend_lease_tension = -0.2
				production_factory_efficiency_gain_factor = 0.1
				production_factory_start_efficiency_factor = 0.1
				
			}
			
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
		}
		
		Enduring_Freedom_Expansion = {
			
			allowed = {
				original_tag = USA
			}
			
			#default = yes
	
			modifier = {
				consumer_goods_factor = 0.01
			}
			
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
		
		}

		
	}
	
	country = {

		petro_dollar = {
			
			allowed = {
				original_tag = USA
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				trade_opinion_factor = 0.25
			}
		}
	
		land_of_the_free = {
			
			allowed = {
				original_tag = USA
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				democratic_drift = 0.05
				drift_defence_factor = 0.50
			}
		}

	##	US NFT related ideas
		usa_bad_procurment = {
			
			allowed = {
				tag = USA
			}
			
			#default = yes
	
			modifier = {
				production_factory_max_efficiency_factor = -0.25
			}
			allowed_civil_war = {
				always = yes
			}
		}
		
		rio_pact_member = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
		}
		Enduring_Freedom = {
			allowed = {
				always = no
			}
			modifier = {
				experience_gain_army = 0.05
				fascism_drift = -0.01
			}
			allowed_civil_war = {
				always = yes
			}
		}
		Inherent_Resolve = {
			allowed = {
				always = no
			}
			modifier = {
				experience_gain_army = 0.05
				fascism_drift = -0.01
			}
			allowed_civil_war = {
				always = yes
			}
		}
		Major_Non_NATO_Ally = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
		}
		usa_russia_investigation = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				political_power_factor = -0.5
			}
		}
		USA_usaid = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				consumer_goods_factor = -0.05
				industrial_capacity_factory = 0.1
				democratic_drift = 0.02
				political_power_gain = -0.25
			}
		}
	}
}