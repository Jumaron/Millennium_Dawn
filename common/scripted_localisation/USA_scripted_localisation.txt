defined_text = {
	name = USA_IsisSubsidiesLoc
	
	text = { #US given subsidies
		trigger = {
			check_variable = {
				var = isis_subsidies
				value = 0
				compare = not_equals
			}
		}
		localization_key = USA_given_isis_subsidies
	}
	text = { #US given no subsidies (Unlikely, but possible)
		trigger = {
			check_variable = {
				var = isis_subsidies
				value = 0
				compare = equals
			}
		}
		localization_key = USA_not_given_isis_subsidies
	}
}