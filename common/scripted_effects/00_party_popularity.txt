#written by ahmeteris
#needed for PKK attack and counter terrorism effects

add_1_popularity = {
	if = {
		limit = { has_government = democratic }
		add_popularity = {
			ideology = democratic
			popularity = 0.01
		}
	}
	if = {
		limit = { has_government = communism }
		add_popularity = {
			ideology = communism
			popularity = 0.01
		}
	}
	if = {
		limit = { has_government = fascism }
		add_popularity = {
			ideology = fascism
			popularity = 0.01
		}
	}
	if = {
		limit = { has_government = neutrality }
		add_popularity = {
			ideology = neutrality
			popularity = 0.01
		}
	}
	if = {
		limit = { has_government = nationalist }
		add_popularity = {
			ideology = nationalist
			popularity = 0.01
		}
	}
}

add_2_popularity = {
	if = {
		limit = { has_government = democratic }
		add_popularity = {
			ideology = democratic
			popularity = 0.02
		}
	}
	if = {
		limit = { has_government = communism }
		add_popularity = {
			ideology = communism
			popularity = 0.02
		}
	}
	if = {
		limit = { has_government = fascism }
		add_popularity = {
			ideology = fascism
			popularity = 0.02
		}
	}
	if = {
		limit = { has_government = neutrality }
		add_popularity = {
			ideology = neutrality
			popularity = 0.02
		}
	}
	if = {
		limit = { has_government = nationalist }
		add_popularity = {
			ideology = nationalist
			popularity = 0.02
		}
	}
}

decrease_popularity_1 = {
	if = {
		limit = { has_government = democratic }
		add_popularity = {
			ideology = democratic
			popularity = -0.01
		}
	}
	if = {
		limit = { has_government = communism }
		add_popularity = {
			ideology = communism
			popularity = -0.01
		}
	}
	if = {
		limit = { has_government = fascism }
		add_popularity = {
			ideology = fascism
			popularity = -0.01
		}
	}
	if = {
		limit = { has_government = neutrality }
		add_popularity = {
			ideology = neutrality
			popularity = -0.01
		}
	}
	if = {
		limit = { has_government = nationalist }
		add_popularity = {
			ideology = nationalist
			popularity = -0.01
		}
	}
}

decrease_popularity_2 = {
	if = {
		limit = { has_government = democratic }
		add_popularity = {
			ideology = democratic
			popularity = -0.02
		}
	}
	if = {
		limit = { has_government = communism }
		add_popularity = {
			ideology = communism
			popularity = -0.02
		}
	}
	if = {
		limit = { has_government = fascism }
		add_popularity = {
			ideology = fascism
			popularity = -0.02
		}
	}
	if = {
		limit = { has_government = neutrality }
		add_popularity = {
			ideology = neutrality
			popularity = -0.02
		}
	}
	if = {
		limit = { has_government = nationalist }
		add_popularity = {
			ideology = nationalist
			popularity = -0.02
		}
	}
}

decrease_popularity_3 = {
	if = {
		limit = { has_government = democratic }
		add_popularity = {
			ideology = democratic
			popularity = -0.03
		}
	}
	if = {
		limit = { has_government = communism }
		add_popularity = {
			ideology = communism
			popularity = -0.03
		}
	}
	if = {
		limit = { has_government = fascism }
		add_popularity = {
			ideology = fascism
			popularity = -0.03
		}
	}
	if = {
		limit = { has_government = neutrality }
		add_popularity = {
			ideology = neutrality
			popularity = -0.03
		}
	}
	if = {
		limit = { has_government = nationalist }
		add_popularity = {
			ideology = nationalist
			popularity = -0.03
		}
	}
}

decrease_popularity_4 = {
	if = {
		limit = { has_government = democratic }
		add_popularity = {
			ideology = democratic
			popularity = -0.04
		}
	}
	if = {
		limit = { has_government = communism }
		add_popularity = {
			ideology = communism
			popularity = -0.04
		}
	}
	if = {
		limit = { has_government = fascism }
		add_popularity = {
			ideology = fascism
			popularity = -0.04
		}
	}
	if = {
		limit = { has_government = neutrality }
		add_popularity = {
			ideology = neutrality
			popularity = -0.04
		}
	}
	if = {
		limit = { has_government = nationalist }
		add_popularity = {
			ideology = nationalist
			popularity = -0.04
		}
	}
}

decrease_popularity_5 = {
	if = {
		limit = { has_government = democratic }
		add_popularity = {
			ideology = democratic
			popularity = -0.05
		}
	}
	if = {
		limit = { has_government = communism }
		add_popularity = {
			ideology = communism
			popularity = -0.05
		}
	}
	if = {
		limit = { has_government = fascism }
		add_popularity = {
			ideology = fascism
			popularity = -0.05
		}
	}
	if = {
		limit = { has_government = neutrality }
		add_popularity = {
			ideology = neutrality
			popularity = -0.05
		}
	}
	if = {
		limit = { has_government = nationalist }
		add_popularity = {
			ideology = nationalist
			popularity = -0.05
		}
	}
}