scripted_gui = {

	domestic_influence_diplo = {
		context_type = selected_country_context
		parent_window_token = selected_country_view_diplomacy
		
		window_name = "domestic_influence_diplo"
		
		visible = {
		  always = yes
		}
		
		effects = { }
	}
	position_1_diplo = {
		context_type = selected_country_context
		parent_window_token = selected_country_view_diplomacy
		
		window_name = "position_1_diplo"
		
		visible = {
		  always = yes
		}
		
		effects = { }
	}
	position_2_diplo = {
		context_type = selected_country_context
		parent_window_token = selected_country_view_diplomacy
		
		window_name = "position_2_diplo"
		
		visible = {
		  always = yes
		}
		
		effects = { }
	}
	position_3_diplo = {
		context_type = selected_country_context
		parent_window_token = selected_country_view_diplomacy
		
		window_name = "position_3_diplo"
		
		visible = {
		  always = yes
		}
		
		effects = { }
	}
	position_4_diplo = {
		context_type = selected_country_context
		parent_window_token = selected_country_view_diplomacy
		
		window_name = "position_4_diplo"
		
		visible = {
		  always = yes
		}
		
		effects = { }
	}
	position_5_diplo = {
		context_type = selected_country_context
		parent_window_token = selected_country_view_diplomacy
		
		window_name = "position_5_diplo"
		
		visible = {
		  always = yes
		}
		
		effects = { }
	}
	position_6_diplo = {
		context_type = selected_country_context
		parent_window_token = selected_country_view_diplomacy
		
		window_name = "position_6_diplo"
		
		visible = {
		  always = yes
		}
		
		effects = { }
	}
	position_7_diplo = {
		context_type = selected_country_context
		parent_window_token = selected_country_view_diplomacy
		
		window_name = "position_7_diplo"
		
		visible = {
		  always = yes
		}
		
		effects = { }
	}
}

