scripted_gui = {

	debt = {
		context_type = player_context
		
		window_name = "debt_container"
		
		visible = {
		  always = yes
		}
		
		effects = {
			#Take debt
			debt_bg_click = {
					add_to_variable = { treasury = 1 }
					add_to_variable = { debt = 1 }
					ingame_calculate_size_modifier = yes
					calculate_interest_rate = yes
					calculate_debt_rate = yes
			}
			debt_bg_control_click = {
					add_to_variable = { treasury = 10 }
					add_to_variable = { debt = 10 }
					ingame_calculate_size_modifier = yes
					calculate_interest_rate = yes
					calculate_debt_rate = yes
			}
			debt_bg_shift_click = {
					add_to_variable = { treasury = 100 }
					add_to_variable = { debt = 100 }
					ingame_calculate_size_modifier = yes
					calculate_interest_rate = yes
					calculate_debt_rate = yes
			}
			
		  #Repay debt
			debt_bg_right_click = {	
				if = {
					limit = { check_variable = { treasury > 1 } }
					subtract_from_variable = { treasury = 1 }
					subtract_from_variable = { debt = 0.95 }
					ingame_calculate_size_modifier = yes
					calculate_interest_rate = yes
					calculate_debt_rate = yes
				}
			}
			debt_bg_control_right_click = {
				if = {
					limit = { check_variable = { treasury > 10 } }
					subtract_from_variable = { treasury = 10 }
					subtract_from_variable = { debt = 9.5 }
					ingame_calculate_size_modifier = yes
					calculate_interest_rate = yes
					calculate_debt_rate = yes
				}
			}
			debt_bg_shift_right_click = {
				if = {
					limit = { check_variable = { treasury > 100 } }
					subtract_from_variable = { treasury = 100 }
					subtract_from_variable = { debt = 95 }
					ingame_calculate_size_modifier = yes
					calculate_interest_rate = yes
					calculate_debt_rate = yes
				}
			}
		}
		triggers = {
			debt_bg_right_click_enabled = {
				check_variable = { debt > 0.95 }
			}
			debt_bg_control_right_click_enabled = {
				check_variable = { debt > 9.5 }
			}
			debt_bg_shift_right_click_enabled = {
				check_variable = { debt > 95 }
			}
		}
	}
	treasury = {
		context_type = player_context
		
		window_name = "treasury_container"
		
		visible = {
		  always = yes
		}
		
		effects = { }
	}
	
	int_investments = {
		context_type = player_context
		
		window_name = "int_investments_container"
		
		visible = {
		  always = yes
		}
		
		effects = { 
			#Sell off investments
			int_investments_bg_right_click = {	
				if = {
					limit = { check_variable = { int_investments > 1 } }
					subtract_from_variable = { int_investments = 1 }
					add_to_variable = { treasury = 0.95 }
					calculate_int_investments_rate = yes
				}
			}
			int_investments_bg_control_right_click = {
				if = {
					limit = { check_variable = { int_investments > 10 } }
					subtract_from_variable = { int_investments = 10 }
					add_to_variable = { treasury = 9.5 }
					calculate_int_investments_rate = yes
				}
			}
			int_investments_bg_shift_right_click = {
				if = {
					limit = { check_variable = { int_investments > 100 } }
					subtract_from_variable = { int_investments = 100 }
					add_to_variable = { treasury = 95 }
					calculate_int_investments_rate = yes
				}
			}
		}
	}
	### Politics Screen ###
	tax_rate = {
		context_type = player_context
		
		window_name = "tax_rate_politics_tab"
		parent_window_token = politics_tab
	   
		visible = {
		  always = yes
		}
		effects = { }
	}
	tax_rate_small_plus_button = {
		context_type = player_context
		
		window_name = "small_plus_button_politics_tab"
		parent_window_token = politics_tab
	   
		visible = {
		  always = yes
		}
		effects = {
			small_plus_button_bg_click = {	
					add_to_variable = { tax_rate = 1 }
					increase_tax_cost = yes
					calculate_tax_gain = yes
					ingame_calculate_size_modifier = yes
					calculate_resource_sale_rate = yes
					
			}
		}
		triggers = {
			small_plus_button_bg_click_enabled = {
				check_variable = { tax_rate < 50 }
			}
		}
	}
	tax_rate_small_minus_button = {
		context_type = player_context
		
		window_name = "small_minus_button_politics_tab"
		parent_window_token = politics_tab
	   
		visible = {
		  always = yes
		}
		effects = {
			small_minus_button_bg_click = {
					subtract_from_variable = { tax_rate = 1 }
					decrease_tax_cost = yes
					calculate_tax_gain = yes
					ingame_calculate_size_modifier = yes
					calculate_resource_sale_rate = yes
			}
		}
		triggers = {
			small_minus_button_bg_click_enabled = {
				check_variable = { tax_rate > 0 }
			}
		}
	}
	expense_gain = {
		context_type = player_context
		
		window_name = "expense_gain_politics_tab"
		parent_window_token = politics_tab
	   
		visible = {
		  always = yes
		}
		effects = { }
	}
	tax_gain = {
		context_type = player_context
		
		window_name = "tax_gain_politics_tab"
		parent_window_token = politics_tab
	   
		visible = {
		  always = yes
		}
		effects = { }
	}
	### Diplomacy view other countries ###
	treasury_dip = {
		context_type = selected_country_context
		
		window_name = "diplo_treasury_container"
		parent_window_token = selected_country_view
		
		visible = {
		  always = yes
		}
		
		effects = { }
	}
	int_investments_dip = {
		context_type = selected_country_context
		
		window_name = "diplo_int_investments_container"
		parent_window_token = selected_country_view
		
		visible = {
		  always = yes
		}
		
		effects = { }
	}
	debt_dip = {
		context_type = selected_country_context
		
		window_name = "diplo_debt_container"
		parent_window_token = selected_country_view
		
		visible = {
		  always = yes
		}
		
		effects = { }
	}
	tax_rate_dip = {
		context_type = selected_country_context
		
		window_name = "tax_rate_diplo"
		parent_window_token = selected_country_view_info 
		
		visible = {
		  always = yes
		}
		
		effects = { }
	}
	expense_gain_dip = {
		context_type = selected_country_context
		
		window_name = "expense_gain_diplo"
		parent_window_token = selected_country_view_info 
		
		visible = {
		  always = yes
		}
		
		effects = { }
	}
	tax_gain_dip = {
		context_type = selected_country_context
		
		window_name = "tax_gain_diplo"
		parent_window_token = selected_country_view_info 
		
		visible = {
		  always = yes
		}
		
		effects = { }
	}
}

