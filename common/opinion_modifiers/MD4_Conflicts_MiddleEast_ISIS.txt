#########################################################################
# OPINION MODIFIERS
##########################################################################
# value
# min_trust
# max_trust
# decay
# months/years/days = timer
# trade = yes/no

opinion_modifiers = {
	# ISIS
	ISIS_Are_Terrorists = {
		value = -60
	}
	ISIS_Are_Terrorists_Trade = {
		trade = yes
		value = -100
	}
	# Al-Qaida
	AlQaida_Are_Terrorists = {
		value = -60
	}
	AlQaida_Are_Terrorists_Trade = {
		trade = yes
		value = -100
	}
	terrorism_is_no_no = {
		value = -60
	}
	terrorism_is_no_no_trade = {
		trade = yes
		value = -100
	}
}
