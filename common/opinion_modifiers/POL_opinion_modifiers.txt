opinion_modifiers = {

	poland_helped_us = {
		value = 25
	}

	poland_helped_us_timed = {
		value = 25
		decay = 1
	}

	poland_refused_help = {
		value = -25
		decay = 1
	}

}