#########################################################################
# OPINION MODIFIERS
##########################################################################
# value
# min_trust
# max_trust
# decay = value
# months/years/days = timer
# trade = yes/no

opinion_modifiers = {

random_diplo_start_angry = {
		value = -25
		decay = 0.3
	}
	
random_diplo_start_angry_nationalists = {
		value = -35
		decay = 0.3
	}
	
random_diplo_esc_refuced_our_apology = {
		value = -10
		decay = 1
	}

}