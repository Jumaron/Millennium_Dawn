influence_desicions = {
	combat_foreign_influence = {

		icon = decision
		
		cost = 150
		days_remove = 30
		days_re_enable = 0
		
		complete_effect = {
			custom_effect_tooltip = increase_domestic_influence_amount_TT
			hidden_effect = {
				add_to_variable = { domestic_influence_amount = 10 }
				recalculate_influence = yes
			}
		}

		ai_will_do = {
			base = 20
			modifier = {
				factor = 0
				check_variable = { domestic_influence_amount_calc > 0.6 }
			}
		}
	}
	
	target_hostile_influence = {

		icon = decision
		
		available = {
			FROM = { has_opinion = { target = ROOT value < -5 } }
			NOT = { FROM = { tag = ROOT } }
		}
		
		visible = {
			FROM = { has_opinion = { target = ROOT value < -5 } }
			NOT = { FROM = { tag = ROOT } }
		}
		
		cost = 150
		days_remove = 60
		days_re_enable = 0
		
		complete_effect = {
			custom_effect_tooltip = target_hostile_influence_TT
			FROM = {
				add_opinion_modifier = { 
					target = ROOT 
					modifier = combated_our_influence
				}
			}
			#hidden_effect = {
				if = { limit = { check_variable = { influencer1 = FROM } }
					subtract_from_variable = { influencer1_amount = 15 }
					clamp_variable = {
						var = influencer1_amount
						min = 0
						max = influence_total
					}
					
						else = { if = { limit = { check_variable = { influencer2 = FROM } }
						subtract_from_variable = { influencer2_amount = 15 }
						clamp_variable = {
							var = influencer2_amount
							min = 0
							max = influence_total
						}
						
							else = { if = { limit = { check_variable = { influencer3 = FROM } }
							subtract_from_variable = { influencer3_amount = 15 }
							clamp_variable = {
								var = influencer3_amount
								min = 0
								max = influence_total
							}
							
								else = { if = { limit = { check_variable = { influencer4 = FROM } }
								subtract_from_variable = { influencer4_amount = 15 }
								clamp_variable = {
									var = influencer4_amount
									min = 0
									max = influence_total
								}
								
									else = { if = { limit = { check_variable = { influencer5 = FROM } }
									subtract_from_variable = { influencer5_amount = 15 }
									clamp_variable = {
										var = influencer5_amount
										min = 0
										max = influence_total
									}
									
										else = { if = { limit = { check_variable = { influencer6 = FROM } }
										subtract_from_variable = { influencer6_amount = 15 }
										clamp_variable = {
											var = influencer6_amount
											min = 0
											max = influence_total
										}
										
											else = { if = { limit = { check_variable = { influencer7 = FROM } }
											subtract_from_variable = { influencer7_amount = 15 }
											clamp_variable = {
												var = influencer7_amount
												min = 0
												max = influence_total
											}
										} }
									} }
								} }
							} }
						} }
					} }
				}
			#}
			recalculate_influence = yes
		}
	}
}
