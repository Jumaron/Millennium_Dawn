ENG_AFG = {
	eng_intervene_AFG = {
		allowed = { tag = ENG }

		available = {
			or = {
				AFG = { has_volunteers_amount_from = { tag = ENG count > 0 } }
				TAL = { has_volunteers_amount_from = { tag = ENG count > 0 } }
				
			}
		}


		activation = {
			AFG = {
				or = {
					has_war_with = TAL
				}
			}
		}


		Is_good = yes

		selectable_mission = yes
		days_mission_timeout = 60
		days_remove = 5
		

		fire_only_once = yes


		timeout_effect = {
		
		}


		complete_effect = {
			add_political_power = 25
		}
		
		ai_will_do = {
			factor = 1
		}
	}
}

ENG_GNA = {
	eng_intervene_GNA = {
		allowed = { tag = ENG }


		available = {
			or = {
				GNA = { has_volunteers_amount_from = { tag = ENG count > 0 } }
				HOR = { has_volunteers_amount_from = { tag = ENG count > 0 } }
				TUA = { has_volunteers_amount_from = { tag = ENG count > 0 } }
				
			}
			
		}


		activation = {
			GNA = {
				or = {
					has_war_with = HOR
					has_war_with = TUA
				}
			}
		}
		

		Is_good = yes

		selectable_mission = yes
		days_mission_timeout = 60
		days_remove = 5
		

		fire_only_once = yes
		

		timeout_effect = {
		
		}


		complete_effect = {
			add_political_power = 25
		}
		ai_will_do = {
			factor = 1
		}
	}
}

ENG_YEM = {
	eng_intervene_YEM = {
		allowed = { tag = ENG }


		available = {
			or = {
				YEM = { has_volunteers_amount_from = { tag = ENG count > 0 } }
				HOU = { has_volunteers_amount_from = { tag = ENG count > 0 } }
				
			}
			
		}


		activation = {
			YEM = {
				or = {
					has_war_with = HOU
				}
			}
		}


		Is_good = yes

		selectable_mission = yes
		days_mission_timeout = 60
		days_remove = 5
		

		fire_only_once = yes


		timeout_effect = {
		
		}


		complete_effect = {
			add_political_power = 25
		}
		
		ai_will_do = {
			factor = 1
		}
	}
}

decisions_ENG = {
	falkland_policy = {
		visible = {
			country_exists = ARG
			NOT = { has_country_flag = falkland_policy }
		}
		
		available = {
			NOT = { has_war_with = ARG }
			is_subject = no
		}
		
		icon = falkland_islands
		
		cost = 5
		fire_only_once = yes
		
		complete_effect = {
			set_country_flag = falkland_policy
			country_event = United_Kingdom.1
		}
		
		ai_will_do = {
			base = 0
			modifier = {
				date > 2000.6.1
				add = 5
			}
		}
	}
}