#Generic AI Templates created by Hiddengearz

garrison_generic = { #garrison
	
	#available_for = { #Who is this template available for? Left blank as it's for everyone
	   #tag
	# }
	#blocked_for = { #who is this template blocked for? Who has their own custom templates?
		#tag
	# }
	roles = {
		garrison
	}
	
	upgrade_prio = {
		factor = 10

		modifier = {
			factor = 0
			ai_has_role_template = garrison
		}
	}
	
	match_to_count = 0.70
	
	Militia_gar = {
		
		
		reinforce_prio = 0
		custom_icon = 7
	
		upgrade_prio = {
			factor = 1

			modifier = {
				factor = 1
			}
		}
		
		target_width = 12
		width_weight = 0.6
		column_swap_factor = 0.5
		
		stat_weights = {
			0.00 #-- default_morale
			0.00 #-- defense
			0.00 #-- breakthrough
			0.00 #-- hardness
			2.00 #-- soft_attack
			2.00 #-- hard_attack
			0.00 #-- recon
			0.00 #-- entrenchment
			0.00 #-- initiative
			0.00 #-- casualty_trickleback
			0.00 #-- supply_consumption_factor
			0.00 #-- supply_consumption
			0.00 #-- suppression
			0.00 #-- suppression_factor
			0.00 #-- experience_loss_factor
			#-- Air Values
			0.00 #-- air_attack
			#-- Common Values
			8.00 #-- max_organisation
			0.40 #-- max_strength
			0.00 #-- build_cost_ic
			0.00 #-- maximum_speed
			0.00 #-- armor_value
			0.20 #-- ap_attack
			0.00 #-- reliability
			0.00 #-- reliability_factor
			0.00 #-- weight
		}
		
		target_template = {
			weight = 0.6
			match_value = 3000.0
			
			support = {
				Arty_Battery = 1
			}
			
			regiments = {
				Militia_Bat = 3
				
			}
		}
		
		allowed_types = {
			Militia_Bat
			Arty_Bat
			Arty_Battery
			L_Engi_Bat
			L_Engi_Comp
			Mot_Militia_Bat
		}
		
		replace_at_match = 0.7
		replace_with = L_Inf_garrison
		target_min_match = 0.5
		
	}
	L_Inf_gar = {
		
		
		reinforce_prio = 0
		custom_icon = 7
	
		upgrade_prio = {
			factor = 1

			modifier = {
				factor = 1
			}
		}
		
		target_width = 12.0
		width_weight = 0.6
		column_swap_factor = 0.5
		
		stat_weights = {
			0.00 #-- default_morale
			0.00 #-- defense
			0.00 #-- breakthrough
			0.00 #-- hardness
			2.00 #-- soft_attack
			2.00 #-- hard_attack
			0.00 #-- recon
			0.00 #-- entrenchment
			0.00 #-- initiative
			0.00 #-- casualty_trickleback
			0.00 #-- supply_consumption_factor
			0.00 #-- supply_consumption
			0.00 #-- suppression
			0.00 #-- suppression_factor
			0.00 #-- experience_loss_factor
			#-- Air Values
			0.00 #-- air_attack
			#-- Common Values
			8.00 #-- max_organisation
			0.40 #-- max_strength
			0.00 #-- build_cost_ic
			0.00 #-- maximum_speed
			0.00 #-- armor_value
			0.20 #-- ap_attack
			0.00 #-- reliability
			0.00 #-- reliability_factor
			0.00 #-- weight
		}
		
		target_template = {
			weight = 0.6
			match_value = 3000.0
			
			support = {
				Arty_Battery = 1
			}
			
			regiments = {
				L_Inf_Bat = 3
				
			}
		}
		
		allowed_types = {
			L_Inf_Bat
			Arty_Bat
			Arty_Battery
			L_Engi_Bat
			L_Engi_Comp
			
		}
		
	}
	
}

Militia_generic = { #L_Inf
	
	#available_for = { #Who is this template available for? Left blank as it's for everyone
	   #tag
	# }
	#blocked_for = { #who is this template blocked for? Who has their own custom templates?
		#tag
	# }
	roles = {
		Militia
	}
	
	upgrade_prio = {
		factor = 1

		modifier = {
			factor = 1
		}
	}
	
	match_to_count = 0.1
	
	Militia = {
		
		upgrade_prio = {
			factor = 1

			modifier = {
				factor = 1
			}
		}
		
		target_width = 15
		width_weight = 0.6
		column_swap_factor = 0.5

		stat_weights = {
			0.00 #-- default_morale
			0.00 #-- defense
			1.00 #-- breakthrough
			0.00 #-- hardness
			2.00 #-- soft_attack
			2.00 #-- hard_attack
			0.00 #-- recon
			0.00 #-- entrenchment
			0.00 #-- initiative
			0.00 #-- casualty_trickleback
			0.00 #-- supply_consumption_factor
			0.00 #-- supply_consumption
			0.00 #-- suppression
			0.00 #-- suppression_factor
			0.00 #-- experience_loss_factor
			#-- Air Values
			0.00 #-- air_attack
			#-- Common Values
			8.00 #-- max_organisation
			0.40 #-- max_strength
			0.00 #-- build_cost_ic
			0.00 #-- maximum_speed
			0.00 #-- armor_value
			0.20 #-- ap_attack
			0.00 #-- reliability
			0.00 #-- reliability_factor
			0.00 #-- weight
		}
		
		target_template = {
			weight = 0.6
			match_value = 3000
			
			#support = {
			#	L_Recce_Comp = 1
			# }
			
			regiments = {
				L_Inf_Bat = 3
				#Arty_Battery = 1
				#L_Engi_Comp = 1
			}
		}
		
		allowed_types = {
			Militia_Bat
			L_Recce_Bat
			L_Recce_Comp
			Mot_Recce_Bat
			Mot_Recce_Comp
			Mech_Recce_Bat
			Mech_Recce_Comp
			Arm_Recce_Bat
			Arm_Recce_Comp
			armor_Recce_Bat
			armor_Recce_Comp
			armor_Bat
			armor_Comp
			Arty_Bat
			Arty_Battery
			L_Engi_Bat
			L_Engi_Comp
			Special_Forces
			SP_Arty_Bat
			SP_Arty_Battery
			SP_AA_Bat
			SP_AA_Battery
		}
		target_min_match = 0.1
	}
	Mot_Militia = {
		
		upgrade_prio = {
			factor = 1

			modifier = {
				factor = 1
			}
		}
		
		target_width = 15
		width_weight = 0.6
		column_swap_factor = 0.5

		stat_weights = {
			0.00 #-- default_morale
			0.00 #-- defense
			1.00 #-- breakthrough
			0.00 #-- hardness
			2.00 #-- soft_attack
			2.00 #-- hard_attack
			0.00 #-- recon
			0.00 #-- entrenchment
			0.00 #-- initiative
			0.00 #-- casualty_trickleback
			0.00 #-- supply_consumption_factor
			0.00 #-- supply_consumption
			0.00 #-- suppression
			0.00 #-- suppression_factor
			0.00 #-- experience_loss_factor
			#-- Air Values
			0.00 #-- air_attack
			#-- Common Values
			8.00 #-- max_organisation
			0.40 #-- max_strength
			0.00 #-- build_cost_ic
			0.00 #-- maximum_speed
			0.00 #-- armor_value
			0.20 #-- ap_attack
			0.00 #-- reliability
			0.00 #-- reliability_factor
			0.00 #-- weight
		}
		
		target_template = {
			weight = 0.6
			match_value = 3000
			
			#support = {
			#	L_Recce_Comp = 1
			# }
			
			regiments = {
				Mot_Militia_Bat = 3
				#Arty_Battery = 1
				#L_Engi_Comp = 1
				#Mot_Recce_Comp = 1
			}
			support = {
				#Mot_Militia_Bat = 3
				Arty_Battery = 1
				L_Engi_Comp = 1
				Mot_Recce_Comp = 1
			}
			
		}
		
		allowed_types = {
			Mot_Militia_Bat
			L_Recce_Bat
			L_Recce_Comp
			Mot_Recce_Bat
			Mot_Recce_Comp
			Mech_Recce_Bat
			Mech_Recce_Comp
			Arm_Recce_Bat
			Arm_Recce_Comp
			armor_Recce_Bat
			armor_Recce_Comp
			armor_Bat
			armor_Comp
			Arty_Bat
			Arty_Battery
			L_Engi_Bat
			L_Engi_Comp
			Special_Forces
			SP_Arty_Bat
			SP_Arty_Battery
			SP_AA_Bat
			SP_AA_Battery
		}
		target_min_match = 0.1
	}
	Mot_Militia2 = {
		
		upgrade_prio = {
			factor = 1

			modifier = {
				factor = 1
			}
		}
		
		target_width = 15
		width_weight = 0.2
		column_swap_factor = 0.5

		stat_weights = {
			0.00 #-- default_morale
			0.00 #-- defense
			1.00 #-- breakthrough
			0.00 #-- hardness
			2.00 #-- soft_attack
			2.00 #-- hard_attack
			0.00 #-- recon
			0.00 #-- entrenchment
			0.00 #-- initiative
			0.00 #-- casualty_trickleback
			0.00 #-- supply_consumption_factor
			0.00 #-- supply_consumption
			0.00 #-- suppression
			0.00 #-- suppression_factor
			0.00 #-- experience_loss_factor
			#-- Air Values
			0.00 #-- air_attack
			#-- Common Values
			8.00 #-- max_organisation
			0.40 #-- max_strength
			0.00 #-- build_cost_ic
			0.00 #-- maximum_speed
			0.00 #-- armor_value
			0.20 #-- ap_attack
			0.00 #-- reliability
			0.00 #-- reliability_factor
			0.00 #-- weight
		}
		
		target_template = {
			weight = 0.2
			match_value = 3000
			
			#support = {
			#	L_Recce_Comp = 1
			# }
			
			regiments = {
				Mot_Militia_Bat = 3
				#Arty_Battery = 1
				#L_Engi_Comp = 1
				#Mot_Recce_Comp = 1
			}
		}
		
		allowed_types = {
			Mot_Militia_Bat
			L_Recce_Bat
			L_Recce_Comp
			Mot_Recce_Bat
			Mot_Recce_Comp
			Mech_Recce_Bat
			Mech_Recce_Comp
			Arm_Recce_Bat
			Arm_Recce_Comp
			armor_Recce_Bat
			armor_Recce_Comp
			armor_Bat
			armor_Comp
			Arty_Bat
			Arty_Battery
			L_Engi_Bat
			L_Engi_Comp
			Special_Forces
			SP_Arty_Bat
			SP_Arty_Battery
			SP_AA_Bat
			SP_AA_Battery
		}
		target_min_match = 0.1
	}
	
}

L_Inf_generic = { #L_Inf
	
	#available_for = { #Who is this template available for? Left blank as it's for everyone
	   #tag
	# }
	#blocked_for = { #who is this template blocked for? Who has their own custom templates?
		#tag
	# }
	roles = {
		L_Inf
	}
	
	upgrade_prio = {
		factor = 1

		modifier = {
			factor = 1
		}
	}
	
	match_to_count = 0.4
	
	L_Infantry = {
		
		upgrade_prio = {
			factor = 1

			modifier = {
				factor = 1
			}
		}
		
		target_width = 15
		width_weight = 0.6
		column_swap_factor = 0.5

		stat_weights = {
			0.00 #-- default_morale
			0.00 #-- defense
			1.00 #-- breakthrough
			0.00 #-- hardness
			2.00 #-- soft_attack
			2.00 #-- hard_attack
			0.00 #-- recon
			0.00 #-- entrenchment
			0.00 #-- initiative
			0.00 #-- casualty_trickleback
			0.00 #-- supply_consumption_factor
			0.00 #-- supply_consumption
			0.00 #-- suppression
			0.00 #-- suppression_factor
			0.00 #-- experience_loss_factor
			#-- Air Values
			0.00 #-- air_attack
			#-- Common Values
			8.00 #-- max_organisation
			0.40 #-- max_strength
			0.00 #-- build_cost_ic
			0.00 #-- maximum_speed
			0.00 #-- armor_value
			0.20 #-- ap_attack
			0.00 #-- reliability
			0.00 #-- reliability_factor
			0.00 #-- weight
		}
		
		target_template = {
			weight = 0.6
			match_value = 3000
			
			#support = {
			#	L_Recce_Comp = 1
			# }
			
			regiments = {
				L_Inf_Bat = 3
				#Arty_Battery = 1
				#L_Engi_Comp = 1
			}
		}
		
		allowed_types = {
			L_Inf_Bat
			L_Recce_Bat
			L_Recce_Comp
			Arty_Bat
			Arty_Battery
			L_Engi_Bat
			L_Engi_Comp
			Special_Forces
		}
		target_min_match = 0.5
	}
	
}

Infantry_generic = { #infantry
	
	#available_for = { #Who is this template available for? Left blank as it's for everyone
	   #tag
	# }
	#blocked_for = { #who is this template blocked for? Who has their own custom templates?
		#tag
	# }
	roles = {
		infantry
	}
	
	match_to_count = 0.50
	
	upgrade_prio = {
		factor = 1

		modifier = {
			factor = 1
		}
	}
	
	Mot_Inf_Brg = {
	
		upgrade_prio = {
			factor = 1

			modifier = {
				factor = 1
			}
		}
		
		target_width = 20
		width_weight = 0.6
		column_swap_factor = 0.3
		
		stat_weights = {
				0.00 #-- default_morale
				0.00 #-- defense
				1.00 #-- breakthrough
				0.00 #-- hardness
				2.00 #-- soft_attack
				2.00 #-- hard_attack
				0.00 #-- recon
				0.00 #-- entrenchment
				0.00 #-- initiative
				0.00 #-- casualty_trickleback
				0.00 #-- supply_consumption_factor
				0.00 #-- supply_consumption
				0.00 #-- suppression
				0.00 #-- suppression_factor
				0.00 #-- experience_loss_factor
				#-- Air Values
				0.00 #-- air_attack
				#-- Common Values
				8.00 #-- max_organisation
				0.40 #-- max_strength
				0.00 #-- build_cost_ic
				0.00 #-- maximum_speed
				0.00 #-- armor_value
				0.20 #-- ap_attack
				0.00 #-- reliability
				0.00 #-- reliability_factor
				0.00 #-- weight
			}
		
		target_template = {
			weight = 0.6
			match_value = 3000
			
			#support = {
			#	#Mot_Recce_Comp = 1
			# }
			
			regiments = {
				Mot_Inf_Bat = 3
				#Arty_Battery = 1
				#L_Engi_Comp = 1
			}
		}
		
		allowed_types = {
			Mot_Inf_Bat
			L_Recce_Bat
			L_Recce_Comp
			Mot_Recce_Bat
			Mot_Recce_Comp
			Mech_Recce_Bat
			Mech_Recce_Comp
			Arm_Recce_Bat
			Arm_Recce_Comp
			armor_Recce_Bat
			armor_Recce_Comp
			armor_Bat
			armor_Comp
			Arty_Bat
			Arty_Battery
			L_Engi_Bat
			L_Engi_Comp
			Special_Forces
			SP_Arty_Bat
			SP_Arty_Battery
			SP_AA_Bat
			SP_AA_Battery
		}
		target_min_match = 0.7
	}
	
}

Mech_Inf_generic = { #mechanized
	
	#available_for = { #Who is this template available for? Left blank as it's for everyone
	   #tag
   # }
	#blocked_for = { #who is this template blocked for? Who has their own custom templates?
		#tag
	# }
	roles = {
		mechanized
	}
	
	match_to_count = 0.30
	
	upgrade_prio = {
		factor = 1

		modifier = {
			factor = 1
		}
	}
	
	
	Mech_Inf_Brg = {
		
		upgrade_prio = {
			factor = 1

			modifier = {
				factor = 1
			}
		}
		
		target_width = 20
		width_weight = 0.6
		column_swap_factor = 0.5

		stat_weights = {
			0.00 #-- default_morale
			0.00 #-- defense
			1.00 #-- breakthrough
			0.00 #-- hardness
			2.00 #-- soft_attack
			2.00 #-- hard_attack
			0.00 #-- recon
			0.00 #-- entrenchment
			0.00 #-- initiative
			0.00 #-- casualty_trickleback
			0.00 #-- supply_consumption_factor
			0.00 #-- supply_consumption
			0.00 #-- suppression
			0.00 #-- suppression_factor
			0.00 #-- experience_loss_factor
			#-- Air Values
			0.00 #-- air_attack
			#-- Common Values
			8.00 #-- max_organisation
			0.40 #-- max_strength
			0.00 #-- build_cost_ic
			0.00 #-- maximum_speed
			0.00 #-- armor_value
			0.20 #-- ap_attack
			0.00 #-- reliability
			0.00 #-- reliability_factor
			0.00 #-- weight
		}
		
		target_template = {
			weight = 0.6
			match_value = 3000
			
			# support = {
				# SP_AA_Battery = 1
			# }
			
			regiments = {
				Mech_Inf_Bat = 3
				SP_#Arty_Battery = 1
				H_Engi_Comp = 1
			}
		}
		
		allowed_types = {
			Arm_Inf_Bat
			Mot_Recce_Bat
			Mot_Recce_Comp
			Mech_Recce_Bat
			Mech_Recce_Comp
			Arm_Recce_Bat
			Arm_Recce_Comp
			armor_Recce_Bat
			armor_Recce_Comp
			armor_Bat
			armor_Comp
			Arty_Bat
			Arty_Battery
			L_Engi_Bat
			L_Engi_Comp
			H_Engi_Bat
			H_Engi_Comp
			SP_Arty_Bat
			SP_Arty_Battery
			SP_AA_Bat
			SP_AA_Battery
		}
		replace_at_match = 0.7
		replace_with = Arm_Inf_Brg
		target_min_match = 0.5
		
	}
	Arm_Inf_Brg = {
		
		upgrade_prio = {
			factor = 1

			modifier = {
				factor = 1
			}
		}
		
		target_width = 20
		width_weight = 0.6
		column_swap_factor = 0.5

		stat_weights = {
			0.00 #-- default_morale
			0.00 #-- defense
			1.00 #-- breakthrough
			0.00 #-- hardness
			2.00 #-- soft_attack
			2.00 #-- hard_attack
			0.00 #-- recon
			0.00 #-- entrenchment
			0.00 #-- initiative
			0.00 #-- casualty_trickleback
			0.00 #-- supply_consumption_factor
			0.00 #-- supply_consumption
			0.00 #-- suppression
			0.00 #-- suppression_factor
			0.00 #-- experience_loss_factor
			#-- Air Values
			0.00 #-- air_attack
			#-- Common Values
			8.00 #-- max_organisation
			0.40 #-- max_strength
			0.00 #-- build_cost_ic
			0.00 #-- maximum_speed
			0.00 #-- armor_value
			0.20 #-- ap_attack
			0.00 #-- reliability
			0.00 #-- reliability_factor
			0.00 #-- weight
		}
		
		target_template = {
			weight = 0.6
			match_value = 3000
			
			# support = {
				# SP_AA_Battery = 1
			# }
			
			regiments = {
				Arm_Inf_Bat = 3
				SP_#Arty_Battery = 1
				H_Engi_Comp = 1
			}
		}
		
		allowed_types = {
			Arm_Inf_Bat
			Mot_Recce_Bat
			Mot_Recce_Comp
			Mech_Recce_Bat
			Mech_Recce_Comp
			Arm_Recce_Bat
			Arm_Recce_Comp
			armor_Recce_Bat
			armor_Recce_Comp
			armor_Bat
			armor_Comp
			Arty_Bat
			Arty_Battery
			L_Engi_Bat
			L_Engi_Comp
			H_Engi_Bat
			H_Engi_Comp
			SP_Arty_Bat
			SP_Arty_Battery
			SP_AA_Bat
			SP_AA_Battery
		}
	}
	
	
	
}

Air_Inf_generic = { #Air_Inf
	
	#available_for = { #Who is this template available for? Left blank as it's for everyone
	   #tag
	# }
	#blocked_for = { #who is this template blocked for? Who has their own custom templates?
		#tag
	# }
	roles = {
		Air_Inf
	}
	
	match_to_count = 0.30
	
	upgrade_prio = {
		factor = 1

		modifier = {
			factor = 1
		}
	}
	
	
	L_Air_Inf_Brg = {
		
		upgrade_prio = {
			factor = 1

			modifier = {
				factor = 1
			}
		}
		
		target_width = 15
		width_weight = 0.6
		column_swap_factor = 0.5

		stat_weights = {
			0.00 #-- default_morale
			0.00 #-- defense
			1.00 #-- breakthrough
			0.00 #-- hardness
			2.00 #-- soft_attack
			2.00 #-- hard_attack
			0.00 #-- recon
			0.00 #-- entrenchment
			0.00 #-- initiative
			0.00 #-- casualty_trickleback
			0.00 #-- supply_consumption_factor
			0.00 #-- supply_consumption
			0.00 #-- suppression
			0.00 #-- suppression_factor
			0.00 #-- experience_loss_factor
			#-- Air Values
			0.00 #-- air_attack
			#-- Common Values
			8.00 #-- max_organisation
			0.40 #-- max_strength
			0.00 #-- build_cost_ic
			0.00 #-- maximum_speed
			0.00 #-- armor_value
			0.20 #-- ap_attack
			0.00 #-- reliability
			0.00 #-- reliability_factor
			0.00 #-- weight
		}
		
		target_template = {
			weight = 0.6
			match_value = 3000
			
			# support = {
				# SP_AA_Battery = 1
			# }
			
			regiments = {
				L_Air_Inf_Bat = 3
				#Arty_Battery = 1
				#L_Engi_Comp = 1
			}
		}
		
		allowed_types = {
			L_Air_Inf_Bat
			Mot_Recce_Bat
			Mot_Recce_Comp
			Mech_Recce_Bat
			Mech_Recce_Comp
			Arm_Recce_Bat
			Arm_Recce_Comp
			armor_Recce_Bat
			armor_Recce_Comp
			Special_Forces
			Arty_Bat
			Arty_Battery
			L_Engi_Bat
			L_Engi_Comp
			H_Engi_Bat
			H_Engi_Comp
			SP_AA_Bat
			SP_AA_Battery
		}
		replace_at_match = 0.7
		replace_with = Mot_Air_Inf_Brg
		target_min_match = 0.5
	}
	Mot_Air_Inf_Brg = {
		
		upgrade_prio = {
			factor = 1

			modifier = {
				factor = 1
			}
		}
		
		target_width = 15
		width_weight = 0.6
		column_swap_factor = 0.5

		stat_weights = {
			0.00 #-- default_morale
			0.00 #-- defense
			1.00 #-- breakthrough
			0.00 #-- hardness
			2.00 #-- soft_attack
			2.00 #-- hard_attack
			0.00 #-- recon
			0.00 #-- entrenchment
			0.00 #-- initiative
			0.00 #-- casualty_trickleback
			0.00 #-- supply_consumption_factor
			0.00 #-- supply_consumption
			0.00 #-- suppression
			0.00 #-- suppression_factor
			0.00 #-- experience_loss_factor
			#-- Air Values
			0.00 #-- air_attack
			#-- Common Values
			8.00 #-- max_organisation
			0.40 #-- max_strength
			0.00 #-- build_cost_ic
			0.00 #-- maximum_speed
			0.00 #-- armor_value
			0.20 #-- ap_attack
			0.00 #-- reliability
			0.00 #-- reliability_factor
			0.00 #-- weight
		}
		
		target_template = {
			weight = 0.6
			match_value = 3000
			
			# support = {
				# SP_AA_Battery = 1
			# }
			
			regiments = {
				Mot_Air_Inf_Bat = 3
				#Arty_Battery = 1
				#L_Engi_Comp = 1
			}
		}
		
		allowed_types = {
			Mot_Air_Inf_Bat
			Mot_Recce_Bat
			Mot_Recce_Comp
			Mech_Recce_Bat
			Mech_Recce_Comp
			Arm_Recce_Bat
			Arm_Recce_Comp
			armor_Recce_Bat
			armor_Recce_Comp
			Special_Forces
			Arty_Bat
			Arty_Battery
			L_Engi_Bat
			L_Engi_Comp
			H_Engi_Bat
			H_Engi_Comp
			SP_AA_Bat
			SP_AA_Battery
		}
		
	}
	Mech_Air_Inf_Brg = {
		
		upgrade_prio = {
			factor = 1

			modifier = {
				factor = 1
			}
		}
		
		target_width = 20
		width_weight = 0.6
		column_swap_factor = 0.5

		stat_weights = {
			0.00 #-- default_morale
			0.00 #-- defense
			1.00 #-- breakthrough
			0.00 #-- hardness
			2.00 #-- soft_attack
			2.00 #-- hard_attack
			0.00 #-- recon
			0.00 #-- entrenchment
			0.00 #-- initiative
			0.00 #-- casualty_trickleback
			0.00 #-- supply_consumption_factor
			0.00 #-- supply_consumption
			0.00 #-- suppression
			0.00 #-- suppression_factor
			0.00 #-- experience_loss_factor
			#-- Air Values
			0.00 #-- air_attack
			#-- Common Values
			8.00 #-- max_organisation
			0.40 #-- max_strength
			0.00 #-- build_cost_ic
			0.00 #-- maximum_speed
			0.00 #-- armor_value
			0.20 #-- ap_attack
			0.00 #-- reliability
			0.00 #-- reliability_factor
			0.00 #-- weight
		}
		
		target_template = {
			weight = 0.6
			match_value = 3000
			
			# support = {
				# SP_AA_Battery = 1
			# }
			
			regiments = {
				Mech_Air_Inf_Bat = 3
				#Arty_Battery = 1
				#L_Engi_Comp = 1
			}
		}
		
		allowed_types = {
			Mech_Air_Inf_Bat
			Mot_Recce_Bat
			Mot_Recce_Comp
			Mech_Recce_Bat
			Mech_Recce_Comp
			Arm_Recce_Bat
			Arm_Recce_Comp
			armor_Recce_Bat
			armor_Recce_Comp
			Special_Forces
			Arty_Bat
			Arty_Battery
			L_Engi_Bat
			L_Engi_Comp
			H_Engi_Bat
			H_Engi_Comp
			SP_AA_Bat
			SP_AA_Battery
		}
		
	}
	Arm_Air_Inf_Brg = {
		
		upgrade_prio = {
			factor = 1

			modifier = {
				factor = 1
			}
		}
		
		target_width = 20
		width_weight = 0.6
		column_swap_factor = 0.5

		stat_weights = {
			0.00 #-- default_morale
			0.00 #-- defense
			1.00 #-- breakthrough
			0.00 #-- hardness
			2.00 #-- soft_attack
			2.00 #-- hard_attack
			0.00 #-- recon
			0.00 #-- entrenchment
			0.00 #-- initiative
			0.00 #-- casualty_trickleback
			0.00 #-- supply_consumption_factor
			0.00 #-- supply_consumption
			0.00 #-- suppression
			0.00 #-- suppression_factor
			0.00 #-- experience_loss_factor
			#-- Air Values
			0.00 #-- air_attack
			#-- Common Values
			8.00 #-- max_organisation
			0.40 #-- max_strength
			0.00 #-- build_cost_ic
			0.00 #-- maximum_speed
			0.00 #-- armor_value
			0.20 #-- ap_attack
			0.00 #-- reliability
			0.00 #-- reliability_factor
			0.00 #-- weight
		}
		
		target_template = {
			weight = 0.6
			match_value = 3000
			
			# support = {
				# SP_AA_Battery = 1
			# }
			
			regiments = {
				Arm_Air_Inf_Bat = 3
				#Arty_Battery = 1
				#L_Engi_Comp = 1
			}
		}
		
		allowed_types = {
			Arm_Air_Inf_Bat
			Mot_Recce_Bat
			Mot_Recce_Comp
			Mech_Recce_Bat
			Mech_Recce_Comp
			Arm_Recce_Bat
			Arm_Recce_Comp
			armor_Recce_Bat
			armor_Recce_Comp
			Special_Forces
			Arty_Bat
			Arty_Battery
			L_Engi_Bat
			L_Engi_Comp
			H_Engi_Bat
			H_Engi_Comp
			SP_AA_Bat
			SP_AA_Battery
		}
		
	}
	
	
}

Marine_Inf_generic = { #marines
	
	#available_for = { #Who is this template available for? Left blank as it's for everyone
	   #tag
	# }
	#blocked_for = { #who is this template blocked for? Who has their own custom templates?
		#tag
	# }
	roles = {
		marines
	}
	
	match_to_count = 0.50
	
	upgrade_prio = {
		factor = 1

		modifier = {
			factor = 1
		}
	}
	
	L_Marine_Brg = {
	
		upgrade_prio = {
			factor = 1

			modifier = {
				factor = 1
			}
		}
		
		target_width = 15
		width_weight = 0.6
		column_swap_factor = 0.3
		
		stat_weights = {
				0.00 #-- default_morale
				0.00 #-- defense
				1.00 #-- breakthrough
				0.00 #-- hardness
				2.00 #-- soft_attack
				2.00 #-- hard_attack
				0.00 #-- recon
				0.00 #-- entrenchment
				0.00 #-- initiative
				0.00 #-- casualty_trickleback
				0.00 #-- supply_consumption_factor
				0.00 #-- supply_consumption
				0.00 #-- suppression
				0.00 #-- suppression_factor
				0.00 #-- experience_loss_factor
				#-- Air Values
				0.00 #-- air_attack
				#-- Common Values
				8.00 #-- max_organisation
				0.40 #-- max_strength
				0.00 #-- build_cost_ic
				0.00 #-- maximum_speed
				0.00 #-- armor_value
				0.20 #-- ap_attack
				0.00 #-- reliability
				0.00 #-- reliability_factor
				0.00 #-- weight
			}
		
		target_template = {
			weight = 0.6
			match_value = 3000
			
			#support = {
			#	#Mot_Recce_Comp = 1
			# }
			
			regiments = {
				L_Marine_Bat = 3
				#Arty_Battery = 1
				#L_Engi_Comp = 1
			}
		}
		
		allowed_types = {
			L_Marine_Bat
			L_Recce_Bat
			L_Recce_Comp
			Mot_Recce_Bat
			Mot_Recce_Comp
			Mech_Recce_Bat
			Mech_Recce_Comp
			Arm_Recce_Bat
			Arm_Recce_Comp
			armor_Recce_Bat
			armor_Recce_Comp
			armor_Bat
			armor_Comp
			Special_Forces
			Arty_Bat
			Arty_Battery
			L_Engi_Bat
			L_Engi_Comp
			Special_Forces
			SP_Arty_Bat
			SP_Arty_Battery
			SP_AA_Bat
			SP_AA_Battery
		}
		replace_at_match = 0.7
		replace_with = Mot_Marine_Brg
		target_min_match = 0.5
	}
	Mot_Marine_Brg = {
	
		upgrade_prio = {
			factor = 1

			modifier = {
				factor = 1
			}
		}
		
		target_width = 20
		width_weight = 0.6
		column_swap_factor = 0.3
		
		stat_weights = {
				0.00 #-- default_morale
				0.00 #-- defense
				1.00 #-- breakthrough
				0.00 #-- hardness
				2.00 #-- soft_attack
				2.00 #-- hard_attack
				0.00 #-- recon
				0.00 #-- entrenchment
				0.00 #-- initiative
				0.00 #-- casualty_trickleback
				0.00 #-- supply_consumption_factor
				0.00 #-- supply_consumption
				0.00 #-- suppression
				0.00 #-- suppression_factor
				0.00 #-- experience_loss_factor
				#-- Air Values
				0.00 #-- air_attack
				#-- Common Values
				8.00 #-- max_organisation
				0.40 #-- max_strength
				0.00 #-- build_cost_ic
				0.00 #-- maximum_speed
				0.00 #-- armor_value
				0.20 #-- ap_attack
				0.00 #-- reliability
				0.00 #-- reliability_factor
				0.00 #-- weight
			}
		
		target_template = {
			weight = 0.6
			match_value = 3000
			
			#support = {
			#	#Mot_Recce_Comp = 1
			# }
			
			regiments = {
				Mot_Marine_Bat = 3
				#Arty_Battery = 1
				#L_Engi_Comp = 1
			}
		}
		
		allowed_types = {
			Mot_Marine_Bat
			L_Recce_Bat
			L_Recce_Comp
			Mot_Recce_Bat
			Mot_Recce_Comp
			Mech_Recce_Bat
			Mech_Recce_Comp
			Arm_Recce_Bat
			Arm_Recce_Comp
			armor_Recce_Bat
			armor_Recce_Comp
			armor_Bat
			armor_Comp
			Special_Forces
			Arty_Bat
			Arty_Battery
			L_Engi_Bat
			L_Engi_Comp
			Special_Forces
			SP_Arty_Bat
			SP_Arty_Battery
			SP_AA_Bat
			SP_AA_Battery
		}
		target_min_match = 0.7
	}
	Mech_Marine_Brg = {
	
		upgrade_prio = {
			factor = 1

			modifier = {
				factor = 1
			}
		}
		
		target_width = 20
		width_weight = 0.6
		column_swap_factor = 0.3
		
		stat_weights = {
				0.00 #-- default_morale
				0.00 #-- defense
				1.00 #-- breakthrough
				0.00 #-- hardness
				2.00 #-- soft_attack
				2.00 #-- hard_attack
				0.00 #-- recon
				0.00 #-- entrenchment
				0.00 #-- initiative
				0.00 #-- casualty_trickleback
				0.00 #-- supply_consumption_factor
				0.00 #-- supply_consumption
				0.00 #-- suppression
				0.00 #-- suppression_factor
				0.00 #-- experience_loss_factor
				#-- Air Values
				0.00 #-- air_attack
				#-- Common Values
				8.00 #-- max_organisation
				0.40 #-- max_strength
				0.00 #-- build_cost_ic
				0.00 #-- maximum_speed
				0.00 #-- armor_value
				0.20 #-- ap_attack
				0.00 #-- reliability
				0.00 #-- reliability_factor
				0.00 #-- weight
			}
		
		target_template = {
			weight = 0.6
			match_value = 3000
			
			#support = {
			#	#Mot_Recce_Comp = 1
			# }
			
			regiments = {
				Mech_Marine_Bat = 3
				#Arty_Battery = 1
				#L_Engi_Comp = 1
			}
		}
		
		allowed_types = {
			Mech_Marine_Bat
			Mot_Recce_Bat
			Mot_Recce_Comp
			Mech_Recce_Bat
			Mech_Recce_Comp
			Arm_Recce_Bat
			Arm_Recce_Comp
			armor_Recce_Bat
			armor_Recce_Comp
			armor_Bat
			armor_Comp
			Arty_Bat
			Arty_Battery
			L_Engi_Bat
			L_Engi_Comp
			H_Engi_Bat
			H_Engi_Comp
			SP_Arty_Bat
			SP_Arty_Battery
			SP_AA_Bat
			SP_AA_Battery
		}
		replace_at_match = 0.7
		replace_with = Arm_Marine_Brg
		target_min_match = 0.5
	}
	Arm_Marine_Brg = {
	
		upgrade_prio = {
			factor = 1

			modifier = {
				factor = 1
			}
		}
		
		target_width = 20
		width_weight = 0.6
		column_swap_factor = 0.3
		
		stat_weights = {
				0.00 #-- default_morale
				0.00 #-- defense
				1.00 #-- breakthrough
				0.00 #-- hardness
				2.00 #-- soft_attack
				2.00 #-- hard_attack
				0.00 #-- recon
				0.00 #-- entrenchment
				0.00 #-- initiative
				0.00 #-- casualty_trickleback
				0.00 #-- supply_consumption_factor
				0.00 #-- supply_consumption
				0.00 #-- suppression
				0.00 #-- suppression_factor
				0.00 #-- experience_loss_factor
				#-- Air Values
				0.00 #-- air_attack
				#-- Common Values
				8.00 #-- max_organisation
				0.40 #-- max_strength
				0.00 #-- build_cost_ic
				0.00 #-- maximum_speed
				0.00 #-- armor_value
				0.20 #-- ap_attack
				0.00 #-- reliability
				0.00 #-- reliability_factor
				0.00 #-- weight
			}
		
		target_template = {
			weight = 0.6
			match_value = 3000
			
			#support = {
			#	#Mot_Recce_Comp = 1
			# }
			
			regiments = {
				Arm_Marine_Bat = 3
				#Arty_Battery = 1
				#L_Engi_Comp = 1
			}
		}
		
		allowed_types = {
			Arm_Marine_Bat
			Mot_Recce_Bat
			Mot_Recce_Comp
			Mech_Recce_Bat
			Mech_Recce_Comp
			Arm_Recce_Bat
			Arm_Recce_Comp
			armor_Recce_Bat
			armor_Recce_Comp
			armor_Bat
			armor_Comp
			Arty_Bat
			Arty_Battery
			L_Engi_Bat
			L_Engi_Comp
			H_Engi_Bat
			H_Engi_Comp
			SP_Arty_Bat
			SP_Arty_Battery
			SP_AA_Bat
			SP_AA_Battery
		}
		
	}
	
}

Air_assault_generic = { #Air_assault
	
	#available_for = { #Who is this template available for? Left blank as it's for everyone
	   #tag
	# }
	#blocked_for = { #who is this template blocked for? Who has their own custom templates?
		#tag
	# }
	roles = {
		Air_assault
	}
	
	upgrade_prio = {
		factor = 1

		modifier = {
			factor = 1
		}
	}
	
	match_to_count = 0.5
	
	L_Air_assault_Brg = {
	
		upgrade_prio = {
			factor = 1

			modifier = {
				factor = 1
			}
		}
		
		target_width = 15
		width_weight = 0.6
		column_swap_factor = 0.3
		
		stat_weights = {
				0.00 #-- default_morale
				0.00 #-- defense
				1.00 #-- breakthrough
				0.00 #-- hardness
				2.00 #-- soft_attack
				2.00 #-- hard_attack
				0.00 #-- recon
				0.00 #-- entrenchment
				0.00 #-- initiative
				0.00 #-- casualty_trickleback
				0.00 #-- supply_consumption_factor
				0.00 #-- supply_consumption
				0.00 #-- suppression
				0.00 #-- suppression_factor
				0.00 #-- experience_loss_factor
				#-- Air Values
				0.00 #-- air_attack
				#-- Common Values
				8.00 #-- max_organisation
				0.40 #-- max_strength
				0.00 #-- build_cost_ic
				0.00 #-- maximum_speed
				0.00 #-- armor_value
				0.20 #-- ap_attack
				0.00 #-- reliability
				0.00 #-- reliability_factor
				0.00 #-- weight
			}
		
		target_template = {
			weight = 0.6
			match_value = 3000
			
			#support = {
			#	#Mot_Recce_Comp = 1
			# }
			
			regiments = {
				L_Air_assault_Bat = 3
				#Arty_Battery = 1
				#L_Engi_Comp = 1
			}
		}
		
		allowed_types = {
			L_Air_assault_Bat
			L_Recce_Bat
			L_Recce_Comp
			Mot_Recce_Bat
			Mot_Recce_Comp
			Mech_Recce_Bat
			Mech_Recce_Comp
			Arm_Recce_Bat
			Arm_Recce_Comp
			armor_Recce_Bat
			armor_Recce_Comp
			armor_Bat
			armor_Comp
			Special_Forces
			Arty_Bat
			Arty_Battery
			L_Engi_Bat
			L_Engi_Comp
			Special_Forces
			SP_Arty_Bat
			SP_Arty_Battery
			SP_AA_Bat
			SP_AA_Battery
		}
		replace_at_match = 0.7
		replace_with = Arm_Air_assault_Brg
		target_min_match = 0.5
	}
	Arm_Air_assault_Brg = {
	
		upgrade_prio = {
			factor = 1

			modifier = {
				factor = 1
			}
		}
		
		target_width = 20
		width_weight = 0.6
		column_swap_factor = 0.3
		
		stat_weights = {
				0.00 #-- default_morale
				0.00 #-- defense
				1.00 #-- breakthrough
				0.00 #-- hardness
				2.00 #-- soft_attack
				2.00 #-- hard_attack
				0.00 #-- recon
				0.00 #-- entrenchment
				0.00 #-- initiative
				0.00 #-- casualty_trickleback
				0.00 #-- supply_consumption_factor
				0.00 #-- supply_consumption
				0.00 #-- suppression
				0.00 #-- suppression_factor
				0.00 #-- experience_loss_factor
				#-- Air Values
				0.00 #-- air_attack
				#-- Common Values
				8.00 #-- max_organisation
				0.40 #-- max_strength
				0.00 #-- build_cost_ic
				0.00 #-- maximum_speed
				0.00 #-- armor_value
				0.20 #-- ap_attack
				0.00 #-- reliability
				0.00 #-- reliability_factor
				0.00 #-- weight
			}
		
		target_template = {
			weight = 0.6
			match_value = 3000
			
			#support = {
			#	#Mot_Recce_Comp = 1
			# }
			
			regiments = {
				Arm_Air_assault_Bat = 3
				#Arty_Battery = 1
				#L_Engi_Comp = 1
			}
		}
		
		allowed_types = {
			Arm_Air_assault_Bat
			Mot_Recce_Bat
			Mot_Recce_Comp
			Mech_Recce_Bat
			Mech_Recce_Comp
			Arm_Recce_Bat
			Arm_Recce_Comp
			armor_Recce_Bat
			armor_Recce_Comp
			armor_Bat
			armor_Comp
			Arty_Bat
			Arty_Battery
			L_Engi_Bat
			L_Engi_Comp
			H_Engi_Bat
			H_Engi_Comp
			SP_Arty_Bat
			SP_Arty_Battery
			SP_AA_Bat
			SP_AA_Battery
		}
		
	}
	
}

Special_Forces_generic = { #Special_Forces
	
	#available_for = { #Who is this template available for? Left blank as it's for everyone
	   #tag
	# }
	#blocked_for = { #who is this template blocked for? Who has their own custom templates?
		#tag
	# }
	roles = {
		Special_Forces
	}
	
	upgrade_prio = {
		factor = 1

		modifier = {
			factor = 1
		}
	}
	
	match_to_count = 0.5
	
	Special_Forces = {
		
		upgrade_prio = {
			factor = 1

			modifier = {
				factor = 1
			}
		}
		
		target_width = 10
		width_weight = 0.6
		column_swap_factor = 0.5

		stat_weights = {
			0.00 #-- default_morale
			0.00 #-- defense
			1.00 #-- breakthrough
			0.00 #-- hardness
			2.00 #-- soft_attack
			2.00 #-- hard_attack
			0.00 #-- recon
			0.00 #-- entrenchment
			0.00 #-- initiative
			0.00 #-- casualty_trickleback
			0.00 #-- supply_consumption_factor
			0.00 #-- supply_consumption
			0.00 #-- suppression
			0.00 #-- suppression_factor
			0.00 #-- experience_loss_factor
			#-- Air Values
			0.00 #-- air_attack
			#-- Common Values
			8.00 #-- max_organisation
			0.40 #-- max_strength
			0.00 #-- build_cost_ic
			0.00 #-- maximum_speed
			0.00 #-- armor_value
			0.20 #-- ap_attack
			0.00 #-- reliability
			0.00 #-- reliability_factor
			0.00 #-- weight
		}
		
		target_template = {
			weight = 0.6
			match_value = 3000
			
			#support = {
			#	L_Recce_Comp = 1
			# }
			
			regiments = {
				Special_Forces = 2
			}
		}
		
		allowed_types = {
			L_Inf_Bat
			L_Recce_Bat
			L_Recce_Comp
			Arty_Bat
			Arty_Battery
			L_Engi_Bat
			L_Engi_Comp
			Special_Forces
		}
		target_min_match = 0.5
	}
	
}

armor_generic = { #armor
	
	#available_for = { #Who is this template available for? Left blank as it's for everyone
	   #tag
	# }
	#blocked_for = { #who is this template blocked for? Who has their own custom templates?
		#tag
	# }
	roles = {
		armor
	}
	
	match_to_count = 0.30
	
	upgrade_prio = {
		factor = 1

		modifier = {
			factor = 1
		}
	}
	
	armor_Brg = {
		
		upgrade_prio = {
			factor = 1

			modifier = {
				factor = 1
			}
		}
		
		target_width = 40
		width_weight = 0.6
		column_swap_factor = 0.5

		stat_weights = {
			0.00 #-- default_morale
			0.00 #-- defense
			1.00 #-- breakthrough
			0.00 #-- hardness
			2.00 #-- soft_attack
			2.00 #-- hard_attack
			0.00 #-- recon
			0.00 #-- entrenchment
			0.00 #-- initiative
			0.00 #-- casualty_trickleback
			0.00 #-- supply_consumption_factor
			0.00 #-- supply_consumption
			0.00 #-- suppression
			0.00 #-- suppression_factor
			0.00 #-- experience_loss_factor
			#-- Air Values
			0.00 #-- air_attack
			#-- Common Values
			8.00 #-- max_organisation
			0.40 #-- max_strength
			0.00 #-- build_cost_ic
			0.00 #-- maximum_speed
			0.00 #-- armor_value
			0.20 #-- ap_attack
			0.00 #-- reliability
			0.00 #-- reliability_factor
			0.00 #-- weight
		}
		
		target_template = {
			weight = 0.6
			match_value = 3000
			
			# support = {
				# SP_AA_Battery = 1
			# }
			
			regiments = {
				Arm_Inf_Bat = 3
				armor_Bat = 3
				SP_#Arty_Battery = 1
				H_Engi_Comp = 1
			}
		}
		
		allowed_types = {
			Arm_Inf_Bat
			Mech_Inf_Bat
			Mot_Recce_Bat
			Mot_Recce_Comp
			Mech_Recce_Bat
			Mech_Recce_Comp
			Arm_Recce_Bat
			Arm_Recce_Comp
			armor_Recce_Bat
			armor_Recce_Comp
			armor_Bat
			armor_Comp
			Arty_Bat
			Arty_Battery
			L_Engi_Bat
			L_Engi_Comp
			H_Engi_Bat
			H_Engi_Comp
			SP_Arty_Bat
			SP_Arty_Battery
			SP_AA_Bat
			SP_AA_Battery
		}
	}

	 
}

