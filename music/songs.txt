
music = {
	song = "maintheme"
	chance = {
		modifier = {
			factor = 0
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Conspiracy"
	chance = {
		modifier = {
			factor = 1
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Mastermind"
	chance = {
		modifier = {
			factor = 1
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "The Martyred"
	chance = {
		modifier = {
			factor = 1
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Conflict"
	chance = {
		modifier = {
			factor = 1
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Global Suspense"
	chance = {
		modifier = {
			factor = 1
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Intense Negotiations"
	chance = {
		modifier = {
			factor = 0
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Rising Dragon"
	chance = {
		modifier = {
			factor = 1
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Tension"
	chance = {
		modifier = {
			factor = 1
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "The War of Nations"
	chance = {
		modifier = {
			factor = 1
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}
### Fake Commercials
# Set to play seldom - are meant to occationally lift the mood

music = {
	song = "Commerical - Cashkick"
	chance = {
		modifier = {
			factor = 0.15
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Commerical - Incredible Opportunity"
	chance = {
		modifier = {
			factor = 0.15
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Commerical - Juicy Fresh"
	chance = {
		modifier = {
			factor = 0.15
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Commerical - Moon Realestate Corporation"
	chance = {
		modifier = {
			factor = 0.15
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Commerical - Nigeria Buisnessman"
	chance = {
		modifier = {
			factor = 0.15
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Commerical - The Cheesy Finale"
	chance = {
		modifier = {
			factor = 0.15
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Commerical - Transnistrian Investment Authority"
	chance = {
		modifier = {
			factor = 0.15
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

music = {
	song = "Commerical - The Love Confirmator"
	chance = {
		modifier = {
			factor = 0.15
		}
		modifier = {
			factor = 0
			has_government = fascism
		}
	}
}

### Salafist Music ###

music = {
	song = "Jihad Nasheed 01"
	chance = {
		modifier = {
			factor = 20
			OR = {
				is_puppet_of = ISI
				tag = ISI
			}
			has_government = fascism
		}
		modifier = {
			factor = 0
			NOT = {
				OR = {
					is_puppet_of = ISI
					tag = ISI
				}
			}
		}
	}
}

music = {
	song = "Jihad Nasheed 02"
	chance = {
		modifier = {
			factor = 20
			OR = {
				is_puppet_of = ISI
				tag = ISI
			}
			has_government = fascism
		}
		modifier = {
			factor = 0
			NOT = {
				OR = {
					is_puppet_of = ISI
					tag = ISI
				}
			}
		}
	}
}

music = {
	song = "Jihad Nasheed 03"
	chance = {
		modifier = {
			factor = 20
			OR = {
				is_puppet_of = ISI
				tag = ISI
			}
			has_government = fascism
		}
		modifier = {
			factor = 0
			NOT = {
				OR = {
					is_puppet_of = ISI
					tag = ISI
				}
			}
		}
	}
}

music = {
	song = "Jihad Nasheed 04"
	chance = {
		modifier = {
			factor = 20
			OR = {
				tag = NUS
				tag = FSA
				tag = ISI
				}
			OR = {
				has_government = neutrality
				has_government = fascism
			}
		}
		modifier = {
			factor = 0
			NOT = {
				OR = {
					is_puppet_of = ISI
					tag = ISI
					tag = NUS
					tag = FSA
				}
			}
		}
	}
}

music = {
	song = "Jihad Nasheed 05"
	chance = {
		modifier = {
			factor = 20
			OR = {
				is_puppet_of = ISI
				tag = ISI
			}
			has_government = fascism
		}
		modifier = {
			factor = 0
			NOT = {
				OR = {
					is_puppet_of = ISI
					tag = ISI
				}
			}
		}
	}
}

music = {
	song = "Jihad Nasheed 06"
	chance = {
		modifier = {
			factor = 20
			OR = {
				is_puppet_of = ISI
				tag = ISI
			}
			has_government = fascism
		}
		modifier = {
			factor = 0
			NOT = {
				OR = {
					is_puppet_of = ISI
					tag = ISI
				}
			}
		}
	}
}

music = {
	song = "Jihad Nasheed 07"
	chance = {
		modifier = {
			factor = 20
			OR = {
				is_puppet_of = ISI
				tag = ISI
			}
			has_government = fascism
		}
		modifier = {
			factor = 0
			NOT = {
				OR = {
					is_puppet_of = ISI
					tag = ISI
				}
			}
		}
	}
}

music = {
	song = "Jihad Nasheed 08"
	chance = {
		modifier = {
			factor = 20
			OR = {
				is_puppet_of = ISI
				tag = ISI
			}
			has_government = fascism
		}
		modifier = {
			factor = 0
			NOT = {
				OR = {
					is_puppet_of = ISI
					tag = ISI
				}
			}
		}
	}
}

music = {
	song = "Jihad Nasheed 09"
	chance = {
		modifier = {
			factor = 20
			OR = {
				is_puppet_of = ISI
				tag = ISI
			}
			has_government = fascism
		}
		modifier = {
			factor = 0
			NOT = {
				OR = {
					is_puppet_of = ISI
					tag = ISI
				}
			}
		}
	}
}

music = {
	song = "Jihad Nasheed 11"
	chance = {
		modifier = {
			factor = 20
			OR = {
				is_puppet_of = ISI
				tag = ISI
			}
			has_government = fascism
		}
		modifier = {
			factor = 0
			NOT = {
				OR = {
					is_puppet_of = ISI
					tag = ISI
				}
			}
		}
	}
}

music = {
	song = "Jihad Nasheed 12"
	chance = {
		modifier = {
			factor = 20
			OR = {
				is_puppet_of = ISI
				tag = ISI
			}
			has_government = fascism
		}
		modifier = {
			factor = 0
			NOT = {
				OR = {
					is_puppet_of = ISI
					tag = ISI
				}
			}
		}
	}
}

music = {
	song = "Jihad Nasheed 14"
	chance = {
		modifier = {
			factor = 20
			OR = {
				is_puppet_of = ISI
				tag = ISI
			}
			has_government = fascism
		}
		modifier = {
			factor = 0
			NOT = {
				OR = {
					is_puppet_of = ISI
					tag = ISI
				}
			}
		}
	}
}

music = {
	song = "Jihad Nasheed 15"
	chance = {
		modifier = {
			factor = 20
			OR = {
				is_puppet_of = ISI
				tag = ISI
			}
			has_government = fascism
		}
		modifier = {
			factor = 0
			NOT = {
				OR = {
					is_puppet_of = ISI
					tag = ISI
				}
			}
		}
	}
}

music = {
	song = "Jihad Nasheed 16"
	chance = {
		modifier = {
			factor = 20
			OR = {
				is_puppet_of = ISI
				tag = ISI
			}
			has_government = fascism
		}
		modifier = {
			factor = 0
			NOT = {
				OR = {
					is_puppet_of = ISI
					tag = ISI
				}
			}
		}
	}
}

music = {
	song = "Jihad Nasheed 18" # Qalu innaha wa'ad, all of them like this one
	chance = {
		modifier = {
			factor = 20
			OR = {
				tag = NUS
				tag = FSA
				tag = ISI
			}
			OR = {
				has_government = neutrality
				has_government = fascism
			}
		}
		modifier = {
			factor = 0
			NOT = {
				OR = {
					tag = NUS
					tag = FSA
					tag = ISI
				}
			}
		}
	}
}

music = {
	song = "Jihad Nasheed 19" #Lightning by Ahmad al-Muqit
	chance = {
		modifier = {
			factor = 20
			OR = {
				tag = NUS
				tag = FSA
			}
			OR = {
				has_government = neutrality
				has_government = fascism
			}
		}
		modifier = {
			factor = 0
			NOT = {
				OR = {
					tag = NUS
					tag = FSA
				}
			}
		}
	}
}

music = {
	song = "Jihad Nasheed 20" #Zalziluhum by Faylaq al Sham
	chance = {
		modifier = {
			factor = 20
			tag = FSA 
			OR = {
				has_government = neutrality
				has_government = fascism
			}
		}
		modifier = {
			factor = 0
			NOT = {
				OR = {
					tag = FSA
				}
			}
		}
	}
}

music = {
	song = "Jihad Nasheed 21" #FSA song with instruments that HTS won't like
	chance = {
		modifier = {
			factor = 20
			tag = FSA 
			OR = {
				has_government = neutrality
				has_government = fascism
			}
		}
		modifier = {
			factor = 0
			NOT = {
				OR = {
					tag = FSA
				}
			}
		}
	}
}

music = {
	song = "Jihad Nasheed 22" #Nasheed made for conquest of Idlib
	chance = {
		modifier = {
			factor = 20
			OR = {
				tag = NUS
				tag = FSA
			}
			OR = {
				has_government = neutrality
				has_government = fascism
			}
		}
		modifier = {
			factor = 0
			NOT = {
				OR = {
					tag = NUS
					tag = FSA
				}
			}
		}
	}
}

music = {
	song = "Jihad Nasheed 23" #Ahrar al Sham song
	chance = {
		modifier = {
			factor = 20
			OR = {
				tag = NUS
				tag = FSA
			}
			OR = {
				has_government = neutrality
				has_government = fascism
			}
		}
		modifier = {
			factor = 0
			NOT = {
				OR = {
					tag = NUS
					tag = FSA
				}
			}
		}
	}
}

music = {
	song = "Jihad Nasheed 24" #caeke ya ceyşü küfrü aramram / we are coming oh army of infidels
	chance = {
		modifier = {
			factor = 20
			OR = {
				tag = NUS
				tag = FSA
			}
			OR = {
				has_government = neutrality
				has_government = fascism
			}
			has_war = yes
		}
		modifier = {
			factor = 0
			NOT = {
				OR = {
					tag = NUS
					tag = FSA
				}
			}
		}
	}
}