﻿add_namespace = jihadi

#####Jihadis
country_event = {
	id = jihadi.1
	title = jihadi.1.t
	desc = jihadi.1.d
	
	trigger = {
		NOT = { has_government = fascism }
		OR = {
			has_idea = sunni
			has_idea = sufi_islam
		}
		fascism > 0.2
	}

	mean_time_to_happen = {
		days = 1000
		#Economic factors
		
		modifier = {
			factor = 1.4
			has_idea = economic_boom
		}
		modifier = {
			factor = 1.2
			has_idea = fast_growth
		}
		modifier = {
			factor = 0.9
			has_idea = stagnation
		}
		modifier = {
			factor = 0.8
			has_idea = recession
		}
		modifier = {
			factor = 0.7
			has_idea = depression
		}
		modifier = {
			factor = 0.9
			has_idea = gdp_4
		}
		modifier = {
			factor = 0.9
			has_idea = gdp_3
		}
		modifier = {
			factor = 0.8
			has_idea = gdp_2
		}
		modifier = {
			factor = 0.7
			has_idea = gdp_1
		}
		modifier = {
			factor = 0.5
			has_idea = paralyzing_corruption
		}
		modifier = {
			factor = 0.5
			has_idea = crippling_corruption
		}
		modifier = {
			factor = 0.6
			has_idea = rampant_corruption
		}
		modifier = {
			factor = 0.7
			has_idea = unrestrained_corruption
		}
		modifier = {
			factor = 0.8
			has_idea = systematic_corruption
		}
		modifier = {
			factor = 0.9
			has_idea = widespread_corruption
		}
		
		#Policy factors
		modifier = {
			factor = 1.5
			has_idea = police_05
		}
		modifier = {
			factor = 1.3
			has_idea = police_04
		}
		modifier = {
			factor = 0.9
			has_idea = police_02
		}
		modifier = {
			factor = 0.7
			has_idea = police_01
		}
		modifier = {
			factor = 1.2
			has_idea = bureau_05
		}
		modifier = {
			factor = 1.1
			has_idea = bureau_04
		}
		modifier = {
			factor = 0.9
			has_idea = bureau_02
		}
		modifier = {
			factor = 0.8
			has_idea = bureau_01
		}
		modifier = {
			factor = 1.6
			has_idea = social_06
		}
		modifier = {
			factor = 1.4
			has_idea = social_05
		}
		modifier = {
			factor = 1.2
			has_idea = social_04
		}
		modifier = {
			factor = 2
			tag = PAK
		}
	}

	option = {
		name = jihadi.1.a
		log = "[GetDateText]: [This.GetName]: jihadi.1.a executed"
		add_popularity = {
			ideology = fascism
			popularity = 0.05
		}
	}
}

### Evaluating Saudi Aid ###
country_event = {
	id = jihadi.2
	title = jihadi.2.t
	desc = jihadi.2.d
	
	trigger = {
		NOT = { has_government = fascism }
		NOT = { has_country_flag = keep_saudi_aid }
		OR = {
			has_idea = sunni
			has_idea = sufi_islam
		}
		has_idea = saudi_aid
		fascism > 0.35
	}

	mean_time_to_happen = {
		days = 350
		
	}

	option = {
		name = jihadi.2.a
		log = "[GetDateText]: [This.GetName]: jihadi.2.a executed"
		ai_chance = { factor = 25 }
		set_country_flag = keep_saudi_aid
	}
	
	option = {
		name = jihadi.2.b
		log = "[GetDateText]: [This.GetName]: jihadi.2.b executed"
		ai_chance = { factor = 75 }
		remove_ideas = saudi_aid
	}
}

#####Terrorist attack
country_event = {
	id = jihadi.3
	title = jihadi.3.t
	desc = jihadi.3.d
	
	trigger = {
		NOT = { has_government = fascism }
		OR = {
			has_idea = sunni
			has_idea = sufi_islam
		}
		fascism > 0.2
	}

	mean_time_to_happen = {
		days = 1000
		#Economic factors
		modifier = {
			factor = 1.4
			has_idea = economic_boom
		}
		modifier = {
			factor = 1.2
			has_idea = fast_growth
		}
		modifier = {
			factor = 0.9
			has_idea = stagnation
		}
		modifier = {
			factor = 0.8
			has_idea = recession
		}
		modifier = {
			factor = 0.7
			has_idea = depression
		}
		modifier = {
			factor = 0.9
			has_idea = gdp_4
		}
		modifier = {
			factor = 0.9
			has_idea = gdp_3
		}
		modifier = {
			factor = 0.8
			has_idea = gdp_2
		}
		modifier = {
			factor = 0.7
			has_idea = gdp_1
		}
		modifier = {
			factor = 0.5
			has_idea = paralyzing_corruption
		}
		modifier = {
			factor = 0.5
			has_idea = crippling_corruption
		}
		modifier = {
			factor = 0.6
			has_idea = rampant_corruption
		}
		modifier = {
			factor = 0.7
			has_idea = unrestrained_corruption
		}
		modifier = {
			factor = 0.8
			has_idea = systematic_corruption
		}
		modifier = {
			factor = 0.9
			has_idea = widespread_corruption
		}
		
		#Policy factors
		modifier = {
			factor = 1.5
			has_idea = police_05
		}
		modifier = {
			factor = 1.3
			has_idea = police_04
		}
		modifier = {
			factor = 0.9
			has_idea = police_02
		}
		modifier = {
			factor = 0.7
			has_idea = police_01
		}
		modifier = {
			factor = 1.2
			has_idea = bureau_05
		}
		modifier = {
			factor = 1.1
			has_idea = bureau_04
		}
		modifier = {
			factor = 0.9
			has_idea = bureau_02
		}
		modifier = {
			factor = 0.8
			has_idea = bureau_01
		}
		modifier = {
			factor = 1.6
			has_idea = social_06
		}
		modifier = {
			factor = 1.4
			has_idea = social_05
		}
		modifier = {
			factor = 1.2
			has_idea = social_04
		}
		modifier = {
			factor = 2
			tag = PAK
		}
	}

	option = {
		name = jihadi.3.a
		log = "[GetDateText]: [This.GetName]: jihadi.3.a executed"
		add_manpower = -100
		add_popularity = {
			ideology = fascism
			popularity = 0.03
		}
	}
}

country_event = {
	id = jihadi.4
	title = jihadi.4.t
	desc = jihadi.4.d
	
	trigger = {
		NOT = { has_government = fascism }
		OR = {
			has_idea = sunni
			has_idea = sufi_islam
		}
		fascism > 0.2
	}

	mean_time_to_happen = {
		days = 1000
		#Economic factors
		modifier = {
			factor = 1.1
			has_idea = gdp_4
		}
		modifier = {
			factor = 1.2
			has_idea = gdp_3
		}
		modifier = {
			factor = 1.3
			has_idea = gdp_2
		}
		modifier = {
			factor = 1.4
			has_idea = gdp_1
		}
		modifier = {
			factor = 1.5
			has_idea = paralyzing_corruption
		}
		modifier = {
			factor = 1.4
			has_idea = crippling_corruption
		}
		modifier = {
			factor = 1.3
			has_idea = rampant_corruption
		}
		modifier = {
			factor = 1.2
			has_idea = unrestrained_corruption
		}
		modifier = {
			factor = 1.1
			has_idea = systematic_corruption
		}
		
		#Policy factors
		modifier = {
			factor = 0.4
			has_idea = police_05
		}
		modifier = {
			factor = 0.6
			has_idea = police_04
		}
		modifier = {
			factor = 0.8
			has_idea = police_03
		}
		modifier = {
			factor = 1.1
			has_idea = police_02
		}
		modifier = {
			factor = 1.3
			has_idea = police_01
		}
		modifier = {
			factor = 0.7
			has_idea = bureau_05
		}
		modifier = {
			factor = 0.8
			has_idea = bureau_04
		}
		modifier = {
			factor = 1.1
			has_idea = bureau_02
		}
		modifier = {
			factor = 1.2
			has_idea = bureau_01
		}
		modifier = {
			factor = 0.5
			has_war = yes
		}
	}

	option = {
		name = jihadi.4.a
		log = "[GetDateText]: [This.GetName]: jihadi.4.a executed"
		add_popularity = {
			ideology = fascism
			popularity = -0.1
		}
	}
}

### Civil War
country_event = {
	id = jihadi.5
	title = jihadi.5.t
	desc = jihadi.5.d
	
	trigger = {
		NOT = { has_government = fascism }
		NOT = { has_war = yes }
		OR = {
			has_idea = sunni
			has_idea = sufi_islam
		}
		fascism > 0.5
	}

	mean_time_to_happen = {
		days = 50
	}

	option = {
		name = jihadi.5.a
		log = "[GetDateText]: [This.GetName]: jihadi.5.a executed"
		trigger = { NOT = { tag = PAK } }
		set_country_flag = fighting_jihadis
		start_civil_war = { ideology = fascism size = 0.45 }
		add_popularity = {
			ideology = fascism
			popularity = -0.3
		}
	}
	#For Pakistan
	option = {
		name = jihadi.5.a
		log = "[GetDateText]: [This.GetName]: jihadi.5.a executed"
		trigger = { tag = PAK }
		set_country_flag = fighting_jihadis
		hidden_effect = {		#clear out TTP cores, they have their own country now
			every_owned_state = {
				remove_core_of = TTP
			}
		}
		start_civil_war = {
			ideology = fascism
			size = 0.4
			capital = 418
			states = { 417 418 416 419 }
		}
		random_other_country = {
			limit = {
				original_tag = PAK
				has_government = fascism
			}
			annex_country = { target = TTP transfer_troops = yes }
		}
		add_popularity = {
			ideology = fascism
			popularity = -0.3
		}
	}
	option = { #play as the jihadists
		name = jihadi.5.b
		log = "[GetDateText]: [This.GetName]: jihadi.5.b executed"
		trigger = { is_ai = no NOT = { tag = PAK } }
		set_country_flag = is_jihadist
		if = {
			limit = { has_government = democratic }
			start_civil_war = {
				ruling_party = fascism
				ideology = democratic
				size = 0.6
			}
		}
		if = {
			limit = { has_government = communism }
			start_civil_war = {
					ruling_party = fascism
					ideology = communism
					size = 0.6
			}
		}
		if = {
			limit = { has_government = neutrality }
			start_civil_war = {
				ruling_party = fascism
				ideology = neutrality
				size = 0.6
			}
		}
		if = {
			limit = { has_government = nationalist }
			start_civil_war = {
				ruling_party = fascism
				ideology = nationalist
				size = 0.6
			}
		}
	}
	option = { #play as the pakistani taliban
		name = jihadi.5.b
		log = "[GetDateText]: [This.GetName]: jihadi.5.b executed"
		trigger = { tag = PAK is_ai = no }
		set_country_flag = is_jihadist
		set_capital = 418
		hidden_effect = {		#clear out TTP cores, they have their own country now
			every_owned_state = {
				remove_core_of = TTP
			}
		}
		if = {
			limit = { has_government = democratic }
			start_civil_war = {
				ruling_party = fascism
				ideology = democratic
				size = 0.65
				capital = 422
				states = { 422 573 563 426 420 421 }
			}
		}
		if = {
			limit = { has_government = communism }
			start_civil_war = {
				ruling_party = fascism
				ideology = communism
				size = 0.65
				capital = 422
				states = { 422 573 563 426 420 421 }
			}
		}
		if = {
			limit = { has_government = neutrality }
			start_civil_war = {
				ruling_party = fascism
				ideology = neutrality
				size = 0.65
				capital = 422
				states = { 422 573 563 426 420 421 }
			}
		}
		if = {
			limit = { has_government = nationalist }
			start_civil_war = {
				ruling_party = fascism
				ideology = nationalist
				size = 0.65
				capital = 422
				states = { 422 573 563 426 420 421 }
			}
		}
		annex_country = { target = TTP transfer_troops = yes }
	}
}	





