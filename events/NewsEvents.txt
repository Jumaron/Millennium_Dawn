﻿###########################
# News Events
# 1-x by HansNery
###########################

add_namespace = news

# Free Trade Agreement Signed
news_event = {
	id = news.1
	title = news.1.t
	desc = news.1.d
	picture = GFX_trade_agreement
	
	major = yes
	
	is_triggered_only = yes
	
	option = {
		name = news.1.a
		log = "[GetDateText]: [This.GetName]: news.1.a executed"
	}
}

# Military Cooperation Treaty Signed
news_event = {
	id = news.2
	title = news.2.t
	desc = news.2.d
	picture = trade_agreement
	
	major = yes
	
	is_triggered_only = yes
	
	option = {
		name = news.2.a
		log = "[GetDateText]: [This.GetName]: news.2.a executed"
	}
}

#USS Cole News Event
news_event = {
	id = news.3
	title = news.3.t
	desc = news.3.d
	picture = GFX_news_cole_attack
	
	major = yes
	
	is_triggered_only = yes
	
	option = {
		name = news.3.a
		log = "[GetDateText]: [This.GetName]: news.3.a executed"
		trigger = {
			original_tag = USA
		}
	}
	
	option = {
		name = news.3.b
		log = "[GetDateText]: [This.GetName]: news.3.b executed"
		trigger = {
			NOT = { original_tag = USA }
			NOT = { original_tag = NKO }
			NOT = { original_tag = SUD }
			NOT = { has_war_with = USA }
		}
	}
	
	option = {
		name = news.3.c
		log = "[GetDateText]: [This.GetName]: news.3.c executed"
		trigger = {
			OR = {
				original_tag = NKO
				has_war_with = USA
			}
		}
	}
	
	option = {
		name = news.3.e
		log = "[GetDateText]: [This.GetName]: news.3.e executed"
		trigger = {
			original_tag = SUD
		}
	}
}

#9/11 News Event
news_event = {
	id = news.4
	title = news.4.t
	desc = news.4.d
	picture = GFX_news_nine_eleven
	
	major = yes
	
	is_triggered_only = yes
	
	option = {
		name = news.4.a
		log = "[GetDateText]: [This.GetName]: news.4.a executed"
		trigger = { original_tag = USA }
	}
	
	option = {
		name = news.4.b
		log = "[GetDateText]: [This.GetName]: news.4.b executed"
		trigger = {
			is_in_faction_with = USA
			NOT = { original_tag = USA }
		}
	}
	
	option = {
		name = news.4.c
		log = "[GetDateText]: [This.GetName]: news.4.c executed"
		trigger = {
			NOT = { is_in_faction_with = USA }
			NOT = { original_tag = USA }
			NOT = { has_government = fascism }
		}
	}
	
	option = {
		name = news.4.e
		log = "[GetDateText]: [This.GetName]: news.4.e executed"
		trigger = {
			NOT = { original_tag = USA }
			has_government = fascism
		}
	}
}

#Democrats rally behind the Republican Government after 9/11
news_event = {
	id = news.5
	title = news.5.t
	desc = news.5.d
	picture = GFX_news_hillary_clinton_young
	
	is_triggered_only = yes
	major = yes
	
	option = {
		log = "[GetDateText]: [This.GetName]: news.5.a executed"
		name = usa_news.5.a
	}
}

#National Prayer Service
news_event = {
	id = news.6
	title = news.6.t
	desc = news.6.d
	picture = GFX_news_national_prayer_service_2001
	
	is_triggered_only = yes
	major = yes
	
	option = {
		name = news.6.a
		log = "[GetDateText]: [This.GetName]: news.6.a executed"
	}
}

#Murder of Balbir Singh Sodhi
news_event = {
	id = news.7
	title = news.7.t
	desc = news.7.d
	picture = GFX_news_balbir_singh_sodhi
	
	is_triggered_only = yes
	major = yes
	
	option = {
		name = news.7.a
		log = "[GetDateText]: [This.GetName]: news.7.a executed"
	}
}

#Anthrax Attacks
news_event = {
	id = news.8
	title = news.8.t
	desc = news.8.d
	picture = GFX_news_fbi
	
	is_triggered_only = yes
	major = yes
	
	option = {
		name = news.8.a
		log = "[GetDateText]: [This.GetName]: news.8.a executed"
	}
}

#The War on Terror Speech
news_event = {
	id = news.9
	title = news.9.t
	desc = news.9.d
	picture = GFX_news_george_w_bush
	
	is_triggered_only = yes
	major = yes
	
	option = {
		name = news.9.a
		log = "[GetDateText]: [This.GetName]: news.9.a executed"
	}
}

#Bush announces the Invasion of Afghanistan
news_event = {
	id = news.10
	title = news.10.t
	desc = news.10.d
	picture = GFX_afghanistan_war4
	
	is_triggered_only = yes
	major = yes
	
	option = {
		name = usa_news.10.a
		log = "[GetDateText]: [This.GetName]: news.10.a executed"
	}
}