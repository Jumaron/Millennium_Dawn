﻿2000.1.1 = {
	capital = 396
	oob = "HOR_2000"
	
	set_convoys = 20
}

2017.1.1 = {
	capital = 396

	oob = "HOR_2017"

	declare_war_on = {
		target = TUA
		type = annex_everything
	}

	add_manpower = 10000

	# Starting tech
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		#For templates
		infantry_weapons = 1
		command_control_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		artillery_0 = 1
		SP_arty_0 = 1
		Anti_Air_0 = 1
		early_APC = 1
		APC_1 = 1
		MBT_1 = 1
		IFV_1 = 1
		util_vehicle_0 = 1

		corvette_1 = 1
		corvette_2 = 1

		landing_craft = 1
	}

	add_ideas = {
		pop_050
		crippling_corruption
		gdp_4
		sunni
		defence_09
		edu_02
		health_02
		bureau_03
		police_02
		rentier_state
		export_economy
		volunteer_army
		volunteer_women
		the_military
		The_Ulema
		fossil_fuel_industry
		tribalism
	}
	#set_country_flag = gdp_4

	#Nat focus
		complete_national_focus = bonus_tech_slots
		complete_national_focus = Generic_4K_GDPC_slot

	set_convoys = 20

	set_politics = {

		parties = {
			democratic = { 
				popularity = 25
			}

			fascism = {
				popularity = 5
			}
			
			communism = {
				popularity = 50
				#banned = no #default is no
			}
			
			neutrality = { 
				popularity = 5
			}
			
			nationalist = {
				popularity = 10
			}
		}
		
		ruling_party = communism
		last_election = "2014.1.1"
		election_frequency = 48
		elections_allowed = yes
	}

	#Update to new unity gov (and civ. war) planned

	create_country_leader = {
		name = "Aguila Saleh Issa"
		desc = ""
		picture = "Emerging_HoR_Aguila_Saleh_Issa.dds"
		ideology = Conservative
		traits = {
			#
		}
	}
}