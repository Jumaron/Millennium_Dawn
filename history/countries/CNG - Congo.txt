﻿2000.1.1 = {
	capital = 315
	oob = "CNG_2000"
	
	set_convoys = 20
	
	add_ideas = {
		gdp_3
		tax_cost_17
		multi_ethnic_state_idea
	}
	
	set_technology = { 
		#For templates
		infantry_weapons = 1
		combat_eng_equipment = 1
		command_control_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		artillery_0 = 1
		SP_arty_0 = 1
		Anti_Air_0 = 1
		Early_APC = 1
		APC_1 = 1
		MBT_1 = 1
		util_vehicle_0 = 1
		Rec_tank_0 = 1
	}
	
	set_variable = { var = debt value = 6  }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = treasury value = 0 }
	set_variable = { var = tax_rate value = 17 }
	
	create_corps_commander = {
		name = "Guy Blanchard Okoi"
		picture = "generals/Guy_Okoi.dds"
		id = 13200
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
}

2017.1.1 = {
	capital = 315
	oob = "CNG_2017"
	set_convoys = 20

	add_ideas = {
		pop_050
		rampant_corruption
		christian
		gdp_1
		stable_growth
		defence_07
		edu_03
		health_02
		social_02
		bureau_02
		police_01
		rentier_state
		export_economy
		volunteer_army
		volunteer_women
		fossil_fuel_industry
		international_bankers
		farmers
		hybrid
		tax_cost_12
		multi_ethnic_state_idea
	}
	
	set_country_flag = enthusiastic_fossil_fuel_industry
	set_country_flag = positive_international_bankers
	set_country_flag = negative_farmers
	
	set_variable = { var = debt value = 10 }
	set_variable = { var = treasury value = 1 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = tax_rate value = 12 }
	
	set_variable = { var = size_modifier value = 0.08 } #1 CIC
	initial_money_setup = yes

	#Nat focus
	complete_national_focus = bonus_tech_slots

	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		#For templates
		infantry_weapons = 1
		combat_eng_equipment = 1
		command_control_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		artillery_0 = 1
		SP_arty_0 = 1
		Anti_Air_0 = 1
		Early_APC = 1
		APC_1 = 1
		MBT_1 = 1
		util_vehicle_0 = 1
		Rec_tank_0 = 1
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 10
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 70
				#banned = no #default is no
			}
			
			neutrality = { 
				popularity = 20
			}
		}
		
		ruling_party = communism
		last_election = "2016.3.20"
		election_frequency = 60
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Denis Sassou Nguesso"
		desc = "POLITICS_Mohamed_Cheikh Biadillah_DESC" 
		picture = "CGO_Denis_Sassou_Nguesso.dds"
		expire = "2050.1.1"
		ideology = Conservative
		traits = {
			#
		}
	}
}