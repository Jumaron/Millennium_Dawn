﻿2000.1.1 = {
	capital = 763
	oob = "CAN_2000"
	set_convoys = 150
	
	add_ideas = {
		gdp_8
		pop_050
		negligible_corruption
		pluralist
		g7_member
		defence_01
		export_economy
		edu_04
		health_05
		social_04
		bureau_03
		police_01
		volunteer_army
		volunteer_women
		intervention_limited_interventionism
		NATO_member
		western_country
		small_medium_business_owners
		landowners
		fossil_fuel_industry
		common_law
		tax_cost_15
	}

	set_country_flag = positive_small_medium_business_owners
	set_country_flag = positive_landowners
	set_country_flag = enthusiastic_fossil_fuel_industry  

	set_variable = { var = debt value = 874 }
	set_variable = { var = treasury value = 47 }
	set_variable = { var = tax_rate value = 15 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { french_happiness_var = 46 }
	clamp_variable = { var = french_happiness_var min = 0 max = 100 }
	
	#NATO military access
	diplomatic_relation = {
		country = BEL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CZH
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = DEN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = FRA
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = GER
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = GRE
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = HUN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ICE
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ITA
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = LUX
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = HOL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = NOR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = POL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = POR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = SPR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = TUR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ENG
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = USA
		relation = military_access
		active = yes
	}
	
	
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 100
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 0
				#banned = no #default is no
			}
			
			neutrality = {
				popularity = 0
			}
			
			nationalist = {
				popularity = 0
			}
		}
		
		ruling_party = democratic
		last_election = "1997.6.2"
		election_frequency = 48
		elections_allowed = yes
	}
	
	set_variable = { conservatism_pop = 0.34 }
	set_variable = { liberalism_pop = 0.56 }
	set_variable = { socialism_pop = 0.1 }
	
	recalculate_party = yes
	
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1

		#Colt C8
		infantry_weapons = 1
		infantry_weapons1 = 1
		infantry_weapons2 = 1
		infantry_weapons3 = 1
		
		combat_eng_equipment = 1
		
		night_vision_1 = 1
		night_vision_2 = 1
		night_vision_3 = 1
		
		command_control_equipment = 1
		command_control_equipment1 = 1
		command_control_equipment2 = 1
		command_control_equipment3 = 1
		
		land_Drone_equipment = 1
		land_Drone_equipment1 = 1
		land_Drone_equipment2 = 1
		
		Early_APC = 1
		
		APC_1 = 1
		APC_2 = 1
		APC_3 = 1
		APC_4 = 1
		
		IFV_1 = 1
		IFV_2 = 1
		IFV_3 = 1
		IFV_4 = 1
		
		Rec_tank_0 = 1
		Rec_tank_1 = 1
		
		
		util_vehicle_0 = 1
		util_vehicle_1 = 1
		util_vehicle_2 = 1
		util_vehicle_3 = 1
		
		
		MBT_1 = 1
		ENGI_MBT_1 = 1
		
		artillery_0 = 1
		Arty_upgrade_1 = 1
		
		Anti_tank_0 = 1
		AT_upgrade_1 = 1
		Anti_tank_1 = 1
		Heavy_Anti_tank_0 = 1
		
		Anti_Air_0 = 1
		
		early_bomber = 1
		naval_plane1 = 1
		naval_plane2 = 1
		
		#Halifax Class
		frigate_1 = 1
		frigate_2 = 1
		missile_frigate_1 = 1
		
		#Iroquois Class
		destroyer_1 = 1
		destroyer_2 = 1
		
		#Victoria Class
		diesel_attack_submarine_1 = 1
		diesel_attack_submarine_2 = 1
		diesel_attack_submarine_3 = 1
		
		landing_craft = 1

	}
	
	create_country_leader = {
		name = "Joe Clark"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "joe_clark.dds"
		expire = "2001.1.1" #Date not done yet
		ideology = conservatism
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Alexa Ann MacDonough"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "alexa_mcdonough.dds"
		expire = "2001.1.1" #Date not done yet
		ideology = socialism
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Miguel Figueroa"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "miguel_figueroa.dds"
		expire = "2001.1.1" #Date not done yet
		ideology = Communist-State
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Ron Gray"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "ron_gray.dds"
		expire = "2001.1.1" #Date not done yet
		ideology = Conservative
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Ernie Schreiber"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		#picture = "ron_gray.dds"  #Picture not found
		expire = "2001.1.1" #Date not done yet
		ideology = Neutral_conservatism
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Jean-Serge Brisson"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "jean_serge_brisson.dds"
		expire = "2001.1.1" #Date not done yet
		ideology = Neutral_Libertarian
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Sandra L. Smith"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "sandra_smith.dds"
		expire = "2001.1.1" #Date not done yet
		ideology = Neutral_Communism
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Jamal Badawi"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "jamal_badawi.dds"
		expire = "2001.1.1" #Date not done yet
		ideology = Neutral_Muslim_Brotherhood
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Joan Russow"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "joan_russow.dds"
		expire = "2001.1.1"
		ideology = Neutral_green
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Maurice Baril"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "maurice_baril.dds"
		expire = "2001.1.1" #Date not done yet
		ideology = Nat_Autocracy
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Don Andrews"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "don_andrews.dds"
		expire = "2001.1.1" #Date not done yet
		ideology = Nat_Fascism
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Jean Chrétien"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "jean_chretien.dds"
		expire = "2004.6.28"
		ideology = liberalism
		traits = {
			#
		}
	}
	create_equipment_variant = {
		name = "Cougar"
		type = Rec_tank_1
		upgrades = {
			
		}
		obsolete = yes
	}
	
	
}

2017.1.1 = {
	capital = 763
	oob = "CAN_2017"
	set_convoys = 150
	
	add_ideas = {
		pop_050
		negligible_corruption
		pluralist
		gdp_9
		g7_member
		stable_growth
		defence_01
		export_economy
		edu_04
		health_05
		social_04
		bureau_03
		police_01
		volunteer_army
		volunteer_women
		intervention_limited_interventionism
		NATO_member
		western_country
		medium_far_right_movement
		small_medium_business_owners
		landowners
		fossil_fuel_industry
		common_law
		tax_cost_32
	}
	
	set_country_flag = TPP_Signatory
	set_country_flag = Major_Importer_US_Arms
	set_country_flag = positive_small_medium_business_owners
	set_country_flag = positive_landowners
	set_country_flag = enthusiastic_fossil_fuel_industry	  
	
	set_variable = { var = debt value = 869 }
	set_variable = { var = int_investments value = 13.4 }
	set_variable = { var = treasury value = 83 }
	set_variable = { var = tax_rate value = 32 }
	
	set_variable = { var = size_modifier value = 4.26 } #22 CIC
	initial_money_setup = yes
	
	set_variable = { french_happiness_var = 53 }
	clamp_variable = { var = french_happiness_var min = 0 max = 100 }
	
	#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_15K_GDPC_slot
	complete_national_focus = Generic_30K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot
	complete_national_focus = Generic_16_IC_slot
	complete_national_focus = Generic_30_IC_slot

	# Starting tech
	set_technology = { 
		command_control_equipment4 = 1
		land_Drone_equipment3 = 1
		APC_5 = 1
		Rec_tank_2 = 1
		util_vehicle_4 = 1
		util_vehicle_5 = 1

	}

	#NATO military access
	diplomatic_relation = {
		country = ALB
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = BEL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = BUL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CRO
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CZH
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = DEN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = EST
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = FRA
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = GER
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = GRE
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = HUN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ICE
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ITA
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = LAT
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = LIT
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = LUX
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = HOL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = NOR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = POL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = POR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ROM
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = SLO
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = SLV
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = SPR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = TUR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ENG
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = USA
		relation = military_access
		active = yes
	}

	set_politics = {

		parties = {
			democratic = { 
				popularity = 98
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 0
				#banned = no #default is no
			}
			
			neutrality = {
				popularity = 1
			}
			
			nationalist = {
				popularity = 1
			}
		}
		
		ruling_party = democratic
		last_election = "2015.10.19"
		election_frequency = 48
		elections_allowed = yes
	}
	
	set_variable = { conservatism_pop = 0.3 }
	set_variable = { liberalism_pop = 0.55 }
	set_variable = { socialism_pop = 0.13 }
	set_variable = { Neutral_Green_pop = 0.01 }
	set_variable = { Nat_Populism_pop = 0.01 }
	
	recalculate_party = yes

	create_country_leader = {
		name = "Gilles Duceppe"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "Gilles_Duceppe.dds"
		expire = "2019.10.21"
		ideology = conservatism
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Justin Trudeau"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "CAN_Justin_Trudeau.dds"
		expire = "2019.10.21"
		ideology = liberalism
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Elizabeth May"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "CAN_Elizabeth_May.dds"
		expire = "2023.10.23"
		ideology = Neutral_green
		traits = {
			#
		}
	}
	create_country_leader = {
		name = "Elizabeth Rowley"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "CAN_Elizabeth_Rowley.dds"
		expire = "2023.10.23"
		ideology = Neutral_Communism
		traits = {
			#
		}
	}
	
	create_corps_commander = {
		name = "J. C. G. Juneau"
		picture = "generals/JCG_Juneau.dds"
		id = 10500
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}
	create_field_marshal = {
		name = "Paul Wynnyk"
		picture = ""
		#traits = { thorough_planner }
		id = 10501
		skill = 5
		attack_skill = 5
		defense_skill = 5
		planning_skill = 5
		logistics_skill = 5
	}
	create_field_marshal = {
		name = "Alain Guimond"
		picture = ""
		#traits = { logistics_wizard charismatic }
		id = 10502
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}
	create_corps_commander = {
		name = "Carl Turenne"
		picture = ""
		#traits = { hill_fighter trait_engineer winter_specialist }
		id = 10503
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}
	create_corps_commander = {
		name = "Hercule Gosselin"
		picture = ""
		#traits = { trait_mountaineer winter_specialist }
		id = 10504
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}
	create_corps_commander = {
		name = "S.C. Hetherington"
		picture = ""
		#traits = { trait_mountaineer winter_specialist ranger }
		id = 10505
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}
	create_corps_commander = {
		name = "S.M. Cadden"
		picture = ""
		#traits = { trait_mountaineer winter_specialist ranger }
		id = 10506
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}
	create_navy_leader = {
		name = "Ron Lloyd"
		picture = ""
		#traits = { superior_tactician sea_wolf }
		id = 10507
	}
	create_navy_leader = {
		name = "Peter Bissonnette"
		picture = ""
		#traits = { }
		id = 10508
	}
	create_navy_leader = {
		name = "Fred George"
		picture = ""
		#traits = { }
		id = 10509
	}
	create_field_marshal = {
		name = "Jonathan Vance"
		picture = "Portrait_Jonathan_Vance.dds"
		traits = { old_guard inspirational_leader }
		id = 10510
		skill = 4
		attack_skill = 4
		defense_skill = 4
		planning_skill = 4
		logistics_skill = 4
	}


	create_corps_commander = {
		name = "Dean Milner"
		picture = "Portrait_Dean_Milner.dds"
		traits = { panzer_leader }
		id = 10511
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Christian Juneau"
		picture = "Portrait_Christian_Juneau.dds"
		traits = { trickster }
		id = 10512
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Omer Lavoie"
		picture = "Portrait_Omer_Lavoie.dds"
		traits = { panzer_leader }
		id = 10513
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "C.J. Turenne"
		picture = "Portrait_C_J_Turenne.dds"
		traits = { winter_specialist }
		id = 10514
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Eric Landry"
		picture = "Portrait_Eric_Landry.dds"
		traits = { panzer_leader }
		id = 10515
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Stéphan Joudry"
		picture = "Portrait_Stephan_Joudrey.dds"
		traits = { hill_fighter }
		id = 10516
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "John Cochrane"
		picture = "Portrait_John_Cochrane.dds"
		traits = { ranger }
		id = 10517
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "John Hlibchuk"
		picture = "Portrait_Hlibchuk.dds"
		traits = { panzer_leader }
		id = 10518
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Sylvie Pelletier"
		picture = "Portrait_Sylvie_Pelletier.dds"
		traits = { bearer_of_artillery }
		id = 10519
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Wajahat Ali Beg"
		picture = "Portrait_Wajahat_Ali_Beg.dds"
		traits = { fortress_buster }
		id = 10520
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Jonathan Chouinard"
		picture = "Portrait_Jonathan_Chouinard.dds"
		traits = { ranger }
		id = 10521
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Bill Fletcher"
		picture = "Portrait_Bill_Fletcher.dds"
		traits = { panzer_leader }
		id = 10522
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Luc Girouard"
		picture = "Portrait_Luc_Girouard.dds"
		traits = { trait_engineer }
		id = 10523
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Geoff Abthorpe"
		picture = "Portrait_Geoff_Abthorpe.dds"
		traits = { fortress_buster }
		id = 10524
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Eppo van Weelderen"
		picture = "Portrait_Eppo_van_Weelderen.dds"
		traits = { ranger }
		id = 10525
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Josée Robidoux"
		picture = "Portrait_Josee_Robidoux.dds"
		traits = { ranger }
		id = 10526
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Stephen Bowes"
		picture = "Portrait_Stephen_Bowes.dds"
		traits = { trait_engineer }
		id = 10527
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Michael Hood"
		picture = "Portrait_Michael_Hood.dds"
		traits = {  }
		id = 10528
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Michel Rouleau"
		picture = "Portrait_Michel_Rouleau.dds"
		traits = { naval_invader }
		id = 10529
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Denis Thompson"
		picture = "Portrait_Denis_Thompson.dds"
		traits = { commando }
		id = 10530
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Stephen Hunter"
		picture = "Portrait_Steven_Hunter.dds"
		traits = { trait_mountaineer }
		id = 10531
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Blaise Cathcart"
		picture = "Portrait_Blaise_Cathcart.dds"
		traits = {  }
		id = 10532
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Colleen Halpin"
		picture = "Portrait_Colleen_Halpin.dds"
		traits = {  }
		id = 10533
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Alan Guimond"
		picture = "Portrait_Alan_Guimond.dds"
		traits = {  }
		id = 10534
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Gérard Poitras"
		picture = "Portrait_Gerard_Poitras.dds"
		traits = {  }
		id = 10535
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Charles Lamarre"
		picture = "Portrait_C_A_Lamarre.dds"
		traits = { trait_engineer }
		id = 10536
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "H.C. MacKay"
		picture = "Portrait_H_C_MacKay.dds"
		traits = { trait_engineer }
		id = 10537
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Jean-Robert Bernier"
		picture = "Portrait_Jean_Robert_Bernier.dds"
		traits = {  }
		id = 10538
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Pierre St-Amand"
		picture = "Portrait_Pierre_St-Amand.dds"
		traits = {  }
		id = 10539
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Terry Garand"
		picture = "Portrait_Terry_Garand.dds"
		traits = {  }
		id = 10540
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Stuart Hartnell"
		picture = "Portrait_Stuart_Hartnell.dds"
		traits = {  }
		id = 10541
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Jennie Carignan"
		picture = "Portrait_Jennie_Carignan.dds"
		traits = { trait_engineer }
		id = 10542
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Derek Macaulay"
		picture = "Portrait_Derek_Macauley.dds"
		traits = { ranger }
		id = 10543
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Rob Roy MacKenzie"
		picture = "Portrait_Rob_Roy_MacKenzie.dds"
		traits = { hill_fighter }
		id = 10544
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Jean-Marc Lanthier"
		picture = "Portrait_Jean-Marc_Lanthier.dds"
		traits = { trickster }
		id = 10545
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_navy_leader = {
		name = "Bill Truelove"
		picture = "Portrait_Bill_Truelove.dds"
		traits = { seawolf }
		id = 10546
	}

	create_navy_leader = {
		name = "Art McDonald"
		picture = "Portrait_Art_MacDonald.dds"
		traits = { blockade_runner }
		id = 10547
	}

	create_navy_leader = {
		name = "Jeff Zwick"
		picture = "Portrait_Jeff_Zwick.dds"
		traits = { spotter }
		id = 10548
	}

	create_navy_leader = {
		name = "Michel Vigneault"
		picture = "Portrait_Michel_Vigneault.dds"
		traits = {  }
		id = 10549
	}

	create_navy_leader = {
		name = "Marta Mulkins"
		picture = "Portrait_M_B_Mulkins.dds"
		traits = {  }
		id = 10550
	}

	create_navy_leader = {
		name = "John Newton"
		picture = "Portrait_John_Newton.dds"
		traits = { ironside }
		id = 10551
	}

	create_navy_leader = {
		name = "Craig Baines"
		picture = "Portrait_Craig_Baines.dds"
		traits = {  }
		id = 10552
	}

	create_navy_leader = {
		name = "Scott Bishop"
		picture = "Portrait_Scott_Bishop.dds"
		traits = { ironside }
		id = 10553
	}

	create_navy_leader = {
		name = "Gilles Couturier"
		picture = "Portrait_Gilles_Couturier.dds"
		traits = { blockade_runner }
		id = 10554
	}

	create_navy_leader = {
		name = "David Arsenault"
		picture = "Portrait_David_Arsenault.dds"
		traits = {  }
		id = 10555
	}

	create_navy_leader = {
		name = "Gilles Grégoire"
		picture = "Portrait_Gilles_Gregoire.dds"
		traits = {  }
		id = 10556
	}

	create_navy_leader = {
		name = "Steven Waddell"
		picture = "Portrait_Steven_Waddell.dds"
		traits = {  }
		id = 10557
	}
}