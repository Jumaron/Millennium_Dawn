﻿2000.1.1 = {
	capital = 264
	oob = "MOZ_2000"
	set_convoys = 20
	
	add_ideas = {
		gdp_1
		tax_cost_17
	}
	
	set_variable = { var = debt value = 8 }
	set_variable = { var = treasury value = 1 }
	set_variable = { var = tax_rate value = 17 }
	set_variable = { var = int_investments value = 0 }
	
	set_technology = { 
	 
		infantry_weapons = 1

		command_control_equipment = 1
		
		Anti_tank_0 = 1
		
		Heavy_Anti_tank_0 = 1
		
		Anti_Air_0 = 1
		
		combat_eng_equipment = 1
		
		Early_APC = 1
		MBT_1 = 1
		
		IFV_1 = 1
		
		APC_1 = 1
		
		Rec_tank_0 = 1
		
		util_vehicle_0 = 1
		
		SP_arty_0 = 1
		
		artillery_0 = 1
		SP_Anti_Air_0 = 1

	}
	
	create_field_marshal = {
		name = "Atanásio Salvador M'tumuke"
		picture = "Portrait_Atanasio_Mtumuke.dds"
		traits = { defensive_doctrine }
		id = 41700
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_field_marshal = {
		name = "Graça Chongo"
		picture = "Portrait_Graca_Chongo.dds"
		traits = { organisational_leader }
		id = 41701
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_field_marshal = {
		name = "Olímpio Cambona"
		picture = "Portrait_Olimpio_Cambona.dds"
		traits = { old_guard fast_planner }
		id = 41702
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_field_marshal = {
		name = "Francisco Mataruca"
		picture = "Portrait_Francisco_Zacarias_Mataruca.dds"
		traits = { offensive_doctrine }
		id = 41703
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_field_marshal = {
		name = "Raúl Dique"
		picture = "Portrait_Raul_Dique.dds"
		traits = { logistics_wizard }
		id = 41704
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Mauro Bacar Litamba"
		picture = "Portrait_Mauro_Litamba.dds"
		traits = { commando }
		id = 41705
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Joaquim Marcos Manjate"
		picture = "Portrait_Joaquim_Manjate.dds"
		traits = { trait_engineer }
		id = 41706
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Victor Muirequetule"
		picture = "Portrait_Victor_Muirequetule.dds"
		traits = { fortress_buster }
		id = 41707
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Eugénio Mussa"
		picture = "Portrait_Eugenio_Mussa.dds"
		traits = { panzer_leader }
		id = 41708
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Margarido de Sousa Pinto"
		picture = "Portrait_Margarido_Pinto.dds"
		traits = {  }
		id = 41709
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Daniel Frazão Chale"
		picture = "Portrait_Daniel_Chale.dds"
		traits = { ranger }
		id = 41710
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Albino Gabriel Mandlate"
		picture = "Portrait_Albino_Mandlate.dds"
		traits = { trickster }
		id = 41711
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Marcos Fabião Manjate"
		picture = "Portrait_Marcos_Manjate.dds"
		traits = { bearer_of_artillery }
		id = 41712
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_navy_leader = {
		name = "Lázaro Henriques L. Menete"
		picture = "Portrait_Lazaro_Menete.dds"
		traits = { old_guard_navy superior_tactician }
		id = 41713
	}

	create_navy_leader = {
		name = "Joaquim Rivas Mangrasse"
		picture = "Portrait_Joaquim_Mangrasse.dds"
		traits = { spotter }
		id = 41714
	}
}

2017.1.1 = {
	capital = 264
	oob = "MOZ_2017"
	set_convoys = 20
	
	add_ideas = {
		pop_050
		unrestrained_corruption
		gdp_1
		christian
		stable_growth
		defence_01
		edu_03
		health_03
		social_01
		bureau_04
		police_03
		draft_army
		volunteer_women
		USA_usaid #https://explorer.usaid.gov/aid-dashboard.html
		international_bankers
		landowners
		industrial_conglomerates
		hybrid
		tax_cost_22
	}
	
	#set_country_flag = gdp_1
	set_country_flag = positive_international_bankers
	set_country_flag = negative_landowners
	set_country_flag = positive_industrial_conglomerates
	
	set_variable = { var = debt value = 13 }
	set_variable = { var = treasury value = 2 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = tax_rate value = 22 }
	
	set_variable = { var = size_modifier value = 0.14 } #2 CIC
	initial_money_setup = yes

	#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4_IC_slot
	
	# Starting tech
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
	 
		infantry_weapons = 1

		command_control_equipment = 1
		
		Anti_tank_0 = 1
		
		Heavy_Anti_tank_0 = 1
		
		Anti_Air_0 = 1
		
		combat_eng_equipment = 1
		
		Early_APC = 1
		MBT_1 = 1
		
		IFV_1 = 1
		
		APC_1 = 1
		
		Rec_tank_0 = 1
		
		util_vehicle_0 = 1
		
		SP_arty_0 = 1
		
		artillery_0 = 1
		SP_Anti_Air_0 = 1

	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 20
			}

			fascism = {
				popularity = 16
			}
			
			communism = {
				popularity = 20
				#banned = no #default is no
			}
			
			neutrality = { 
				popularity = 60
			}
		}
		
		ruling_party = neutrality
		last_election = "2014.10.15"
		election_frequency = 60
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Filipe Nyusi"
		desc = "POLITICS_Filipe_Nyusi_DESC"
		picture = "Filipe_Nyusi.dds"
		expire = "2050.1.1"
		ideology = Neutral_conservatism
		traits = {
			#
		}
	}
	
}