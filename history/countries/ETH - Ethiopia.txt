﻿2000.1.1 = {
	capital = 233
	oob = "ETH_2000"
	set_convoys = 5

	add_ideas = {
		gdp_1
		pop_050
		orthodox_christian
		defence_01
		edu_03
		health_02
		social_01
		bureau_01
		volunteer_army
		no_women_in_military
		international_bankers
		farmers
		oligarchs
		hybrid
		tax_cost_10
		multi_ethnic_state_idea
	}

	set_technology = {

		util_vehicle_0 = 1

		#For templates
		infantry_weapons = 1
		combat_eng_equipment = 1
		command_control_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		artillery_0 = 1
		SP_arty_0 = 1
		Anti_Air_0 = 1
		SP_Anti_Air_0 = 1
		Early_APC = 1
		APC_1 = 1
		IFV_1 = 1
		MBT_1 = 1
	}

	set_country_flag = enthusiastic_international_bankers
	set_country_flag = hostile_farmers
	set_country_flag = positive_oligarchs

	set_variable = { var = debt value = 11 }
	set_variable = { var = treasury value = 1 }
	set_variable = { var = tax_rate value = 10 }
	set_variable = { var = int_investments value = 0 }

	set_variable = { var = domestic_influence_amount value = 100 }
	set_variable = { var = influencer1 value = USA.id }
	set_variable = { var = influencer1_amount value = 0 }
	set_variable = { var = influencer2 value = CHI.id }
	set_variable = { var = influencer2_amount value = 0 }
	set_variable = { var = influencer3 value = RAJ.id }
	set_variable = { var = influencer3_amount value = 0 }
	set_variable = { var = influencer4 value = ENG.id }
	set_variable = { var = influencer4_amount value = 0 }
	set_variable = { var = influencer5 value = FRA.id }
	set_variable = { var = influencer5_amount value = 0 }
	set_variable = { var = influencer6 value = GER.id }
	set_variable = { var = influencer6_amount value = 0 }
	recalculate_influence = yes

	set_politics = {

		parties = {
			democratic = {
				popularity = 10
			}

			fascism = {
				popularity = 0
			}

			communism = {
				popularity = 15
			}

			neutrality = {
				popularity = 75
			}

			nationalist = {
				popularity = 0
			}
		}

		ruling_party = neutrality
		last_election = "1995.5.15"
		election_frequency = 60
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Negasso Gidada"
		desc = "POLITICS_Negasso_Gidada_DESC"
		picture = "Negasso_Gidada.dds"
		expire = "2065.1.1"
		ideology = neutral_Social
		traits = {
			#
		}
	}

	### Field marshal's

	create_field_marshal = {
		name = "Samora Muhammad Yunis"
		picture = "Portrait_Samora_Yunis.dds"
		traits = { old_guard organisational_leader }
		id = 20701
		skill = 4
		attack_skill = 4
		defense_skill = 4
		planning_skill = 4
		logistics_skill = 4
	}
	create_field_marshal = {
		name = "Saere Mekonen"
		picture = "Portrait_Saere_Mekonen.dds"
		traits = { }
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}
	create_field_marshal = {
		name = "Tsadkan Gebretensay"
		picture = "Portrait_Tsadkan_Gebretensay.dds"
		traits = { armor_officer }
		id = 20706
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

### Corps commander

	create_corps_commander = {
		name = "Birhanu Julla"
		picture = "Portrait_Birhanu_Julla.dds"
		id = 20700
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Zewdu Kiros Gebrekidan"
		picture = "Portrait_Zewdu_Kiros_Gebrekidan.dds"
		traits = { trickster }
		id = 20703
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Gebremeskel Gebregzeaber"
		picture = "Portrait_Gebremeskel_Gebregzeaber.dds"
		traits = { desert_fox }
		id = 20704
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Abraha Tesfaye"
		picture = "Portrait_Abraha_Tesfaye.dds"
		traits = { trait_engineer }
		id = 20705
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Molla Hailemariam"
		picture = "Portrait_Molla_Hailemariam.dds"
		traits = {  }
		id = 20702
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Demeke Zewdu"
		picture = "Portrait_Demeke_Zewdu.dds"
		traits = { hill_fighter }
		id = 20707
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Gebre Adhana Woldezgu"
		picture = "Portrait_Gebre_Adhana_Woldezgu.dds"
		traits = {  }
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Tesfay Gidey Hailemichae"
		picture = "Portrait_Tesfay_Gidey_Hailemichae.dds"
		traits = {  }
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Hassen Ebrahim Mussa"
		picture = "Portrait_Hassen_Ebrahim_Mussa.dds"
		traits = {  }
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Yohannes Gebremeskel Tesfamariam"
		picture = "Portrait_Yohannes_Gebremeskel_Tesfamariam.dds"
		traits = {  }
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Tadesse Werede Tesfay"
		picture = "Portrait_Tadesse_Werede_Tesfay.dds"
		traits = {  }
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	### Navy leaders

	create_navy_leader = {
		name = "Samora Muhammad Yunis"
		picture = "Portrait_Samora_Yunis.dds"
		traits = {	}
		id = 20708
	}

}

2017.1.1 = {
	capital = 233
	oob = "ETH_2017"
	set_convoys = 5

	add_ideas = {
		pop_050
		unrestrained_corruption
		orthodox_christian
		gdp_1
		fast_growth
		defence_01
		edu_03
		health_02
		social_01
		bureau_01
		volunteer_army
		no_women_in_military
		Enduring_Freedom
		USA_usaid #https://explorer.usaid.gov/aid-dashboard.html
		international_bankers
		farmers
		oligarchs
		hybrid
		tax_cost_13
		multi_ethnic_state_idea
	}

	#set_country_flag = gdp_1
	set_country_flag = enthusiastic_international_bankers
	set_country_flag = hostile_farmers
	set_country_flag = positive_oligarchs

	set_variable = { var = debt value = 2 }
	set_variable = { var = treasury value = }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = tax_rate value = 13 }

	set_variable = { var = size_modifier value = 0.74 } #6 CIC
	initial_money_setup = yes

	# Starting tech
	set_technology = {
		legacy_doctrines = 1
		modern_blitzkrieg = 1
		forward_defense = 1
		encourage_nco_iniative = 1
		air_land_battle = 1
		night_vision_1 = 1

		util_vehicle_0 = 1

		#For templates
		infantry_weapons = 1
		combat_eng_equipment = 1
		command_control_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		artillery_0 = 1
		SP_arty_0 = 1
		Anti_Air_0 = 1
		SP_Anti_Air_0 = 1
		Early_APC = 1
		APC_1 = 1
		IFV_1 = 1
		MBT_1 = 1
	}

	set_politics = {

		parties = {
			democratic = {
				popularity = 10
			}

			fascism = {
				popularity = 0
			}

			communism = {
				popularity = 15
			}

			neutrality = {
				popularity = 75
			}

			nationalist = {
				popularity = 0
			}
		}

		ruling_party = neutrality
		last_election = "2015.5.24"
		election_frequency = 60
		elections_allowed = yes
	}

	add_opinion_modifier = { target = ERI modifier = ERI_Border_Disputes }

	create_country_leader = {
		name = "Hailemariam Desalegn"
		desc = "POLITICS_Hailemariam_Desalegn_DESC"
		picture = "Hailemariam_Desalegn.dds"
		expire = "2065.1.1"
		ideology = Neutral_Autocracy
		traits = {
			#
		}
	}
}
