﻿2000.1.1 = {
	capital = 131
	oob = "SER_2000"
	set_convoys = 25
	
	add_ideas = {
		gdp_4
		multi_ethnic_state_idea
	}

	set_cosmetic_tag = SER_FRY
	
	set_technology = { 
		legacy_doctrines = 1 
		infiltration_assault = 1 
		defence_in_depth = 1 
		early_tunnel_warfare = 1 
		guerilla_specialisation = 1
		
		infantry_weapons = 1
		infantry_weapons1 = 1
		infantry_weapons2 = 1
		
		command_control_equipment = 1
		
		land_Drone_equipment = 1
		
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		
		Anti_Air_0 = 1
		SP_Anti_Air_0 = 1
		
		combat_eng_equipment = 1
		
		util_vehicle_0 = 1
		util_vehicle_1 = 1
		util_vehicle_2 = 1
		
		early_helicopter = 1
		transport_helicopter1 = 1
		
		night_vision_1 = 1
		night_vision_2 = 1
		
		Early_APC = 1
		MBT_1 = 1
		MBT_2 = 1 #M-84A
		
		ENGI_MBT_1 = 1
		ENGI_MBT_2 = 1 #M-84A1
		
		IFV_1 = 1
		IFV_2 = 1 #BVP M-80
		
		APC_1 = 1
		APC_2 = 1
		APC_3 = 1 #BOV-1
		
		artillery_0 = 1
		artillery_1 = 1 #M-84 Nora
		Arty_upgrade_1 = 1
		
		SP_arty_0 = 1
		SP_arty_1 = 1
		
		SP_R_arty_0 = 1
		SP_R_arty_1 = 1 #M-77 Oganj
		
		early_fighter = 1
		L_Strike_fighter1 = 1
		L_Strike_fighter2 = 1 #G-4 Super Galeb
	   
		Strike_fighter1 = 1
		Strike_fighter2 = 1 #J-22 Orao
		
		landing_craft = 1
		
		submarine_1 = 1
		diesel_attack_submarine_1 = 1
		
		corvette_1 = 1
		
		frigate_1 = 1
		frigate_2 = 1
		
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 64
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 18
			}
			
			neutrality = {
				popularity = 6
			}
			
			nationalist = {
				popularity = 11
			}
		}
		
		ruling_party = communism
		last_election = "1997.9.21"
		election_frequency = 48
		elections_allowed = yes
	}
	
	create_country_leader = {
		name = "Vojislav Šešelj"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "vojislav_seselj.dds"
		expire = "2065.1.1"
		ideology = Nat_populism
		traits = {
		
		}
	}
	create_country_leader = {
		name = "Slobodan Milošević"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = ""
		expire = "2006.3.26"
		ideology = Communist-State
		traits = {
		
		}
	}
	create_country_leader = {
		name = "Zoran ?inđić"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = ""
		expire = "2055.3.12"
		ideology = conservatism
		traits = {
		
		}
	}
	create_country_leader = {
		name = "Borislav Pelević"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = ""
		expire = "2065.1.1"
		ideology = Neutral_conservatism
		traits = {
		
		}
	}
	create_country_leader = {
		name = "Živko Radišić"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "zivko_radisic.dds"
		expire = "2065.1.1"
		ideology = Conservative
		traits = {
		
		}
	}
	
	create_corps_commander = {
		name = "Ljubisa Dikovic"
		picture = "Gen_Igor_Borisovich_Tymofeyev.dds"
		traits = { commando trickster }
		id = 54300
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3

	}

	create_corps_commander = {
		name = "Milan Mojsilovic"
		picture = "Gen_Igor_Borisovich_Tymofeyev.dds"
		traits = { panzer_leader }
		id = 54301
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3

	}

	create_corps_commander = {
		name = "Sladjan Djordjevic"
		picture = "Gen_Igor_Borisovich_Tymofeyev.dds"
		traits = { panzer_leader }
		id = 54302
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3

	}
	create_corps_commander = {
		name = "Željko Petrović"
		picture = "Portrait_Zeljko_Petrovic.dds"
		traits = { panzer_leader }
		id = 54303
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Dušan Stojanović"
		picture = "Portrait_Dusan_Stojanovic.dds"
		traits = { panzer_leader }
		id = 54304
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Želimir Glisović"
		picture = "Portrait_Zelimir_Glisovic.dds"
		traits = { trait_engineer }
		id = 54305
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Milosav Simović"
		picture = "Portrait_Milosav_Simovic.dds"
		traits = { ranger }
		id = 54306
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Jelesije Radivojević"
		picture = "Portrait_Jelesije_Radivojevic.dds"
		traits = { commando }
		id = 54307
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Ranko Živak"
		picture = "Portrait_Ranko_Zivak.dds"
		traits = { commando }
		id = 54308
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Predrag Bandić"
		picture = "Portrait_Predrag_Bandic.dds"
		traits = { commando }
		id = 54309
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Dejan Joksimović"
		picture = "Portrait_Dejan_Joksimovic.dds"
		traits = { commando }
		id = 54310
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Miodrag Gordić"
		picture = "Portrait_Miodrag_Gordic.dds"
		traits = { bearer_of_artillery }
		id = 54311
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Branko Andrić"
		picture = "Portrait_Branko_Andric.dds"
		traits = {  }
		id = 54312
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Vojin Jondić"
		picture = "Portrait_Vojin_Jondic.dds"
		traits = { trickster }
		id = 54313
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Mladjen Nišević"
		picture = "Portrait_Mladjen_Nisevic.dds"
		traits = {  }
		id = 54314
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Djokica Petrović"
		picture = "Portrait_Djokica_Petrovic.dds"
		traits = { hill_fighter }
		id = 54315
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Stojan Batinić"
		picture = "Portrait_Stojan_Batinic.dds"
		traits = {  }
		id = 54316
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Zarko Lazarević"
		picture = "Portrait_Zarko_Lazarevic.dds"
		traits = {  }
		id = 54317
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Zoran Lubura"
		picture = "Portrait_Zoran_Lubura.dds"
		traits = {  }
		id = 54318
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Ljubomir Nikolić"
		picture = "Portrait_Ljubomir_Nikolic.dds"
		traits = {  }
		id = 54319
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Marinko Pavlović"
		picture = "Portrait_Marinko_Pavlovic.dds"
		traits = {  }
		id = 54320
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Zoran Popović"
		picture = "Portrait_Zoran_Popovic.dds"
		traits = {  }
		id = 54321
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Predra Simović"
		picture = "Portrait_Predrag_Simovic.dds"
		traits = {  }
		id = 54322
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Vladimir Vukajlović"
		picture = "Portrait_Vladimir_Vukaljovic.dds"
		traits = { urban_assault_specialist }
		id = 54323
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_navy_leader = {
		name = "Andrija Andrić"
		picture = "Portrait_Andrija_Andric.dds"
		traits = { old_guard_navy superior_tactician }
		id = 54324
	}
}

2017.1.1 = {
	drop_cosmetic_tag = yes
	capital = 131
	oob = "SER_2017"
	set_convoys = 5

	set_technology = { 
		infantry_weapons3 = 1
		infantry_weapons4 = 1
		
		util_vehicle_3 = 1
		util_vehicle_4 = 1
		
		AT_upgrade_1 = 1
		Anti_tank_1 = 1
		Heavy_Anti_tank_1 = 1
		
		MBT_3 = 1
		MBT_4 = 1 #M-84A
		
		IFV_3 = 1
		IFV_4 = 1 #BVP M-80
		
		APC_4 = 1
		APC_5 = 1
		
		AA_upgrade_1 = 1
		Anti_Air_1 = 1
		SP_Anti_Air_1 = 1
	}

	add_ideas = {
		pop_050
		orthodox_christian
		unrestrained_corruption
		gdp_4
		stable_growth
		defence_02
		edu_03
		health_04
		social_04
		bureau_02
		police_05
		volunteer_army
		volunteer_women
		medium_far_right_movement
		small_medium_business_owners
		landowners
		The_Clergy
		civil_law
		tax_cost_35
		multi_ethnic_state_idea
	}
	
	#set_country_flag = gdp_4
	set_country_flag = positive_small_medium_business_owners
	set_country_flag = positive_landowners
	
	set_variable = { var = debt value = 26 }
	set_variable = { var = treasury value = 11 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = tax_rate value = 35 }
	
	set_variable = { var = size_modifier value = 0.38 } #4 CIC
	initial_money_setup = yes
	
	#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 25
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 20
			}
			
			neutrality = {
				popularity = 45
			}
			
			nationalist = {
				popularity = 10
			}
		}
		
		ruling_party = neutrality
		last_election = "2016.4.4"
		election_frequency = 48
		elections_allowed = yes
	}


	create_country_leader = {
		name = "Sasa Radulovic"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "sasa_radulovic.dds"
		expire = "2060.1.1"
		ideology = liberalism
		traits = {
		
		}
	}

	create_country_leader = {
		name = "Bojan Pajtic"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "bojan_pajtic.dds"
		expire = "2050.1.1"
		ideology = socialism
		traits = {
		
		}
	}

	create_country_leader = {
		name = "Ivica Dacic"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "ivica_dacic.dds"
		expire = "2060.1.1"
		ideology = Communist-State
		traits = {
		
		}
	}

	create_country_leader = {
		name = "Vojislav Seselj"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "vojislav_seselj.dds"
		expire = "2065.1.1"
		ideology = Nat_populism
		traits = {
		
		}
	}

	create_country_leader = {
		name = "Ana Brnabić"
		picture = "Ana_Brnabic.dds"
		expire = "2060.1.1"
		ideology = liberalism
		traits = {
		
		}
	}

	create_country_leader = {
		name = "Aleksandar Vucic" 
		desc = ""
		picture = "Aleksandar_Vucic.dds"
		expire = "2050.1.1"
		ideology = Neutral_conservatism
		traits = {
			#
		}
	}

}