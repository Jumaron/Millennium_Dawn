﻿2000.1.1 = {
	capital = 32
	oob = "NOR_2000"
	set_convoys = 150
		
	add_ideas = {
		gdp_7
		pop_050
		christian
		slight_corruption
		gdp_9
		stagnation
		defence_02
		edu_05
		health_05
		social_06
		bureau_04
		police_02
		rentier_state
		export_economy
		volunteer_army
		drafted_women
		labour_unions
		fossil_fuel_industry
		maritime_industry
		intervention_limited_interventionism
		NATO_member
		western_country
		medium_far_right_movement
		civil_law
		tax_cost_44
	}
	
	set_country_flag = positive_fossil_fuel_industry
	
	set_variable = { var = debt value = 66.43 }
	set_variable = { var = int_investments value = 58.4 }
	set_variable = { var = treasury value = 40.74 }
	set_variable = { var = tax_rate value = 44 }
	
	#NATO military access
	diplomatic_relation = {
		country = BEL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CAN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CZH
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = DEN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = FRA
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = GER
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = GRE
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = HUN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ICE
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ITA
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = LUX
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = HOL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = POL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = POR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = SPR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = TUR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ENG
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = USA
		relation = military_access
		active = yes
	}

	set_politics = {

		parties = {
			democratic = { 
				popularity = 50.4
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 1.5
			}
			
			neutrality = {
				popularity = 32.8
			}
			
			nationalist = {
				popularity = 15.3
			}
		}
		
		ruling_party = neutrality
		last_election = "1997.9.15"
		election_frequency = 48
		elections_allowed = yes
	}
	
	set_variable = { conservatism_pop = 0.205 }
	set_variable = { liberalism_pop = 0.041 }
	set_variable = { socialism_pop = 0.258 }
	set_variable = { anarchist_communism_pop = 0.015 }
	set_variable = { Neutral_conservatism_pop = 0.133 }
	set_variable = { oligarchism_pop = 0.071 }
	set_variable = { neutral_Social_pop = 0.121 }
	set_variable = { Neutral_Libertarian_pop = 0.001 }
	set_variable = { Neutral_Green_pop = 0.002 }
	set_variable = { Nat_Populism_pop = 0.153 }
	
	coalition_government_set_up = yes
	
	create_country_leader = {
		name = "Jens Stoltenberg"
		picture = "Jens_Stoltenberg.dds"
		expire = "2015.1.1"
		ideology = socialism
		traits = {
			career_politician
			western_socialism
			capable
		}
	}
	
	create_country_leader = {
		name = "Kjell Magne Bondevik"
		picture = "Kjell_Magne_Bondevik.dds"
		expire = "2012.1.1"
		ideology = Neutral_conservatism
		traits = {
			cleric
			neutrality_Neutral_conservatism
			likeable
			cautious
		}
	}
	

	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1

		#for template
		early_fighter = 1
		MR_Fighter1 = 1
		Early_APC = 1
		IFV_1 = 1
		combat_eng_equipment = 1
		MBT_1 = 1
		ENGI_MBT_1 = 1 #
		artillery_0 = 1
		SP_arty_0 = 1
		SP_R_arty_0 = 1
		naval_plane1 = 1
		early_bomber = 1
		early_helicopter = 1
		transport_helicopter1 = 1
		
		infantry_weapons = 1
		
		night_vision_1 = 1
		night_vision_2 = 1
		
		command_control_equipment = 1
		command_control_equipment1 = 1
		command_control_equipment2 = 1
		
		APC_1 = 1
		APC_2 = 1
		APC_3 = 1 
		
		IFV_2 = 1
		
		ENGI_MBT_2 = 1
		
		Rec_tank_0 = 1 #1965
		
		util_vehicle_0 = 1
		
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		AT_upgrade_1 = 1
		Anti_tank_1 = 1
		Heavy_Anti_tank_1 = 1 #NM142 Rakettpanserjager
		
		Anti_Air_0 = 1
		SP_Anti_Air_0 = 1
		AA_upgrade_1 = 1
		Anti_Air_1 = 1 #NALLADS
		SP_Anti_Air_1 = 1
		AA_upgrade_3 = 1
		Anti_Air_2 = 1
		SP_Anti_Air_2 = 1 #NASAMS
		
		corvette_1 = 1
		
		frigate_1 = 1 #Oslo-Class

		diesel_attack_submarine_1 = 1
		diesel_attack_submarine_2 = 1
		diesel_attack_submarine_3 = 1 #Type 210 Ula-Class
		
		landing_craft = 1
		
	}

	
	#NATO military access
	diplomatic_relation = {
		country = ALB
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = BEL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = BUL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CAN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CRO
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CZH
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = DEN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = EST
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = FRA
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = GER
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = GRE
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = HUN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ICE
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ITA
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = LAT
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = LIT
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = LUX
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = HOL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = POL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = POR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ROM
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = SLO
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = SLV
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = SPR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = TUR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ENG
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = USA
		relation = military_access
		active = yes
	}

	#Nordic good relations
	add_opinion_modifier = { target = SWE modifier = nordic }
	add_opinion_modifier = { target = ICE modifier = nordic }
	add_opinion_modifier = { target = FIN modifier = nordic }
	add_opinion_modifier = { target = DEN modifier = nordic }
	
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 50.4
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 1.5
			}
			
			neutrality = {
				popularity = 32.8
			}
			
			nationalist = {
				popularity = 15.3
			}
		}
		
		ruling_party = neutrality
		last_election = "1997.9.15"
		election_frequency = 48
		elections_allowed = yes
	}
	
	set_variable = { conservatism_pop = 0.205 }
	set_variable = { liberalism_pop = 0.041 }
	set_variable = { socialism_pop = 0.258 }
	set_variable = { anarchist_communism_pop = 0.015 }
	set_variable = { Neutral_conservatism_pop = 0.133 }
	set_variable = { oligarchism_pop = 0.071 }
	set_variable = { neutral_Social_pop = 0.121 }
	set_variable = { Neutral_Libertarian_pop = 0.001 }
	set_variable = { Neutral_Green_pop = 0.002 }
	set_variable = { Nat_Populism_pop = 0.153 }
	
	recalculate_party = yes
	
	#Easter Egg - famous retired al-qaeda commander living in norway
	create_country_leader = {
		name = "Mullah Krekar"
		desc = "POLITICS_JOHAN_NYGAARDSVOLD_DESC"
		picture = "Salafist_Mullah_Krekar.dds"
		expire = "2028.1.1"
		ideology = Caliphate
		traits = {
			cleric
			salafist_Caliphate
			pro_al_qaeda
			capable
			stubborn
		}
	}
	
	create_country_leader = {
		name = "Ingrid Alexandra I"
		picture = "Ingrid_Alexandra_I.dds"
		expire = "2065.1.1"
		ideology = Monarchist
		traits = {
		}
	}
	
	create_country_leader = {
		name = "Haakon Magnus I"
		picture = "Haakon_Magnus_I.dds"
		expire = "2035.1.1"
		ideology = Monarchist
		traits = {
			king
		}
	}
	
	create_country_leader = {
		name = "Harald V"
		picture = "Harald_V.dds"
		expire = "2021.1.1"
		ideology = Monarchist
		traits = {
			king
			pro_american
			capable
			likeable
		}
	}
	
	create_country_leader = {
		name = "Carl Ivar Hagen"
		picture = "Carl_I_Hagen.dds"
		expire = "2012.1.1"
		ideology = Nat_Populism
		traits = {
			nationalist_Nat_Populism
			pro_american
		}
	}
	
	create_country_leader = {
		name = "Torstein Dahle"
		picture = "Torstein_Dahle.dds"
		expire = "2015.1.1"
		ideology = anarchist_communism
		traits = {
			emerging_anarchist_communism
			honest
		}
	}
	
	create_country_leader = {
		name = "Jens Stoltenberg"
		picture = "Jens_Stoltenberg.dds"
		expire = "2015.1.1"
		ideology = socialism
		traits = {
			career_politician
			western_socialism
			capable
		}
	}
	create_country_leader = {
		name = "Kristin Halvorsen"
		picture = "Kristin_Halvorsen.dds"
		expire = "2012.1.1"
		ideology = neutral_Social
		traits = {
			neutrality_neutral_Social
			cautious
		}
	}
	create_country_leader = {
		name = "Kjell Magne Bondevik"
		picture = "Kjell_Magne_Bondevik.dds"
		expire = "2012.1.1"
		ideology = Neutral_conservatism
		traits = {
			cleric
			neutrality_Neutral_conservatism
			likeable
			cautious
		}
	}
	

	create_field_marshal = {
		name = "Haakon Bruun-Hanssen"
		picture = "Portrait_Haakon_Bruun-Hanssen.dds"
		traits = { old_guard thorough_planner }
		id = 44412
		skill = 4
		attack_skill = 4
		defense_skill = 4
		planning_skill = 4
		logistics_skill = 4
	}

	create_field_marshal = {
		name = "Erik Gustafsson"
		picture = "Portrait_Erik_Gustafsson.dds"
		traits = { logistics_wizard }
		id = 44413
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_field_marshal = {
		name = "Robert Mood"
		picture = "Portrait_Robert_Mood.dds"
		traits = { inspirational_leader }
		id = 44414
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_field_marshal = {
		name = "Harald Sunde"
		picture = "Portrait_Harald_Sunde.dds"
		traits = { old_guard organisational_leader }
		id = 44415
		skill = 4
		attack_skill = 4
		defense_skill = 4
		planning_skill = 4
		logistics_skill = 4
	}

	create_corps_commander = {
		name = "Anders Jernberg"
		picture = "Portrait_Anders_Jernberg.dds"
		traits = { trait_engineer }
		id = 44416
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Arild Dregelid"
		picture = "Portrait_Arild_Dregelid.dds"
		traits = { trickster }
		id = 44417
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Eldar Berli"
		picture = "Portrait_Eldar_Berli.dds"
		traits = { panzer_leader }
		id = 44418
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Inge Kampenes"
		picture = "Portrait_Inge_Kampenes.dds"
		traits = { trickster }
		id = 44419
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Ivar Sakserud"
		picture = "Portrait_Ivar_Sakserud.dds"
		traits = { ranger }
		id = 44420
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Jan Erik Thoresen"
		picture = "Portrait_Jan_Erik_Thoresen.dds"
		traits = { trait_engineer }
		id = 44421
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Jan Frederik Geiner"
		picture = "Portrait_Jan_Frederik_Geiner.dds"
		traits = { urban_assault_specialist }
		id = 44422
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Jan Per Ole Janitz"
		picture = "Portrait_Jan_Per_Ole_Janitz.dds"
		traits = { trait_mountaineer }
		id = 44423
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Jon Ivar Kjellin"
		picture = "Portrait_Jon_Ivar_Kjellin.dds"
		traits = { naval_invader }
		id = 44424
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Kjell Grandhagen"
		picture = "Portrait_Kjell_Grandhagen.dds"
		traits = { trickster }
		id = 44425
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Kurt Pedersen"
		picture = "Portrait_Kurt_Pedersen.dds"
		traits = { trickster }
		id = 44426
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Lars Christian Aamodt"
		picture = "Portrait_Lars_Christian_Aamodt.dds"
		traits = { panzer_leader }
		id = 44427
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Morten Haga Lunde"
		picture = "Portrait_Morten_Haga_Lunde.dds"
		traits = { trait_engineer }
		id = 44428
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Ingvar Seland"
		picture = "Portrait_Ingvar_Seland.dds"
		traits = { urban_assault_specialist }
		id = 44429
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}


	create_corps_commander = {
		name = "Odd-Andreas Søbstad"
		picture = "Portrait_Odd_Andreas_Soebstad.dds"
		traits = { desert_fox hill_fighter }
		id = 44430
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Ole Asbjørn Fauske"
		picture = "Portrait_Ole_Fauske.dds"
		traits = { trait_engineer commando }
		id = 44431
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Per Egil Rygg"
		picture = "Portrait_Per_Egil_Rygg.dds"
		traits = { commando }
		id = 44432
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Petter Jansen"
		picture = "Portrait_Petter_Jansen.dds"
		traits = { trait_engineer }
		id = 44433
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Rolf Erik Bjerk"
		picture = "Portrait_Rolf_Erik_Bjerk.dds"
		traits = { trait_engineer }
		id = 44434
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Tor Rune Jakobsen"
		picture = "Portrait_Tor_Rune_Jakobsen.dds"
		traits = { panzer_leader ranger }
		id = 44435
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Tom Guttormsen"
		picture = "Portrait_Tom_Guttormsen.dds"
		traits = {  }
		id = 44436
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Tonje Skinnarland"
		picture = "Portrait_Tonje_Skinnarland.dds"
		traits = {  }
		id = 44437
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Tor Rune Raabye"
		picture = "Portrait_Tor_Rune_Raabye.dds"
		traits = { urban_assault_specialist ranger }
		id = 44438
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Jan-Erik Haug"
		picture = "Portrait_Jan-Erik_Haug.dds"
		traits = { ranger winter_specialist }
		id = 44439
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Ivar Halset"
		picture = "Portrait_Ivar_Halset.dds"
		traits = { ranger hill_fighter }
		id = 44440
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Aril Brandvik"
		picture = "Portrait_Aril_Brandvik.dds"
		traits = { ranger }
		id = 44441
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Ingrid Gjerde"
		picture = "Portrait_Ingrid_Gjerde.dds"
		traits = { trickster bearer_of_artillery }
		id = 44442
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Ole Johan Skogmo"
		picture = "Portrait_Ole_Johan_Skogmo.dds"
		traits = { urban_assault_specialist }
		id = 44443
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Roy Helge Olsen"
		picture = "Portrait_Roy_Helge_Olsen.dds"
		traits = { ranger }
		id = 44444
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Svein Harald Halvorsen"
		picture = "Portrait_Svein_Harald_Halvorsen.dds"
		traits = {  }
		id = 44445
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_navy_leader = {
		name = "Jan Knudtzon Sommerfelt-Petersen"
		picture = "Portrait_Jan_Sommerfelt-Pettersen.dds"
		traits = { blockade_runner }
		id = 44446
	}

	create_navy_leader = {
		name = "Ketil Olsen"
		picture = "Portrait_Ketil_Olsen.dds"
		traits = { seawolf }
		id = 44447
	}

	create_navy_leader = {
		name = "Per Rostad"
		picture = "Portrait_Per_Rostad.dds"
		traits = { ironside }
		id = 44448
	}

	create_navy_leader = {
		name = "Nils Andreas Stensønes"
		picture = "Portrait_Nils_Andreas_Stensoenes.dds"
		traits = { air_controller }
		id = 44449
	}

	create_navy_leader = {
		name = "Henning Amundsen"
		picture = "Portrait_Henning_Amundsen.dds"
		traits = { ironside }
		id = 44450
	}

	create_navy_leader = {
		name = "Håkon Tronstad"
		picture = "Portrait_Hakon_Tronstad.dds"
		traits = { spotter }
		id = 44451
	}

	create_navy_leader = {
		name = "Trond Grytting"
		picture = "Portrait_Trond_Grytting.dds"
		traits = { fly_swatter }
		id = 44452
	}

	create_navy_leader = {
		name = "Bernt Grimstvedt"
		picture = "Portrait_Bernt_Grimstvedt.dds"
		traits = { old_guard_navy ironside }
		id = 44453
	}

	create_navy_leader = {
		name = "Sverre Nordahl Engeness"
		picture = "Portrait_Sverre_Nordahl_Engeness.dds"
		traits = { blockade_runner }
		id = 44454
	}

}

2017.1.1 = {
	capital = 32
	oob = "NOR_2017"
	
	remove_ideas = {
		gdp_7
		stagnation
		defence_02
		edu_05
		health_05
		social_06
		bureau_04
		police_02
		rentier_state
		export_economy
		volunteer_army
		drafted_women
		labour_unions
		fossil_fuel_industry
		maritime_industry
		intervention_limited_interventionism
		tax_cost_44
	}
	
	add_ideas = {
		pop_050
		christian
		slight_corruption
		gdp_9
		stagnation
		defence_02
		edu_05
		health_05
		social_06
		bureau_04
		police_02
		rentier_state
		export_economy
		volunteer_army
		drafted_women
		labour_unions
		fossil_fuel_industry
		maritime_industry
		intervention_limited_interventionism
		NATO_member
		western_country
		medium_far_right_movement
		civil_law
		tax_cost_36
	}
	#set_country_flag = gdp_9
	set_country_flag = negative_labour_unions
	set_country_flag = positive_fossil_fuel_industry
	set_country_flag = defence_02
	set_country_flag = edu_05
	set_country_flag = health_05
	set_country_flag = social_06
	set_country_flag = bureau_04
	set_country_flag = police_02
		
	set_variable = { var = debt value = 145 }
	set_variable = { var = int_investments value = 1038 }
	set_variable = { var = treasury value = 60 }
	set_variable = { var = tax_rate value = 36 }
	
	set_variable = { var = size_modifier value = 0.74 } #6 CIC
	initial_money_setup = yes
	
	set_technology = { 
		
		night_vision_3 = 1 #1985
		
		command_control_equipment3 = 1 #2005
		command_control_equipment4 = 1 #2015
		
		land_Drone_equipment = 1
		land_Drone_equipment1 = 1
		
		infantry_weapons1 = 1
		infantry_weapons2 = 1

	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 57
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 2.5
			}
			
			neutrality = {
				popularity = 24.2
			}
			
			nationalist = {
				popularity = 15.4
			}
		}
		
		ruling_party = democratic
		last_election = "2013.9.9"
		election_frequency = 48
		elections_allowed = yes
	}
	
	set_variable = { conservatism_pop = 0.25 }
	set_variable = { liberalism_pop = 0.046 }
	set_variable = { socialism_pop = 0.274 }
	set_variable = { Communist-State_pop = 0.001 }
	set_variable = { anarchist_communism_pop = 0.024 }
	set_variable = { Neutral_conservatism_pop = 0.045 }
	set_variable = { oligarchism_pop = 0.103 }
	set_variable = { neutral_Social_pop = 0.06 }
	set_variable = { Neutral_Green_pop = 0.032 }
	set_variable = { Neutral_Libertarian_pop = 0.002 }
	set_variable = { Nat_Fascism_pop = 0.002 }
	set_variable = { Nat_Populism_pop = 0.152 }
	
	coalition_government_set_up = yes

	#Nat focus
		complete_national_focus = bonus_tech_slots
		complete_national_focus = Generic_4K_GDPC_slot
		complete_national_focus = Generic_15K_GDPC_slot
		complete_national_focus = Generic_30K_GDPC_slot
		complete_national_focus = Generic_4_IC_slot
		complete_national_focus = Generic_8_IC_slot

	create_country_leader = {
		name = "Inga Marte Thorkildsen"
		picture = "Inga_Marte_Thorkildsen.dds"
		expire = "2055.1.1"
		ideology = neutral_Social
		traits = {
			neutrality_neutral_Social
		}
	}
	#Leader of norwegian salafists. Pledges allegiance to ISIS
	create_country_leader = {
		name = "Ubaydullah Hussain"
		picture = "Salafist_Ubaydullah_Hussain.dds"
		expire = "2065.1.1"
		ideology = Caliphate
		traits = {
			guerrilla_leader
			salafist_Caliphate
			pro_isis
			inexperienced
			sly
			tech_savy
		}
	}

	#Fascist-like far right anti-islamist, known as Fjordman
	create_country_leader = {
		name = "Peder Nøstvold Jensen"
		picture = "Fascist_Fjordman.dds"
		expire = "2065.1.1"
		ideology = Nat_Fascism
		traits = {
			writer
			nationalist_Nat_Fascism
			inexperienced
			islamophobe
			ruthless
			emotional
		}
	}

	#Far Right Populist Candidate
	create_country_leader = {
		name = "Christian Tybring Gjedde"
		picture = "nationalist_Christian_Tybring_Gjedde.dds"
		expire = "2065.1.1"
		ideology = Nat_Populism
		traits = {
			military_career
			nationalist_Nat_Populism
			pro_american
			islamophobe
			emotional
			sly
		}
	}
	
	create_country_leader = {
		name = "Sylvi Listhaug"
		picture = "Sylvi_Listhaug.dds"
		expire = "2065.1.1"
		ideology = Nat_Populism
		traits = {
			nationalist_Nat_Populism
			islamophobe
			emotional
		}
	}
	
	create_country_leader = {
		name = "Bjørnar Moxnes"
		picture = "Bjoernar_Moxnes.dds"
		expire = "2065.1.1"
		ideology = anarchist_communism
		traits = {
			emerging_anarchist_communism
			likeable
			honest
		}
	}

	create_country_leader = {
		name = "Lan Marie Nguyen Berg"
		picture = "Lan_Marie_Nguyen_Berg.dds"
		expire = "2065.1.1"
		ideology = Neutral_green
		traits = {
			scientist
			neutrality_Neutral_green
			likeable
			humble
			capable
		}
	}

	#Liberalist
	create_country_leader = {
		name = "Sveinung Rotevatn"
		picture = "Liberal_Sveinung_Rotevatn.dds"
		expire = "2065.1.1"
		ideology = liberalism
		traits = {
			lawyer
			western_liberalism
			polished
			capable
			tech_savy
			
		}
	}
	#Likely future candidate for the social democrats
	create_country_leader = {
		name = "Hadia Tajik"
		picture = "Labor_Hadia_Tajik.dds"
		expire = "2065.1.1"
		ideology = socialism
		traits = {
			writer
			western_socialism
			likeable
			humble
			capable
		}
	}
	#Likely future candidate for conservatives
	create_country_leader = {
		name = "Ine Marie Eriksen Søreide"
		picture = "Cons_Ine_Marie_Eriksen_Soereide.dds"
		expire = "2065.1.1"
		ideology = conservatism
		traits = {
			lawyer
			western_conservatism
			polished
			rational
		}
	}
	create_country_leader = {
		name = "Jonas Gahr Støre"
		picture = "Labor_Jonas_Gahr_Stoere.dds"
		expire = "2065.1.1"
		ideology = socialism
		traits = {
			career_politician
			western_socialism
			polished
			cautious
			capable
		}
	}

	create_country_leader = {
		name = "Erna Solberg"
		picture = "Cons_Erna_Solberg.dds"
		expire = "2050.1.1"
		ideology = conservatism
		traits = {
			career_politician
			western_conservatism
			likeable
			rational
			capable
		}
	}
	
		create_corps_commander = {
		name = "Anne Rydning"
		picture = "gen_Anne_Rydning.dds"
		traits = {  trait_mountaineer winter_specialist }
		id = 44400
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Kristin Lund"
		picture = "gen_Kristin_Lund.dds"
		traits = { desert_fox hill_fighter }
		id = 44401
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Benedicte Haslestad"
		picture = "gen_Benedicte_Haslestad.dds"
		traits = { winter_specialist }
		id = 44402
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Odin Johannessen"
		picture = "Gen_odin_johannessen.dds"
		traits = {  panzer_leader ranger winter_specialist }
		id = 44403
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Rune Jakobsen"
		picture = "Gen_Rune_Jakobsen.dds"
		traits = {  hill_fighter winter_specialist }
		id = 44404
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Yngve Odlo"
		picture = "Gen_yngve_odlo.dds"
		traits = {  trait_engineer winter_specialist }
		id = 44405
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Trond Haande"
		picture = "Gen_Trond_Haande.dds"
		traits = {  panzer_leader hill_fighter ranger winter_specialist }
		id = 44406
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Frode Kristoffersen"
		picture = "Gen_Frode_Kristoffersen.dds"
		traits = {  naval_invader commando winter_specialist }
		id = 44407
		skill = 4
		attack_skill = 4
		defense_skill = 4
		planning_skill = 4
		logistics_skill = 4
	}


	create_navy_leader = {
		name = "Lars Saunes"
		picture = "admiral_lars_saunes.dds"
		traits = { spotter }
		id = 44408
	}

	create_navy_leader = {
		name = "Louise Kathrine Dedichen"
		picture = "Adm_Louise_Kathrine_Dedichen.dds"
		traits = { superior_tactician }
		id = 44409
	}

	create_navy_leader = {
		name = "Nils Johan Holte"
		picture = "adm_nils_johan_holte.dds"
		traits = { fly_swatter }
		id = 44410
	}

	create_navy_leader = {
		name = "Solveig Krey"
		picture = "Adm_solveig_krey.dds"
		traits = { seawolf }
		id = 44411
	}
	
}