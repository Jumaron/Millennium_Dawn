﻿2000.1.1 = {
	capital = 468
	oob = "CHL_2000"
	set_convoys = 5
	
	add_ideas = {
		gdp_6
		pop_050
		christian
		stagnation
		defence_02
		edu_03
		health_03
		social_03
		bureau_02
		police_02
		partial_draft_army
		volunteer_women
		industrial_conglomerates
		international_bankers
		landowners
		civil_law
		cartels_2
		tax_cost_16
	}
	set_country_flag = positive_industrial_conglomerates
	set_country_flag = positive_international_bankers
	set_country_flag = positive_landowners

	set_variable = { var = debt value = 15 }
	set_variable = { var = treasury value = 22 }
	set_variable = { var = tax_rate value = 16 }
	set_variable = { var = int_investments value = 0 }
	
	set_technology = { 
		
		#For templates
		infantry_weapons = 1
		infantry_weapons2 = 1
		combat_eng_equipment = 1
		command_control_equipment = 1
		Anti_tank_0 = 1	
		Heavy_Anti_tank_0 = 1	
		artillery_0 = 1	
		SP_arty_0 = 1	
		Anti_Air_0 = 1	
		Early_APC = 1	
		APC_1 = 1	
		IFV_1 = 1	
		MBT_1 = 1	
		util_vehicle_0 = 1	

		landing_craft = 1
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 47
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 4
			}
			
			neutrality = {
				popularity = 49
			}
			
			nationalist = {
				 popularity = 11
			}
		}
		
		ruling_party = democratic
		last_election = "1997.1.1"
		election_frequency = 48
		elections_allowed = yes
	}
	
	create_country_leader = {
		name = "Ricardo Lagos"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = ""
		expire = "2065.1.1"
		ideology = neutral_Social
		traits = {
		
		}
	}
	create_country_leader = {
		name = "Joaquín Lavín"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = ""
		expire = "2065.1.1"
		ideology = socialism
		traits = {
		
		}
	}
	create_country_leader = {
		name = "Gladys Marín"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = ""
		expire = "2065.1.1"
		ideology = Communist-State 
		traits = {
		
		}
	}
	create_country_leader = {
		name = "Eduardo Ruiz-Tagle"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "eduardo_ruiz_tagle.dds"
		expire = "2065.1.1"
		ideology = conservatism
		traits = {
		
		}
	}
	
	create_corps_commander = {
		name = "Iván González López"
		picture = "generals/Ivan_Lopez.dds"
		id = 12900
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
	create_corps_commander = {
		name = "Guido Montini Gómez"
		picture = "generals/Guido_Gomez.dds"
		id = 12901
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
	create_field_marshal = {
		name = "Humberto Oviedo Arriagada"
		picture = "Portrait_Humberto_Oviedo.dds"
		traits = { old_guard organisational_leader }
		id = 12902
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_field_marshal = {
		name = "Ricardo Martínez Menanteau"
		picture = "Portrait_Ricardo_Martinez.dds"
		traits = { thorough_planner }
		id = 12903
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_field_marshal = {
		name = "Jorge Robles Mella"
		picture = "Portrait_Jorge_Robles_Mella.dds"
		traits = { logistics_wizard }
		id = 12904
		skill = 4
		attack_skill = 4
		defense_skill = 4
		planning_skill = 4
		logistics_skill = 4
	}

	create_corps_commander = {
		name = "Ángelo Hernández"
		picture = "Portrait_Hernandez.dds"
		traits = { commando }
		id = 12905
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Ramón Oyarzún"
		picture = "Portrait_Oyarzun.dds"
		traits = { commando }
		id = 12906
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Rodrigo Urrutia Oyarzún"
		picture = "Portrait_Rodrigo_Urrutia.dds"
		traits = { ranger }
		id = 12907
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Rodrigo Goicochea"
		picture = "Portrait_Rodrigo_Goicochea.dds"
		traits = { commando }
		id = 12908
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Javier Abarzúa"
		picture = "Portrait_Abarzua.dds"
		traits = { commando }
		id = 12909
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Germán Moreno"
		picture = "Portrait_Moreno.dds"
		traits = { commando }
		id = 12910
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Luis Farías Gallardo"
		picture = "Portrait_Luis_Farias.dds"
		traits = { trickster }
		id = 12911
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Luis Chamorro Heilig"
		picture = "Portrait_Luis_Heilig.dds"
		traits = { commando }
		id = 12912
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Felipe Arancibia Clavel"
		picture = "Portrait_Felipe_Clavel.dds"
		traits = { trait_engineer }
		id = 12913
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Luis Espinoza Arenas"
		picture = "Portrait_Luis_Arenas.dds"
		traits = { panzer_leader }
		id = 12914
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Jorge Peña Leiva"
		picture = "Portrait_Jorge_Pena.dds"
		traits = { jungle_rat }
		id = 12915
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Schafik Nazal Lázaro"
		picture = "Portrait_Schafik_Lazaro.dds"
		traits = { trickster }
		id = 12916
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Miguel Alfonso Bellet"
		picture = "Portrait_Miguel_Alfonso_Bellet.dds"
		traits = { panzer_leader }
		id = 12917
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Andrés Fuentealba Gómez"
		picture = "Portrait_Andres_Fuentealba.dds"
		traits = { fortress_buster }
		id = 12918
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Hernán Araya Santis"
		picture = "Portrait_Hernan_Araya.dds"
		traits = { trickster }
		id = 12919
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Javier Iturriaga Del Campo"
		picture = "Portrait_Javier_Iturriaga.dds"
		traits = { urban_assault_specialist }
		id = 12920
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Sergio Retamal"
		picture = "Portrait_Sergio_Retamal.dds"
		traits = { hill_fighter }
		id = 12921
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Ernesto Tejos Méndez"
		picture = "Portrait_Ernesto_Tejos.dds"
		traits = { trait_mountaineer }
		id = 12922
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Fernando San Cristóbal Schott"
		picture = "Portrait_Fernando_San_Cristobal.dds"
		traits = { ranger }
		id = 12923
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Gustavo Núñez Kocher"
		picture = "Portrait_Gustavo_Nunez.dds"
		traits = { commando }
		id = 12924
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_navy_leader = {
		name = "Enrique Larrañaga Martin"
		picture = "Portrait_Enrique_Martin.dds"
		traits = { old_guard_navy superior_tactician }
		id = 12925
	}

	create_navy_leader = {
		name = "Edmundo González Robles"
		picture = "Portrait_Edmundo_Gonzalez.dds"
		traits = { blockade_runner }
		id = 12926
	}
}

2017.1.1 = {
	capital = 468
	oob = "CHL_2017"
	set_convoys = 5
	
	add_ideas = {
		pop_050
		modest_corruption
		gdp_6
		rio_pact_member
		christian
		stagnation
		defence_02
		export_economy
		edu_03
		health_03
		social_03
		bureau_02
		police_02
		partial_draft_army
		volunteer_women
		industrial_conglomerates
		international_bankers
		landowners
		civil_law
		cartels_2
		tax_cost_18
	}
	
	set_country_flag = TPP_Signatory
	set_country_flag = positive_industrial_conglomerates
	set_country_flag = positive_international_bankers
	set_country_flag = positive_landowners
	
	set_variable = { var = debt value = 65 }
	set_variable = { var = treasury value = 40 }
	set_variable = { var = int_investments value = 24.1 }
	set_variable = { var = tax_rate value = 18 }
	
	set_variable = { var = size_modifier value = 1.40 } #9 CIC
	initial_money_setup = yes

	# Starting tech
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		night_vision_1 = 1
		night_vision_2 = 1
		night_vision_3 = 1
		
		#For templates
		infantry_weapons = 1
		combat_eng_equipment = 1
		command_control_equipment = 1
		Anti_tank_0 = 1	
		Heavy_Anti_tank_0 = 1	
		artillery_0 = 1	
		SP_arty_0 = 1	
		Anti_Air_0 = 1	
		Early_APC = 1	
		APC_1 = 1	
		IFV_1 = 1	
		MBT_1 = 1	
		util_vehicle_0 = 1	

		landing_craft = 1
	}

	#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_15K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot
	complete_national_focus = Generic_16_IC_slot

	set_politics = {

		parties = {
			democratic = { 
				popularity = 40
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 10
				#banned = no #default is no
			}
			
			neutrality = { 
				popularity = 50
			}
		}
		
		ruling_party = democratic
		last_election = "2014.3.11"
		election_frequency = 48
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Michelle Bachelet"
		desc = "POLITICS_ARTURO_ALESSANDRI_DESC"
		picture = "CHL_Michelle_Bachelet.dds"
		expire = "2050.1.1"
		ideology = conservatism
		traits = {
			#
		}
	}
	
}