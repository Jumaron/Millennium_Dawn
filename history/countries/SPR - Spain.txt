﻿2000.1.1 = {
	capital = 91
	oob = "SPR_2000"
	set_convoys = 150
	
	add_ideas = {
		gdp_7
		pop_050
		christian
		EU_member
		defence_01
		edu_03
		health_04
		social_06
		bureau_03
		police_04
		volunteer_army
		volunteer_women
		NATO_member
		western_country
		small_medium_business_owners
		labour_unions
		landowners
		civil_law
		tax_cost_34
		multi_ethnic_state_idea
	}
	
	set_country_flag = positive_small_medium_business_owners
	set_country_flag = positive_landowners
	
	set_variable = { var = debt value = 506 }
	set_variable = { var = treasury value = 52 }
	set_variable = { var = tax_rate value = 20 }
	set_variable = { var = int_investments value = 0 }
	
	#NATO military access
	diplomatic_relation = {
		country = BEL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CAN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CZH
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = DEN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = FRA
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = GER
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = GRE
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = HUN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ICE
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ITA
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = LUX
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = HOL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = NOR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = POL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = POR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = TUR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ENG
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = USA
		relation = military_access
		active = yes
	}
	
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		
		modern_carrier_0 = 1
		modern_carrier_1 = 1
		modern_carrier_2 = 1
		
		corvette_1 = 1
		corvette_2 = 1
		
		LPD_0 = 1
		LPD_1 = 1
		
		frigate_1 = 1
		frigate_2 = 1
		
		submarine_1 = 1
		diesel_attack_submarine_1 = 1
		diesel_attack_submarine_2 = 1
		
		infantry_weapons = 1
		infantry_weapons1 = 1
		infantry_weapons2 = 1
		infantry_weapons3 = 1
		
		command_control_equipment = 1
		command_control_equipment1 = 1
		command_control_equipment2 = 1
		
		land_Drone_equipment = 1
		land_Drone_equipment1 = 1
		land_Drone_equipment2 = 1
		
		Anti_tank_0 = 1
		Anti_tank_1 = 1
		AT_upgrade_1 = 1
		
		Heavy_Anti_tank_0 = 1
		Heavy_Anti_tank_1 = 1
		
		Anti_Air_0 = 1
		Anti_Air_1 = 1
		AA_upgrade_1 = 1
		
		combat_eng_equipment = 1
		
		Early_APC = 1
		MBT_1 = 1
		MBT_2 = 1
		
		ENGI_MBT_1 = 1
		ENGI_MBT_2 = 1
		
		util_vehicle_0 = 1
		util_vehicle_1 = 1
		
		Rec_tank_0 = 1
		Rec_tank_1 = 1
		
		IFV_1 = 1
		IFV_2 = 1
		IFV_3 = 1
		IFV_4 = 1
		IFV_5 = 1
		
		APC_1 = 1
		APC_2 = 1
		
		Air_APC_1 = 1
		Air_APC_2 = 1
	
		SP_arty_0 = 1
		
		artillery_0 = 1
		artillery_1 = 1
		Arty_upgrade_1 = 1
		artillery_2 = 1 #SBT 155/52 SIAC
		
		SP_Anti_Air_0 = 1
		SP_Anti_Air_1 = 1
		
		early_helicopter = 1
		transport_helicopter1 = 1
		attack_helicopter1 = 1
		
		early_fighter = 1
		MR_Fighter1 = 1
		
		AS_Fighter1 = 1
		AS_Fighter2 = 1
		AS_upgrade_1 = 1
		AS_Fighter3 = 1 #4.5gen
		
		MR_Fighter1 = 1
		MR_Fighter2 = 1
		MR_upgrade_1 = 1
		MR_Fighter3 = 1
		
		L_Strike_fighter1 = 1
		
		Strike_fighter1 = 1
		
		early_bomber = 1
		naval_plane1 = 1
		
		transport_plane1 = 1
		transport_plane2 = 1
		transport_plane3 = 1
		
		landing_craft = 1
		
		night_vision_1 = 1
		night_vision_2 = 1
		night_vision_3 = 1
		
		
	}
	
	set_politics = {
		parties = {
			democratic = { 
				popularity = 66
			}

			fascism = {
				popularity = 0.5
			}
			
			communism = {
				popularity = 20
			}
			
			neutrality = {
				popularity = 10
			}
			
			nationalist = {
				popularity = 4.5
			}
		}
		
		ruling_party = democratic
		last_election = "1997.1.1"
		election_frequency = 48
		elections_allowed = yes
		
	}

	create_country_leader = {
		name = "José María Aznar"
		picture = "Jose_Maria_Aznar.dds"
		ideology = conservatism
	}
	
	create_corps_commander = {
		name = "Jaime Domínguez Buj"
		picture = "generals/Jaime_D_Buj.dds"
		id = 58800
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}
	create_corps_commander = {
		name = "José Julio Rodríguez Fernández"
		picture = "generals/Jose_JR_Fernandez.dds"
		id = 58801
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
	create_corps_commander = {
		name = "Alfredo Ramirez"
		picture = "generals/Alfredo_Ramirez.dds"
		id = 58802
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
	create_corps_commander = {
		name = "Bernardo Alvarez"
		picture = "generals/Bernardo_Alvarez.dds"
		id = 58803
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
	create_corps_commander = {
		name = "Fernando Gutierrez"
		picture = "generals/Fernando_Gutierrez.dds"
		id = 58804
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
	create_corps_commander = {
		name = "Miguel Alcañiz"
		picture = "generals/Miguel_Alcaniz.dds"
		id = 58805
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
	create_corps_commander = {
		name = "Pedro Pitarch"
		picture = "generals/Pedro_Pitarch.dds"
		id = 58806
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
	create_corps_commander = {
		name = "Santiago Camarero"
		picture = "generals/Santiago_Camarero.dds"
		id = 58807
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_navy_leader = {
		name = "Jaime Muñoz"
		picture = "admirals/Jaime_Munoz.dds"
		id = 58808
	}
	create_navy_leader = {
		name = "Jose Antonio Ruesta"
		picture = "admirals/Jose_A_Ruesta.dds"
		id = 58809
	}
	
	create_equipment_variant = {
		name = "Príncipe de Asturias"
		type = carrier_2
		upgrades = {
			
		}
	}
}

2017.1.1 = {
	capital = 91
	oob = "SPR_2017"
	set_convoys = 150

	add_ideas = {
		pop_050
		widespread_corruption
		gdp_8
		christian
		EU_member
		stable_growth
		defence_01
		edu_03
		health_04
		social_06
		bureau_03
		police_04
		volunteer_army
		volunteer_women
		intervention_limited_interventionism
		NATO_member
		western_country
		medium_far_right_movement
		small_medium_business_owners
		labour_unions
		landowners
		civil_law
		tax_cost_34
		multi_ethnic_state_idea
	}
	
	#set_country_flag = gdp_8
	set_country_flag = positive_small_medium_business_owners
	set_country_flag = positive_landowners
	
	set_variable = { var = debt value = 1293 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = treasury value = 63 }
	set_variable = { var = tax_rate value = 34 }
	
	set_variable = { var = size_modifier value = 4.92 } #25 CIC
	initial_money_setup = yes

	#NATO military access
	diplomatic_relation = {
		country = ALB
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = BEL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = BUL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CAN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CRO
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CZH
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = DEN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = EST
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = FRA
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = GER
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = GRE
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = HUN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ICE
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ITA
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = LAT
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = LIT
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = LUX
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = HOL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = NOR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = POL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = POR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ROM
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = SLO
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = SLV
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = TUR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ENG
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = USA
		relation = military_access
		active = yes
	}

	#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_15K_GDPC_slot
	complete_national_focus = Generic_30K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot
	complete_national_focus = Generic_16_IC_slot
	complete_national_focus = Generic_30_IC_slot
	
	set_technology = { 
		missile_frigate_1 = 1
		missile_frigate_2 = 1
		missile_frigate_3 = 1
		
		
		MBT_3 = 1
		MBT_4 = 1
		
		ENGI_MBT_3 = 1
		ENGI_MBT_4 = 1 #Centauro REC
		
		util_vehicle_2 = 1
		util_vehicle_3 = 1
		
		Rec_tank_2 = 1
		
		APC_3 = 1
		APC_4 = 1
		
		attack_helicopter2 = 1
		attack_helicopter3 = 1
		
		naval_plane2 = 1
		naval_plane3 = 1
		
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 75
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 20
			}
			
			neutrality = {
				popularity = 0
			}
			
			nationalist = {
				popularity = 5
			}
		}
		
		ruling_party = democratic
		last_election = "2016.6.26"
		election_frequency = 48
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Cristina Cifuentes"
		desc = "POLITICS_EARL_BROWDER_DESC"
		picture = "cristina_cifuentes.dds"
		expire = "2055.1.1"
		ideology = conservatism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Pablo Casado"
		desc = "POLITICS_WILLIAM_DUDLEY_PELLEY_DESC"
		picture = "pablo_casado.dds"
		expire = "2060.1.1"
		ideology = conservatism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Albert Rivera"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "albert_rivera.dds"
		expire = "2055.1.1"
		ideology = liberalism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Ines Arrimadas"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "ines_arrimadas.dds"
		expire = "2060.1.1"
		ideology = liberalism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Pedro Sanchez"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "pedro_sanchez.dds"
		expire = "2050.1.1"
		ideology = socialism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Susana Diaz"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "susana_diaz.dds"
		expire = "2060.1.1"
		ideology = socialism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Pablo Iglesias"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "pablo_iglesias.dds"
		expire = "2055.1.1"
		ideology = Communist-State
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Inigo Errejon"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "inigo_errejon.dds"
		expire = "2060.1.1"
		ideology = Communist-State
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Silvia Barquero Nogales"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "silvia_barquero.dds"
		expire = "2060.1.1"
		ideology = Neutral_Libertarian
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Norberto Pico"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "norberto_pico.dds"
		expire = "2065.1.1"
		ideology = Nat_Fascism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Mariano Rajoy"
		desc = "POLITICS_AGUSTIN_PEDRO_JUSTO_DESC"
		picture = "Mariano_Rajoy.dds"
		expire = "2065.1.1"
		ideology = conservatism
		traits = {
			#
		}
	}

	create_equipment_variant = {
		name = "Alvaro de Bazan-class"
		type = missile_frigate_3
		upgrades = {
			frigate_vls_upgrade = 5
			frigate_asw_upgrade = 0
			frigate_cm_upgrade = 0
			frigate_radar_upgrade = 5
			frigate_stealth_upgrade = 0
		}
	}
	
	create_equipment_variant = {
		name = "Fridtjof Nansen-Class"
		type = missile_frigate_3
		upgrades = {
			
		}
	}

}