﻿2000.1.1 = {
	capital = 7
	oob = "ICE_2000"
	set_convoys = 20
	
	add_ideas = {
		gdp_8
		pop_050
		pluralist
		defence_00
		edu_05
		health_05
		social_04
		bureau_03
		police_02
		small_medium_business_owners
		maritime_industry
		labour_unions
		NATO_member
		western_country
		civil_law
		tax_cost_25
	}

	set_variable = { var = debt value = 1 }
	set_variable = { var = treasury value = 5 }
	set_variable = { var = tax_rate value = 25 }
	set_variable = { var = int_investments value = 0 }
	
	#NATO military access
	diplomatic_relation = {
		country = BEL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CAN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CZH
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = DEN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = FRA
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = GER
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = GRE
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = HUN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ITA
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = LUX
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = HOL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = NOR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = POL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = POR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = SPR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = TUR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ENG
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = USA
		relation = military_access
		active = yes
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 71.7
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 0.1
			}
			
			neutrality = {
				popularity = 27.6
			}
			
			nationalist = {
				popularity = 0.1
			}
		}
		
		ruling_party = democratic
		last_election = "1997.5.8"
		election_frequency = 48
		elections_allowed = yes
	}
	
	set_variable = { conservatism_pop = 0.407 }
	set_variable = { liberalism_pop = 0.042 }
	set_variable = { socialism_pop = 0.268 }
	set_variable = { Communist-State_pop = 0.001 }
	set_variable = { oligarchism_pop = 0.184 }
	set_variable = { neutral_Social_pop = 0.091 }
	set_variable = { Neutral_Libertarian_pop = 0.001 }
	set_variable = { Nat_Populism_pop = 0.001 }
	
	recalculate_party = yes
	
	create_country_leader = {
		name = "Davíð Oddsson"
		ideology = conservatism
		picture = "David_Oddsson.dds"
	}
	
	create_field_marshal = {
		name = "Arnor Sigurjonsson"
		picture = "Portrait_Arnor_Sigurjonsson.dds"
		traits = { old_guard fast_planner }
		id = 29100
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Sindri Steingrimsson"
		picture = "Portrait_Sindri_Steingrimsson.dds"
		traits = { trait_engineer }
		id = 29101
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Halli Sigurdsson"
		picture = "Portrait_Halli_Sigurdsson.dds"
		traits = { commando }
		id = 29102
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Haraldur Johannessen"
		picture = "Portrait_Haraldur_Johannessen.dds"
		traits = { urban_assault_specialist }
		id = 29103
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_navy_leader = {
		name = "Georg Kristinn Larusson"
		picture = "Portrait_Georg_Kristinn_Larusson.dds"
		traits = { old_guard_navy superior_tactician }
		id = 29104
	}

	create_navy_leader = {
		name = "Asgrimur Asgrimsson"
		picture = "Portrait_Asgrimur_Asgrimsson.dds"
		traits = { blockade_runner }
		id = 29105
	}
}

2017.1.1 = {
	capital = 7
	oob = "ICE_2017"
	
	add_ideas = {
		pop_050
		slight_corruption
		gdp_9
		pluralist
		economic_boom
		defence_00
		edu_05
		health_05
		social_04
		bureau_03
		police_02
		no_military
		volunteer_women
		small_medium_business_owners
		maritime_industry
		labour_unions
		NATO_member
		western_country
		medium_far_right_movement
		large_libertarian_movement
		large_green_movement
		civil_law
		tax_cost_37
	}
	
	set_variable = { var = debt value = 10 }
	set_variable = { var = treasury value = 7 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = tax_rate value = 37 }
	
	set_variable = { var = size_modifier value = 0.08 } #1 CIC
	initial_money_setup = yes

	#NATO military access
	diplomatic_relation = {
		country = ALB
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = BEL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = BUL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CAN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CRO
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CZH
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = DEN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = EST
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = FRA
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = GER
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = GRE
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = HUN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ITA
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = LAT
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = LIT
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = LUX
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = HOL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = NOR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = POL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = POR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ROM
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = SLO
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = SLV
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = SPR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = TUR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ENG
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = USA
		relation = military_access
		active = yes
	}

	#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_15K_GDPC_slot
	complete_national_focus = Generic_30K_GDPC_slot

	add_opinion_modifier = { target = SWE modifier = nordic }
	add_opinion_modifier = { target = ICE modifier = nordic }
	add_opinion_modifier = { target = NOR modifier = nordic }
	add_opinion_modifier = { target = FIN modifier = nordic }
	
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		#No army, but let's give them some basic tech in case they change their mind
		infantry_weapons = 1
		command_control_equipment = 1
		Anti_tank_0 = 1
		Anti_Air_0 = 1
		util_vehicle_0 = 1
		
		night_vision_1 = 1
		night_vision_2 = 1
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 48.4
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 0.3
			}
			
			neutrality = {
				popularity = 47.6
			}
			
			nationalist = {
				popularity = 5.7
			}
		}
		
		ruling_party = democratic
		last_election = "2016.10.29"
		election_frequency = 36
		elections_allowed = yes
	}
	
	set_variable = { conservatism_pop = 0.29 }
	set_variable = { liberalism_pop = 0.104 }
	set_variable = { socialism_pop = 0.09 }
	set_variable = { Communist-State_pop = 0.003 }
	set_variable = { oligarchism_pop = 0.147 }
	set_variable = { neutral_Social_pop = 0.184 }
	set_variable = { Neutral_Libertarian_pop = 0.145 }
	set_variable = { Nat_Populism_pop = 0.055 }
	set_variable = { Nat_Fascism_pop = 0.002 }
	
	recalculate_party = yes
	
	create_country_leader = {
		name = "Sigurdur Ingi Johannsson"
		desc = ""
		picture = "Western_progressive_Sigurdur_Ingi_Johannsson.dds"
		expire = "2050.1.1"
		ideology = socialism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Margret Tryggvadottir"
		picture = "margret_tryggvadottir.dds"
		expire = "2065.1.1"
		ideology = Nat_populism
		traits = {
		
		}
	}

	create_country_leader = {
		name = "Arni Pall Arnason"
		desc = ""
		picture = "Western_Soc_dem_Arni-pall-arnason.dds"
		expire = "2050.1.1"
		ideology = liberalism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Birgitta Jonsdottir"
		desc = ""
		picture = "Neutral_Libertarian_Birgitta_Jonsdottir.dds"
		expire = "2050.1.1"
		ideology = Neutral_Libertarian
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Katrín Jakobsdóttir"
		picture = "Katrin_Jakobsdottir.dds"
		expire = "2055.1.1"
		ideology = socialism
		traits = {
			writer
			western_socialism
			likeable
			humble
			capable
		}
	}

	create_country_leader = {
		name = "Bjarni Benediktsson"
		picture = "Bjarni_Benediktsson.dds"
		expire = "2050.1.1"
		ideology = conservatism
		traits = {
			#
		}
	}
	

}