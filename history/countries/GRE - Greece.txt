﻿2000.1.1 = {
	capital = 141
	oob = "GRE_2000"
	set_convoys = 150
	
	add_ideas = {
		gdp_7
		pop_050
		orthodox_christian
		EU_member
		defence_03
		edu_03
		health_03
		social_05
		bureau_03
		police_04
		international_bankers
		labour_unions
		the_military
		NATO_member
		western_country
		civil_law
		tax_cost_22
	}

	set_country_flag = hostile_international_bankers
	set_country_flag = hostile_labour_unions

	set_variable = { var = debt value = 202 }
	set_variable = { var = treasury value = 21 }
	set_variable = { var = tax_rate value = 22 }
	set_variable = { var = int_investments value = 0 }
	
	#NATO military access
	diplomatic_relation = {
		country = BEL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CAN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CZH
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = DEN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = FRA
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = GER
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = HUN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ICE
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ITA
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = LUX
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = HOL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = NOR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = POL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = POR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = SPR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = TUR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ENG
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = USA
		relation = military_access
		active = yes
	}
	
	set_technology = { 
		#EAS G36
		infantry_weapons = 1

		#ELVO Leonidas-2
		APC_1 = 1
		
		#ELVO Kentaurus
		IFV_1 = 1
		
		night_vision_1 = 1
		
		#HAI Pegasus II
		land_Drone_equipment = 1		
		
		#For templates
		combat_eng_equipment = 1
		command_control_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		artillery_0 = 1
		SP_arty_0 = 1
		SP_R_arty_0 = 1
		Anti_Air_0 = 1
		SP_Anti_Air_0 = 1
		Early_APC = 1
		util_vehicle_0 = 1
		MBT_1 = 1
		ENGI_MBT_1 = 1
		early_helicopter = 1
		transport_helicopter1 = 1
		
		landing_craft = 1
		amphibious_assault_ship = 1
		
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 45
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 11
			}
			
			neutrality = {
				popularity = 44
			}
			
			nationalist = {
				 popularity = 0
			}
		}
		
		ruling_party = democratic
		last_election = "1996.4.9"
		election_frequency = 72
		elections_allowed = yes
	}
	
	create_country_leader = {
		name = "Costas Simitis"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "kostas_simitis.dds"
		expire = "2065.1.1"
		ideology = socialism
		traits = {
		
		}
	}	
	create_country_leader = {
		name = "Kostas Karamanlis"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = ""
		expire = "2065.1.1"
		ideology = Neutral_Libertarian
		traits = {
		
		}
	}
	create_country_leader = {
		name = "Aleka Papariga"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = ""
		expire = "2065.1.1"
		ideology = Communist-State
		traits = {
		
		}
	}
	
	create_field_marshal = {
		name = "Alkiviadis Stefanis"
		picture = "Portrait_Alkiviadis_Stefanis.dds"
		traits = { old_guard organisational_leader }
		id = 24900
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_field_marshal = {
		name = "Ioannis Iliopoulos"
		picture = "Portrait_Ioannis_Iliopoulos.dds"
		traits = { offensive_doctrine }
		id = 24901
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Konstantinos Floros"
		picture = "Portrait_Konstantinos_Floros.dds"
		traits = { naval_invader }
		id = 24902
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Nikolaos Dimitrios Christopoulos"
		picture = "Portrait_Nikolaos_Dimitrios_Christopoulos.dds"
		traits = {  }
		id = 24903
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Vasileios Tellidis"
		picture = "Portrait_Vasileios_Tellidis.dds"
		traits = { trickster }
		id = 24904
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Andreas Iliopoulos"
		picture = "Portrait_Andreas_Iliopoulos.dds"
		traits = {  }
		id = 24905
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Alexandros Oikonomou"
		picture = "Portrait_Alexandros_Oikonomou.dds"
		traits = {  }
		id = 24906
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Nikolaos Manouris"
		picture = "Portrait_Nikolaos_Manouris.dds"
		traits = { trait_engineer }
		id = 24907
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Dimokritos Zervakis"
		picture = "Portrait_Dimokritos_Zervakis.dds"
		traits = { fortress_buster }
		id = 24908
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Konstantinos Vasiliadis"
		picture = "Portrait_Konstantinos_Vasiliadis.dds"
		traits = { panzer_leader }
		id = 24909
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Georgios Kambas"
		picture = "Portrait_Georgios_Kambas.dds"
		traits = { bearer_of_artillery }
		id = 24910
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Antonios Nomikos"
		picture = "Portrait_Antonios_Nomikos.dds"
		traits = { commando }
		id = 24911
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Dimitrios Bikos"
		picture = "Portrait_Dimitrios_Bikos.dds"
		traits = { commando }
		id = 24912
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Nikolaos Manolakos"
		picture = "Portrait_Nikolaos_Manolakos.dds"
		traits = { commando }
		id = 24913
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Menelaos Meimaris"
		picture = "Portrait_Menelaos_Meimaris.dds"
		traits = { fortress_buster }
		id = 24914
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Christos Christodolou"
		picture = "Portrait_Christos_Christodolou.dds"
		traits = {  }
		id = 24915
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_navy_leader = {
		name = "Evangelos Apostolakis"
		picture = "Portrait_Evangelos_Apostolakis.dds"
		traits = { old_guard_navy superior_tactician }
		id = 24916
	}

	create_navy_leader = {
		name = "Nikolaos Tsounis"
		picture = "Portrait_Nikolaos_Tsounis.dds"
		traits = { superior_tactician }
		id = 24917
	}

	create_navy_leader = {
		name = "Ioannis Pavlopoulos"
		picture = "Portrait_Ioannis_Pavlopoulos.dds"
		traits = { blockade_runner }
		id = 24918
	}

	create_navy_leader = {
		name = "Alexandros Diakopoulos"
		picture = "Portrait_Alexandros_Diakopoulos.dds"
		traits = { seawolf }
		id = 24919
	}

	create_navy_leader = {
		name = "Nikolaos Kafetsis"
		picture = "Portrait_Nikolaos_Kafetsis.dds"
		traits = { spotter }
		id = 24920
	}

	create_navy_leader = {
		name = "Ioannis Korakakis"
		picture = "Portrait_Ioannis_Korakakis.dds"
		traits = { fly_swatter }
		id = 24921
	}

	create_navy_leader = {
		name = "Efthymios Mikros"
		picture = "Portrait_Efthymios_Mikros.dds"
		traits = { air_controller }
		id = 24922
	}

	create_navy_leader = {
		name = "Georgios Pelekanakis"
		picture = "Portrait_Georgios_Pelekanakis.dds"
		traits = { ironside }
		id = 24923
	}

	create_navy_leader = {
		name = "Stavros Banos"
		picture = "Portrait_Stavros_Banos.dds"
		traits = { spotter }
		id = 24924
	}

	create_navy_leader = {
		name = "Aristidis Alexopoulos"
		picture = "Portrait_Aristidis_Alexopoulos.dds"
		traits = {  }
		id = 24925
	}

}

2017.1.1 = {
	capital = 141
	oob = "GRE_2017"
	set_convoys = 150

	add_ideas = {
		pop_050
		gdp_7
		orthodox_christian
		EU_member
			stagnation
			defence_03
		edu_03
		health_03
		social_05
		bureau_03
		police_04
		draft_army
		volunteer_women
		international_bankers
		labour_unions
		the_military
		intervention_limited_interventionism
		NATO_member
		western_country
		large_far_right_movement
		civil_law
		tax_cost_40
	}
	
	#set_country_flag = gdp_7
	set_country_flag = Major_Importer_GER_Arms
	set_country_flag = hostile_international_bankers
	set_country_flag = hostile_labour_unions
	
	set_variable = { var = debt value = 365 }
	set_variable = { var = treasury value = 7 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = tax_rate value = 40 }
	
	set_variable = { var = size_modifier value = 0.74 } #6 CIC
	initial_money_setup = yes

	#NATO military access
	diplomatic_relation = {
		country = ALB
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = BEL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = BUL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CAN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CRO
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CZH
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = DEN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = EST
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = FRA
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = GER
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = HUN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ICE
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ITA
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = LAT
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = LIT
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = LUX
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = HOL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = NOR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = POL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = POR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ROM
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = SLO
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = SLV
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = SPR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = TUR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ENG
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = USA
		relation = military_access
		active = yes
	}

	#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_15K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot
	
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		#EAS G36
		infantry_weapons = 1
		infantry_weapons1 = 1
		
		
		
		#ELVO Leonidas-2
		APC_1 = 1
		APC_2 = 1
		APC_3 = 1
		
		#ELVO Kentaurus
		IFV_1 = 1
		IFV_2 = 1
		IFV_3 = 1
		IFV_4 = 1
		
		night_vision_1 = 1
		night_vision_2 = 1
		night_vision_3 = 1
		
		#HAI Pegasus II
		land_Drone_equipment = 1	
		land_Drone_equipment1 = 1	
		land_Drone_equipment2 = 1	
		
		#For templates
		combat_eng_equipment = 1
		command_control_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		artillery_0 = 1
		SP_arty_0 = 1
		SP_R_arty_0 = 1
		Anti_Air_0 = 1
		SP_Anti_Air_0 = 1
		Early_APC = 1
		util_vehicle_0 = 1
		MBT_1 = 1
		ENGI_MBT_1 = 1
		early_helicopter = 1
		transport_helicopter1 = 1
		
		landing_craft = 1
		amphibious_assault_ship = 1
		
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 44
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 37
			}
			
			neutrality = {
				popularity = 3
			}
			
			nationalist = {
				popularity = 16
			}
		}
		
		ruling_party = democratic
		last_election = "2015.9.20"
		election_frequency = 48
		elections_allowed = yes
	}
	
	create_country_leader = {
		name = "Kyriakos Mitsotakis" 
		desc = "POLITICS_EARL_BROWDER_DESC"
		picture = "kyriakos_mitsotakis.dds"
		expire = "2050.1.1"
		ideology = conservatism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Fofi Gennimata"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "fofi_gennimata.dds"
		expire = "2060.1.1"
		ideology = socialism
		traits = {
		
		}
	}

	create_country_leader = {
		name = "Panos Kammenos"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "panos_kammenos.dds"
		expire = "2060.1.1"
		ideology = Neutral_conservatism
		traits = {
		
		}
	}

	create_country_leader = {
		name = "Nikolaos Michaloliakos"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "Fascist_Nikolaos_Michaloliakos.dds"
		expire = "2065.1.1"
		ideology = Nat_Fascism
		traits = {
		
		}
	}

	create_country_leader = {
		name = "Alexis Tsipras"
		desc = "POLITICS_Alexis_Tsipras_DESC"
		picture = "Alexis_Tsipras.dds"
		expire = "2050.1.1"
		ideology = socialism
		traits = {
			#
		}
	}
	
}