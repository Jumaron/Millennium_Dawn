﻿2000.1.1 = {
	capital = 734
	oob = "TIM_2017"
	set_convoys = 15
	
	add_ideas = {
		gdp_2
		multi_ethnic_state_idea
	}
	
	# Starting tech
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		
		infantry_weapons = 1
		infantry_weapons1 = 1
		
		command_control_equipment = 1
		
		land_Drone_equipment = 1
		
		Anti_tank_0 = 1
		
		Heavy_Anti_tank_0 = 1
		
		Anti_Air_0 = 1
		
		combat_eng_equipment = 1

	}
	
	create_field_marshal = {
		name = "Lere Anan Timur"
		picture = "Portrait_Lere_Anan_Timur.dds"
		traits = { organisational_leader }
		id = 64500
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Filomeno Paixao"
		picture = "Portrait_Filomeno_Paixao.dds"
		traits = { ranger }
		id = 64501
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Longuinhos Monteiro"
		picture = "Portrait_Longuinhos_Monteiro.dds"
		traits = { urban_assault_specialist }
		id = 64502
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Falur Rate Laek"
		picture = "Portrait_Falur_Rate_Laek.dds"
		traits = { panzer_leader }
		id = 64503
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Faustino da Costa"
		picture = "Portrait_Faustino_da_Costa.dds"
		traits = { urban_assault_specialist }
		id = 64504
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Joao Freitas"
		picture = "Portrait_Joao_Freitas.dds"
		traits = { naval_invader }
		id = 64505
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Julio Hornay"
		picture = "Portrait_Julio_Hornay.dds"
		traits = { urban_assault_specialist }
		id = 64506
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_navy_leader = {
		name = "Pedro Klamar Fuik"
		picture = "Portrait_Pedro_Klamar_Fuik.dds"
		traits = { blockade_runner }
		id = 64507
	}
}

2017.1.1 = {
	capital = 734
	oob = "TIM_2017"
	set_convoys = 15

	set_variable = { var = debt value = 16 }
	set_variable = { var = treasury value = 0 }
	set_variable = { var = int_investments value = 17 }
	set_variable = { var = tax_rate value = 4 }
	
	set_variable = { var = size_modifier value = 0.08 } #1 CIC
	initial_money_setup = yes

	add_ideas = {
		pop_050
		unrestrained_corruption
		christian
		gdp_2
		rentier_state
		export_economy
		fast_growth
		defence_03
		edu_04
		health_01
		social_02
		bureau_05
		police_03
		volunteer_army
		volunteer_women
		farmers
		fossil_fuel_industry 
		small_medium_business_owners
		hybrid
		tax_cost_04
		multi_ethnic_state_idea
	}
	
	#set_country_flag = gdp_2
	set_country_flag = positive_farmers
	set_country_flag = positive_fossil_fuel_industry 
	set_country_flag = positive_small_medium_business_owners

	#Nat focus
	complete_national_focus = bonus_tech_slots
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 10
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 20
				#banned = no #default is no
			}
			
			neutrality = {
				popularity = 70
			}
		}
		
		ruling_party = neutrality
		last_election = "2012.7.7"
		election_frequency = 60
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Taur Matan Ruak"
		desc = "POLITICS_PHRAYA_PHAHON_DESC"
		picture = "Taur_Matan_Ruak.dds"
		expire = "2050.1.1"
		ideology = neutral_Social
		traits = {
			#
		}
	}

}