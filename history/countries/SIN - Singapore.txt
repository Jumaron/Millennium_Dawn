﻿2000.1.1 = {
	capital = 530
	oob = "SIN_2000"
	set_convoys = 75
	
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		
		infantry_weapons = 1
		infantry_weapons1 = 1
		infantry_weapons2 = 1
		infantry_weapons3 = 1
		
		command_control_equipment = 1
		command_control_equipment1 = 1
		command_control_equipment2 = 1
		
		land_Drone_equipment = 1
		land_Drone_equipment1 = 1
		
		Anti_tank_0 = 1
		
		Heavy_Anti_tank_0 = 1
		
		Anti_Air_0 = 1
		SP_Anti_Air_0 = 1
		
		ENGI_MBT_1 = 1
		
		combat_eng_equipment = 1
		
		Early_APC = 1
		MBT_1 = 1
		
		IFV_1 = 1
		IFV_2 = 1
		IFV_3 = 1
		IFV_4 = 1
		
		APC_1 = 1
		APC_2 = 1
		APC_3 = 1
		APC_4 = 1
		
		Rec_tank_0 = 1
		
		util_vehicle_0 = 1
		util_vehicle_1 = 1
		util_vehicle_2 = 1
		
		SP_arty_0 = 1
		
		artillery_0 = 1
		artillery_1 = 1
		
		Arty_upgrade_1 = 1
		
		SP_R_arty_0 = 1
		
		early_helicopter = 1
		attack_helicopter1 = 1
		transport_helicopter1 = 1
		
		L_Strike_fighter1 = 1
		L_Strike_fighter2 = 1
		
		
		early_fighter = 1
		MR_Fighter1 = 1
		
		early_bomber = 1
		
		night_vision_1 = 1
		night_vision_2 = 1
		
		submarine_1 = 1
		
		LPD_0 = 1
		LPD_1 = 1 #Endurance-class landing platform dock

		landing_craft = 1
		
	}
		
	
	add_ideas = {
		gdp_9
		maritime_industry
		international_bankers
		landowners
		hybrid
		tax_cost_14
	}
	
	set_country_flag = enthusiastic_maritime_industry
	set_country_flag = enthusiastic_international_bankers
	set_country_flag = positive_landowners
	
	set_variable = { var = debt value = 112 }
	set_variable = { var = treasury value = 118 }
	set_variable = { var = tax_rate value = 15 }
	set_variable = { var = int_investments value = 0 }
	
	#Chinese diaspora trading network
	add_opinion_modifier = { target = SIA modifier = bamboo_network }
	reverse_add_opinion_modifier = { target = SIA modifier = bamboo_network  }
	
	add_opinion_modifier = { target = SIA modifier = asean_relations }
	reverse_add_opinion_modifier = { target = SIA modifier = asean_relations }
	add_opinion_modifier = { target = VIE modifier = asean_relations }
	reverse_add_opinion_modifier = { target = VIE modifier = asean_relations }
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 20
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 15
				#banned = no #default is no
			}
			
			neutrality = {
				popularity = 65
			}
		}
		
		ruling_party = neutrality
		last_election = "1997.1.2"
		election_frequency = 60
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Goh Chok Tong"
		picture = "Goh_Chok_Tong.dds"
		expire = "2005.1.1"
		ideology = Neutral_conservatism
		traits = {
			neutrality_Neutral_conservatism
		}
	}
	
}

2017.1.1 = {
	capital = 530
	oob = "SIN_2017"
	set_convoys = 20
	
	# Starting tech
	set_technology = { 
				
		command_control_equipment3 = 1
		
		land_Drone_equipment2 = 1
		
		IFV_5 = 1 #Bionix II
		APC_5 = 1 #Terrex AV-81
		
		diesel_attack_submarine_1 = 1
		
		corvette_1 = 1
		corvette_2 = 1
		missile_corvette_1 = 1
		missile_corvette_2 = 1
		missile_corvette_3 = 1 #Independence-Class
		
		frigate_1 = 1
		frigate_2 = 1
		missile_frigate_1 = 1
		missile_frigate_2 = 1
		missile_frigate_3 = 1 #Formidable-Class/La Fayette-class, tech transfer from france. https://en.wikipedia.org/wiki/Formidable-class_frigate
		
		night_vision_3 = 1
		
		artillery_2 = 1 #SLWH Pegasus
		
		Arty_upgrade_2 = 1
		Arty_upgrade_3 = 1
		
		SP_arty_1 = 1
		SP_arty_2 = 1 #SSPH Primus
		
		util_vehicle_3 = 1
		util_vehicle_4 = 1
		util_vehicle_5 = 1
		
	}

	add_ideas = {
		pop_050
		slight_corruption
		gdp_9
		pluralist
			stable_growth
			defence_04
		edu_03
		health_03
		social_02
		bureau_02
		police_05
		draft_army
		volunteer_women
		maritime_industry
		international_bankers
		landowners
		hybrid
		tax_cost_23
	}
	
	set_variable = { var = debt value = 359 }
	set_variable = { var = treasury value = 272 }
	set_variable = { var = int_investments value = 556 }
	set_variable = { var = tax_rate value = 23 }
	
	set_variable = { var = size_modifier value = 1.18 } #8 CIC
	initial_money_setup = yes

	#set_country_flag = gdp_9
	set_country_flag = TPP_Signatory
	set_country_flag = Major_Importer_US_Arms
	set_country_flag = enthusiastic_maritime_industry
	set_country_flag = enthusiastic_international_bankers
	set_country_flag = positive_landowners

	#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_15K_GDPC_slot
	complete_national_focus = Generic_30K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot
	
	add_opinion_modifier = { target = SIA modifier = asean_plus_three_relations }
	reverse_add_opinion_modifier = { target = SIA modifier = asean_plus_three_relations }
	add_opinion_modifier = { target = VIE modifier = asean_plus_three_relations }
	reverse_add_opinion_modifier = { target = VIE modifier = asean_plus_three_relations }
	add_opinion_modifier = { target = CHI modifier = asean_plus_three_relations }
	reverse_add_opinion_modifier = { target = CHI modifier = asean_plus_three_relations }
	add_opinion_modifier = { target = KOR modifier = asean_plus_three_relations }
	reverse_add_opinion_modifier = { target = KOR modifier = asean_plus_three_relations }
	add_opinion_modifier = { target = JAP modifier = asean_plus_three_relations }
	reverse_add_opinion_modifier = { target = JAP modifier = asean_plus_three_relations }

	set_politics = {

		parties = {
			democratic = { 
				popularity = 10
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 30
				#banned = no #default is no
			}
			
			neutrality = {
				popularity = 60
			}
		}
		
		ruling_party = neutrality
		last_election = "2015.9.11"
		election_frequency = 60
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Lee Hsien Loong"
		picture = "Lee_Hsien_Loong.dds"
		expire = "2050.1.1"
		ideology = Neutral_conservatism
		traits = {
			military_career
			neutrality_Neutral_conservatism
			tech_savy
			greedy
		}
	}

	create_equipment_variant = {
		name = "FH-88"
		type = artillery_1
		upgrades = {
			
		}
		obsolete = yes
	}
}