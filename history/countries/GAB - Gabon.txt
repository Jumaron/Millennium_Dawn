﻿2000.1.1 = {
	capital = 317
	oob = "GAB_2000"
	set_convoys = 20
	
	add_ideas = {
		gdp_6
		tax_cost_17
		multi_ethnic_state_idea
	}
	
	set_variable = { var = debt value = 6 }
	set_variable = { var = treasury value = 0 }
	set_variable = { var = tax_rate value = 17 }
	set_variable = { var = int_investments value = 0 }
	
	# Starting tech
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		#For templates
		infantry_weapons = 1
		command_control_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		artillery_0 = 1
		Anti_Air_0 = 1
		SP_Anti_Air_0 = 1
		Early_APC = 1
		APC_1 = 1
		util_vehicle_0 = 1
		Rec_tank_0 = 1
	}
	
	create_field_marshal = {
		name = "Auguste Bibaye Itandas"
		picture = "Portrait_Auguste_Bibaye_Itandas.dds"
		traits = { organisational_leader }
		id = 22500
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}
}

2017.1.1 = {
	capital = 317
	oob = "GAB_2017"
	set_convoys = 20
	
	add_ideas = {
		pop_050
		unrestrained_corruption
		gdp_4
		christian
		rentier_state
		export_economy
		stable_growth
		defence_01
		edu_02
		health_02
		social_01
		bureau_03
		police_03
		volunteer_army
		no_women_in_military
		fossil_fuel_industry
		international_bankers
		landowners
		hybrid
		tax_cost_12
		multi_ethnic_state_idea
	}
	
	#set_country_flag = gdp_4
	set_country_flag = positive_fossil_fuel_industry
	set_country_flag = positive_landowners
	
	set_variable = { var = debt value = 9 }
	set_variable = { var = treasury value = }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = tax_rate value = 12 }
	
	set_variable = { var = size_modifier value = 0.08 } #1 CIC
	initial_money_setup = yes
	
	#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot

	set_politics = {

		parties = {
			democratic = { 
				popularity = 15
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 5
				#banned = no #default is no
			}
			
			neutrality = { 
				popularity = 80
			}
		}
		
		ruling_party = neutrality
		last_election = "2016.8.27"
		election_frequency = 84
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Ali Bongo Ondimba"
		desc = "POLITICS_Ali_Bongo_Ondimba_DESC"
		picture = "Ali_Bongo_Ondimba.dds"
		expire = "2050.1.1"
		ideology = Neutral_conservatism
		traits = {
			#
		}
	}
	
}