﻿2000.1.1 = {
	capital = 411
	oob = "AFG_2000"
	
	set_technology = {
		infantry_weapons = 1
		command_control_equipment = 1
		Early_APC = 1
		APC_1 = 1
		IFV_1 = 1
		Rec_tank_0 = 1
		MBT_1 = 1
		util_vehicle_0 = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		Anti_Air_0 = 1
		artillery_0 = 1
	}
	
	add_ideas = {
		gdp_1
		tax_cost_10
		multi_ethnic_state_idea
	}
	set_variable = { var = debt value = 5.5 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = treasury value = 0.2 }
	set_variable = { var = tax_rate value = 10 }
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 28
			}

			fascism = {
				popularity = 7
			}
			
			communism = {
				popularity = 12
			}
			
			neutrality = {
				popularity = 37
			}
			
			nationalist = {
				popularity = 11
			}
		}
		
		ruling_party = neutrality
		last_election = "1999.9.21"
		election_frequency = 48
		elections_allowed = yes
	}
	
	create_country_leader = {
		name = "Mohammad Mohaqiq"
		picture = ""
		expire = "2065.1.1"
		ideology = Neutral_Muslim_Brotherhood #should be something else
		traits = {
		
		}
	}
	
	create_country_leader = {
		name = "Gulbuddin Hekmatyar"
		picture = "Gulbuddin_Hekmatyar.dds"
		expire = "2030.1.1"
		ideology = Neutral_Muslim_Brotherhood
		traits = {
		
		}
	}
	
	create_country_leader = {
		name = "Hamid Karzai"
		picture = "Hamid_Karzai.dds"
		expire = "2050.1.1"
		ideology = conservatism
		traits = {
		
		}
	}
	create_country_leader = {
		name = "Karim Khalili"
		picture = "Karim_Khalili.dds"
		expire = "2050.1.1"
		ideology = Mod_Vilayat_e_Faqih
		traits = {
		
		}
	}
	create_country_leader = {
		name = "Asif Mohseni"
		picture = "Asif_Mohseni.dds"
		expire = "2050.1.1"
		ideology = Vilayat_e_Faqih
		traits = {
		
		}
	}
	create_country_leader = {
		name = "Abdullah Abdullah"
		picture = "abdullah_abdullah.dds"
		expire = "2026.1.1"
		ideology = Neutral_conservatism
		traits = {
		
		}
	}
	create_country_leader = {
		name = "Yunus Qanuni"
		picture = ""
		expire = "2050.1.1"
		ideology = Neutral_conservatism
		traits = {
		
		}
	}
	create_country_leader = {
		name = "Ahmed Shah Massoud"
		picture = "Ahmad_Shah_Massoud.dds"
		expire = "2001.9.9"
		ideology = Neutral_conservatism
		traits = {
		
		}
	}
	create_country_leader = {
		name = "Abdul Rashid Dostum"
		picture = "Abdul_Rashid_Dostum.dds"
		expire = "2050.1.1"
		ideology = Nat_Populism
		traits = {
		
		}
	}
	create_country_leader = {
		name = "Burhanuddin Rabbani"
		picture = "Burhanuddin_Rabbani.dds"
		expire = "2050.1.1"
		ideology = Nat_Populism
		traits = {
		
		}
	}
	
	create_field_marshal = {
		name = "Mohammad Sharif Yaftali"
		picture = "generals/Portrait_Mohammad_Yaftali.dds"
		traits = { old_guard organisational_leader }
		id = 600
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_field_marshal = {
		name = "Mohammad Afzal Aman"
		picture = "generals/Portrait_Mohammad_Afzal_Zaman.dds"
		traits = { fast_planner }
		id = 601
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_field_marshal = {
		name = "Abdul Wahab Wardak"
		picture = "generals/Portrait_Abdul_Wahab_Wardak.dds"
		traits = { logistics_wizard }
		id = 602
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Murad Ali Murad"
		picture = "generals/Portrait_Murad_Ali_Murad.dds"
		traits = { panzer_leader }
		id = 603
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Dawood Shah Wafadar"
		picture = "generals/Portrait_Dawood_Shah_Wafadar.dds"
		traits = { commando hill_fighter }
		id = 604
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Mohammad Zaman Waziri"
		picture = "generals/Portrait_Mohammad_Waziri.dds"
		traits = { trait_engineer }
		id = 605
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Taj Mohammad Jahid"
		picture = "generals/Portrait_Taj_Mohammad_Jahid.dds"
		traits = { fortress_buster }
		id = 606
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Sayed Malook"
		picture = "generals/Portrait_Sayed_Malook.dds"
		traits = { hill_fighter }
		id = 607
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Dawlat Waziri"
		picture = "generals/Portrait_Dawlat_Waziri.dds"
		traits = { trickster }
		id = 608
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Roshan Safi"
		picture = "generals/Portrait_Roshan_Safi.dds"
		traits = { ranger }
		id = 609
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Aminullah Karim"
		picture = "generals/Portrait_Aminullah_Karim.dds"
		traits = { trickster trait_engineer }
		id = 610
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_navy_leader = {
		name = "Mohammad Sharif Yaftali"
		picture = "admirals/Portrait_Mohammad_Yaftali.dds"
		traits = {  }
		id = 611
	}	
}

2017.1.1 = {
	capital = 414
	oob = "AFG_2017"
	
	add_ideas = {
		pop_050
		crippling_corruption
		gdp_2
		sunni
		youth_radicalization
		stagnation
		defence_06
		edu_02
		health_02
		social_01
		bureau_02
		police_04
		gerrymandering
		accountable_press
		state_religion
		volunteer_army
		volunteer_women
		Resolute_Support_Mission
		Major_Non_NATO_Ally
		USA_usaid #https://explorer.usaid.gov/aid-dashboard.html
		The_Ulema
		international_bankers
		farmers
		hybrid
		tax_cost_07
		multi_ethnic_state_idea
	}
	
	set_country_flag = Major_Non_NATO_Ally
	set_country_flag = Major_Importer_US_Arms
	set_country_flag = positive_international_bankers
	set_country_flag = negative_farmers
	
	set_variable = { var = debt value = 2 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = treasury value = 7.5 }
	set_variable = { var = tax_rate value = 6 }
	
	set_variable = { var = size_modifier value = 0.08 } #1 CIC
	initial_money_setup = yes
	
	#Nat focus
	complete_national_focus = bonus_tech_slots
	
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		#Basic Rifles
		infantry_weapons = 1
		
		
		#Give some night vision
		night_vision_1 = 1
		
		#Old Radios
		command_control_equipment = 1
		
		#Old artillery
		artillery_0 = 1
		
		#Needed for SPAA template
		Anti_Air_0 = 1
		SP_Anti_Air_0 = 1
		
		#Needed for Mech/Arm Inf/Arm templates
		Early_APC = 1
		APC_1 = 1
		IFV_1 = 1
		Rec_tank_0 = 1
		MBT_1 = 1
		
		#Needed for Mot
		util_vehicle_0 = 1
		
		#Needed for HAT and HIW
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		
	}

	set_politics = {

		parties = {
			democratic = { 
				popularity = 40
			}

			fascism = {
				popularity = 30
			}
			
			communism = {
				popularity = 20
				#banned = no #default is no
			}
			
			neutrality = { 
				popularity = 10
			}
		}
		
		ruling_party = democratic
		last_election = "2014.4.5"
		election_frequency = 60
		elections_allowed = yes
	}

	#Leader of the Taliban
	create_country_leader = {
		name = "Akhtar Muhammad Mansor"
		desc = "POLITICS_MOHAMMED_ZAHIR_SHAH_DESC"
		picture = "AFG-Salafi-akhtar-muhammad-mansor.dds"
		expire = "2050.1.1"
		ideology = Caliphate
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Ashraf Ghani"
		desc = "POLITICS_MOHAMMED_ZAHIR_SHAH_DESC"
		picture = "AFG-Ashraf-Ghani.dds"
		expire = "2050.1.1"
		ideology = conservatism
		traits = {
			#
		}
	}
	
	create_corps_commander = {
		name = "Khatol Mohammad Zai"
		picture = "generals/Gen_Khatol_Mohammad_Zai.dds"
		traits = { trickster trait_engineer }
		id = 612
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

}