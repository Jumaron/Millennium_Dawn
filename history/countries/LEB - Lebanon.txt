﻿2000.1.1 = {
	capital = 202
	oob = "LEB_2000"
	set_convoys = 25
	
	add_ideas = {
		gdp_5
		pop_050
		defence_06
		edu_01
		health_02
		social_01
		bureau_02
		police_02
		landowners
		small_medium_business_owners
		iranian_quds_force
		hybrid
		tax_cost_12
	}
	set_country_flag = positive_landowners
	set_country_flag = positive_small_medium_business_owners

	set_variable = { var = debt value = 37 }
	set_variable = { var = treasury value = 12 }
	set_variable = { var = tax_rate value = 12 }
	set_variable = { var = int_investments value = 0 }
	
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 48
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 20
			}
			
			neutrality = { 
				popularity = 27
			}
			
			nationalist = {
				popularity = 5
			}
		}
		
		ruling_party = democratic
		last_election = "1997.1.1"
		election_frequency = 48
		elections_allowed = yes
	}
	
	set_variable = { conservatism_pop = 0.15 }
	set_variable = { liberalism_pop = 0.23 }
	set_variable = { socialism_pop = 0.08 }
	set_variable = { Western_Autocracy_pop = 0.02 }
	set_variable = { Vilayat_e_Faqih_pop = 0.1 }
	set_variable = { Mod_Vilayat_e_Faqih_pop = 0.1 }
	set_variable = { Neutral_conservatism_pop = 0.22 }
	set_variable = { Neutral_Libertarian_pop = 0.05 }
	set_variable = { Nat_Fascism_pop = 0.03 }
	set_variable = { Nat_Populism_pop = 0.02 }
	
	recalculate_party = yes
	
	set_technology = { 
		#For templates
		infantry_weapons = 1
		combat_eng_equipment = 1
		command_control_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		artillery_0 = 1
		SP_arty_0 = 1
		Anti_Air_0 = 1
		Early_APC = 1
		APC_1 = 1
		MBT_1 = 1
		util_vehicle_0 = 1
		Rec_tank_0 = 1

		landing_craft = 1
		
	}
	
	create_country_leader = {
		name = "Samir Geagea"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "samir_geagea.dds"
		expire = "2001.1.1" #Date not done yet
		ideology = conservatism
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Saad Hariri"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "saad_hariri_2000.dds"
		expire = "2001.1.1" #Date not done yet
		ideology = liberalism
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Walid Jumblatt"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "walid_jumblatt.dds"
		expire = "2001.1.1" #Date not done yet
		ideology = socialism
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Carlos Edde"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "carlos_edde.dds"
		expire = "2001.1.1" #Date not done yet
		ideology = Western_Autocracy
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Farouk Dahrouj"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "farouk_dahrouj.dds"
		expire = "2001.1.1" #Date not done yet
		ideology = Communist-State
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Abdelrahim Mourad"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "abdelrahim_mourad.dds"
		expire = "2001.1.1" #Date not done yet
		ideology = Conservative
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Elie Hobeika"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "elie_hobeika.dds"
		expire = "2001.1.1" #Date not done yet
		ideology = Autocracy
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Hassan Nasrallah"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "hassan_nasrallah.dds"
		expire = "2001.1.1" #Date not done yet
		ideology = Vilayat_e_Faqih
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Nabih Berri"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "nabih_birri.dds"
		expire = "2001.1.1" #Date not done yet
		ideology = Mod_Vilayat_e_Faqih
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Assem Qanso"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "assem_qanso.dds"
		expire = "2001.1.1" #Date not done yet
		ideology = anarchist_communism
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Michel Aoun"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "michel_aoun.dds"
		expire = "2001.1.1" #Date not done yet
		ideology = Neutral_conservatism
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Fouad Makhzoumi"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "fouad_makhzoumi.dds"
		expire = "2001.1.1" #Date not done yet
		ideology = oligarchism
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Hagop Pakradouni"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "hagop_pakradouni.dds"
		expire = "2001.1.1" #Date not done yet
		ideology = neutral_Social
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Suleiman Frangieh Jr."
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "suleiman_frangieh.dds"
		expire = "2001.1.1" #Date not done yet
		ideology = Neutral_Libertarian
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Elie Mahfoud Mahfoud"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "elie_mahfoud_mahfoud.dds"
		expire = "2001.1.1" #Date not done yet
		ideology = Neutral_Autocracy
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Zaher Khatib"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "zaher_khatib.dds"
		expire = "2001.1.1" #Date not done yet
		ideology = Neutral_Communism
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Michel Suleiman"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "michel_suleiman.dds"
		expire = "2001.1.1" #Date not done yet
		ideology = Nat_Autocracy
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Assaad Hardan"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "assaad_hardan.dds"
		expire = "2001.1.1" #Date not done yet
		ideology = Nat_Fascism
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Karim Pakradouni"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "karim_pakradouni.dds"
		expire = "2001.1.1" #Date not done yet
		ideology = Nat_Populism
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Emile Lahoud"
		desc = "POLITICS_MACKENZIE_KING_DESC"
		picture = "emile_lahoud.dds"
		expire = "2007.11.24" 
		ideology = conservatism
		traits = {
			#
		}
	}
	
	
	
	create_field_marshal = {
		name = "Joseph Aoun"
		picture = "generals/Portrait_Joseph_Aoun.dds"
		traits = { offensive_doctrine }
		id = 35400
		skill = 4
		attack_skill = 4
		defense_skill = 4
		planning_skill = 4
		logistics_skill = 4
	}

	create_field_marshal = {
		name = "Jean Kahwagi"
		picture = "generals/Portrait_Jean_Kahwagi.dds"
		traits = { old_guard inspirational_leader }
		id = 35401
		skill = 4
		attack_skill = 4
		defense_skill = 4
		planning_skill = 4
		logistics_skill = 4
	}

	create_field_marshal = {
		name = "Ghassan Chahine"
		picture = "generals/Portrait_Ghassan_Chahine.dds"
		traits = { logistics_wizard }
		id = 35402
		skill = 4
		attack_skill = 4
		defense_skill = 4
		planning_skill = 4
		logistics_skill = 4
	}

	create_corps_commander = {
		name = "Walid Salman"
		picture = "generals/Portrait_Walid_Salman.dds"
		traits = { panzer_leader trait_engineer }
		id = 35403
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Hatem Mallak"
		picture = "generals/Portrait_Hatem_Mallak.dds"
		traits = { bearer_of_artillery }
		id = 35404
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Chamel Roukoz"
		picture = "generals/Portrait_Chamel_Roukoz.dds"
		traits = { commando naval_invader ranger }
		id = 35405
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Imad Othman"
		picture = "generals/Portrait_Imad_Othman.dds"
		traits = { trickster urban_assault_specialist }
		id = 35406
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_navy_leader = {
		name = "Majed Alwan"
		picture = "admirals/Portrait_Majed_Alwan.dds"
		traits = { superior_tactician }
		id = 35407
	}
}

2017.1.1 = {
	capital = 202
	oob = "LEB_2017"

	diplomatic_relation = {
		country = HEZ
		relation = military_access
		active = yes
	}
	
	add_ideas = {
		pop_050
		unrestrained_corruption
		gdp_5
		pluralist
		LoAS_member
		stable_growth
		defence_06
		edu_01
		health_02
		social_01
		bureau_02
		police_02
		accountable_press
		non_secret_ballots
		state_religion
		al_jazeera_allowed
		volunteer_army
		volunteer_women
		landowners
		small_medium_business_owners
		iranian_quds_force
		hybrid
		tax_cost_13
	}
	#set_country_flag = gdp_5
	set_country_flag = positive_landowners
	set_country_flag = positive_small_medium_business_owners
	
	set_variable = { var = debt value = 79 }
	set_variable = { var = treasury value = 54 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = tax_rate value = 13 }
	
	set_variable = { var = size_modifier value = 0.14 } #2 CIC
	initial_money_setup = yes
	
	#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		night_vision_1 = 1
		
		#For templates
		infantry_weapons = 1
		combat_eng_equipment = 1
		command_control_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		artillery_0 = 1
		SP_arty_0 = 1
		Anti_Air_0 = 1
		Early_APC = 1
		APC_1 = 1
		MBT_1 = 1
		util_vehicle_0 = 1
		Rec_tank_0 = 1

		landing_craft = 1
		
	}

	set_politics = {

		parties = {
			democratic = { 
				popularity = 38
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 26
			}
			
			neutrality = { 
				popularity = 32
			}
			
			nationalist = {
				popularity = 4
			}
		}
		
		ruling_party = neutrality
		last_election = "2013.6.1" #Will give next elections correct
		election_frequency = 48
		elections_allowed = yes
	}
	
	set_variable = { conservatism_pop = 0.14 }
	set_variable = { liberalism_pop = 0.16 }
	set_variable = { socialism_pop = 0.08 }
	set_variable = { Vilayat_e_Faqih_pop = 0.12 }
	set_variable = { Mod_Vilayat_e_Faqih_pop = 0.14 }
	set_variable = { Neutral_conservatism_pop = 0.25 }
	set_variable = { neutral_Social_pop = 0.03 }
	set_variable = { Neutral_Libertarian_pop = 0.04 }
	set_variable = { Nat_Fascism_pop = 0.02 }
	set_variable = { Nat_Populism_pop = 0.02 }

	recalculate_party = yes
	
	create_country_leader = {
		name = "Mohammad Raad"
		picture = "Emerging_HEZ_Mohammad_Raad.dds"
		ideology = Vilayat_e_Faqih
		traits = {
			guerrilla_leader
			emerging_Vilayat_e_Faqih
			pro_iranian
			cautious
			zealous
		}
	}

	create_country_leader = {
		name = "Ahmed al-Assir"
		picture = "Salafist_Ahmed_al-Assir.dds"
		ideology = Caliphate
		traits = {
			cleric
			salafist_Caliphate
			zealous
			ruthless
		}
	}

	create_country_leader = {
		name = "Michel Aoun"
		picture = "Neutral_Michel_Aoun.dds"
		ideology = Neutral_conservatism
		traits = {
			military_career
			neutrality_Neutral_conservatism
			political_dancer
			cautious
		}
	}

	create_country_leader = {
		name = "Saad Hariri"
		picture = "Western_Saad_Hariri.dds"
		ideology = Conservative
		traits = {
			businessman
			western_conservatism
			pro_saudi
			rash
			emotional
			greedy
		}
	}
	
}