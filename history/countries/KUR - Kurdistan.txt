﻿2000.1.1 = {
	capital = 164
	oob = "KUR_2000"
	
	set_convoys = 20
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 25
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 25
			}
			
			neutrality = { 
				popularity = 50
			}
			
			nationalist = { 
				popularity = 0
			}
		}
		
		ruling_party = neutrality
		last_election = "1999.1.30" 
		election_frequency = 72
		elections_allowed = yes
	}
	
	create_country_leader = {
		name = "Masoud Barzani"
		picture = "Masoud_Barzani.dds"
		expire = "2050.1.1"
		ideology = oligarchism
		traits = {
			#
		}
	}
	
	create_corps_commander = {
		name = "Hussein Mansor"
		picture = "generals/Hussein_Mansor.dds"
		traits = { panzer_leader }
		id = 33600
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Abdula Musla"
		picture = "generals/Abdula_Musla.dds"
		traits = { commando }
		id = 33601
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Hossein Yazdan Bna"
		picture = "generals/Hossein_Yazdan_Bna.dds"
		traits = { trickster }
		id = 33602
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Sirwan Barzani"
		picture = "generals/Sirwan_Barzani.dds"
		traits = { ranger }
		id = 33603
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Karzan Mahmoud Ahmed"
		picture = "generals/Karzan.dds"
		traits = { desert_fox }
		id = 33604
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Sherko Shwani"
		picture = "generals/Sherko.dds"
		traits = { trait_engineer }
		id = 33605
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

}

2017.1.1 = {
	capital = 164
	oob = "KUR_2017"
	set_convoys = 20
	
	add_ideas = {
		pop_050
		unrestrained_corruption
		gdp_3
		sunni
		small_medium_business_owners
		Labour_Unions
		#Mass_Media
		bureau_03
		recession
		defence_05
		edu_02
		health_02
		social_01
		bureau_04
		police_01
		accountable_press
		gerrymandering
		secular_state
		rentier_state
		export_economy
		volunteer_army
		volunteer_women
		Inherent_Resolve
		fossil_fuel_industry
		landowners
		small_medium_business_owners
		hybrid
		
		tax_cost_33
	}
	
	set_variable = { var = debt value = 17 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = treasury value = 3 }
	set_variable = { var = tax_rate value = 33 }
	
	set_variable = { var = size_modifier value = 0.08 } #1 CIC
	initial_money_setup = yes
	
	diplomatic_relation = {
		country = IRQ
		relation = military_access
		active = yes
	}
	
	diplomatic_relation = {
		country = ROJ
		relation = military_access
		active = yes
	}
		   
	#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	
	# Starting tech
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		night_vision_1 = 1
		night_vision_2 = 1
		night_vision_3 = 1

		infantry_weapons = 1
		combat_eng_equipment = 1
		command_control_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		artillery_0 = 1
		SP_arty_0 = 1
		Anti_Air_0 = 1
		Early_APC = 1
		APC_1 = 1
		IFV_1 = 1
		MBT_1 = 1
		util_vehicle_0 = 1
		Rec_tank_0 = 1
		ENGI_MBT_1 = 1
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 25
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 25
			}
			
			neutrality = { 
				popularity = 50
			}
			
			nationalist = { 
				popularity = 0
			}
		}
		
		ruling_party = neutrality
		last_election = "2009.7.25" 
		election_frequency = 72
		elections_allowed = no #indefinately postponed
	}
	
	add_opinion_modifier = { target = ISI modifier = hostile_status }
	add_opinion_modifier = { target = TUR modifier = TUR_Dont_Support_Kurdish_Independence }
	add_opinion_modifier = { target = PER modifier = PER_Dont_Support_Kurdish_Independence }
	add_opinion_modifier = { target = IRQ modifier = IRQ_Dont_Support_Kurdish_Independence }

	
}