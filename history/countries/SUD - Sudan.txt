﻿2000.1.1 = {
	capital = 220
	oob = "SUD_2000"
	set_convoys = 20
	
	add_ideas = {
		gdp_2	
		pop_060
		sunni
		draft_army
		volunteer_women
		defence_04
		tax_cost_17
		multi_ethnic_state_idea
	}
	
	#set_country_flag = gdp_3
	
	set_variable = { var = debt value = 27 }
	set_variable = { var = treasury value = 0 }
	set_variable = { var = tax_rate value = 17 }
	set_variable = { var = int_investments value = 0 }
	
	#Nat focus
	complete_national_focus = bonus_tech_slots
	
	# Starting tech
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		
		#For Templates
		infantry_weapons = 1
		command_control_equipment = 1
		land_Drone_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		Anti_Air_0 = 1
		combat_eng_equipment = 1
		early_helicopter = 1
		transport_helicopter1 = 1
		attack_helicopter1 = 1
		early_fighter = 1
		MR_Fighter1 = 1
		Strike_fighter1 = 1
		night_vision_1 = 1
		Rec_tank_0 = 1
		Air_APC_1 = 1
		ENGI_MBT_1 = 1
		
		Early_APC = 1
		
		#Al Zubair MBT
		MBT_1 = 1
		MBT_2 = 1	
		
		#Khatim 1 IFV
		IFV_1 = 1
		IFV_2 = 1 
		
		#Shareef 1 IFV
		APC_1 = 1
		APC_2 = 1
		APC_3 = 1
		APC_4 = 1 
		
		#Amir IFV
		util_vehicle_0 = 1
		util_vehicle_1 = 1
		util_vehicle_2 = 1
		util_vehicle_3 = 1
		util_vehicle_4 = 1	
		
		#Khalifa
		artillery_0 = 1 
		
		#Abu Fatma SPG
		SP_arty_0 = 1 
		SP_R_arty_0 = 1 

		landing_craft = 1
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 0
			}

			fascism = {
				popularity = 25
			}
			
			communism = {
				popularity = 0
				#banned = no #default is no
			}
			
			neutrality = { 
				popularity = 75
			}
			
			nationalist = { 
				popularity = 0
			}
		}
		
		ruling_party = neutrality
		last_election = "1996.4.1"
		election_frequency = 48
		elections_allowed = yes
	}
	
	declare_war_on = {
		target = SSU
		type = civil_war
	}

	create_country_leader = {
		name = "Muhammad Ibrahim Nugud"		#Sudanese communist party
		picture = "SUD_Muhammad_Ibrahim_Nugud.dds"
		expire = "2012.1.1"
		ideology = Communist-State
		traits = {
		}
	}

	create_country_leader = {
		name = "Jalal al-Digair"		#Competing faction in Democratic Unionis party
		picture = "SUD_Jalal_al_Digair.dds"
		expire = "2050.1.1"
		ideology = conservatism
		traits = {
		}
	}

	create_country_leader = {
		name = "Ahmed al-Mirghani"		#Leader of Democratic Unionis party
		picture = "SUD_Ahmed_al_Mirghani.dds"
		expire = "2008.1.1"
		ideology = conservatism
		traits = {
		}
	}

	create_country_leader = {
		name = "Fatima Abdel Mahmoud"		#leader of Sudanese Socialist Union (anti-communist, Arab nationalist/socialist)
		picture = "SUD_Fatima_Abdel_Mahmoud.dds"
		expire = "2050.1.1"
		ideology = Nat_Autocracy
		traits = {
		}
	}

	create_country_leader = {
		name = "Abdel Rahman Swar al-Dahab"		#Couped Gaafar Nimeiry out of power
		picture = "SUD_Abdel_Rahman_Swar_al_Dahab.dds"
		expire = "2050.1.1"
		ideology = Nat_Autocracy
		traits = {
		}
	}
	
	create_country_leader = {
		name = "Malik Agar"
		desc = "POLITICS_Mohamed Cheikh Biadillah_DESC" 
		picture = "Malik_Agar.dds"
		expire = "2011.1.1"
		ideology = Neutral_conservatism  
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Sadiq al-Mahdi"		#Imam of Ansar Sufis, 
		picture = "sudan_western_Sadiq_al-Mahdi.dds"
		expire = "2050.1.1"
		ideology = Neutral_conservatism
		traits = {
		}
	}

	create_country_leader = {
		name = "Gaafar Nimeiry"		#Previous president, took part in 2000 elections
		picture = "SUD_Gaafar_Nimeiry.dds"
		expire = "2009.1.1"
		ideology = Neutral_Muslim_Brotherhood
		traits = {
		}
	}

	create_country_leader = {
		name = "Omar al-Bashir"
		picture = "Omar_al-Bashir.dds"
		expire = "2050.1.1"
		ideology = Neutral_Muslim_Brotherhood
		traits = {
			military_career
			neutrality_Neutral_Muslim_Brotherhood
			pro_saudi
			war_criminal
			greedy
			ruthless
			
		}
	}
	
	create_corps_commander = {
		name = "Mustafa Osman Obeid Salim"
		picture = "generals/Mustafa_Salim.dds"
		id = 61200
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
	
	#Land variants
	create_equipment_variant = {
		name = "Al Zubair 2 MBT"
		type = MBT_2
		upgrades = {
			tank_reliability_upgrade = 1
			tank_engine_upgrade = 1
			tank_armor_upgrade = 1
			tank_gun_upgrade = 1
		}
		obsolete = yes
	}
}

2017.1.1 = {
	white_peace = SSU
	capital = 220
	oob = "SUD_2017"
	set_convoys = 20

	add_ideas = {
		pop_050
		crippling_corruption
		gdp_3
		sunni
		youth_radicalization
		saudi_aid
		defence_04
		tax_cost_06
		stable_growth
		edu_02
		health_01
		social_01
		bureau_01
		police_04
		state_press
		parties_harassment
		theocracy
		al_jazeera_allowed
		draft_army
		volunteer_women
		restoring_hope
		farmers
		the_military
		international_bankers
		hybrid
		tax_cost_08
		multi_ethnic_state_idea
	}
	
	set_variable = { var = debt value = 47.1 }
	set_variable = { var = treasury value = 0.2 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = tax_rate value = 8 }
	
	set_variable = { var = size_modifier value = 0.23 } #3 CIC
	initial_money_setup = yes
	
	#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	
	# Starting tech
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		
		#For Templates
		infantry_weapons = 1
		command_control_equipment = 1
		land_Drone_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		Anti_Air_0 = 1
		combat_eng_equipment = 1
		early_helicopter = 1
		transport_helicopter1 = 1
		attack_helicopter1 = 1
		early_fighter = 1
		MR_Fighter1 = 1
		Strike_fighter1 = 1
		night_vision_1 = 1
		Rec_tank_0 = 1
		Air_APC_1 = 1
		ENGI_MBT_1 = 1
		
		Early_APC = 1
		
		#Al Bashier MBT
		MBT_1 = 1
		MBT_2 = 1	
		MBT_3 = 1	
		
		#Khatim 1 IFV
		IFV_1 = 1
		IFV_2 = 1 
		
		#Shareef 1 IFV
		APC_1 = 1
		APC_2 = 1
		APC_3 = 1
		APC_4 = 1 
		
		#Amir IFV
		util_vehicle_0 = 1
		util_vehicle_1 = 1
		util_vehicle_2 = 1
		util_vehicle_3 = 1
		util_vehicle_4 = 1	
		
		#Khalifa
		artillery_0 = 1 
		
		#Abu Fatma SPG
		SP_arty_0 = 1 
		SP_R_arty_0 = 1 

		landing_craft = 1
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 5
			}

			fascism = {
				popularity = 25
			}
			
			communism = {
				popularity = 8
				#banned = no #default is no
			}
			
			neutrality = { 
				popularity = 55
			}
			
			nationalist = { 
				popularity = 7
			}
		}
		
		ruling_party = neutrality
		last_election = "2015.4.16"
		election_frequency = 60
		elections_allowed = yes
	}
}