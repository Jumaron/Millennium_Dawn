﻿2000.1.1 = {
	capital = 153
	oob = "BUL_2000"
	set_convoys = 21
	
	add_ideas = {
		gdp_4
		pop_050
		Industrial_Conglomerates
		orthodox_christian
		defence_01
		edu_03
		health_03
		social_03
		bureau_03
		police_05
		volunteer_army
		volunteer_women
		NATO_member
		landowners
		small_medium_business_owners
		industrial_conglomerates
		civil_law
		tax_cost_17
	}
	
	set_country_flag = positive_landowners
	set_country_flag = positive_small_medium_business_owners
	set_country_flag = positive_industrial_conglomerates

	set_variable = { var = debt value = 14 }
	set_variable = { var = treasury value = 5 }
	set_variable = { var = tax_rate value = 17 }
	set_variable = { var = int_investments value = 0 }
	
	set_technology = { 

		night_vision_1 = 1
		
		IFV_1 = 1
		
		SP_arty_0 = 1	

		artillery_0 = 1	
		
		 #1975

		Heavy_Anti_tank_0 = 1 #1965

		Anti_tank_0 = 1

		SP_Anti_Air_0 = 1 #1965

		Anti_Air_0 = 1 #1965

		Early_APC = 1
		APC_1 = 1 #1965

		combat_eng_equipment = 1

		infantry_weapons = 1

		landing_craft = 1
		
		command_control_equipment = 1
		
		ENGI_MBT_1 = 1
		
		util_vehicle_0 = 1
		Rec_tank_0 = 1
		MBT_1 = 1
		
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 52
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 23
			}
			
			neutrality = {
				popularity = 25
			}
			
			nationalist = {
				 popularity = 0
			}
		}
		
		ruling_party = communism
		last_election = "1997.4.19"
		election_frequency = 48
		elections_allowed = yes
	}	
	create_country_leader = {
		name = "Georgi Parvanov"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = ""
		expire = "2002.6.10"
		ideology = socialism
		traits = {
		
		}
	}
	create_country_leader = {
		name = "Petar Stoyanov"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "petar_stoyanov.dds"
		expire = "2065.1.1"
		ideology = Communist-State
		traits = {
		
		}
	}
	create_country_leader = {
		name = "Bogomil Bonev"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = ""
		expire = "2065.1.1"
		ideology = neutral_Social
		traits = {
		
		}
	}
	
	create_field_marshal = {
		name = "Konstantin Popov"
		picture = "Portrait_Konstantin_Popov.dds"
		traits = { old_guard organisational_leader }
		id = 9600
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_field_marshal = {
		name = "Andrei Botsev"
		picture = "Portrait_Andrei_Botsev.dds"
		traits = { thorough_planner }
		id = 9601
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Yavor Mateev"
		picture = "Portrait_Yavor_Mateev.dds"
		traits = { commando urban_assault_specialist }
		id = 9602
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Tsanko Ivanov Stoykov"
		picture = "Portrait_Tsanko_Stoykov.dds"
		traits = {  }
		id = 9603
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Petyo Mirchev"
		picture = "Portrait_Petyo_Mirchev.dds"
		traits = {  }
		id = 9604
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Anatoliy Krustev"
		picture = "Portrait_Anatolyi_Krustev.dds"
		traits = {  }
		id = 9605
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Lyubcho Spasov Todorov"
		picture = "Portrait_Lyubcho_Todorov.dds"
		traits = { panzer_leader }
		id = 9606
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Todor Tzonev Dochev"
		picture = "Portrait_Todor_Donchev.dds"
		traits = { trait_engineer }
		id = 9607
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Mihail Popov"
		picture = "Portrait_Mihail_Popov.dds"
		traits = { ranger }
		id = 9608
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Rumen Radev"
		picture = "Portrait_Rumen_Radev.dds"
		traits = {  }
		id = 9609
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Valeri Konstantin Tsolov"
		picture = "Portrait_Valerie_Tsolov.dds"
		traits = { fortress_buster }
		id = 9610
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Kostadin Kuzmov"
		picture = "Portrait_Kostadin_Kuzmov.dds"
		traits = { panzer_leader }
		id = 9611
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Dimitar Iliev"
		picture = "Portrait_Dimitar_Iliev.dds"
		traits = { panzer_leader }
		id = 9612
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Rusi Rusev"
		picture = "Portrait_Rusi_Rusev.dds"
		traits = { trait_mountaineer }
		id = 9613
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Boyan Stavrev"
		picture = "Portrait_Bojan_Stavrev.dds"
		traits = { urban_assault_specialist }
		id = 9614
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Plamen Yordanov"
		picture = "Portrait_Plamen_Yordanov.dds"
		traits = { panzer_leader }
		id = 9615
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Gruda Angelov"
		picture = "Portrait_Gruda_Angelov.dds"
		traits = { panzer_leader }
		id = 9616
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Yordan Yordanov"
		picture = "Portrait_Yordan_Yordanov.dds"
		traits = { trickster }
		id = 9617
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Atanas Lefterov"
		picture = "Portrait_Atanas_Lefterov.dds"
		traits = { hill_fighter }
		id = 9618
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Georgi Petkov"
		picture = "Portrait_Georgi_Petkov.dds"
		traits = {  }
		id = 9619
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Georgi Dimov"
		picture = "Portrait_Georgi_Dimov.dds"
		traits = { trait_engineer }
		id = 9620
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_navy_leader = {
		name = "Emil Eftimov"
		picture = "Portrait_Emil_Eftimov.dds"
		traits = { old_guard_navy superior_tactician }
		id = 9621
	}

	create_navy_leader = {
		name = "Rumen Nikolov"
		picture = "Portrait_Rumen_Nikolov.dds"
		traits = { blockade_runner }
		id = 9622
	}

	create_navy_leader = {
		name = "Dimitar Vasilev Yordanov"
		picture = "Portrait_Dimitar_Yordanov.dds"
		traits = { spotter }
		id = 9623
	}

	create_navy_leader = {
		name = "Kosta Andreev"
		picture = "Portrait_Kosta_Andreev.dds"
		traits = { seawolf }
		id = 9624
	}

	create_navy_leader = {
		name = "Mitko Alexander Petev"
		picture = "Portrait_Mitko_Petev.dds"
		traits = { ironside }
		id = 9625
	}

}

2017.1.1 = {
	capital = 153
	oob = "BUL_2017"
	set_convoys = 21

	add_ideas = {
		pop_050

		small_medium_business_owners
		Labour_Unions
		Industrial_Conglomerates
		orthodox_christian
		unrestrained_corruption
		gdp_4
		stable_growth
		defence_01
		edu_03
		health_03
		social_03
		bureau_03
		police_05
		volunteer_army
		volunteer_women
		NATO_member
		landowners
		small_medium_business_owners
		industrial_conglomerates
		civil_law
		tax_cost_29
	}
	
	set_country_flag = positive_landowners
	set_country_flag = positive_small_medium_business_owners
	set_country_flag = positive_industrial_conglomerates
	
	set_variable = { var = debt value = 14 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = treasury value = 0 }
	set_variable = { var = tax_rate value = 29 }
	
	set_variable = { var = size_modifier value = 0.38 } #4 CIC
	initial_money_setup = yes

	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1

		night_vision_1 = 1
		night_vision_2 = 1
		night_vision_3 = 1
		
		IFV_1 = 1
		IFV_2 = 1
		IFV_3 = 1
		
		SP_arty_0 = 1	

		artillery_0 = 1	
		
		 #1975

		Heavy_Anti_tank_0 = 1 #1965

		Anti_tank_0 = 1

		SP_Anti_Air_0 = 1 #1965

		Anti_Air_0 = 1 #1965

		Early_APC = 1
		APC_1 = 1 #1965

		combat_eng_equipment = 1

		infantry_weapons = 1
		infantry_weapons1 = 1

		landing_craft = 1
		
		command_control_equipment = 1
		
		ENGI_MBT_1 = 1
		
		util_vehicle_0 = 1
		Rec_tank_0 = 1
		MBT_1 = 1
		
	}
	
	#NATO military access
	diplomatic_relation = {
		country = ALB
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = BEL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CAN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CRO
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CZH
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = DEN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = EST
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = FRA
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = GER
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = GRE
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = HUN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ICE
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ITA
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = LAT
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = LIT
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = LUX
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = HOL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = NOR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = POL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = POR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ROM
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = SLO
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = SLV
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = SPR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = TUR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ENG
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = USA
		relation = military_access
		active = yes
	}

	#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot

	set_politics = {

		parties = {
			democratic = { 
				popularity = 77
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 10
			}
			
			neutrality = {
				popularity = 8
			}
			
			nationalist = {
				popularity = 5
			}
		}
		
		ruling_party = democratic
		last_election = "2014.10.5"
		election_frequency = 48
		elections_allowed = yes
	}


	create_country_leader = {
		name = "Mustafa Karadaya"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "mustafa_karadaya.dds"
		expire = "2065.1.1"
		ideology = liberalism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Mihail Mikov"   
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "mihail_mikov.dds"
		expire = "2065.1.1"
		ideology = socialism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Volen Siderov"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "volen_siderov.dds"
		expire = "2065.1.1"
		ideology = Conservative
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Nikolay Barekov"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "nikolay_barekov.dds"
		expire = "2065.1.1"
		ideology = Neutral_conservatism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Krasimir Karakachanov"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "krasimir_karakachanov.dds"
		expire = "2065.1.1"
		ideology = Nat_Populism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Boyko Borissov"
		desc = ""
		picture = "BUL_Boyko_Borissov.dds"
		expire = "2065.1.1"
		ideology = conservatism
		traits = {
			#
		}
	}
}