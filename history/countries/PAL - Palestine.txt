﻿2000.1.1 = {
	capital = 208
	oob = "PAL_2000"
	set_convoys = 10

	add_ideas = {
		gdp_1
	}

	set_technology = { 
		infantry_weapons = 1
		command_control_equipment = 1
		Anti_tank_0 = 1
		Anti_Air_0 = 1
		util_vehicle_0 = 1
		night_vision_1 = 1
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 23
			}

			fascism = {
				popularity = 12
			}
			
			communism = {
				popularity = 27
			}
			
			neutrality = { 
				popularity = 38
			}
			
		}
		
		ruling_party = neutrality
		last_election = "1996.1.20"
		election_frequency = 60
		elections_allowed = no
	}
	
	create_country_leader = {
		name = "Yasser Arafat"
		ideology = neutral_Social
		picture = "Yassir_Arafat.dds"
	}
	
	create_corps_commander = { 
		name = "Saeb al-Ajuz" 
		picture = "Saeb_al-Ajuz.dds"
		traits = { urban_assault_specialist }
		id = 46200
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}
	create_corps_commander = { 
		name = "Mohamed Talib Bouji" 
		picture = "Mohamed_Bouji.dds"
		traits = { commando }
		id = 46201
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
	create_corps_commander = { 
		name = "Ziad Al-Atrash" 
		picture = "Ziad_Al-Atrash.dds"
		id = 46202
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
	create_corps_commander = { 
		name = "Amjad Abu-Omar" 
		picture = "Amjad Abu-Omar.dds"
		id = 46203
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
}

2017.1.1 = {
	capital = 208
	oob = "PAL_2017"
	set_convoys = 5

	add_opinion_modifier = { target = ISR modifier = HAM_ISR_Occupation }


	add_ideas = {
		pop_050
		unrestrained_corruption
		gdp_2
		sunni
		LoAS_member
		stagnation
		defence_04
		edu_03
		health_03
		social_01
		bureau_05
		police_05
		volunteer_army
		volunteer_women
		hybrid
		
	}
	#set_country_flag = gdp_2

	#Nat focus
	complete_national_focus = bonus_tech_slots
	
	# Starting tech
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		infantry_weapons = 1
		command_control_equipment = 1
		Anti_tank_0 = 1
		Anti_Air_0 = 1
		util_vehicle_0 = 1
		night_vision_1 = 1
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 42
			}

			fascism = {
				popularity = 33
			}
			
			communism = {
				popularity = 6
			}
			
			neutrality = { 
				popularity = 19
			}
			
		}
		
		ruling_party = democratic
		last_election = "2005.1.15"
		election_frequency = 60
		elections_allowed = no
	}

	create_country_leader = {
		name = "Mahmoud Abbas"
		desc = ""
		picture = "PAL_Mahmoud_Abbas.dds"
		expire = "2040.1.1"
		ideology = Western_Autocracy
		traits = {
			#
		}
	}
}