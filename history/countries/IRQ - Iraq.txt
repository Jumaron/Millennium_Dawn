﻿2000.1.1 = {
	capital = 557
	oob = "IRQ_2000"
	set_convoys = 30
	
	add_ideas = {
		gdp_3
		tax_cost_15
	}
	
	set_variable = { var = debt value = 130 }
	set_variable = { var = treasury value = 10 }
	set_variable = { var = tax_rate value = 15 }
	set_variable = { var = int_investments value = 0 }

	
	set_technology = { 
		legacy_doctrines = 1 
		grand_battleplan = 1 
		deep_echelon_advance = 1 
		rigid_hierarchy = 1 
		praetorian_guard = 1
		
		infantry_weapons = 1
		infantry_weapons1 = 1
		
		combat_eng_equipment = 1
		
		night_vision_1 = 1
		
		command_control_equipment = 1
		
		Early_APC = 1 #Vehicle Design
		
		APC_1 = 1
		
		IFV_1 = 1
		
		MBT_1 = 1
		MBT_2 = 1
		
		ENGI_MBT_1 = 1
		
		Rec_tank_0 = 1

		util_vehicle_0 = 1
		
		artillery_0 = 1
		SP_arty_0 = 1
		SP_arty_1 = 1
		SP_R_arty_0 = 1
		SP_R_arty_1 = 1
		
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		
		Anti_Air_0 = 1
		SP_Anti_Air_0 = 1
		
		early_fighter = 1
		
		MR_Fighter1 = 1
		
		AS_Fighter1 = 1
		
		landing_craft = 1
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 1
			}

			fascism = {
				popularity = 17
			}
			
			communism = {
				popularity = 41
				#banned = no #default is no
			}
			
			neutrality = { 
				popularity = 4
			}
			
			nationalist = { 
				popularity = 37
			}
		}
		
		ruling_party = nationalist
		last_election = "1999.4.30"
		election_frequency = 48
		elections_allowed = no
	}

	create_country_leader = {
		name = "Saddam Hussein"
		picture = "saddam_hussein.dds"
		expire = "2010.1.1"
		ideology = Nat_Autocracy
		traits = {
			military_career
			nationalist_Nat_Autocracy
			ruthless
			sly
		}
	}
	
	create_corps_commander = {
		name = "Abdul Amir Rashid Yarallah"
		picture = "Irq_Abdul_Amir_Rashid_Yarallah.dds"
		traits = { urban_assault_specialist }
		id = 30000
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
		
	}

	create_corps_commander = {
		name = "Jassem Nazal Qassim"
		picture = "irq_tank_Jassem_Nazal_Qassim.dds"
		traits = { panzer_leader }
		id = 30001
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
		
	}

	create_corps_commander = {
		name = "Fadhil Jamil al-Barwari"
		picture = "Iraq_Fadhil_Jamil_al-Barwari.dds"
		traits = { desert_fox }
		id = 30002
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
		
	}
	create_field_marshal = {
		name = "Othman Al Ghanimi"
		picture = "Portrait_Othman_Al_Ghanimi.dds"
		traits = { old_guard thorough_planner }
		id = 30003
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_field_marshal = {
		name = "Babaker Zebari"
		picture = "Portrait_Babaker_Zebari.dds"
		traits = { offensive_doctrine }
		id = 30004
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Qassi Jassem Nazal"
		picture = "Portrait_Qassi_Jassem_Nazal.dds"
		traits = { panzer_leader }
		id = 30005
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Abud Qanbar"
		picture = "Portrait_Abud_Qanbar.dds"
		traits = { trait_engineer }
		id = 30006
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Mutaa Al Khazraji"
		picture = "Portrait_Mutaa_Al_Khazraji.dds"
		traits = { desert_fox }
		id = 30007
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Othman Ali Farhood"
		picture = "Portrait_Othman_Ali_Farhood.dds"
		traits = { ranger }
		id = 30008
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Hamid Al Maliki"
		picture = "Portrait_Hamid_Al_Maliki.dds"
		traits = { commando }
		id = 30009
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Abdel Wahal Al Saadi"
		picture = "Portrait_Abdel_Wahab_Al_Saadi.dds"
		traits = { urban_assault_specialist }
		id = 30010
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Ali Ghaidan Majid"
		picture = "Portrait_Ali_Ghaidan_Majid.dds"
		traits = { panzer_leader }
		id = 30011
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Nassir Al Hiti"
		picture = "Portrait_Nassir_Al_Hiti.dds"
		traits = { ranger }
		id = 30012
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Sabah Al Fatlawi"
		picture = "Portrait_Sabah_Al_Fatlawi.dds"
		traits = { urban_assault_specialist }
		id = 30013
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Talib Shaghati"
		picture = "Portrait_Talib_Shaghati.dds"
		traits = { commando }
		id = 30014
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_navy_leader = {
		name = "Ali Hussain Ali"
		picture = "Portrait_Ali_Hussain_Ali.dds"
		traits = { old_guard_navy superior_tactician }
		id = 30015
	}
}

2017.1.1 = {
	capital = 557
	oob = "IRQ_2017"
	set_convoys = 30
	
	# Starting tech
	set_technology = { 
		legacy_doctrines = 1 
		grand_battleplan = 1 
		deep_echelon_advance = 1 
		rigid_hierarchy = 1 
		praetorian_guard = 1
		
		infantry_weapons = 1
		infantry_weapons1 = 1
		
		combat_eng_equipment = 1
		
		night_vision_1 = 1
		night_vision_2 = 1
		
		command_control_equipment = 1
		
		land_Drone_equipment = 1
		
		Early_APC = 1 #Vehicle Design
		
		APC_1 = 1
		
		IFV_1 = 1
		
		MBT_1 = 1
		
		ENGI_MBT_1 = 1
		
		Rec_tank_0 = 1

		util_vehicle_0 = 1
		
		artillery_0 = 1
		SP_arty_0 = 1
		SP_R_arty_0 = 1
		
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		
		Anti_Air_0 = 1
		SP_Anti_Air_0 = 1
		
		early_fighter = 1
		Strike_fighter1 = 1
		
		MR_Fighter1 = 1
		MR_Fighter2 = 1
		
		AS_Fighter1 = 1
		
		L_Strike_fighter1 = 1
		
		corvette_1 = 1
		diesel_attack_submarine_1 = 1
		
		early_helicopter = 1
		attack_helicopter1 = 1
		
		landing_craft = 1
	}

	add_ideas = {
		pop_050
		Inherent_Resolve
		crippling_corruption
		gdp_3
		shia
		rentier_state
		sadrist_militias
		export_economy
		youth_radicalization
		LoAS_member
		stable_growth
		defence_04
		edu_02
		health_02
		social_02
		bureau_04
		police_04
		accountable_press
		non_secret_ballots
		state_religion
		al_jazeera_banned
		iranian_aid
		volunteer_army
		volunteer_women
		USA_usaid #https://explorer.usaid.gov/aid-dashboard.html
		fossil_fuel_industry
		the_military
		iranian_quds_force
		hybrid
		tax_cost_36
		
	}
	
	set_country_flag = Major_Importer_US_Arms #Trends-in-international-arms-transfers-2016.pdf
	set_country_flag = enthusiastic_fossil_fuel_industry
	set_country_flag = positive_the_military
	
	set_variable = { var = debt value = 114.6 }
	set_variable = { var = treasury value = 44.87 }
	set_variable = { var = int_investments value = 0.9 }
	set_variable = { var = tax_rate value = 36 }
	
	set_variable = { var = size_modifier value = 0.38 } #4 CIC
	initial_money_setup = yes

	#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
		

	add_opinion_modifier = { target = PER modifier = resistance_axis }
	add_opinion_modifier = { target = SYR modifier = resistance_axis }

	add_opinion_modifier = { target = KUR modifier = IRQ_Dont_Support_Kurdish_Independence }
	add_opinion_modifier = { target = ROJ modifier = IRQ_Dont_Support_Kurdish_Independence }

	add_opinion_modifier = { target = SAU modifier = hostile_status }
	add_opinion_modifier = { target = QAT modifier = hostile_status }
	add_opinion_modifier = { target = KUW modifier = hostile_status }
	add_opinion_modifier = { target = UAE modifier = hostile_status }

	diplomatic_relation = {
		country = SYR
		relation = military_access
		active = yes
	}

	set_politics = {

		parties = {
			democratic = { 
				popularity = 15
			}

			fascism = {
				popularity = 17
			}
			
			communism = {
				popularity = 45
				#banned = no #default is no
			}
			
			neutrality = { 
				popularity = 0
			}
			
			nationalist = { 
				popularity = 23
			}
		}
		
		ruling_party = communism
		last_election = "2014.4.30"
		election_frequency = 48
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Ahmed Abu Risha"
		picture = "IRQ_ind_or_western_Ahmed_Abu_Risha.dds"
		expire = "2050.1.1"
		ideology = Western_Autocracy
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Muqtada al-Sadr"
		picture = "Muqtada_al-Sadr.dds"
		expire = "2050.1.1"
		ideology = Nat_Populism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Ayad Allawi"
		picture = "ayad_allawi.dds"
		expire = "2050.1.1"
		ideology = Conservative
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Haider al-Abadi"
		picture = "Haider_al-Abadi.dds"
		expire = "2050.1.1"
		ideology = Conservative
		traits = {
			#
		}
	}

	

}