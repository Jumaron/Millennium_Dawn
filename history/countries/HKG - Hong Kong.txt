﻿2000.1.1 = {
	capital = 595
	oob = "HKG_2000"
	
	set_convoys = 30
	
	add_ideas = {
		gdp_8
	}

}

2017.1.1 = {
	capital = 595
	oob = "HKG_2017"
	set_convoys = 30
	
	add_ideas = {
		pop_050
		#small_medium_business_owners
		#Labour_Unions
		#Mass_Media
		modest_corruption
		gdp_7
		pluralist
			stable_growth
			defence_00
		edu_03
		health_02
		social_02
		bureau_01
		police_04
		volunteer_army
		volunteer_women
		international_bankers
		small_medium_business_owners
		hybrid
		tax_cost_16
	}
	
	#set_country_flag = gdp_8
	set_country_flag = enthusiastic_international_bankers
	set_country_flag = positive_small_medium_business_owners
	set_country_flag = positive_landowners
	
	set_variable = { var = debt value = 0  }
	set_variable = { var = treasury value = 382 }
	set_variable = { var = int_investments value = 457 }
	set_variable = { var = tax_rate value = 16 }

	#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_15K_GDPC_slot
	complete_national_focus = Generic_30K_GDPC_slot
	
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
	 
		#No army, but let's give them some basic tech in case they change their mind
		infantry_weapons = 1
		command_control_equipment = 1
		Anti_tank_0 = 1
		Anti_Air_0 = 1
		util_vehicle_0 = 1
		
		night_vision_1 = 1
	}
			
	set_politics = {

		parties = {
			democratic = { 
				popularity = 95
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 0
			}
			
			neutrality = {
				popularity = 2
			}
			
			nationalist = {
				popularity = 3
			}
		}
		
		ruling_party = democratic
		last_election = "2015.3.1"
		election_frequency = 48
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Carrie Lam"
		picture = "HON_CARRIE_LAM.dds"
		expire = "2055.1.1"
		ideology = conservatism
		traits = {
		}
	}
}