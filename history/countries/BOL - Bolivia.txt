﻿2000.1.1 = {
	capital = 897
	oob = "BOL_2000"
	set_convoys = 16
	
	add_ideas = {
		gdp_4
		tax_cost_12
	}
	
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		night_vision_1 = 1
		night_vision_2 = 1

		#For Templates
		infantry_weapons = 1
		
		combat_eng_equipment = 1
		command_control_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		artillery_0 = 1
		Anti_Air_0 = 1
		Early_APC = 1
		APC_1 = 1
		util_vehicle_0 = 1
		Rec_tank_0 = 1
	}
	
	set_variable = { var = debt value = 8 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = treasury value = 2 }
	set_variable = { var = tax_rate value = 12 }
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 17
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 21
			}
			
			neutrality = {
				popularity = 23
			}
			
			nationalist = {
				 popularity = 21
			}
		}
		
		ruling_party = neutrality
		last_election = "1999.6.13"
		election_frequency = 48
		elections_allowed = yes
	}
	
	create_country_leader = {
		name = "Evo Morales"
		desc = ""
		picture = "BOL_evo-morales.dds"
		expire = "2065.1.1"
		ideology = anarchist_communism
		traits = {
			#
		}
	}
	
	create_country_leader = {
		name = "Manfred Reyes Villa"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "BOL_Manfred_Reyes_Villa"
		expire = "2065.1.1"
		ideology = Nat_Populism
		traits = {
		
		}
	}
	create_country_leader = {
		name = "Jaime Paz Zamora"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "BOL_Jaime_Paz_Zamora"
		expire = "2065.1.1"
		ideology = socialism
		traits = {
		
		}
	}
	
	create_country_leader = {
		name = "Hugo Banzer"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "hugo_banzer.dds"
		expire = "2001.7.8"
		ideology = neutral_Social
		traits = {
		
		}
	}
	
	create_field_marshal = {
		name = "Luis Orlando Ariñez Bazán"
		picture = "Portrait_Luis_Bazan.dds"
		traits = { organisational_leader }
		id = 7800
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_field_marshal = {
		name = "Marcelo Antezana"
		picture = "Portrait_Marcelo_Antezana.dds"
		traits = { old_guard inspirational_leader }
		id = 7801
		skill = 4
		attack_skill = 4
		defense_skill = 4
		planning_skill = 4
		logistics_skill = 4
	}

	create_field_marshal = {
		name = "Óscar Naranjo"
		picture = "Portrait_Oscar_Naranjo.dds"
		traits = { old_guard offensive_doctrine }
		id = 7802
		skill = 4
		attack_skill = 4
		defense_skill = 4
		planning_skill = 4
		logistics_skill = 4
	}

	create_field_marshal = {
		name = "Juan Gonzalo Duran Flores"
		picture = "Portrait_Juan_Flores.dds"
		traits = { old_guard fast_planner }
		id = 7803
		skill = 4
		attack_skill = 4
		defense_skill = 4
		planning_skill = 4
		logistics_skill = 4
	}

	create_corps_commander = {
		name = "Carlos Erik Rück Arzabe"
		picture = "Portrait_Carlos_Arzabe.dds"
		traits = { panzer_leader }
		id = 7804
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Melvin Arteaga Aguada"
		picture = "Portrait_Melvin_Aguada.dds"
		traits = { trait_engineer }
		id = 7805
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Abel Galo de la Barra Cáceres"
		picture = "Portrait_Abel_Caceres.dds"
		traits = { urban_assault_specialist }
		id = 7806
		skill = 4
		attack_skill = 4
		defense_skill = 4
		planning_skill = 4
		logistics_skill = 4
	}

	create_corps_commander = {
		name = "Gina Reque Terán"
		picture = "Portrait_Gina_Teran.dds"
		traits = { ranger }
		id = 7807
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Luis Fernando Aramayo Mercado"
		picture = "Portrait_Luis_Mercado.dds"
		traits = { trickster }
		id = 7808
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Felipe Eduardo Vasquez Moya"
		picture = "Portrait_Felipe_Moya.dds"
		traits = { trait_engineer }
		id = 7809
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Guillermo Chalup Liendo"
		picture = "Portrait_Guillermo_Liendo.dds"
		traits = { fortress_buster }
		id = 7810
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_navy_leader = {
		name = "José Manuel Puente Guarachi"
		picture = "Portrait_Jose_Guarachi.dds"
		traits = { old_guard_navy superior_tactician }
		id = 7811
	}

}

2017.1.1 = {
	capital = 897
	oob = "BOL_2017"
	set_convoys = 16
	
	set_variable = { var = debt value = 19 }
	set_variable = { var = int_investments value = 1.2 }
	set_variable = { var = treasury value = 10 }
	set_variable = { var = tax_rate value = 24 }
	
	set_variable = { var = size_modifier value = 0.23 } #3 CIC
	initial_money_setup = yes

	# Starting tech
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		night_vision_1 = 1
		night_vision_2 = 1

		#For Templates
		infantry_weapons = 1
		
		combat_eng_equipment = 1
		command_control_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		artillery_0 = 1
		Anti_Air_0 = 1
		Early_APC = 1
		APC_1 = 1
		util_vehicle_0 = 1
		Rec_tank_0 = 1
	}

	add_ideas = {
		pop_050
		unrestrained_corruption
		christian
		gdp_3
			fast_growth
			defence_02
		export_economy
		edu_04
		health_04
		social_03
		bureau_01
		police_03
		non_secret_ballots
		accountable_press
		draft_army
		drafted_women
		international_bankers
		industrial_conglomerates
		civil_law
		cartels_3
		tax_cost_24
	}
	
	set_country_flag = bud_neg_8
	set_country_flag = positive_international_bankers
	set_country_flag = positive_industrial_conglomerates
	set_country_flag = positive_fossil_fuel_industry

	#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot

		set_politics = {

		parties = {
			democratic = { 
				popularity = 38
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 52
			}
			
			neutrality = { 
				popularity = 10
			}
		}
		
		ruling_party = communism
		last_election = "2014.10.12"
		election_frequency = 60
		elections_allowed = yes
	}
	
	create_country_leader = {
		name = "Jorge Quiroga"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "BOL_Jorge_Quiroga"
		expire = "2002.6.8"
		ideology = neutral_Social
		traits = {
		
		}
	}
	
	create_country_leader = {
		name = "Gonzalo Sánchez de Lozada"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "BOL_Gonzalo_Sanchez_de_Lozada"
		expire = "2065.1.1"
		ideology = neutral_Social
		traits = {
		
		}
	}
}