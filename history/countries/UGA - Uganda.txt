﻿2000.1.1 = {
	capital = 249
	oob = "UGA_2000"
	set_convoys = 20
	
	add_ideas = {
		gdp_1
		tax_cost_17
		multi_ethnic_state_idea
	}
	
	set_variable = { var = debt value = 6 }
	set_variable = { var = treasury value = 1 }
	set_variable = { var = tax_rate value = 17 }
	set_variable = { var = int_investments value = 0 }
	
	set_autonomy = {
		target = MLC
		autonomy_state = autonomy_satellite_state
	}
	
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		
		infantry_weapons = 1
		command_control_equipment = 1
		land_Drone_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		Anti_Air_0 = 1
		combat_eng_equipment = 1
		
		Early_APC = 1
		MBT_1 = 1
		IFV_1 = 1
		APC_1 = 1
		Air_APC_1 = 1
		Rec_tank_0 = 1
		util_vehicle_0 = 1
		SP_arty_0 = 1
		artillery_0 = 1
		SP_R_arty_0 = 1
		
		early_helicopter = 1
		transport_helicopter1 = 1
		SP_Anti_Air_0 = 1
		
		ENGI_MBT_1 = 1
		
		early_fighter = 1
		MR_Fighter1 = 1
		CV_MR_Fighter1 = 1
		
		night_vision_1 = 1
		
	}

	set_politics = {

		parties = {
			democratic = { 
				popularity = 80
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 15
				#banned = no #default is no
			}
			
			neutrality = { 
				popularity = 5
			}
		}
		
		ruling_party = democratic
		last_election = "2016.2.18"
		election_frequency = 60
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Yoweri Museveni"
		desc = "POLITICS_Mohamed Cheikh Biadillah_DESC" 
		picture = "Yoweri_Museveni.dds"
		expire = "2050.1.1"
		ideology = Western_Autocracy
		traits = {
			#
		}
	}
	
	create_corps_commander = {
		name = "Byomere Livingstone"
		picture = "gfx/leaders/Generic_Africa/african_gen_12.dds"
		traits = { commando }
		id = 67500
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "David Muhoozi"
		picture = "UGA_David_Muhoozi.dds"
		traits = { jungle_rat }
		id = 67501
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Edward Katumba Wamala"
		picture = "UGA_Edward_Katumba_Wamala.dds"
		traits = { urban_assault_specialist panzer_leader }
		id = 67502
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}
	create_corps_commander = {
		name = "Elly Tumwine"
		picture = "UGA_Elly_Tumwine.dds"
		traits = { hill_fighter ranger }
		id = 67503
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}
	create_corps_commander = {
		name = "Patrick Kankiriho"
		picture = "generals/Patrick_Kankiriho.dds"
		id = 67504
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}
	create_corps_commander = {
		name = "Nathan Mugisha"
		picture = "generals/Nathan_Mugisha.dds"
		id = 67505
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Kasirye Ggwanga"
		picture = "UGA_Kasirye_Ggwanga.dds"
		traits = { jungle_rat }
		id = 67506
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Muhoozi Kainerugaba"
		picture = "UGA_Muhoozi_Kainerugaba.dds"
		traits = { commando jungle_rat }
		id = 67507
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}
	
}

2017.1.1 = {
	capital = 249
	oob = "UGA_2017"
	set_convoys = 20
	
	add_ideas = {
		pop_050
		rampant_corruption
		christian
		gdp_1
		stable_growth
		defence_02
		edu_01
		health_01
		social_02
		bureau_01
		police_02
		volunteer_army
		no_women_in_military
		USA_usaid #https://explorer.usaid.gov/aid-dashboard.html
		international_bankers
		small_medium_business_owners
		farmers
		hybrid
		tax_cost_12
		multi_ethnic_state_idea
	}
	
	#set_country_flag = gdp_1
	set_country_flag = positive_international_bankers
	set_country_flag = positive_small_medium_business_owners
	set_country_flag = negative_farmers
	
	set_variable = { var = debt value = 10 }
	set_variable = { var = treasury value = 3 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = tax_rate value = 12 }
	
	set_variable = { var = size_modifier value = 0.23 } #3 CIC
	initial_money_setup = yes
	
	#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4_IC_slot
	
	# Starting tech
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		
		infantry_weapons = 1
		command_control_equipment = 1
		land_Drone_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		Anti_Air_0 = 1
		combat_eng_equipment = 1
		
		Early_APC = 1
		MBT_1 = 1
		IFV_1 = 1
		APC_1 = 1
		Air_APC_1 = 1
		Rec_tank_0 = 1
		util_vehicle_0 = 1
		SP_arty_0 = 1
		artillery_0 = 1
		SP_R_arty_0 = 1
		
		early_helicopter = 1
		transport_helicopter1 = 1
		SP_Anti_Air_0 = 1
		
		ENGI_MBT_1 = 1
		
		early_fighter = 1
		MR_Fighter1 = 1
		CV_MR_Fighter1 = 1
		
		night_vision_1 = 1
		
	}
}