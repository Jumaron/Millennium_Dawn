﻿2000.1.1 = {
	capital = 391
	oob = "LBA_2000"
	set_convoys = 20
	
	add_ideas = {
		gdp_7
		tax_cost_10
	}
	
	set_variable = { var = debt value = 4 }
	set_variable = { var = treasury value = 20 }
	set_variable = { var = tax_rate value = 10 }
	set_variable = { var = int_investments value = 0 }

	
	set_technology = { 
		#For templates
		infantry_weapons = 1
		command_control_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		artillery_0 = 1
		SP_arty_0 = 1
		Anti_Air_0 = 1
		early_APC = 1
		APC_1 = 1
		MBT_1 = 1
		IFV_1 = 1
		util_vehicle_0 = 1

		landing_craft = 1
		
		submarine_1 = 1
		diesel_attack_submarine_1 = 1
		cruiser_1 = 1
		cruiser_2 = 1
		destroyer_1 = 1
		frigate_1 = 1
		frigate_2 = 1
		corvette_1 = 1
		corvette_2 = 1
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 25
			}

			fascism = {
				popularity = 10
			}
			
			communism = {
				popularity = 45
				#banned = no #default is no
			}
			
			neutrality = { 
				popularity = 20
			}
		}
		
		ruling_party = communism
		last_election = "1936.1.1"
		election_frequency = 48
		elections_allowed = no
	}
	
	create_country_leader = {
		name = "Muammar Gaddafi"
		picture = "gaddafi.dds"
		ideology = anarchist_communism
		traits = {
			#
		}
	}
	
	create_field_marshal = {
		name = "Khalifa Haftar"
		picture = "generals/Khalifa_Haftar.dds"
		traits = { old_guard thorough_planner }
		id = 35100
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = { 
		name = "Yousef Mangoush"
		picture = "generals/Yousef_Mangoush.dds"
		traits = { panzer_leader trait_engineer }
		id = 35101
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = { 
		name = "Khamis Gaddafi"
		picture = "generals/Khamis_Gaddafi.dds"
		traits = { desert_fox }
		id = 35102
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = { 
		name = "Suleiman Mahmoud"
		picture = "generals/Suleiman_Mahmoud.dds"
		traits = { ranger }
		id = 35103
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = { 
		name = "Abu-Bakr Younis Jaber"
		picture = "generals/Abubakr_Jaber.dds"
		traits = { commando }
		id = 35104
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = { 
		name = "Mohamed El-Mismari"
		picture = "generals/Mohamed_El_Mismari.dds"
		traits = { desert_fox }
		id = 35105
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = { 
		name = "Fatah Younis Al-Obeidi"
		picture = "generals/Fatah_Al-Obeidi.dds"
		traits = { trickster urban_assault_specialist }
		id = 35106
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = { 
		name = "Ahmed Oun"
		picture = "generals/Ahmed_Oun.dds"
		traits = { trait_engineer }
		id = 35107
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = { 
		name = "Mohammed Bougfir"
		picture = "generals/Mohammed_Bougfir.dds"
		traits = { bearer_of_artillery }
		id = 35108
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = { 
		name = "Al-Saadi Gaddafi"
		picture = "generals/Al-Saadi_Gaddafi.dds"
		traits = { ranger }
		id = 35109
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = { 
		name = "Massoud Abdelhafid"
		picture = "generals/Massoud_Abdelhafid.dds"
		traits = { trait_mountaineer }
		id = 35110
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = { 
		name = "Al-Mahdi Al-Barghathi"
		picture = "generals/Al-Mahdi_Al-Barghathi.dds"
		traits = { ranger hill_fighter }
		id = 35111
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_navy_leader = { 
		name = "Abdolazim Ahmad"
		picture = "admirals/Abdolazim_Ahmad.dds"
		traits = { old_guard_navy superior_tactician }
		id = 35112
	}
}

2017.1.1 = {
	capital = 391
	oob = "LBA_2017"
	set_convoys = 20
	
	add_ideas = {
		pop_050
		crippling_corruption
		gdp_4
		sunni
		LoAS_member
		
		depression
		defence_08
		edu_02
		health_02
		social_01
		bureau_03
		police_01
		rentier_state
		export_economy
		volunteer_army
		volunteer_women
		hybrid
		tax_cost_01
	}
	
	#set_country_flag = gdp_4
	
	set_variable = { var = debt value = }
	set_variable = { var = treasury value = 66 }
	set_variable = { var = int_investments value = 66 }
	set_variable = { var = tax_rate value = 1 }

	#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	
	# Starting tech
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		#For templates
		infantry_weapons = 1
		command_control_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		artillery_0 = 1
		SP_arty_0 = 1
		Anti_Air_0 = 1
		early_APC = 1
		APC_1 = 1
		MBT_1 = 1
		IFV_1 = 1
		util_vehicle_0 = 1

		landing_craft = 1
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 30
			}

			fascism = {
				popularity = 35
			}
			
			communism = {
				popularity = 0
				#banned = no #default is no
			}
			
			neutrality = { 
				popularity = 35
			}
		}
		
		ruling_party = democratic
		last_election = "1936.1.1"
		election_frequency = 48
		elections_allowed = no
	}

	#Update to new unity gov (and civ. war) planned

	create_country_leader = {
		name = "Aguila Saleh Issa"
		desc = ""
		picture = "Emerging_HoR_Aguila_Saleh_Issa.dds"
		ideology = Conservative
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Nouri Abusahmain"
		desc = ""
		picture = "Emerging_GNC_Nouri_Abusahmain.dds"
		ideology = Neutral_Muslim_Brotherhood 
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Fayez al-Sarraj"
		desc = ""
		picture = "Western_GNA_Fayez_al_Sarraj.dds"
		ideology = Western_Autocracy
		traits = {
			#
		}
	}
}