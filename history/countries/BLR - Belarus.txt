﻿2000.1.1 = {
	capital = 703
	oob = "BLR_2000"
	set_convoys = 5
	
	add_ideas = {
		gdp_4
		tax_cost_25
	}
	
	set_variable = { var = debt value = 3 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = treasury value = 0 }
	set_variable = { var = tax_rate value = 25 }
	
	set_technology = { 
		legacy_doctrines = 1 
		armoured_mass_assault = 1 
		deep_echelon_advance = 1 
		army_group_operational_freedom = 1 
		massed_artillery = 1
		artillery_0 = 1
		SP_arty_0 = 1
		SP_R_arty_0 = 1
		
		night_vision_1 = 1
		night_vision_2 = 1
		
		#For templates
		infantry_weapons = 1
		
		combat_eng_equipment = 1
		command_control_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		Anti_Air_0 = 1
		SP_Anti_Air_0 = 1
		Early_APC = 1
		APC_1 = 1
		Air_APC_1 = 1
		IFV_1 = 1
		Air_IFV_1 = 1
		util_vehicle_0 = 1
		MBT_1 = 1
		ENGI_MBT_1 = 1
		early_helicopter = 1
		transport_helicopter1 = 1
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 3
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 79
			}
			
			neutrality = {
				popularity = 18
			}
			
			nationalist = {
				 popularity = 0
			}
		}
		
		ruling_party = democratic
		last_election = "1999.6.13"
		election_frequency = 48
		elections_allowed = yes
	}
	
	create_country_leader = {
		name = "Vladimir Goncharik"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = ""
		expire = "2065.1.1"
		ideology = Neutral_conservatism
		traits = {
		
		}
	}
	create_country_leader = {
		name = "Sergei Gaidukevich"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = ""
		expire = "2065.1.1"
		ideology = liberalism
		traits = {
		
		}
	}
	create_country_leader = {
		name = "Alexander Lukashenko"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "BLR_Alexander_Lukashenko.dds"
		expire = "2065.1.1"
		ideology = Autocracy
		traits = {
		
		}
	}
	
	create_field_marshal = {
		name = "Andrei Ravkov"
		picture = "Portrait_Andrei_Ravkov.dds"
		traits = { old_guard organisational_leader }
		id = 7200
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_field_marshal = {
		name = "Oleg Belokonev"
		picture = "Portrait_Oleg_Belokonev.dds"
		traits = { fast_planner }
		id = 7201
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_field_marshal = {
		name = "Sergei Potapenko"
		picture = "Portrait_Sergei_Potapenko.dds"
		traits = { defensive_doctrine }
		id = 7202
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_field_marshal = {
		name = "Igor Lotenkov"
		picture = "Portrait_Igor_Lotenkov.dds"
		traits = { offensive_doctrine }
		id = 7203
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Vitaly Kireyev"
		picture = "Portrait_Vitaly_Kireyev.dds"
		traits = { trait_engineer }
		id = 7204
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Viktor Hrenin"
		picture = "Portrait_Viktor_Hrenin.dds"
		traits = { panzer_leader }
		id = 7205
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Ruslan Kosygin"
		picture = "Portrait_Ruslan_Kosygin.dds"
		traits = { trait_engineer }
		id = 7206
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Andrei Zhuk"
		picture = "Portrait_Andrei_Zhuk.dds"
		traits = { ranger }
		id = 7207
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Anatolyi Bulavko"
		picture = "Portrait_Anatoliy_Bulavko.dds"
		traits = { fortress_buster }
		id = 7208
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Andrei Fedin"
		picture = "Portrait_Andrei_Fedin.dds"
		traits = { hill_fighter }
		id = 7209
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Oreh Vladimir"
		picture = "Portrait_Oreh_Vladimir.dds"
		traits = { fortress_buster }
		id = 7210
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Alexander Volfovich"
		picture = "Portrait_Alexander_Volfovich.dds"
		traits = { panzer_leader }
		id = 7211
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Valeriy Gnilozub"
		picture = "Portrait_Valery_Gnilozub.dds"
		traits = { trait_engineer }
		id = 7212
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Vladimir Kulazhin"
		picture = "Portrait_Vladimir_Khulazin.dds"
		traits = { ranger }
		id = 7213
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Alexander Gurin"
		picture = "Portrait_Alexander_Gurin.dds"
		traits = { fortress_buster }
		id = 7214
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Igor Kuzmuk"
		picture = "Portrait_Igor_Kuzmuk.dds"
		traits = { hill_fighter }
		id = 7215
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Vitaly Shkadrovich"
		picture = "Portrait_Vitaly_Shkadrovich.dds"
		traits = { ranger }
		id = 7216
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Vadim Denisenko"
		picture = "Portrait_Vadim_Denisenko.dds"
		traits = { commando urban_assault_specialist }
		id = 7217
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Igor Danilchik"
		picture = "Portrait_Igor_Danilchik.dds"
		traits = { trickster }
		id = 7218
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Vyacheslav Starkov"
		picture = "Portrait_Vyacheslav_Starkov.dds"
		traits = { trait_engineer }
		id = 7219
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Dmitry Lukyanenko"
		picture = "Portrait_Dmitry_Lukyanenko.dds"
		traits = { trait_engineer }
		id = 7220
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Oleg Dvigalev"
		picture = "Portrait_Oleg_Dvigalev.dds"
		traits = { commando }
		id = 7221
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Sergei Trus"
		picture = "Portrait_Sergei_Trus.dds"
		traits = { swamp_fox }
		id = 7222
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Igor Golub"
		picture = "Portrait_Igor_Golub.dds"
		traits = { swamp_fox }
		id = 7223
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Alexander Karev"
		picture = "Portrait_Alexander_Karev.dds"
		traits = { urban_assault_specialist }
		id = 7224
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Valery Shevchenko"
		picture = "Portrait_Valery_Shevchenko.dds"
		traits = { trait_mountaineer }
		id = 7225
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Alexander Astrauh"
		picture = "Portrait_Alexander_Astrauh.dds"
		traits = { bearer_of_artillery }
		id = 7226
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Alexander Panfyorov"
		picture = "Portrait_Alexander_Panfyorov.dds"
		traits = { winter_specialist }
		id = 7227
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
}

2017.1.1 = {
	capital = 703
	oob = "BLR_2017"
	set_convoys = 5
	
	set_variable = { var = debt value = 27 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = treasury value = 5 }
	set_variable = { var = tax_rate value = 24 }
	
	set_variable = { var = size_modifier value = 0.38 } #4 CIC
	initial_money_setup = yes
	
	
	add_ideas = {
		pop_050
		#The_Military
		#The_Intelligence_Community
		#National_Police
		orthodox_christian
		unrestrained_corruption
		gdp_4
		stagnation
		defence_01
		edu_04
		health_03
		social_03
		bureau_02
		police_05
		parties_harassment
		state_press
		partial_draft_army
		volunteer_women
		medium_far_right_movement
		oligarchs
		small_medium_business_owners
		fossil_fuel_industry
		civil_law
		tax_cost_24
	}

	set_country_flag = positive_small_medium_business_owners
	set_country_flag = positive_fossil_fuel_industry

	#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot
	
	set_technology = { 
		#Polones MLRS
		Arty_upgrade_1 = 1
		Arty_upgrade_2 = 1
		Arty_upgrade_3 = 1
		SP_arty_1 = 1
		SP_arty_2 = 1
		SP_R_arty_1 = 1
		SP_R_arty_2 = 1
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 4
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 85
			}
			
			neutrality = {
				popularity = 11
			}
			
			nationalist = {
				popularity = 0
			}
		}
		
		ruling_party = communism
		last_election = "2015.10.11"
		election_frequency = 60
		elections_allowed = yes # none after offset 1
	}
	
	create_country_leader = {
		name = "Anatoly Lebedko"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "anatoly_lebedko.dds"
		expire = "2065.1.1"
		ideology = liberalism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Tatsiana Karatkevich"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "tatsiana_karatkevich.dds"
		expire = "2065.1.1"
		ideology = neutral_Social
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Nikolai Ulakhovich"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "nikolai_ulakhovich.dds"
		expire = "2065.1.1"
		ideology = Nat_Autocracy
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Alexander Lukashenko"
		desc = ""
		picture = "BLR_Alexander_Lukashenko.dds"
		expire = "2065.1.1"
		ideology = Autocracy
		traits = {
			#
		}
	}
	
}