﻿2000.1.1 = {
	capital = 185
	oob = "DRU_2000"
	
	set_convoys = 120
}

2017.1.1 = {
	capital = 185
	oob = "DRU_2017"
	set_convoys = 120
	
	add_ideas = {
		pop_050
		gdp_4
		shia
	}
	#set_country_flag = gdp_4

	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	
	# Starting tech
	set_technology = { 
		legacy_doctrines = 1
		armoured_mass_assault = 1 
		deep_echelon_advance = 1 
		rigid_hierarchy = 1 
		praetorian_guard = 1

		infantry_weapons = 1

		command_control_equipment = 1
		util_vehicle_0 = 1	
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		Anti_Air_0 = 1
	}

	set_politics = {

		parties = {
			democratic = { 
				popularity = 10
			}

			fascism = {
				popularity = 10
			}
			
			communism = {
				popularity = 10
			}
			
			neutrality = { 
				popularity = 50
			}
			
			nationalist = {
				popularity = 10
			}
		}
		
		ruling_party = neutrality
		last_election = "2017.1.1"
		election_frequency = 60
		elections_allowed = yes
	}
}