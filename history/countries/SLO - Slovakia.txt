﻿2000.1.1 = {
	capital = 119
	oob = "SLO_2000"
	set_convoys = 150
	
	add_ideas = {
		gdp_5
		landowners
		industrial_conglomerates
		small_medium_business_owners
		civil_law
		tax_cost_18
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 49
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 32
			}
			
			neutrality = {
				popularity = 6
			}
			
			nationalist = {
				popularity = 13
			}
		}
		
		ruling_party = democratic
		last_election = "1998.9.25"
		election_frequency = 48
		elections_allowed = yes
	}
	
	create_country_leader = {
		name = "Rudolf Schuster"
		desc = "POLITICS_EARL_BROWDER_DESC"
		picture = "rudolf_schuster.dds"
		expire = "2055.1.1"
		ideology = conservatism
		traits = {
			
		}
	}
	
	set_variable = { conservatism_pop = 0.34 }
	set_variable = { socialism_pop = 0.15 }
	set_variable = { Communist-State_pop = 0.05 }
	set_variable = { Conservative_pop = 0.27 }
	set_variable = { Neutral_Libertarian_pop = 0.06 }
	set_variable = { Nat_Fascism_pop = 0.03 }
	set_variable = { Nat_Populism_pop = 0.10 }
	
	recalculate_party = yes
	
	set_technology = { 
		legacy_doctrines = 1 
		armoured_mass_assault = 1 
		deep_echelon_advance = 1 
		army_group_operational_freedom = 1 
		massed_artillery = 1
		infantry_weapons = 1
		
		command_control_equipment = 1
		
		land_Drone_equipment = 1
		
		Anti_tank_0 = 1
		
		Heavy_Anti_tank_0 = 1
		
		Anti_Air_0 = 1
		
		combat_eng_equipment = 1
		
		Early_APC = 1
		MBT_1 = 1
		
		ENGI_MBT_1 = 1
		
		IFV_1 = 1
		
		APC_1 = 1
		
		util_vehicle_0 = 1
		
		SP_arty_0 = 1
		SP_arty_1 = 1
		
		artillery_0 = 1
		Arty_upgrade_1 = 1
		artillery_1 = 1
		
		SP_R_arty_0 = 1
		SP_R_arty_1 = 1
		
		early_helicopter = 1
		transport_helicopter1 = 1
		
		SP_Anti_Air_0 = 1
		
		early_fighter = 1
		MR_Fighter1 = 1
		
		night_vision_1 = 1
	}
	
	set_country_flag = positive_landowners
	set_country_flag = positive_industrial_conglomerates
	set_country_flag = positive_small_medium_business_owners
	
	set_variable = { var = debt value = 15 }
	set_variable = { var = treasury value = 6 }
	set_variable = { var = tax_rate value = 18 }
	set_variable = { var = int_investments value = 0 }
	
	create_field_marshal = {
		name = "Milan Maxim"
		picture = "Portrait_Milan_Maxim.dds"
		traits = { old_guard organisational_leader }
		id = 56400
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_field_marshal = {
		name = "Pavel Macko"
		picture = "Portrait_Pavel_Macko.dds"
		traits = { logistics_wizard }
		id = 56401
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Ondřej Novosad"
		picture = "Portrait_Ondrej_Novosad.dds"
		traits = { panzer_leader }
		id = 56402
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Jindřich Joch"
		picture = "Portrait_Jindrich_Joch.dds"
		traits = { trickster }
		id = 56403
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Juraj Krištofovič"
		picture = "Portrait_Juraj_Kristofovic.dds"
		traits = { hill_fighter }
		id = 56404
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Ivan Balog"
		picture = "Portrait_Ivan_Balog.dds"
		traits = { ranger }
		id = 56405
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Jan Dreveniak"
		picture = "Portrait_Jan_Dreveniak.dds"
		traits = { trait_engineer }
		id = 56406
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Miroslav Kocian"
		picture = "Portrait_Miroslav_Kocian.dds"
		traits = { commando }
		id = 56407
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Jan Bujnak"
		picture = "Portrait_Jan_Bujnak.dds"
		traits = { trait_engineer }
		id = 56408
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Jan Parimucha"
		picture = "Portrait_Jan_Parimucha.dds"
		traits = { urban_assault_specialist }
		id = 56409
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Martin Stoklasa"
		picture = "Portrait_Martin_Stoklasa.dds"
		traits = { panzer_leader }
		id = 56410
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Miroslav Korba"
		picture = "Portrait_Miroslav_Korba.dds"
		traits = {  }
		id = 56411
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
}

2017.1.1 = {
	capital = 119
	set_convoys = 150
	oob = "SLO_2017"

	set_technology = { 
		APC_2 = 1
		APC_3 = 1
		APC_4 = 1 #Tatrapan
		
		SP_arty_2 = 1

	}

	set_variable = { var = debt value = 48 }
	set_variable = { var = treasury value = 3 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = tax_rate value = 32 }
	
	set_variable = { var = size_modifier value = 0.38 } #4 CIC
	initial_money_setup = yes

	add_ideas = {
		pop_050
		widespread_corruption
		gdp_7
		christian
		EU_member
		fast_growth
		defence_01
		edu_03
		health_04
		social_04
		bureau_03
		police_04
		volunteer_army
		volunteer_women
		NATO_member
		western_country
		large_far_right_movement
		landowners
		industrial_conglomerates
		small_medium_business_owners
		civil_law
		tax_cost_32
	}
	
	#set_country_flag = gdp_7
	set_country_flag = positive_landowners
	set_country_flag = positive_industrial_conglomerates
	set_country_flag = positive_small_medium_business_owners

	#NATO military access
	diplomatic_relation = {
		country = ALB
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = BEL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = BUL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CAN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CRO
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = CZH
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = DEN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = EST
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = FRA
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = GER
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = GRE
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = HUN
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ICE
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ITA
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = LAT
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = LIT
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = LUX
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = HOL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = NOR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = POL
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = POR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ROM
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = SLV
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = SPR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = TUR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = ENG
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = USA
		relation = military_access
		active = yes
	}

	#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_15K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot

	set_politics = {

		parties = {
			democratic = { 
				popularity = 65
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 10
			}
			
			neutrality = {
				popularity = 15
			}
			
			nationalist = {
				popularity = 10
			}
		}
		
		ruling_party = democratic
		last_election = "2016.3.5"
		election_frequency = 48
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Igor Matovic"
		desc = "POLITICS_EARL_BROWDER_DESC"
		picture = "igor_matovic.dds"
		expire = "2055.1.1"
		ideology = conservatism
		traits = {
			
		}
	}

	create_country_leader = {
		name = "Andrej Danko"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "andrej_danko.dds"
		expire = "2060.1.1"
		ideology = conservative
		traits = {
			
		}
	}

	create_country_leader = {
		name = "Richard Sulik"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "richard_sulik.dds"
		expire = "2060.1.1"
		ideology = Neutral_Libertarian
		traits = {
			
		}
	}

	create_country_leader = {
		name = "Marian Kotleba"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "marian_kotleba.dds"
		expire = "2065.1.1"
		ideology = Nat_Fascism
		traits = {
			
		}
	}

	create_country_leader = {
		name = "Robert Fico"
		desc = ""
		picture = "Robert_Fico.dds"
		expire = "2050.1.1"
		ideology = socialism
		traits = {
			
		}
	}
}