﻿2000.1.1 = {
	capital = 74
	oob = "SWI_2000"
	set_convoys = 20
	
	add_ideas = {
		tax_cost_15
		small_medium_business_owners
		Labour_Unions
		international_bankers
		civil_law
		gdp_8
		draft_army
		volunteer_women
		defence_02
		multi_ethnic_state_idea
	}

	set_country_flag = enthusiastic_international_bankers
	set_country_flag = positive_small_medium_business_owners
	
	set_variable = { var = debt value = 217 }
	set_variable = { var = treasury value = 78 }
	set_variable = { var = tax_rate value = 15 }
	set_variable = { var = int_investments value = 0 }
	
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		
		command_control_equipment = 1
		command_control_equipment1 = 1
		
		#Mowag Piranha
		early_APC = 1
		APC_1 = 1
		APC_2 = 1
		APC_3 = 1
		APC_4 = 1
		
		#Mowag Eagle
		util_vehicle_0 = 1
		util_vehicle_1 = 1
		util_vehicle_2 = 1
		util_vehicle_3 = 1
		util_vehicle_4 = 1
		
		#Mowag Trojan
		IFV_1 = 1
		IFV_2 = 1
		IFV_3 = 1
		IFV_4 = 1
		
		#SIG SG 550
		infantry_weapons = 1
		infantry_weapons1 = 1
		infantry_weapons2 = 1
		
		night_vision_1 = 1
		night_vision_2 = 1
		
		#RUAG Ranger
		land_Drone_equipment = 1
		
		early_fighter = 1
		MR_Fighter1 = 1
		CV_MR_Fighter1 = 1
		
		early_helicopter = 1
		transport_helicopter1 = 1
		
		#Pilatus PC_1
		L_Strike_fighter1 = 1
		L_Strike_fighter2 = 1
		
		#For templates
		
		combat_eng_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		artillery_0 = 1
		SP_arty_0 = 1
		Anti_Air_0 = 1
		SP_Anti_Air_0 = 1
		SP_Anti_Air_1 = 1
		MBT_1 = 1
		MBT_2 = 1
		ENGI_MBT_1 = 1
		ENGI_MBT_2 = 1
		
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 45
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 26
			}
			
			neutrality = {
				popularity = 27
			}
			
			nationalist = {
				popularity = 2
			}
		}
		
		ruling_party = neutrality
		last_election = "1999.9.24"
		election_frequency = 48
		elections_allowed = yes
	}
	
	set_variable = { conservatism_pop = 0.2 }
	set_variable = { socialism_pop = 0.43 }
	set_variable = { Conservative_pop = 0.26 }
	set_variable = { neutral_Social_pop = 0.01 }
	set_variable = { Neutral_Libertarian_pop = 0.2 }
	set_variable = { Neutral_Green_pop = 0.06 }
	set_variable = { Nat_Populism_pop = 0.02 }
	
	recalculate_party = yes
	
	create_country_leader = {
		name = "Adolf Ogi"
		picture = "Adolf_Ogi.dds"
		ideology = Neutral_Libertarian #should be right wing populist
	}

	
	
	create_field_marshal = {
		name = "Philippe Rebord"
		picture = "Portrait_Philippe_Rebord.dds"
		traits = { old_guard organisational_leader }
		id = 62400
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_field_marshal = {
		name = "André Blattmann"
		picture = "Portrait_Andre_Blattmann.dds"
		traits = { thorough_planner }
		id = 62401
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Aldo C. Schellenberg"
		picture = "Portrait_Aldo_Schellenberg.dds"
		traits = { commando }
		id = 62402
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Jean-Paul Theler"
		picture = "Portrait_Jean-Paul_Theler.dds"
		traits = {  }
		id = 62403
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Thomas Kaiser"
		picture = "Portrait_Thomas_Kaiser.dds"
		traits = { trait_engineer }
		id = 62404
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Daniel Keller"
		picture = "Portrait_Daniel_Keller.dds"
		traits = {  }
		id = 62405
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Willy Brülisauer"
		picture = "Portrait_Willy_Bruelisauer.dds"
		traits = { panzer_leader }
		id = 62406
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Peter Baumgartner"
		picture = "Portrait_Peter_Baumgartner.dds"
		traits = { trait_mountaineer }
		id = 62407
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Hans Schori"
		picture = "Portrait_Hans_Schori.dds"
		traits = { commando }
		id = 62408
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Laurent Michaud"
		picture = "Portrait_Laurent_Michaud.dds"
		traits = {  }
		id = 62409
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Raynald Droz"
		picture = "Portrait_Raynald_Droz.dds"
		traits = { trait_engineer }
		id = 62410
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Alain Vuitel"
		picture = "Portrait_Alain_Vuitel.dds"
		traits = {  }
		id = 62411
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Fredy Keller"
		picture = "Portrait_Fredy_Keller.dds"
		traits = { ranger }
		id = 62412
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Hans Schatzmann"
		picture = "Portrait_Hans_Schatzmann.dds"
		traits = { urban_assault_specialist }
		id = 62413
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Claude Meier"
		picture = "Portrait_Claude_Meier.dds"
		traits = {  }
		id = 62414
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Franz Nager"
		picture = "Portrait_Franz_Nager.dds"
		traits = { ranger }
		id = 62415
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Daniel Baumgartner"
		picture = "Portrait_Daniel_Baumgartner.dds"
		traits = { fortress_buster }
		id = 62416
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "René Wellinger"
		picture = "Portrait_Rene_Wellinger.dds"
		traits = { panzer_leader }
		id = 62417
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Yvon Langel"
		picture = "Portrait_Yvon_Langel.dds"
		traits = { panzer_leader }
		id = 62418
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Mathias Tüscher"
		picture = "Portrait_Mathias_Tuescher.dds"
		traits = { hill_fighter }
		id = 62419
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Alexander Kohli"
		picture = "Portrait_Alexander_Kohli.dds"
		traits = { hill_fighter }
		id = 62420
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Martin Vögeli"
		picture = "Portrait_Martin_Voegeli.dds"
		traits = { winter_specialist }
		id = 62421
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Maurizio Dattrino"
		picture = "Portrait_Maurizio_Dattrino.dds"
		traits = { trait_mountaineer }
		id = 62422
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Erik Labara"
		picture = "Portrait_Erik_Labara.dds"
		traits = { trait_mountaineer }
		id = 62423
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Roland Favre"
		picture = "Portrait_Roland_Favre.dds"
		traits = { trait_engineer }
		id = 62424
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Hans-Peter Walser"
		picture = "Portrait_Hans-Peter_Walser.dds"
		traits = { trait_engineer }
		id = 62425
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Lucas Caduff"
		picture = "Portrait_Lucas_Caduff.dds"
		traits = { trait_engineer }
		id = 62426
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Hans-Peter Kellerhals"
		picture = "Portrait_Hans-Peter_Kellerhals.dds"
		traits = { trait_engineer }
		id = 62427
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Dominique Andrey"
		picture = "Portrait_Dominique_Andrey.dds"
		traits = { trickster }
		id = 62428
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
	create_equipment_variant = {
		name = "Oerlikon GDF"
		type = SP_Anti_Air_1 #Oerlikon GDF
		upgrades = {
			
		}
	}
}

2017.1.1 = {
	capital = 74
	oob = "SWI_2017"

	add_ideas = {
		pop_050
		pluralist
		negligible_corruption
		gdp_10
		stagnation
		defence_01
		edu_04
		health_05
		social_05
		bureau_03
		police_02
		draft_army
		volunteer_women
		intervention_isolation
		western_country
		medium_far_right_movement
		small_medium_business_owners
		Labour_Unions
		international_bankers
		civil_law
		tax_cost_28
		multi_ethnic_state_idea
	}
	
	#set_country_flag = gdp_10
	set_country_flag = enthusiastic_international_bankers
	set_country_flag = positive_small_medium_business_owners
	
	set_variable = { var = debt value = 290 }
	set_variable = { var = treasury value = 679 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = tax_rate value = 28 }
	
	set_variable = { var = size_modifier value = 1.84 } #11 CIC
	initial_money_setup = yes
	
	#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_15K_GDPC_slot
	complete_national_focus = Generic_30K_GDPC_slot
	complete_national_focus = Generic_90K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot
	complete_national_focus = Generic_16_IC_slot
	
	set_technology = {
	
		APC_5 = 1
		
		ENGI_MBT_3 = 1
		
		land_Drone_equipment1 = 1
		
		command_control_equipment2 = 1
		
		infantry_weapons3 = 1
		infantry_weapons4 = 1
		
	}

	set_politics = {

		parties = {
			democratic = { 
				popularity = 96
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 0
			}
			
			neutrality = {
				popularity = 4
			}
			
			nationalist = {
				popularity = 0
			}
		}
		
		ruling_party = democratic
		last_election = "2015.10.19"
		election_frequency = 48
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Albert Rösti" 
		desc = "POLITICS_EARL_BROWDER_DESC"
		picture = "albert_rosti.dds"
		expire = "2050.1.1"
		ideology = conservatism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Fulvio Pelli"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "fulvio_pelli.dds"
		expire = "2060.1.1"
		ideology = liberalism
		traits = {
			
		}
	}

	create_country_leader = {
		name = "Christian Levrat"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "christian_levrat.dds"
		expire = "2060.1.1"
		ideology = socialism
		traits = {
			
		}
	}

	create_country_leader = {
		name = "Norberto Crivelli"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "norberto_crivelli.dds"
		expire = "2060.1.1"
		ideology = Communist-State
		traits = {
			
		}
	}

	create_country_leader = {
		name = "Martin Bäumle"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "martin_baumle.dds"
		expire = "2060.1.1"
		ideology = Neutral_social
		traits = {
			
		}
	}

	create_country_leader = {
		name = "Hans Moser"
		desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
		picture = "hans_moser.dds"
		expire = "2065.1.1"
		ideology = Nat_populism
		traits = {
			
		}
	}

	create_country_leader = {
		name = "Johann Schneider-Ammann"
		desc = "POLITICS_FEDERAL_COUNCIL_DESC"
		picture = "Johann_Schneider-Ammann.dds"
		expire = "2050.1.1"
		ideology = conservatism
		traits = {
			#
		}
	}

}