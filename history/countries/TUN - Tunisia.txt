﻿2000.1.1 = {
	capital = 388
	oob = "TUN_2000"
	set_convoys = 20
	
	add_ideas = {
		gdp_4
		tax_cost_19
		small_medium_business_owners
		landowners
		The_Ulema
		hybrid
	}

	set_country_flag = positive_small_medium_business_owners
	set_country_flag = positive_landowners
	
	set_variable = { var = debt value = 21 }
	set_variable = { var = treasury value = 3 }
	set_variable = { var = tax_rate value = 19 }
	set_variable = { var = int_investments value = 0 }
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 50
			}

			fascism = {
				popularity = 10
			}
			
			communism = {
				popularity = 5
				#banned = no #default is no
			}
			
			neutrality = { 
				popularity = 35
			}
		}
		
		ruling_party = democratic
		last_election = "2014.11.23"
		election_frequency = 60
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Zine Ben Ali"
		desc = "" 
		picture = "zine_ben_ali.dds"
		expire = "2050.1.1"
		ideology = conservatism
		traits = {
			#
		}
	}
	
	create_corps_commander = {
		name = "Rachid Ammar"
		picture = "generals/Rachid_Ammar.dds"
		id = 66600
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}
	create_corps_commander = {
		name = "Ismail Fathali"
		picture = "generals/Ismail_Fathali.dds"
		id = 66601
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

}

2017.1.1 = {
	capital = 388
	oob = "TUN_2017"
	set_convoys = 20
	
	add_ideas = {
		pop_050
		unrestrained_corruption
		gdp_3
		sunni
		youth_radicalization
		LoAS_member
		stagnation
		defence_02
		edu_03
		health_03
		social_03
		bureau_03
		police_03
		draft_army
		volunteer_women
		Major_Non_NATO_Ally
		small_medium_business_owners
		landowners
		The_Ulema
		hybrid
		tax_cost_21
		
	}
	
	#set_country_flag = gdp_3
	set_country_flag = Major_Non_NATO_Ally
	set_country_flag = positive_small_medium_business_owners
	set_country_flag = positive_landowners
	
	set_variable = { var = debt value = 28 }
	set_variable = { var = treasury value = 6 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = tax_rate value = 21 }
	
	set_variable = { var = size_modifier value = 0.38 } #4 CIC
	initial_money_setup = yes


	#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	
	# Starting tech
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		
		###FOR TEMPLATES###
		
		infantry_weapons = 1
		command_control_equipment = 1
		land_Drone_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		Anti_Air_0 = 1
		combat_eng_equipment = 1
		Early_APC = 1
		MBT_1 = 1
		IFV_1 = 1
		APC_1 = 1
		Air_APC_1 = 1
		Rec_tank_0 = 1
		util_vehicle_0 = 1
		artillery_0 = 1
		early_helicopter = 1
		transport_plane1 = 1
		SP_Anti_Air_0 = 1
		ENGI_MBT_1 = 1
		early_fighter = 1
		MR_Fighter1 = 1
		L_Strike_fighter1 = 1
		night_vision_1 = 1
		
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 50
			}

			fascism = {
				popularity = 10
			}
			
			communism = {
				popularity = 5
				#banned = no #default is no
			}
			
			neutrality = { 
				popularity = 35
			}
		}
		
		ruling_party = democratic
		last_election = "2014.11.23"
		election_frequency = 60
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Beji Caid Essebsi"
		desc = "POLITICS_Mohamed Cheikh Biadillah_DESC" 
		picture = "Beji_Caid_Essebsi.dds"
		expire = "2050.1.1"
		ideology = conservatism
		traits = {
			#
		}
	}
}