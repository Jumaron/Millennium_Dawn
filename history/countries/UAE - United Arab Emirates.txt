﻿2000.1.1 = {
	capital = 180
	oob = "UAE_2017"
	set_convoys = 40
	
	add_ideas = {
		gdp_10
		pop_050
		sunni	
		defence_06
		edu_04
		health_04
		social_06
		bureau_02
		police_03
		state_press
		underground_parties_only
		theocracy
		draft_army
		volunteer_women
		rentier_state
		export_economy
		fossil_fuel_industry
		wahabi_ulema
		saudi_royal_family
		hybrid
		tax_cost_01
	}
	set_country_flag = positive_fossil_fuel_industry

	set_variable = { var = debt value = 5 }
	set_variable = { var = treasury value = 20 }
	set_variable = { var = tax_rate value = 1 }
	set_variable = { var = int_investments value = 0 }
	
	set_technology = { 

		corvette_1 = 1
		corvette_2 = 1
		missile_corvette_1 = 1
		missile_corvette_2 = 1 #Miray Jib
		
		frigate_1 = 1
		
		infantry_weapons = 1
		command_control_equipment = 1
		land_Drone_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		Anti_Air_0 = 1
		combat_eng_equipment = 1
		
		Early_APC = 1
		MBT_1 = 1
		
		IFV_1 = 1

		APC_1 = 1

		Rec_tank_0 = 1
		
		util_vehicle_0 = 1
		
		SP_arty_0 = 1 #Enigma

		SP_Anti_Air_0 = 1
		
		artillery_0 = 1

		
		SP_R_arty_0 = 1 #Jobaria MCL
		
		early_helicopter = 1
		attack_helicopter1 = 1
		transport_helicopter1 = 1
		
		SP_Anti_Air_0 = 1
		
		ENGI_MBT_1 = 1
		
		early_fighter = 1
		MR_Fighter1 = 1
		L_Strike_fighter1 = 1
		
		
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 12
			}

			fascism = {
				popularity = 79
			}
			
			communism = {
				popularity = 9
				banned = yes
			}
			
			neutrality = { 
				popularity = 0
			}
		}
		
		ruling_party = fascism
		last_election = "1932.11.8"
		election_frequency = 48
		elections_allowed = no
	}

	create_country_leader = {
		name = "Zayed bin Sultan Al-Nahyan"
		picture = "Zayed_bin_Sultan_al-Nahyan.dds"
		expire = "2007.1.1"
		ideology = Kingdom
		traits = {
			king
			salafist_Kingdom
		}
	}
	
	create_field_marshal = {
		name = "Hamad Mohammed Al-Rumaithi"
		picture = "Hamad_Mohammed_Al-Rumaithi.dds"
		id = 67200
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}
	create_corps_commander = {
		name = "Gomaa Ahmed Al-Bawardi"
		picture = "Gomaa_A_Bawardi.dds"
		id = 67201
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Saleh Mohammed Al-Ameri"
		picture = "Saleh_M_Ameri.dds"
		id = 67202
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Ahmed Bin Tahnoon"
		picture = "Ahmed_Tahnoon.dds"
		id = 67203
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Issa Saif Al-Mazrouei"
		picture = "Issa_S_Mazrouei.dds"
		id = 67204
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Al Matrooshi Mourn"
		picture = "Al_Matrooshi_Mourn.dds"
		id = 67205
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Khalifa bin Nahila"
		picture = "Khalifa_Nahila.dds"
		id = 67206
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Mohammed Al Dhahiri"
		picture = "Mohammed_Al_Dhahiri.dds"
		id = 67207
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Abdul Qudous Abdul Razzaq"
		picture = "Abdul_Qudous_Abdul_Razzaq.dds"
		id = 67208
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Hamad Adil Al-Shamsi"
		picture = "Hamad_A_AlShamsi.dds"
		traits = { ranger }
		id = 67209
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Mubarak bin Muhairoum"
		picture = "Mubarak_Muhairoum.dds"
		traits = { trickster }
		id = 67210
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Mubarak Abdullah Al-Muhairi"
		picture = "Al_Muhairi.dds"
		traits = { bearer_of_artillery }
		id = 67211
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_navy_leader = {
		name = "Ibrahim Salem Mohammed"
		picture = "Ib_S_Mohammed.dds"
		traits = { old_guard_navy superior_tactician }
		id = 67212
	}

}
2017.1.1 = {
	capital = 180
	oob = "UAE_2017"
	set_convoys = 40

	add_ideas = {
		pop_050
		widespread_corruption
		gdp_9
		sunni
		uae_expat
		al_jazeera_banned
		cybercaliphate
		youth_radicalization
		muslim_brotherhood_crackdown
		restoring_hope

		GCC_member
		LoAS_member	
		stable_growth
		defence_06
		edu_04
		health_04
		social_06
		bureau_02
		police_03
		state_press
		underground_parties_only
		theocracy
		draft_army
		volunteer_women
		rentier_state
		export_economy
		fossil_fuel_industry
		wahabi_ulema
		saudi_royal_family
		hybrid
		tax_cost_05
	}
	
	set_country_flag = Major_Importer_US_Arms #Trends-in-international-arms-transfers-2016.pdf
	set_country_flag = positive_fossil_fuel_industry
	
	set_variable = { var = debt value = 73 }
	set_variable = { var = treasury value = 85 }
	set_variable = { var = int_investments value = 1299 }
	set_variable = { var = tax_rate value = 5 }
	
	set_variable = { var = size_modifier value = 0.74 } #6 CIC
	initial_money_setup = yes
	
	#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_15K_GDPC_slot
	complete_national_focus = Generic_30K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot

	add_opinion_modifier = { target = PER modifier = hostile_status }
	add_opinion_modifier = { target = SYR modifier = hostile_status }
	add_opinion_modifier = { target = IRQ modifier = hostile_status }


	add_opinion_modifier = { target = SOV modifier = Arabian_Peninsula_Russia_Economical_Relations }

	# Starting tech
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1

		corvette_1 = 1
		corvette_2 = 1
		missile_corvette_1 = 1
		missile_corvette_2 = 1 #Miray Jib
		
		frigate_1 = 1
		
		infantry_weapons = 1
		command_control_equipment = 1
		land_Drone_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		Anti_Air_0 = 1
		combat_eng_equipment = 1
		
		Early_APC = 1
		MBT_1 = 1
		
		IFV_1 = 1
		IFV_2 = 1
		IFV_3 = 1
		IFV_4 = 1
		IFV_5 = 1
		IFV_6 = 1 #EDT Enigma

		APC_1 = 1
		APC_2 = 1
		APC_3 = 1
		APC_4 = 1
		APC_5 = 1
		APC_6 = 1 #Enigma APC

		Rec_tank_0 = 1
		
		util_vehicle_0 = 1
		util_vehicle_1 = 1
		util_vehicle_2 = 1
		util_vehicle_3 = 1
		util_vehicle_4 = 1
		util_vehicle_5 = 1 #Nimr
		
		SP_arty_0 = 1 #Enigma

		SP_Anti_Air_0 = 1
		
		artillery_0 = 1

		
		SP_R_arty_0 = 1 #Jobaria MCL
		
		early_helicopter = 1
		attack_helicopter1 = 1
		transport_helicopter1 = 1
		
		SP_Anti_Air_0 = 1
		
		ENGI_MBT_1 = 1
		
		early_fighter = 1
		MR_Fighter1 = 1
		L_Strike_fighter1 = 1
		
		night_vision_1 = 1
		night_vision_2 = 1
		
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 12
			}

			fascism = {
				popularity = 79
			}
			
			communism = {
				popularity = 9
				banned = yes
			}
			
			neutrality = { 
				popularity = 0
			}
		}
		
		ruling_party = fascism
		last_election = "1932.11.8"
		election_frequency = 48
		elections_allowed = no
	}

	create_country_leader = {
		name = "Khalifa bin Zayed Al-Nahyan"
		desc = "POLITICS_Mohamed Cheikh Biadillah_DESC" 
		picture = "Khalifa_bin_Zayed_Al-Nahyan.dds"
		expire = "2050.1.1"
		ideology = Kingdom
		traits = {
			king
			salafist_Kingdom
			tech_savy
			greedy
			capable
		}
	}

	create_equipment_variant = {
		name = "Baynunah-class"
		type = missile_corvette_2
		upgrades = {
		}
	}
}