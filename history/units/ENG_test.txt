﻿division_template = {
	name = "Infantry Division"			# 1st - 5th divisions = fully equipped, others = reserve divisions
	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 0 y = 2 }
		Mot_Inf_Bat = { x = 0 y = 3 }
		#Mot_Inf_Bat = { x = 1 y = 0 }
		#Mot_Inf_Bat = { x = 1 y = 1 }
	}
	support = {
	}
}
units = {
	
	#Plains
	division = {	
		name = "50th infantry brigade"
		location = 3552
		division_template = "Infantry Division"
		force_equipment_variants = {
			infantry_weapons = { owner = "ENG" }
			
		}
		start_experience_factor = 0.3
	}
	division = {	
		name = "50th infantry brigade"
		location = 6572
		division_template = "Infantry Division"
		force_equipment_variants = {
			infantry_weapons = { owner = "ENG" }
			
		}
		start_experience_factor = 0.3
	}
	
	#Hills
	division = {	
		name = "50th infantry brigade"
		location = 6449
		division_template = "Infantry Division"
		force_equipment_variants = {
			infantry_weapons = { owner = "ENG" }
			
		}
		start_experience_factor = 0.3
	}
	division = {	
		name = "50th infantry brigade"
		location = 3579
		division_template = "Infantry Division"
		force_equipment_variants = {
			infantry_weapons = { owner = "ENG" }
			
		}
		start_experience_factor = 0.3
	}
	division = {	
		name = "50th infantry brigade"
		location = 9550
		division_template = "Infantry Division"
		force_equipment_variants = {
			infantry_weapons = { owner = "ENG" }
			
		}
		start_experience_factor = 0.3
	}
	division = {	
		name = "50th infantry brigade"
		location = 11508
		division_template = "Infantry Division"
		force_equipment_variants = {
			infantry_weapons = { owner = "ENG" }
			
		}
		start_experience_factor = 0.3
	}
	
	#Forest
	division = {	
		name = "50th infantry brigade"
		location = 9737
		division_template = "Infantry Division"
		force_equipment_variants = {
			infantry_weapons = { owner = "ENG" }
			
		}
		start_experience_factor = 0.3
	}
	division = {	
		name = "50th infantry brigade"
		location = 11565
		division_template = "Infantry Division"
		force_equipment_variants = {
			infantry_weapons = { owner = "ENG" }
			
		}
		start_experience_factor = 0.3
	}
	division = {	
		name = "50th infantry brigade"
		location = 9597
		division_template = "Infantry Division"
		force_equipment_variants = {
			infantry_weapons = { owner = "ENG" }
			
		}
		start_experience_factor = 0.3
	}
	division = {	
		name = "50th infantry brigade"
		location = 6792
		division_template = "Infantry Division"
		force_equipment_variants = {
			infantry_weapons = { owner = "ENG" }
			
		}
		start_experience_factor = 0.3
	}
	
	#Mountains
	division = {	
		name = "51st infantry brigade"
		location = 11834
		division_template = "Infantry Division"
		force_equipment_variants = {
			infantry_weapons = { owner = "ENG" }
			
		}
		start_experience_factor = 0.3.5
	}
	division = {	
		name = "50th infantry brigade"
		location = 762
		division_template = "Infantry Division"
		force_equipment_variants = {
			infantry_weapons = { owner = "ENG" }
			
		}
		start_experience_factor = 0.3
	}
	division = {	
		name = "50th infantry brigade"
		location = 11713
		division_template = "Infantry Division"
		force_equipment_variants = {
			infantry_weapons = { owner = "ENG" }
			
		}
		start_experience_factor = 0.3
	}
	division = {	
		name = "50th infantry brigade"
		location = 11875
		division_template = "Infantry Division"
		force_equipment_variants = {
			infantry_weapons = { owner = "ENG" }
			
		}
		start_experience_factor = 0.3
	}
	division = {	
		name = "50th infantry brigade"
		location = 3961
		division_template = "Infantry Division"
		force_equipment_variants = {
			infantry_weapons = { owner = "ENG" }
			
		}
		start_experience_factor = 0.3
	}
	division = {	
		name = "50th infantry brigade"
		location = 9736
		division_template = "Infantry Division"
		force_equipment_variants = {
			infantry_weapons = { owner = "ENG" }
			
		}
		start_experience_factor = 0.3
	}
	#Urban
	division = {	
		name = "50th infantry brigade"
		location = 11535
		division_template = "Infantry Division"
		force_equipment_variants = {
			infantry_weapons = { owner = "ENG" }
			
		}
		start_experience_factor = 0.3
	}
	division = {	
		name = "50th infantry brigade"
		location = 6599
		division_template = "Infantry Division"
		force_equipment_variants = {
			infantry_weapons = { owner = "ENG" }
			
		}
		start_experience_factor = 0.3
	}
	division = {	
		name = "50th infantry brigade"
		location = 9578
		division_template = "Infantry Division"
		force_equipment_variants = {
			infantry_weapons = { owner = "ENG" }
			
		}
		start_experience_factor = 0.3
	}
	division = {
		name = "50th infantry brigade"
		location = 3549
		division_template = "Infantry Division"
		force_equipment_variants = {
			infantry_weapons = { owner = "ENG" }
			
		}
		start_experience_factor = 0.3
	}
	division = {
		name = "50th infantry brigade"
		location = 3549
		division_template = "Infantry Division"
		force_equipment_variants = {
			infantry_weapons = { owner = "ENG" }
			
		}
		start_experience_factor = 0.3
	}
	division = {	
		name = "50th infantry brigade"
		location = 6547
		division_template = "Infantry Division"
		force_equipment_variants = {
			infantry_weapons = { owner = "ENG" }
			
		}
		start_experience_factor = 0.3
	}
	division = {	
		name = "50th infantry brigade"
		location = 9507
		division_template = "Infantry Division"
		force_equipment_variants = {
			infantry_weapons = { owner = "ENG" }
			
		}
		start_experience_factor = 0.3
	}
	division = {	
		name = "59th infantry brigade"
		location = 9507
		division_template = "Infantry Division"
		force_equipment_variants = {
			infantry_weapons = { owner = "ENG" }
			
		}
		start_experience_factor = 0.3
	}
	division = {	
		name = "50th infantry brigade"
		location = 9725
		division_template = "Infantry Division"
		force_equipment_variants = {
			infantry_weapons = { owner = "ENG" }
			
		}
		start_experience_factor = 0.3
	}
	
	navy = {		
		name = "Royal Navy Surface Fleet"				
		base = 540
		location 540
		ship = { name = "HMS Daring" definition = destroyer equipment = { missile_destroyer_4 = { amount = 1 owner = ENG } } }
		#ship = { name = "HMS Ocean" definition = helicopter_carrier equipment = { helicopter_carrier_2 = { amount = 1 owner = ENG } } air_wings = { } }
		ship = { name = "HMS Dauntless" definition = destroyer equipment = { missile_destroyer_4 = { amount = 1 owner = ENG } } }
		ship = { name = "HMS Diamond" definition = destroyer equipment = { missile_destroyer_4 = { amount = 1 owner = ENG } } }
		ship = { name = "HMS Dragon" definition = destroyer equipment = { missile_destroyer_4 = { amount = 1 owner = ENG } } }
		ship = { name = "HMS Defender" definition = destroyer equipment = { missile_destroyer_4 = { amount = 1 owner = ENG } } }
		ship = { name = "HMS Duncan" definition = destroyer equipment = { missile_destroyer_4 = { amount = 1 owner = ENG } } }
		ship = { name = "HMS Argyll" definition = frigate equipment = { missile_frigate_1 = { amount = 1 owner = ENG } } }
		ship = { name = "HMS Lancaster" definition = frigate equipment = { missile_frigate_1 = { amount = 1 owner = ENG } } }
		ship = { name = "HMS Iron Duke" definition = frigate equipment = { missile_frigate_1 = { amount = 1 owner = ENG } } }
		ship = { name = "HMS Monmouth" definition = frigate equipment = { missile_frigate_1 = { amount = 1 owner = ENG } } }
		ship = { name = "HMS Montrose" definition = frigate equipment = { missile_frigate_1 = { amount = 1 owner = ENG } } }
		ship = { name = "HMS Westminster" definition = frigate equipment = { missile_frigate_1 = { amount = 1 owner = ENG } } }
		ship = { name = "HMS Northumberland	" definition = frigate equipment = { missile_frigate_1 = { amount = 1 owner = ENG } } }
		ship = { name = "HMS Richmond" definition = frigate equipment = { missile_frigate_1 = { amount = 1 owner = ENG } } }
		ship = { name = "HMS Somerset" definition = frigate equipment = { missile_frigate_1 = { amount = 1 owner = ENG } } }
		ship = { name = "HMS Sutherland" definition = frigate equipment = { missile_frigate_1 = { amount = 1 owner = ENG } } }
		ship = { name = "HMS Kent" definition = frigate equipment = { missile_frigate_1 = { amount = 1 owner = ENG } } }
		ship = { name = "HMS Portland" definition = frigate equipment = { missile_frigate_1 = { amount = 1 owner = ENG } } }
		ship = { name = "HMS St Albans" definition = frigate equipment = { missile_frigate_1 = { amount = 1 owner = ENG } } }
		}
		
	navy = {		
		name = "Royal Navy Submarine Service"				
		base = 540
		location 540
		ship = { name = "HMS Torbay" definition = attack_submarine equipment = { attack_submarine_3 = { amount = 1 owner = ENG } } }
		ship = { name = "HMS Trenchant" definition = attack_submarine equipment = { attack_submarine_3 = { amount = 1 owner = ENG } } }
		ship = { name = "HMS Talent" definition = attack_submarine equipment = { attack_submarine_3 = { amount = 1 owner = ENG } } }
		ship = { name = "HMS Triumph" definition = attack_submarine equipment = { attack_submarine_3 = { amount = 1 owner = ENG } } }
		ship = { name = "HMS Astute" definition = attack_submarine equipment = { attack_submarine_4 = { amount = 1 owner = ENG } } }
		ship = { name = "HMS Ambush" definition = attack_submarine equipment = { attack_submarine_4 = { amount = 1 owner = ENG } } }
		ship = { name = "HMS Artful" definition = attack_submarine equipment = { attack_submarine_4 = { amount = 1 owner = ENG } } }
		ship = { name = "HMS Vanguard" definition = missile_submarine equipment = { missile_submarine_3 = { amount = 1 owner = ENG } } }
		ship = { name = "HMS Victorious" definition = missile_submarine equipment = { missile_submarine_3 = { amount = 1 owner = ENG } } }
		ship = { name = "HMS Vigilant" definition = missile_submarine equipment = { missile_submarine_3 = { amount = 1 owner = ENG } } }
		ship = { name = "HMS Vengeance" definition = missile_submarine equipment = { missile_submarine_3 = { amount = 1 owner = ENG } } }
		}
		
# instant_effect = {
	# add_equipment_production = {
		# equipment = {
			# type = carrier_4
			# creator = "ENG"
		# }
		# requested_factories = 3
		# progress = 0.8
		# efficiency = 100
	# }
	
	# add_equipment_production = {
		# equipment = {
			# type = carrier_4
			# creator = "ENG"
		# }
		# requested_factories = 3
		# progress = 0.2
		# efficiency = 100
	# }
# }
}