﻿division_template = {
	name = "Infantry Regiment #1"

	regiments = {
	Mot_Recce_Bat = { x = 0 y = 0 }				#Motorized Recce Battalion
	Mech_Recce_Bat = { x = 0 y = 1 }				#Mechanized Recce Battalion
		
	}
	support = {
	}
}


division_template = {
	name = "Infantry Regiment #2"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
		armor_Recce_Bat = { x = 1 y = 0 }
		
	}
	support = {
	}
}

division_template = {
	name = "Commando Battalion"
	
	regiments = {
		Special_Forces = { x = 0 y = 0 }
		Special_Forces = { x = 0 y = 1 }
		Special_Forces = { x = 0 y = 2 }
	}
	
	support = {
	}
	
	priority = 2
}

division_template = {
	name = "Presidential Guard"
	
	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
		Special_Forces = { x = 0 y = 3 }

	}
	
	support = {
	}
	
	priority = 2
}

units = {
	division = {	
		name = "1st Regiment"
		location = 12843
		division_template = "Infantry Regiment #1"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {	
		name = "2nd Regiment"
		location = 1951
		division_template = "Infantry Regiment #2"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	
	division = {	
		name = "Commando Battalion"
		location = 1951
		division_template = "Commando Battalion"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	
	division = {	
		name = "Presidential Guard"
		location = 1951
		division_template = "Presidential Guard"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}

}
	
instant_effect = {
	
	add_equipment_to_stockpile = {
		type = infantry_weapons
		amount = 2000
		producer = SOV
	}
		add_equipment_to_stockpile = {
		type = command_control_equipment
		amount = 300
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = MBT_1 #T-55
		amount = 2
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Rec_tank_1 #FV101 Scorpion
		amount = 9
		producer = ENG
	}
	
	add_equipment_to_stockpile = {
		type = Rec_tank_0 #Panhard AML
		amount = 10
		producer = FRA
	}
	
	add_equipment_to_stockpile = {
		type = Rec_tank_0 #EE-9 Cascavel
		amount = 36
		producer = BRA
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_3 #VBL
		amount = 2
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = IFV_3 #BMP-2
		amount = 20
		producer = SOV
	}
	
	add_equipment_to_stockpile = {
		type = APC_1 #Henschel UR-416
		amount = 30
		producer = GER
	}
		
	add_equipment_to_stockpile = {
		type = SP_R_arty_1 # 122mm
		amount = 6
		producer = GEO
	}
	
	add_equipment_to_stockpile = {
		type = Anti_tank_0
		amount = 80
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_0
		amount = 0
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_1
		amount = 60
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter2 # ALPHA JET
		amount = 5
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter1  #EMB-326
		amount = 4
		producer = BRA
	}
}