﻿
###### Divison Templates ######

division_template = {
	name = "Infantry Brigade"
	regiments = {
	  Mot_Inf_Bat = { x = 0 y = 0 }
	}
	support = {
	   armor_Comp = { x = 0 y = 0 }
	}
		priority = 1
}

division_template = {
	name = "Armored Brigade"
	regiments = {
	  armor_Bat = { x = 0 y = 0 }
	}
	support = {
	   H_Engi_Comp = { x = 0 y = 0 }
	}
		priority = 1
}

division_template = {
	name = "Missile Brigade"
	regiments = {
	  SP_AA_Bat = { x = 0 y = 0 }
	   SP_Arty_Bat = { x = 1 y = 0 }
	}
	support = {
	}
		priority = 1
}

division_template = {
	name = "Naval Infantry Brigade"
	regiments = {
	  Mot_Marine_Bat = { x = 0 y = 0 }
	}
	support = {
	}
		priority = 1
}

division_template = {
	name = "Militia"
	regiments = {
	  Militia_Bat = { x = 0 y = 0 }
	}
	support = {
	}
		priority = 1
}

###### Initial Deployment ######

units = {

 ### Infantry ###

	division = {
		name = "135th Infantry Brigade"
		location = 9181 #al Mahrah
		division_template = "Infantry Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	
	division = {
		name = "13th Infantry Brigade"
		location = 10840 #Sana'a
		division_template = "Infantry Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	
	division = {
		name = "19th Infantry Brigade"
		location = 9150 #Al Hudaydah
		division_template = "Infantry Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	
	division = {
		name = "15th Infantry Brigade"
		location = 9217 #Sa'dah
		division_template = "Infantry Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	
	division = {
		name = "17th Infantry Brigade"
		location = 5074 #West of Aden
		division_template = "Infantry Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	
	division = {
		name = "107th Infantry Brigade"
		location = 9150 #Al Hudaydah
		division_template = "Infantry Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	
	division = {
		name = "119th Infantry Brigade"
		location = 9158 #East of Ataq
		division_template = "Infantry Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	
	division = {
		name = "2nd Naval Infantry Brigade"
		location = 8000 #Socotra
		division_template = "Naval Infantry Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	
	division = {
		name = "78th Infantry Brigade"
		location = 9138 #Aden
		division_template = "Infantry Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	
 ### Armor ###
 
	division = {
		name = "111th Armored Brigade"
		location = 9141 #Balhaf
		division_template = "Armored Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	
	division = {
		name = "37th Armored Brigade"
		location = 9181 #al Mahrah
		division_template = "Armored Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	
	division = {
		name = "31st Armored Brigade"
		location = 9158 #East of Ataq
		division_template = "Armored Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	
	division = {
		name = "14th Armored Brigade"
		location = 10752 #Taiz
		division_template = "Armored Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	
	division = {
		name = "24th Armored Brigade"
		location = 9138 #Aden
		division_template = "Armored Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	
 ### Missiles ###
 
	division = {
		name = "8th Missile Brigade"
		location = 9162 #Sheba Airfield
		division_template = "Missile Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	
	division = {
		name = "5th Missile Brigade"
		location = 9138 #Aden Airfield
		division_template = "Missile Brigade"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	
 ### Militia ###
 
	division = {
		name = "1st Aden Militia"
		location = 9138 #Aden
		division_template = "Militia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	
	division = {
		name = "2nd Aden Militia"
		location = 9138 #Aden
		division_template = "Militia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	
	division = {
		name = "1st Ma'rib Militia"
		location = 9162 #Ma'rib
		division_template = "Militia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	
	division = {
		name = "1st Ataq Militia"
		location = 9158 #Ataq
		division_template = "Militia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	
	division = {
		name = "1st Balhaf Militia"
		location = 9176 #Balhaf
		division_template = "Militia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	
	division = {
		name = "1st Al Ghaydah Militia"
		location = 9201 #Al Ghaydah
		division_template = "Militia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}

	###### Navy ######

	 navy = {
	   name = "Yemeni Navy"
	   base = 9138
		location = 9138
	   ship = { name = "Tarantul-class 1" definition = corvette experience = 10 equipment = { missile_corvette_1 = { amount = 1 owner = YEM creator = SOV } } }
		ship = { name = "Osa-class 1" definition = corvette experience = 10 equipment = { corvette_1 = { amount = 1 owner = YEM creator = SOV version_name = "Osa-class" } } }
		ship = { name = "Osa-class 2" definition = corvette experience = 10 equipment = { corvette_1 = { amount = 1 owner = YEM creator = SOV version_name = "Osa-class" } } }
		ship = { name = "Osa-class 3" definition = corvette experience = 10 equipment = { corvette_1 = { amount = 1 owner = YEM creator = SOV version_name = "Osa-class" } } }
		ship = { name = "Osa-class 4" definition = corvette experience = 10 equipment = { corvette_1 = { amount = 1 owner = YEM creator = SOV version_name = "Osa-class" } } }
		ship = { name = "Osa-class 5" definition = corvette experience = 10 equipment = { corvette_1 = { amount = 1 owner = YEM creator = SOV version_name = "Osa-class" } } }
		ship = { name = "Osa-class 6" definition = corvette experience = 10 equipment = { corvette_1 = { amount = 1 owner = YEM creator = SOV version_name = "Osa-class" } } }
		ship = { name = "Osa-class 7" definition = corvette experience = 10 equipment = { corvette_1 = { amount = 1 owner = YEM creator = SOV version_name = "Osa-class" } } }
		ship = { name = "Osa-class 8" definition = corvette experience = 10 equipment = { corvette_1 = { amount = 1 owner = YEM creator = SOV version_name = "Osa-class" } } }
		
	}
	
}


instant_effect = {
	
	add_equipment_to_stockpile = {
		type = APC_1			
		amount = 710
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = IFV_1 
		amount = 200
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = command_control_equipment			
		amount = 800
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = Anti_Air_0			
		amount = 150
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = SP_Anti_Air_0			
		amount = 50
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = Anti_tank_0			
		amount = 160
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_0			
		amount = 100
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = SP_R_arty_0			
		amount = 50
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = infantry_weapons			
		amount = 4000
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = MBT_1			
		amount = 790
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = ENGI_MBT_1			
		amount = 130
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = util_vehicle_0			
		amount = 900
		producer = SOV 
	}
	
##Aircraft##
	
	add_equipment_to_stockpile = {
	type = MR_Fighter1 #F-5
	amount = 10
	producer = USA
	}
	
	add_equipment_to_stockpile = {
	type = MR_Fighter1 #MiG-21
	amount = 16
	producer = SOV
	}
	
		add_equipment_to_stockpile = {
	type = MR_Fighter2 #MiG-29
	amount = 10
	producer = SOV
	}
	
	add_equipment_to_stockpile = {
	type = Strike_fighter1 #Su-20/22
	amount = 30
	producer = SOV
	}
	
##Helicopters##

	add_equipment_to_stockpile = {
		type = attack_helicopter2 		#Mi-28
		#version_name = "Mi-35"
		amount = 8
		producer = SOV
	}

	add_equipment_to_stockpile = {
	type = transport_helicopter1 		#Mi-8
	amount = 9
	producer = SOV
	}
	

}