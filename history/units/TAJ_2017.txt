﻿division_template = {
	name = "Mechanized Brigade 1"

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Arty_Bat = { x = 1 y = 0 }
		
	}
	support = {
		armor_Comp = { x = 0 y = 0 }
	}
}

division_template = {
	name = "Mechanized Brigade 2"

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		Arty_Bat = { x = 1 y = 0 }
		
	}
	support = {
		armor_Comp = { x = 0 y = 0 }
	}
}
division_template = {
	name = "Air Assault"

	regiments = {
		L_Air_assault_Bat = { x = 0 y = 0 }
		
	}
	support = {
	}
}

units = {
	
	division = {	
		name = "Mechanized Brigade"
		location = 1384
		division_template = "Air Assault"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {	
		name = "Mechanized Brigade"
		location = 10187
		division_template = "Mechanized Brigade 1"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {	
		name = "Mechanized Brigade"
		location = 10417
		division_template = "Mechanized Brigade 1"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {	
		name = "Mechanized Brigade"
		location = 4923
		division_template = "Mechanized Brigade 2"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {	
		name = "Air Assault"
		location = 7224
		division_template = "Air Assault"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	
}	

instant_effect = {
	
	add_equipment_to_stockpile = {
		type = infantry_weapons
		amount = 900
		producer = SOV
	}
		add_equipment_to_stockpile = {
		type = command_control_equipment
		amount = 135
		producer = SOV
	}
		add_equipment_to_stockpile = {
		type = Anti_tank_1
		amount = 44
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1
		amount = 15
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_1
		amount = 44
		producer = SOV
	}
		
	add_equipment_to_stockpile = {
		type = MBT_2 #T-72
		amount = 30
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = MBT_1 #T-62
		amount = 7
		producer = SOV
		#version_name = "T-62"
	}
	add_equipment_to_stockpile = {
		type = IFV_1 #BMP-1
		amount = 8
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = IFV_3 #BMP-2
		amount = 15
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = APC_1 #BTR-60
		amount = 23
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = artillery_0 #D-30
		amount = 10
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = SP_R_arty_0 #BM-21 Grad
		amount = 3
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = attack_helicopter1 #Mil Mi-24 Hind
		amount = 4
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Mil Mi-8
		amount = 8
		producer = SOV
	}
	
	# 11 Mi-8 Hip/Mi-17TM Hip H 
	
}	