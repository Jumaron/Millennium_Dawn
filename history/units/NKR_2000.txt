﻿#Author: Divexz
division_template = {
	name = "Motohradzgayin Gndi"	#Motor Rifle Regiment
	
	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }
	}
	
	support = {
		SP_AA_Battery = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Hradzgayin Gndi"			#Rifle Regiment
	
	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 0 y = 2 }
		Arty_Bat = { x = 0 y = 3 }
	}
	
	support = {
		L_Engi_Comp = { x = 0 y = 0 }
	}
}
division_template = {
	name = "Hatuk Uzhery Gndi"
	
	regiments = { 
		Special_Forces = { x = 0 y = 0 }
		Special_Forces = { x = 0 y = 1 }
	}
	
	support = {
	}
	
	priority = 2
}
division_template = {
	name = "Artillery Brigade"
	
	regiments = {
		Arty_Bat = { x = 0 y = 0 }
		Arty_Bat = { x = 0 y = 1 }
		Arty_Bat = { x = 0 y = 2 }
		L_Engi_Bat = { x = 0 y = 3 }
	}
	
	support = {
		
		Arty_Battery = { x = 0 y = 0 }
	}
}
units = {
#Army HQ - Stepanakert
	division = {	
		name = "Artsakhi Motohradzgayin Gndi"
		location = 1467		#Stepanakert
		division_template = "Motohradzgayin Gndi"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {	
		name = "2-nd Artsakhi Motohradzgayin Gndi"
		location = 1610		#Martuni
		division_template = "Motohradzgayin Gndi"
		start_experience_factor = 0.2
		start_equipment_factor = 0.02
	}
	division = {	
		name = "3-rd Artsakhi Hradzgayin Gndi"
		location = 9715		#Hadrut
		division_template = "Hradzgayin Gndi"
		start_experience_factor = 0.2
		start_equipment_factor = 0.02
	}
}

instant_effect = {
	
	add_equipment_to_stockpile = {
		type = infantry_weapons1			#AK-74
		amount = 10000
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = infantry_weapons3			#AK-105
		amount = 200
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = infantry_weapons3			#K-3
		amount = 200
		producer = ARM 
	}
	
	add_equipment_to_stockpile = {
		type = Anti_tank_1				#Fagot
		amount = 200
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = Anti_tank_1				#MILAN
		amount = 20
		producer = FRA 
	}
	
	add_equipment_to_stockpile = {
		type = Anti_tank_1				#Metis
		#version_name = "AT-7 Metis"
		amount = 200
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1				#Konkurs
		amount = 100
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1				#Spiral
		#version_name = "AT-6 Spiral"
		amount = 100
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = Anti_Air_0				#Strela 2
		amount = 250
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = Anti_Air_2				#Igla-S
		amount = 250
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = MBT_4				#T-90
		amount = 1
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = MBT_3				#T-80
		amount = 20
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = MBT_2				#T-72
		amount = 500
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = MBT_1				#T-55
		amount = 8
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = IFV_3				#BMP-2
		amount = 100
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = IFV_1				#BMP-1 (includes command variants (armed))
		amount = 200
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = Air_IFV_1			#BMD-1
		amount = 10
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = Rec_tank_0			#BRDM-2
		#version_name = "BRDM-2"
		amount = 120
		producer = SOV
	}
	
	add_equipment_to_stockpile = {
		type = APC_4				#BTR-80
		amount = 150
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = APC_3				#BTR-70
		amount = 90
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = APC_2				#MT-LB
		amount = 145
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = APC_1				#BTR-60
		amount = 200
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = util_vehicle_3				#GAZ Tigr
		amount = 4
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = SP_R_arty_1				#BM-30 Smerch
		#version_name = "BM-30 Smerch"
		amount = 6
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = SP_R_arty_1				#TOS-1A
		#version_name = "TOS-1"
		amount = 10
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = SP_R_arty_1				#BM-27 Uragan
		amount = 11
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = SP_R_arty_1				#BM-27 Uragan
		amount = 11
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = SP_R_arty_0				#BM-21 Grad
		amount = 150
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = SP_R_arty_2				#N-2
		amount = 20
		producer = ARM 
	}
	
	add_equipment_to_stockpile = {
		type = SP_arty_0					#2S3 Akatsiya
		amount = 28
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = SP_arty_0					#2S1 Gvozdika
		#version_name = "2S1 Gvozdika"
		amount = 10
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = artillery_0				#D-30
		amount = 69
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = SP_Anti_Air_0				#Shilka
		#version_name = "ZSU-23-4 Shilka"
		amount = 30
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = SP_Anti_Air_1				#Strela-10
		#version_name = "SA-13 Strela-10"
		amount = 30
		producer = SOV 
	}
	
	#Fill up what's missing
	add_equipment_to_stockpile = {
		type = command_control_equipment				
		amount = 1500
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = util_vehicle_0				
		amount = 1000
		producer = SOV 
	}
	
	#Aircraft
	add_equipment_to_stockpile = {
		type = attack_helicopter1	#Mi-24
		amount = 15
		producer = SOV 
	}
	
	add_equipment_to_stockpile = {
		type = cas2					#su-25
		amount = 11
		producer = SOV 
	}
	
}