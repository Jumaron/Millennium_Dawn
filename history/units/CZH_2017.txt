﻿##### Division Templates #####
division_template = {
	name = "Brigáda Rychlého Nasazení"		

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		SP_Arty_Bat = { x = 0 y = 2 }
		armor_Bat = { x = 1 y = 0 }
		Mot_Inf_Bat = { x = 2 y = 0 }
		L_Engi_Bat = { x = 2 y = 1 }
	}
	support = {
	}
}

division_template = {
	name = "Mechanizovaná Brigáda"		

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		SP_Arty_Bat = { x = 0 y = 2 }
		L_Air_Inf_Bat = { x = 1 y = 0 }
		Mot_Inf_Bat = { x = 2 y = 0 }
		L_Engi_Bat = { x = 2 y = 1 }
	}
	support = {
	}
}

units = {

	division = {
		name = "4. Brigáda Rychlého Nasazení"
		location = 3418 		#zatec
		division_template = "Brigáda Rychlého Nasazení"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "7. Mechanizovaná Brigáda"
		location = 6590 		#Hranice
		division_template = "Mechanizovaná Brigáda"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

}

instant_effect = {

	add_equipment_to_stockpile = {
		type = MBT_3 	#T-72M4 CZ
		producer = CZH
		amount = 30
	}

	add_equipment_to_stockpile = {
		type = MBT_2 	#T-72
		producer = SOV
		amount = 90
	}

	add_equipment_to_stockpile = {
		type = APC_5 	#Pandur II
		producer = AUS
		amount = 107
	}

	add_equipment_to_stockpile = {
		type = IFV_3 	#BVP-2
		producer = CZH
		amount = 185
	}

	add_equipment_to_stockpile = {
		type = IFV_2 	#BPzV
		producer = CZH
		amount = 76
	}

	add_equipment_to_stockpile = {
		type = IFV_1 	#BVP-1
		producer = CZH
		amount = 147
	}

	add_equipment_to_stockpile = {
		type = SP_arty_1 	#SpGH DANA
		producer = CZH
		amount = 86
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_2 	#Land Rover Defender
		producer = ENG
		amount = 740
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_1 	#UAZ-469
		producer = SOV
		amount = 630
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_1 	#Tatra T815
		producer = CZH
		amount = 3286
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_5 	#Dingo II
		producer = GER
		amount = 19
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_4 	#Iveco LMV
		producer = ITA
		amount = 19
	}

	add_equipment_to_stockpile = {
		type = APC_1 		#BRDM-2
		 #version_name = "BRDM-2"
		producer = SOV
		amount = 35
	}

	add_equipment_to_stockpile = {
		type = SP_Anti_Air_1 		#Strela-10
		 #version_name = "SA-13 Strela-10"
		producer = SOV
		amount = 16
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_1 		#RBS 70
		producer = SWE
		amount = 100
	}

	add_equipment_to_stockpile = {
		type = land_Drone_equipment2 		#RQ-11 Raven
		producer = USA
		amount = 6
	}

	add_equipment_to_stockpile = {
		type = land_Drone_equipment2 		#Elbit Skylark
		producer = ISR
		amount = 2
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons 		#vz. 58
		producer = CZH
		amount = 4000
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons4 		#CZ 805 BREN
		producer = CZH
		amount = 2000
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_1 		#
		producer = SOV
		amount = 100
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1 		#Konkurs
		producer = SOV
		amount = 50
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_0 		#Malyutka
		producer = SOV
		amount = 50
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_2 		#Spike-LR
		producer = ISR
		amount = 50
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_2 		#Javelin
		producer = USA
		amount = 50
	}

	add_equipment_to_stockpile = {
		type = command_control_equipment 		
		producer = CZH
		amount = 200
	}

	add_equipment_to_stockpile = {
		type = MR_Fighter3 		#JAS Gripen
		producer = SWE
		amount = 14
	}

	add_equipment_to_stockpile = {
		type = L_Strike_fighter2 		#Aero L-159 Alca
		producer = CZH
		amount = 21
	}

	add_equipment_to_stockpile = {
		type = attack_helicopter1 		#Mil Mi-24
		producer = SOV
		amount = 17
	}

	add_equipment_to_stockpile = {
		type = transport_helicopter1 		#Mil Mi-8
		producer = SOV
		amount = 9
	}
	
	if = {
		limit = { has_dlc = "Death or Dishonor" }
		AUS = {
			create_production_license = {
				target = CZH
				equipment = {
					type = APC_5			#Pandur II
				}
				cost_factor = 0
			}
		}
		else = {
			add_equipment_production = {
				equipment = {
					type = APC_5
					creator = "AUS"
				}
				requested_factories = 1
				progress = 0.2
				efficiency = 50
			}
		}
	}
		
	
}