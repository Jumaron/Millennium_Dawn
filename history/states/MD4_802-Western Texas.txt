state = {
	id = 802
	name = "STATE_802"
	manpower = 3404030
	state_category = state_04
	
	resources = {
		oil = 19
	}

	history = {
		owner = USA
		victory_points = { 12782 10 } #San Antonio
		victory_points = { 1927 10 } #Austin
		victory_points = { 10740 5 } #El Paso
		victory_points = { 12301 3 } #Lubbock
		victory_points = { 4955 3 } #Odessa
		victory_points = { 5061 3 } #Laredo
		victory_points = { 12875 3 } #Abilene
		victory_points = { 5085 3 } #Wichita Falls
		victory_points = { 12893 3 } #Amarillo
		victory_points = { 5022 3 } #Killeen
		victory_points = { 8064 1 } #San Angelo
		victory_points = { 12728 1 } #Del Rio
		
		buildings = {
			infrastructure = 6
			industrial_complex = 3
			arms_factory = 4
			air_base = 6

		}
		add_core_of = USA
		add_core_of = CSA
		2017.1.1 = {
			add_manpower = 1216655
			}
	}

	provinces = {
		  1915 1927 1954 1998 2019 2055 3875 3972 4955 5022 5038 5061 5085 6798 6987 7836 7981 8064 9773 9908 9920 10740 10894 12301 12341 12728 12782 12848 12875 12893
		
		}
} 
