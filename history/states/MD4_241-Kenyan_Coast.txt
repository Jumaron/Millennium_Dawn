
state = {
	id = 241
	name = "STATE_241"

	history = {
		owner = KEN
		victory_points = {
			5210 5 
		}
		victory_points = {
			6738 1 
		}
		buildings = {
			infrastructure = 2
			air_base = 2
			5210 = {
				naval_base = 4

			}

		}
		add_core_of = KEN
		2017.1.1 = {
			add_manpower = 1170852
		}
	}

	provinces = {
		5210 6725 6738 6796 
	}
	manpower = 4340346
	buildings_max_level_factor = 1.000
	state_category = state_05
}
