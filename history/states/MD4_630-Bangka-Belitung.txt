state = {
	id = 630
	name = "STATE_630"
	manpower = 1303379
	state_category = state_02
	
	resources = {
		tungsten = 27
	}

	history = {
		owner = IND
		victory_points = { 7384 1 } #Pangkalpinang
		victory_points = { 4211 1 } #Tanjungpandan
		
		
		buildings = {
			infrastructure = 3

		}
		add_core_of = IND
		2017.1.1 = {
			add_manpower = 255118
		}
	}

	provinces = {
		7384 7396 4211
		
		}
}
