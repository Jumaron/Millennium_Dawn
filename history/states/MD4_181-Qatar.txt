
state = {
	id = 181
	name = "STATE_181"
	resources = {
		oil = 45.000
		aluminium = 6.000
	}

	history = {
		owner = QAT
		victory_points = {
			3738 5 
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 3
			arms_factory = 4
			air_base = 10
			8051 = {
				naval_base = 10

			}

		}
		add_core_of = QAT
		2017.1.1 = {
			add_manpower = 1785757
		}
	}

	provinces = {
		8051 3728 3738 
	}
	manpower = 590957
	buildings_max_level_factor = 1.000
	state_category = state_01
}
