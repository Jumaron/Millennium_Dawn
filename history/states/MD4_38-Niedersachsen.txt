
state = {
	id = 38
	name = "STATE_38"
	resources = {
		oil = 6.000
	}

	history = {
		owner = GER
		victory_points = {
			3326 5 
		}
		victory_points = {
			6377 5 
		}
		victory_points = {
			9375 5 
		}
		victory_points = {
			6513 1 
		}
		victory_points = {
			11388 1 
		}
		victory_points = {
			309 1 
		}
		victory_points = {
			3326 5 
		}
		victory_points = {
			6377 10 
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 6
			dockyard = 1
			6325 = {
				naval_base = 1

			}
			241 = {
				naval_base = 8

			}

		}
		add_core_of = GER
		2017.1.1 = {
			add_manpower = 83528
		}
	}

	provinces = {
		241 309 526 3234 3271 3326 3395 6263 6325 6349 6377 6513 9238 9264 9281 9375 11264 11360 11388 11402 11468 11493 
	}
	manpower = 8583482
	buildings_max_level_factor = 1.000
	state_category = state_08
}
