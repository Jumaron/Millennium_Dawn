
state = {
	id = 474
	name = "STATE_474"

	history = {
		owner = RAJ
		victory_points = {
			12395 1 
		}
		victory_points = {
			10514 1 
		}
		buildings = {
			infrastructure = 1

		}
		add_core_of = RAJ
		2017.1.1 = {
			add_manpower = 778141
		}
	}

	provinces = {
		1497 1553 10514 11742 11756 
	}
	manpower = 2680262
	buildings_max_level_factor = 1.000
	state_category = state_04
}
