state = {
	id = 492
	name = "STATE_492"
	
	resources = {
		oil = 2
		steel = 2
	}

	history = {
		owner = BAN
		victory_points = {
			12010 5 
		}
		victory_points = {
			11968 1 
		}
		buildings = {
			infrastructure = 1
			industrial_complex = 2
			arms_factory = 1
			dockyard = 1
			air_base = 2
			12010 = {
				naval_base = 6
			}

		}
		add_core_of = BAN
		2017.1.1 = {
			add_manpower = 2000000
			add_manpower = 2000000
			add_manpower = 2000000
			add_manpower = 756150
		}
	}

	provinces = {
		1142 7179 9991 11968 12010 
	}
	manpower = 23405424
	buildings_max_level_factor = 1.000
	state_category = state_13
}