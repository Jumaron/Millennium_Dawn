
state = {
	id = 265
	name = "STATE_265"

	history = {
		owner = MOZ
		victory_points = {
			2120 5 
		}
		buildings = {
			infrastructure = 2
			2120 = {
				naval_base = 1

			}

		}
		add_core_of = MOZ
		2017.1.1 = {
			add_manpower = 1017412
		}
	}

	provinces = {
		2120 11373 
	}
	manpower = 1515293
	buildings_max_level_factor = 1.000
	state_category = state_03
}
