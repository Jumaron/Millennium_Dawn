
state = {
	id = 536
	name = "STATE_536"

	history = {
		owner = CHI
		victory_points = {
			11913 10 
		}
		victory_points = {
			10034 5 
		}
		victory_points = {
			11982 3 
		}
		victory_points = {
			10076 5 
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 8
			arms_factory = 1

		}
		add_core_of = CHI
		2017.1.1 = {
			add_manpower = 2000000
			add_manpower = 1113602
		}
	}

	provinces = {
		996 1146 4148 7120 10034 10076 11913 11982 12052 13142 
	}
	manpower = 30964398
	buildings_max_level_factor = 1.000
	state_category = state_14
}
