
state = {
	id = 12
	name = "STATE_12"
	resources = {
		steel = 1.000
	}

	history = {
		owner = ENG
		victory_points = {
			9297 10 
		}
		victory_points = {
			3369 5 
		}
		victory_points = {
			6270 1 
		}
		victory_points = {
			6221 1 
		}
		victory_points = {
			11471 1 
		}
		victory_points = {
			11345 1 
		}
		buildings = {
			infrastructure = 6
			arms_factory = 6
			industrial_complex = 4
			air_base = 6
			3369 = {
				naval_base = 2

			}

		}
		add_core_of = ENG
		2017.1.1 = {
			add_manpower = 907733
		}
	}

	provinces = {
		3241 3301 3369 6221 6270 6301 6378 9297 11345 11471 
	}
	manpower = 7539693
	buildings_max_level_factor = 1.000
	state_category = state_07
}
