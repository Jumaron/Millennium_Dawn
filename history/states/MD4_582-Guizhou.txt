
state = {
	id = 582
	name = "STATE_582"
	
	resources = {
		aluminium = 4
	}

	history = {
		owner = CHI
		victory_points = {
			3031 5 
		}
		victory_points = {
			4504 1 
		}
		victory_points = {
			4325 5 
		}
		victory_points = {
			7549 1 
		}
		victory_points = {
			1474 1 
		}
		victory_points = {
			10763 1 
		}
		victory_points = {
			1884 1 
		}
		buildings = {
			infrastructure = 3
			industrial_complex = 4
			arms_factory = 1

		}
		add_core_of = CHI
		2017.1.1 = {
			add_manpower = 2000000
			add_manpower = 1271265
		}
	}

	provinces = {
		1010 1474 1884 4325 4431 4504 4532 7521 7549 7577 8095 9939 10616 10763 10819 359 2996 3003 3009 3013 3031 3034 3050 3059 3065 
	}
	manpower = 32532341
	buildings_max_level_factor = 1.000
	state_category = state_14
}
