
state = {
	id = 304
	name = "STATE_304"

	history = {
		owner = DRC
		RCD = {
			set_province_controller = 3746
			set_province_controller = 8016
			set_province_controller = 3765
		}
		victory_points = {
			2024 10 
		}
		victory_points = {
			1731 1 
		}
		victory_points = {
			3765 1 
		}
		victory_points = {
			3490 2 
		}
		buildings = {
			infrastructure = 0
			air_base = 1

		}
		add_core_of = DRC
		add_core_of = RCD
		add_core_of = MLC
		2017.1.1 = {
			remove_core_of = RCD
			remove_core_of = MLC
			DRC = {
				set_province_controller = 3746
				set_province_controller = 8016
				set_province_controller = 3765
			}
			add_manpower = 2000000
			add_manpower = 1021485
		}
	}

	provinces = {
		1731 2024 8016 3765 3490 3746 
	}
	manpower = 4240845
	buildings_max_level_factor = 1.000
	state_category = state_05
}
