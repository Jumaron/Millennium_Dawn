
state = {
	id = 548
	name = "STATE_548"

	history = {
		owner = CHI
		victory_points = {
			4137 10 
		}
		buildings = {
			infrastructure = 7
			industrial_complex = 3
			dockyard = 3
			4137 = {
				naval_base = 4

			}

		}
		add_core_of = CHI
		2017.1.1 = {
			add_manpower = 1604550
		}
	}

	provinces = {
		4137 10068 3156 
	}
	manpower = 15957050
	buildings_max_level_factor = 1.000
	state_category = state_10
}
