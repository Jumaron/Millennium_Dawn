
state = {
	id = 581
	name = "STATE_581"
	
	resources = {
		tungsten = 1
	}

	history = {
		owner = CHI
		victory_points = {
			10413 5 
		}
		victory_points = {
			261 1 
		}
		victory_points = {
			5009 1 
		}
		buildings = {
			infrastructure = 3

		}
		add_core_of = CHI
		2017.1.1 = {
			add_manpower = 341309
		}
	}

	provinces = {
		5009 10413 10776 261 274 9078 
	}
	manpower = 3394275
	state_category = state_04
}
