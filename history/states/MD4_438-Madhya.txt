
state = {
	id = 438
	name = "STATE_438"

	history = {
		owner = RAJ
		victory_points = {
			12080 5 
		}
		victory_points = {
			2105 5 
		}
		victory_points = {
			4106 1 
		}
		victory_points = {
			11993 1 
		}
		victory_points = {
			4950 1 
		}
		buildings = {
			infrastructure = 2
			industrial_complex = 1
			arms_factory = 1

		}
		add_core_of = RAJ
		2017.1.1 = {
			add_manpower = 2000000
			add_manpower = 2000000
			add_manpower = 2000000
			add_manpower = 2000000
			add_manpower = 236994
		}
	}

	provinces = {
		2105 4106 4950 7205 8076 10816 10872 11993 12080 9096 9618 10334 
	}
	manpower = 28371866
	buildings_max_level_factor = 1.000
	state_category = state_14
}
