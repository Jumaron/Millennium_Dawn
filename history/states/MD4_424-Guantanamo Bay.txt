
state = {
	id = 424
	name = "STATE_424"

	history = {
		owner = USA
		victory_points = { 13247 1 } #Guantanamo Bay
		
		buildings = {
			infrastructure = 5
			
			13247 = {
				naval_base = 6
			}

		}
		add_core_of = USA

	}

	provinces = {
		 13247
	}
	manpower = 5000
	buildings_max_level_factor = 1.000
	state_category = state_00
}
