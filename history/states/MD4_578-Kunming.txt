
state = {
	id = 578
	name = "STATE_578"
	
	resources = {
		aluminium = 1
		tungsten = 3
	}

	history = {
		owner = CHI
		victory_points = {
			2992 5 
		}
		victory_points = {
			4192 2 
		}
		victory_points = {
			12841 1 
		}
		victory_points = {
			5072 1 
		}
		victory_points = {
			7606 2 
		}
		victory_points = {
			12282 2 
		}
		victory_points = {
			10346 2 
		}
		buildings = {
			infrastructure = 4
			industrial_complex = 2
			arms_factory = 2
			air_base = 7

		}
		add_core_of = CHI
		2017.1.1 = {
			add_manpower = 1602940
		}
	}

	provinces = {
		4192 5072 7446 7606 10346 12282 12841 2992 3005 3045 3058 
	}
	manpower = 15941047
	state_category = state_10
}
