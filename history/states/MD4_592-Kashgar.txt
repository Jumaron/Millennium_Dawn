state = {
	id = 592
	name = "STATE_592"
	manpower = 8854969
	state_category = state_08
	
	resources = {
		steel = 3
		aluminium = 1
	}

	history = {
		owner = CHI
		
		victory_points = { 2015 5 } #Kashgar
		victory_points = { 7792 5 } #Korla
		victory_points = { 12526 5 } #Gulja
		victory_points = { 4682 2 } #Aksu
		victory_points = { 7702 2 } #Kuqa
		victory_points = { 1970 1 } #Yarkant
		victory_points = { 8112 1 } #Hotan

		
		buildings = {
			infrastructure = 3
			industrial_complex = 2
			air_base = 2
			
		}
		add_core_of = CHI
		2017.1.1 = {
			add_manpower = 890405
		}
	}

	provinces = {
		 7792 7702 4770 4682 1703 12526 12601 7949 4828 12119 1766 5087 2015 1970 8112
		
		}
}
