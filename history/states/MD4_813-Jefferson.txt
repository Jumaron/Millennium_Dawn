state = {
	id = 813
	name = "STATE_813"
	manpower = 3985838
	state_category = state_05

	history = {
		owner = USA
		victory_points = { 9713 5 } #Sacramento
		victory_points = { 6681 3 } #Santa Rosa
		victory_points = { 9697 3 } #Chico
		victory_points = { 3588 3 } #Redding
		victory_points = { 610 1 } #Eureka
		victory_points = { 11658 3 } #Vallejo
		
		buildings = {
			infrastructure = 6
			industrial_complex = 4
			arms_factory = 1
			air_base = 2
			
			610 = {
				naval_base = 2
			}

		}
		add_core_of = USA
		2017.1.1 = {
			add_manpower = 648857
			}
	}

	provinces = {
		  710 9713 11658 677 6681 6757 9730 610 3644 6482 6665 4331 10159 11698 766 9697 3588 6745 768 6772 781
		
		}
}
