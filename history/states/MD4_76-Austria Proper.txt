
state = {
	id = 76
	name = "STATE_76"
	resources = {
		steel = 4.000
	}

	history = {
		owner = AUS
		victory_points = {
			11666 10 
		}
		victory_points = {
			9665 5 
		}
		buildings = {
			infrastructure = 6
			arms_factory = 5
			industrial_complex = 4
			air_base = 6

		}
		add_core_of = AUS
		2017.1.1 = {
			add_manpower = 350190	
		}
	}

	provinces = {
		704 732 3684 3700 3703 3718 6552 6690 6708 6723 9527 9661 9665 11651 11666 
	}
	manpower = 4691831
	buildings_max_level_factor = 1.000
	state_category = state_05
}
