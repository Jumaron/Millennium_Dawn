
state = {
	id = 640
	name = "STATE_640"
	resources = {
		oil = 3.000
		steel = 1.000
		aluminium = 20.000
	}

	history = {
		owner = IND
		victory_points = {
			4279 5 
		}
		victory_points = {
			2184 3 
		}
		victory_points = {
			12129 1 
		}
		victory_points = {
			7227 1 
		}
		victory_points = {
			12280 1 
		}
		victory_points = {
			4409 1 
		}
		buildings = {
			infrastructure = 2
			industrial_complex = 1
			air_base = 4
			4279 = {
				naval_base = 6

			}

		}
		add_core_of = IND
		2017.1.1 = {
			add_manpower = 1184558
		}
	}

	provinces = {
		1230 1316 2170 2184 2222 4279 4335 4409 5176 6081 6090 6091 7227 7424 10138 10282 10310 12110 12129 12280 12955 
	}
	manpower = 6051821
	buildings_max_level_factor = 1.000
	state_category = state_06
}
