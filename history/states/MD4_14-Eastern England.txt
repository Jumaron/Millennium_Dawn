
state = {
	id = 14
	name = "STATE_14"
	resources = {
		oil = 6.000
	}

	history = {
		owner = ENG
		victory_points = {
			6237 5 
		}
		victory_points = {
			11279 3 
		}
		victory_points = {
			322 1 
		}
		victory_points = {
			11221 1 
		}
		victory_points = {
			9268 1 
		}
		victory_points = {
			9268 1 
		}
		victory_points = {
			221 1 
		}
		buildings = {
			infrastructure = 6
			arms_factory = 4
			industrial_complex = 4
			air_base = 10
			271 = {
				naval_base = 3

			}

		}
		add_core_of = ENG
		2017.1.1 = {
			add_manpower = 782232
		}
	}

	provinces = {
		221 271 296 322 3287 3353 9250 9268 11253 11279 11374 13262 
	}
	manpower = 6497269
	buildings_max_level_factor = 1.000
	state_category = state_06
}
