
state = {
	id = 679
	name = "STATE_679"
	resources = {
		oil = 66.000
	}

	history = {
		owner = SOV
		victory_points = {
			12517 4 
		}
		victory_points = {
			4669 3 
		}
		victory_points = {
			1684 3 
		}
		victory_points = {
			7689 3 
		}
		victory_points = {
			10536 3 
		}
		buildings = {
			infrastructure = 3
			industrial_complex = 1
			air_base = 4

		}
		add_core_of = SOV
		2017.1.1 = {
			add_manpower = -11081
		}
	}

	provinces = {
		96 1664 1684 3415 4669 4761 7689 7738 7809 8461 9142 10536 10564 12517 12545 13076 
	}
	manpower = 578985
	buildings_max_level_factor = 1.000
	state_category = state_01
}
