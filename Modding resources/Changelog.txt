v0.2.2
#New features
- A rewritten anti-bully system
- Unstable (low stability) Neighbors will lower your stability 

#Focus Trees

#Events

#Decisions
- Some fixes to existing intervention missions
- New NATO-related decisions (leave, join, call to arms)

#Units and combat

#Map

#OOBs

#Countries
- Added drug cartel ideas to South America

#Database
- More namelists for countries (N->O)
- Removed the Ulema internal faction from Yemen
- Removed convoys from Zimbabwe
- Added Yukio Edano to Japan
- Lowered the approval rating of Korean government
- Removed 800+ provinces to improve performance
- Added Obama portrait by Tekoa


#Tech

#AI
- Some improvements with AI templates and production
- HEZ/PER now support all Shia countries
- Testing dynamic AI relations

#GFX
- Correct portrait for Andriy Tarasenko
- Remade some US focus icons
- Replaced Putin with a cat

#Music

#Interface

#Stability

#Bugfixes
- Fixed one minor unit name error
- Fixed Canals
- Fixed some localisation issues
- Various other bugs

#Localisation
- Descriptions for CAS and transport planes

v0.2.1

- Changed colour of Nigeria and catalonia
- Icons for land doctrines
- Generic decisions for middle east
- fixes to internal factions
- Everyone can create a faction
- Guaranteeing more limited
- Countries more reluctant to join factions
- Fixed PER decission
- Increased factors for AI to increase manpower related laws
- Fix US decision to intervene in Syria
- Nuclear Cruiser localization fixed

v0.2

#New features
- Added Internal Factions
- Tech slots are now based on total amount of industry and development level
- Added Foreign Intervention Law
- NATO is no longer a faction, but countries defend each other only if attacked
- CIC now consumes oil
- Countries are now influenced towards their neighbours' outlooks
- Added approval rating to leaders
- Added an election system where the victor is determined by idelogy and then by approval rating
- Added Trade Laws
- Integrated the MD4 Generals submod

#Focus Trees
- Added actual effects to most Brazilian focuses
- More stuff to US focus tree
- Nerfed some bonuses of the generic tree
- Generic focus tree industry section no longer gives free slots, but requires you to have slots

#Events
- Some flavor events for China
- Kurdish Independence Referendum event series
- Event to remove Cypercaliphate idea if ISIS is defeated
- Events for interactions between ISIS and Tahrir Al-Sham
- Events for interactions between FSA and Tahrir Al-Sham
- Coalition Airstrikes against ISIS
- Qatari-Saudi Relation Crisis
- Electoral College for USA
- Added back capitulation events
- News event for the ending of the Somali Civil War
- Added the option to play as the Muslim Brotherhood during a civil war
- Pakistan gets a unique Salafist civil war
- Added events about the PKK
- Syria/Iraq should no longer whitepeace the Kurds

#Units and combat
- Made units slightly faster
- Increased combat width from support attacks
- Rebalanced some terrain modifiers
- Amphibious landings are a bit more deadly
- Gave tanks more hard attack
- Increased defense of IFVs and APCs
- Carrier Light Strike Fighters can now actually land on a carrier
- Support units no longer give massive terrain penalties
- Nerfed recon of support companies
- Generals can now command 12 units instead of 18
- Reduced both unit and leader XP gain
- Ships now require less oil to produce
- Nerfed strategic bombing
- Made AT and utility vehicles more expensive, tanks cheaper
- Gave IFVs and APCs more piercing, reduced piercing of AT

#Map
- Increased infrastrucute in Homs by 1
- Traded one CIC to a MIC for ISIS
- Made Sinai a demilitarised zone
- Added Spratly Islands
- Added Paracel Islands
- Added Mandeb, Hormuz, Palk, Sunda, Dover and Malacca straits
- Added some skyscrapers to big cities
- Changed the color of Darfur (this was important, I guess?)
- Gave Lebanon and Hezbollah cores on each other
- Added naval bases in Al Ghaydah and Murqub
- Added impassable areas
- Made Bahrain a little bigger
- Added Jebel Ali, Kuwait, Masirah and NSA Bahrain foreign military bases
- Added level 1 infra to various civil war countries as not to block supplies
- Corrected Uzbek-Kazakh border
- Changed cultural textures of various regions
- Pakistani Taliban now gets cores on Taliban
- Made some coastal province be actually coastal
- Okinawa no longer has weird connections
- Redrew Iraq
- Put a lot more lights on the night map
- Added some more VPs to Finland

#OOBs
- Updated Brazilian OOB
- Updates to Indonesian OOB
- Relocated some Israeli units
- Some rebalancing of Syrian factions
- Added Popular Mobilisation Front units to Iraq
- Renamed some North Korean units
- Gave Al-Shabaab two extra units
- Increased the size of US marine template
- Added US Combat Aviation Brigades and Marine Aircraft Wings
- Updated Norwegian OOB

#Countries
- Updated Brazilian equipment and techs
- Changed Turkish Salafist party to Tevhid Hareketi
- Made Shinzo Abe conservative instead of liberal
- Added leaders and political parties to ALG, ARG, PAK, IND and UKR
- Added leaders for BHR, KOR, ICE, IRQ, NKO and SOV
- Added generals to Indonesia
- Added Utility Vehicles for Philippines
- Added USAID idea to multiple countries
- Gave China its own faction and increased tension limit for joining another faction
- Distributed Security Spending and Administrative Spending to countries
- Japan now has Women Volunteers instead of No Women in the Military
- Norway now has Drated Women instead of Women Volunteers
- Removed unnecessary techs from Turkey

#Database
- Added namelists for ARM -> MAD
- Reduced effects on conscription of Syria: A House Divided
- Reduced the speed nerf from corruption
- Only Western countries can lower World Tension
- Increased starting level of World Tension
- Changed World Tension balance from wars
- Nerfed Political Autonomy idea to a third
- Changing to another Outlook should now be easier
- Increased bonuses from Research Companies
- Reduced penalty for researching future techs
- Increased base daily PP by 50%
- Increased Tax Cost from being in debt
- Added PP penalty for debt
- Added construction speed bonus to having net reserves
- Increased starting military spending of Afghanistan
- Increased threat caused by war justification
- Reduced resistance growth
- Conscripting women into the army now has consequences

#Tech
- Changed historical years of fixed wing aircraft

#AI
- Taught AI to increase military spending
- USA will now send help and volunteers to Rojava
- Iraq less friendly with Kurdistan
- Hezbollah and Iran should be more friendly now
- AI should be more willing to grant licenses now
- Added AI strategies concerning the Somalian Civil War
- Saudi Arabia is now less reluctant to send volunteers to Rojava and Tuaregs
- Saudi Arabia now more likely to send volunteers to Pakistan
- ISIS will avoid taking Expansionism focus so they won't suicide against their neighbours

#GFX
- New icons for Conscription and Women in the Army laws
- Added more unique icons for US focuses
- Icon for Shanghai Cooperation Organisation
- Icon for Russian Legacy
- Recon tanks for Brazil
- MBTs for Brazil
- Converted some leader portraits to MD4 standards
- New non-NATO counters
- All unique defence companies should now have icons
- Hopefully finally fixed missing M16s
- More and better country leader portraits
- Added some generic portraits
- Couple new loading screens
- Ship icons and counters
- All historical leaders should now have portraits

#Music
- Added some special tunes when playing a Salafist country (sponsored by Beats by Dre)
- New intro song

#Interface
- Fixed air combat interface
- You no longer get 999 of resources

#Stability
- Fixed crash from editing a Chinese marine template
- Changing doctrines no longer crashes the game
- Fixed a crash when annexing Libya

#Bugfixes
- Corrected election dates for multiple countries
- Renamed a land tech which was causing an error
- Removed unique puppet names for countries leftover from TfV
- Fixed missing event picture for Shayrat Missile Strike
- Montenegro joins Nato should only trigger once now
- Fixed description of SA-11 Buk
- A lost Venezuelan unit is now back in Venezuela
- Incorrect trigger in a Yemeni event
- Fixed missing portrait for Abdulla Yameen
- Some syntax errors in various OOBs
- ISIS now correctly loses all their cores after defeat
- Fixed typos in OOBs
- Added missing Saudi small arms
- Some various localisation fixes
- Fixed a bug with Libyan civil war cleanup event

#Localisation
- Added French localisation (kudos to Bill Nye the Russian Spy)
- Added string-based localisation for factions
- Reworked some localisation of US focuses
- Al Khalid II for Pakistan
- Post-victory Houthi Yemen no longer called Socialist Yemen
- Changed tooltip of changing corruption
- Gave some starting wars unique names