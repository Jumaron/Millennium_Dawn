Help_Decisions = {
	
	disable_help = {
	
		complete_effect = {
			clr_country_flag = Help_Expanded
			clr_country_flag = Help_Collapsed
			set_country_flag = Help_Disabled
		}
	}

	expand_help = {
		visible = {
			NOT = { has_country_flag = Help_Expanded }
		}
		
		complete_effect = {
			clr_country_flag = Help_Collapsed
			set_country_flag = Help_Expanded
		}
	}
	
	collapse_help = {
	
		visible = {
			has_country_flag = Help_Expanded
		}
	
		complete_effect = {
			clr_country_flag = Help_Expanded
			set_country_flag = Help_Collapsed
		}
	}
	
	help_economics = {
	
		visible = {
			has_country_flag = Help_Expanded
		}
		
		complete_effect = {
			country_event = help_events.0
		}
	}
	
	help_units = {
	
		visible = {
			has_country_flag = Help_Expanded
		}
		
		complete_effect = {
			country_event = help_events.3
		}
	}
}