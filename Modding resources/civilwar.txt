﻿add_namespace = civilwar_emerging
add_namespace = civilwar_western
add_namespace = civilwar_nationalist
############################################Western Civil War############################################

#####Protests
country_event = {#
    id = civilwar_western.1
    title = civilwar_western.1.t
    desc = civilwar_western.1.d
	
	trigger = {
		any_country = { democratic > 0.30 }
		NOT = { has_government = democratic }
		OR = {
		has_country_flag = gdp_7
		has_country_flag = gdp_6
		has_country_flag = gdp_5
		has_country_flag = gdp_4	
		has_country_flag = gdp_3
		has_country_flag = gdp_2
		has_country_flag = gdp_1
    }
    	OR = {
    	#has_idea = blind_eye  
    	#has_idea = low_crime_fighting 
        }
		
    	OR = {
    	#has_idea = very_low_social 
    	#has_idea = low_social 
        }		
    	    has_idea = censored_press
    }
	
	mean_time_to_happen = { days = 30 }

    option = {
        name = civilwar_western.1.a
		add_popularity = {
			ideology = democratic
			popularity = 0.05
		}
		set_country_flag = free_speech_protests
	}
}

#more protests
country_event = {
    id = civilwar_western.2
    title = civilwar_western.2.t
    desc = civilwar_western.2.d
	
	trigger = {
	if = {
		any_country = { has_country_flag = free_speech_protests }
			}
		}
	mean_time_to_happen = { days = 30 }
	
		#######Resist the protests
    option = {
        name = civilwar_western.2.a
		clr_country_flag = free_speech_protests
		set_country_flag = violent_protests
			set_country_flag = violent_protests
			add_popularity = {
			ideology = democratic
			popularity = 0.10
		}
    }
	option = {
    name = civilwar_western.2.b
	clr_country_flag = free_speech_protests
	set_country_flag = violent_protests
			add_popularity = {
			ideology = democratic
			popularity = 0.05
		}
    }
	##### Give them what they want
    option = {
        name = civilwar_western.2.c
		set_politics = { ruling_party = democratic elections_allowed = yes }
		set_political_party = { ideology = communism popularity = 0.10 }
		set_political_party = { ideology = nationalist popularity = 0.15 }
		set_political_party = { ideology = neutrality popularity = 0.05 }
		set_political_party = { ideology = democratic popularity = 0.70 }
		clr_country_flag = free_speech_protests
    }
}
######Rioting
country_event = {
    id = civilwar_western.3
    title = civilwar_western.3.t
    desc = civilwar_western.3.d
	
	trigger = {
	if = {
		any_country = { has_country_flag = violent_protests }
			}
		}
	mean_time_to_happen = { days = 100 }
	
	#######Riots
    option = {
        name = civilwar_western.3.a
		clr_country_flag = violent_protests
		set_country_flag = riots
			add_popularity = {
			ideology = democratic
			popularity = 0.10
		}
    }
	##### Give them what they want
    option = {
        name = civilwar_western.3.b
		set_politics = { ruling_party = democratic elections_allowed = yes }
		set_political_party = { ideology = communism popularity = 0.10 }
		set_political_party = { ideology = nationalist popularity = 0.15 }
		set_political_party = { ideology = neutrality popularity = 0.05 }
		set_political_party = { ideology = democratic popularity = 0.70 }
		clr_country_flag = violent_protests
    }
}

country_event = {
    id = civilwar_western.4
    title = civilwar_western.4.t
    desc = civilwar_western.4.d
	
	trigger = {
	if = {
		any_country = { has_country_flag = riots }
			}
		}
	mean_time_to_happen = { days = 120 }
	
	#######civilwar
    option = {
        name = civilwar_western.4.a
		start_civil_war = { ideology = democratic size = 0.5}
    }
}
############################################Emerging Civil War############################################

#####Protests
#country_event = {#
    id = civilwar_emerging.1
    title = civilwar_emerging.1.t
    desc = civilwar_emerging.1.d
	
	trigger = {
		any_country = { communism > 0.30 }
		NOT = { has_government = communism }
		OR = {
		has_country_flag = gdp_7
		has_country_flag = gdp_6
		has_country_flag = gdp_5
		has_country_flag = gdp_4	
		has_country_flag = gdp_3
		has_country_flag = gdp_2
		has_country_flag = gdp_1
}
	OR = {
	#has_idea = blind_eye
	#has_idea = low_crime_fighting
}
	OR = {
	#has_idea = very_low_social
	#has_idea = low_social 
}
	}
	
	mean_time_to_happen = { days = 30 }

    option = {
        name = civilwar_emerging.1.a
		add_popularity = {
			ideology = communism
			popularity = 0.05
		}
		set_country_flag = economic_reformers
	}
}

#more protests
#country_event = {#
    id = civilwar_emerging.2
    title = civilwar_emerging.2.t
    desc = civilwar_emerging.2.d
	
	trigger = {
	if = {
		any_country = { has_country_flag = economic_reformers }
			}
		}
	mean_time_to_happen = { days = 30 }
	
		#######Resist the protests
    option = {
        name = civilwar_emerging.2.a
		clr_country_flag = economic_reformers
		set_country_flag = revolutionaries
		add_popularity = {
			ideology = communism
			popularity = 0.10
		}
    }
    option = {
        name = civilwar_emerging.2.b
		clr_country_flag = economic_reformers
		set_country_flag = revolutionaries
		add_popularity = {
			ideology = communism
			popularity = 0.05
		}
    }
	##### Give them what they want
    option = {
        name = civilwar_emerging.2.c
		set_politics = { ruling_party = communism elections_allowed = yes }
		set_political_party = { ideology = communism popularity = 0.70 }
		set_political_party = { ideology = nationalist popularity = 0.15 }
		set_political_party = { ideology = neutrality popularity = 0.05 }
		set_political_party = { ideology = democratic popularity = 0.10 }
		clr_country_flag = economic_reformers
    }
}
######Rioting
#country_event = {
    id = civilwar_emerging.3
    title = civilwar_emerging.3.t
    desc = civilwar_emerging.3.d
	
	trigger = {
	if = {
		any_country = { has_country_flag = revolutionaries }
			}
		}
	mean_time_to_happen = { days = 100 }
	
	#######Riots
    option = {
        name = civilwar_emerging.3.a
		clr_country_flag = revolutionaries
		set_country_flag = people_army
    }
	##### Give them what they want
    option = {
        name = civilwar_emerging.3.b
		set_politics = { ruling_party = communism elections_allowed = yes }
		set_political_party = { ideology = communism popularity = 0.70 }
		set_political_party = { ideology = nationalist popularity = 0.15 }
		set_political_party = { ideology = neutrality popularity = 0.05 }
		set_political_party = { ideology = democratic popularity = 0.10 }
		clr_country_flag = revolutionaries
    }
}
#country_event = {
    id = civilwar_emerging.4
    title = civilwar_emerging.4.t
    desc = civilwar_emerging.4.d
	
	trigger = {
	if = {
		any_country = { has_country_flag = people_army }
			}
		}
	mean_time_to_happen = { days = 120 }
	
	#######civilwar
    option = {
        name = civilwar_western.4.a
		start_civil_war = { ideology = communism size = 0.5}
		clr_country_flag = people_army
    }
}
############################################Nationalist Civil War############################################

#####Protests
#country_event = {#
    id = civilwar_nationalist.1
    title = civilwar_nationalist.1.t
    desc = civilwar_nationalist.1.d
	
	trigger = {
		NOT = { has_government = nationalist }
		OR = {
		has_country_flag = gdp_7
		has_country_flag = gdp_6
		has_country_flag = gdp_5
		has_country_flag = gdp_4	
		has_country_flag = gdp_3
		has_country_flag = gdp_2
		has_country_flag = gdp_1
	}
	OR = {
		#has_idea = blind_eye
		#has_idea = low_crime_fighting
	}
	OR = {
		#has_idea = very_low_social
		#has_idea = low_social
	}
}

	mean_time_to_happen = { days = 30 }

    option = {
        name = civilwar_nationalist.1.a
		add_popularity = {
			ideology = nationalist
			popularity = 0.05
		}
		set_country_flag = nationalistic_rally
	}
}

#more protests
#country_event = {
    id = civilwar_nationalist.2
    title = civilwar_nationalist.2.t
    desc = civilwar_nationalist.2.d
	
	trigger = {
	if = {
		any_country = { has_country_flag = nationalistic_rally }
			}
		}
	mean_time_to_happen = { days = 30 }
	
		#######Resist the protests
    option = {
        name = civilwar_nationalist.2.a
		clr_country_flag = nationalistic_rally
		set_country_flag = nationalistic_campaigns
		add_popularity = {
		ideology = nationalist
		popularity = 0.10
		}
    }
    option = {
        name = civilwar_nationalist.2.b
		clr_country_flag = nationalistic_rally
		set_country_flag = nationalistic_campaigns
		add_popularity = {
		ideology = nationalist
		popularity = 0.05
		}
    }
}
######Rioting
#country_event = {
    id = civilwar_nationalist.3
    title = civilwar_nationalist.3.t
    desc = civilwar_nationalist.3.d
	
	trigger = {
	if = {
		any_country = { has_country_flag = nationalistic_telegraphs }
			}
		}
	mean_time_to_happen = { days = 100 }
	
	#######Riots
    option = {
        name = civilwar_nationalist.3.a
		clr_country_flag = nationalistic_telegraphs
		set_country_flag = racial_attacks
    }
}
#country_event = {
    id = civilwar_nationalist.4
    title = civilwar_nationalist.4.t
    desc = civilwar_nationalist.4.d
	
	trigger = {
	if = {
		any_country = { has_country_flag = nationalistic_telegraphs }
			}
		}
	mean_time_to_happen = { days = 120 }
	
	#######civilwar
    option = {
        name = civilwar_nationlist.4.a
		start_civil_war = { ideology = nationalist size = 0.5}
		add_stability = 0.4
    }
}