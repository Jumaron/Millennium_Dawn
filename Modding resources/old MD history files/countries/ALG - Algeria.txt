﻿capital = 459

oob = "ALG_2000"

set_convoys = 20
set_stability = 0.5

set_country_flag = country_language_arabic
set_country_flag = country_language_tamazight

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	corvette1 = 1
	corvette2 = 1
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_2 = 1
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_rapid
	african_union_member
	arab_league_member
	limited_conscription
}

set_country_flag = dominant_religion_islam
set_country_flag = sunni_islam

give_guarantee = WSH

set_politics = {

	parties = {
		islamist = {
			popularity = 15
		}
		nationalist = {
			popularity = 15
		}
		conservative = {
			popularity = 16
		} 
		social_liberal = {
			popularity = 4
		}
		democratic_socialist = {
			popularity = 45
		}
		communist = {
			popularity = 5
		}
	}
	
	ruling_party = democratic_socialist
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = yes
}

add_opinion_modifier = {
	target = ISR
	modifier = death_to_israel
}

create_country_leader = {
	name = "Abdelaziz Buteflika"
	ideology = democratic_socialist_ideology
	picture = "Abdelaziz_Bouteflika.dds"
}

create_country_leader = {
	name = "Saad Abdallah Djaballah"
	ideology = islamic_authoritarian
	picture = "Abdallah_Djaballah.dds"
}

create_country_leader = {
	name = "Moussa Touati"
	ideology = fiscal_conservative
	picture = "Moussa_Touati.dds"
}

create_country_leader = {
	name = "Bouguerra Soltani"
	ideology = counter_progressive_democrat
	picture = "Bouguerra_Soltani.dds"
}

create_country_leader = {
	name = "Amara Benyounès"
	ideology = social_democrat_ideology
	picture = "Amara_Benyounes.dds"
}

create_country_leader = {
	name = "Louisa Hanoune"
	ideology = marxist
	picture = "Louisa_Hanoune.dds"
}
create_country_leader = {
	name = "Ahmed Ouyahia"
	picture = "Ahmed_Ouyahia.dds"
	ideology = libertarian
}
	
create_country_leader = {
	name = "Redha Malek"
	ideology = national_democrat
	picture = "Redha_Malek.dds"
}

create_country_leader = {
	name = "Abderrazak Makri"
	ideology = counter_progressive_democrat
	picture = "Abderrazak_Makrii.dds"
}

create_country_leader = {
	name = "Idris Khodair"
	picture = "Idris_Khodair.dds"
	ideology = green
}

create_country_leader = {
	name = "Saïd Sadi"
	picture = "Said_Sadi.dds"
	ideology = liberalist
}

create_country_leader = {
	name = "Tahar Benbaïbeche"
	picture = "Tahar_Benbaibeche.dds"
	ideology = national_socialist
}


create_country_leader = {
	name = "Khaldoon Makki Al-Hassani"
	ideology = absolute_monarchist
	picture = "Khaldoon.dds"
}

create_corps_commander = { 
	name = "Kaidi Mohamed" 
	picture = "Kaidi_Mohamed.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = { 
	name = "Khaled Nezzar" 
	picture = "Khaled_Nezzar.dds"
	traits = { bearer_of_artillery }
	skill = 2
}                   

create_corps_commander = { 
	name = "Mohamed Mediène" 
	picture = "Mohamed_Medine.dds"
	traits = { commando }
	skill = 1
} 

create_navy_leader  = { 
	name = "Mohamed-Larbi Haouli" 
	picture = "Mohamed_Larbi_Haouli.dds"
	traits = { seawolf }
	skill = 2
} 
create_navy_leader  = { 
	name = "Malik Nasib" 
	picture = "Malik_Nasib.dds"
	traits = { blockade_runner }
	skill = 1
} 

2005.7.17 = {
    create_country_leader = {
	name = "Mohcine Belabbas"
	picture = "Mohcine_Belabbas.dds"
	ideology = liberalist
}

create_equipment_variant = {
	name = "Koni class"
	type = frigate_2
	parent_version = 0
	upgrades = {
		ship_torpedo_upgrade = 1
	}
}
2017.1.1 = {

oob = "ALG_2017"

set_technology = { 
	legacy_doctrines = 1 
	modern_blitzkrieg = 1 
	forward_defense = 1 
	encourage_nco_iniative = 1 
	air_land_battle = 1
	#License produced AK-47s and AK-74s
	infantry_weapons = 1
	infantry_weapons1 = 1
	
	#Nimr in cooperation with UAE
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	util_vehicle_equipment_2 = 1
	util_vehicle_equipment_3 = 1
	
	#For templates
	
	night_vision_1 = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	gw_artillery = 1
	SP_arty_equipment_0 = 1
	SP_R_arty_equipment_0 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	Early_APC = 1
	APC_1 = 1
	IFV_1 = 1
	Rec_tank_0 = 1
	MBT_1 = 1
	
	#Djebel Chenoua-Class
	corvette_1 = 1
	corvette_2 = 1
	missile_corvette_1 = 1
	
	landing_craft = 1
	
}

add_ideas = {
	pop_050
	unrestrained_corruption
	gdp_3
	sunni
	rentier_state
    export_economy
	LoAS_member
	depression
	defence_05
	edu_03
	health_04
	social_05
	bureau_03
	police_04
	al_jazeera_banned
	draft_army
	volunteer_women
	Enduring_Freedom
	fossil_fuel_industry
	the_military
	The_Ulema
	hybrid
}
set_country_flag = gdp_3
set_country_flag = Major_Importer_RUS_Arms
set_country_flag = enthusiastic_fossil_fuel_industry
set_country_flag = enthusiastic_the_military
set_country_flag = negative_The_Ulema


#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot
	complete_national_focus = Generic_16_IC_slot
	set_politics = {

	parties = {
		democratic = { 
			popularity = 10
		}

		fascism = {
			popularity = 16
		}
		
		communism = {
			popularity = 54
			#banned = no #default is no
		}
		
		neutrality = { 
			popularity = 15
		}
		
		nationalist = {
			popularity = 5
		}
	}
	
	ruling_party = communism
	last_election = "2014.4.17"
	election_frequency = 60
	elections_allowed = yes
}

add_opinion_modifier = { target = MOR modifier = ALG_MOR_Is_Occuping_WS_Border_Closed }

create_country_leader = {
	name = "Ali Benhadj"
	desc = ""
	picture = "algeria_ali-benhadj.dds"
	ideology = Caliphate
	traits = {
		#
	}
}

create_country_leader = {
	name = "Ali Laskri"
	desc = ""
	picture = "ALG_Ali_Laskri.dds"
	ideology = Conservative
	traits = {
		#
	}
}

create_country_leader = {
	name = "Said Bouteflika"
	picture = "Said_bouteflika.dds"
	expire = "2077.1.1"
	ideology = anarchist_communism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Abdelaziz Bouteflika"
	picture = "ALG_Abdelaziz_Bouteflika.dds"
	expire = "2020.1.1"
	ideology = anarchist_communism
	traits = {
		#
	}
}

create_field_marshal = {
	name = "Ahmed Gaid Salah"
	picture = "generals/Portrait_Ahmed_Gaid_Salah.dds"
	traits = { old_guard organisational_leader }
	skill = 3
}

create_field_marshal = {
	name = "Abdelkader Lounes"
	picture = "generals/Portrait_Abdelkader_Lounes.dds"
	traits = { logistics_wizard }
	skill = 2
}

create_corps_commander = {
	name = "Ahcène Tafer"
	picture = "generals/Portrait_Ahcene_Tafer.dds"
	traits = { panzer_leader desert_fox }
	skill = 2
}

create_corps_commander = {
	name = "Amrani Amar"
	picture = "generals/Portrait_Amrani_Amar.dds"
	traits = { bearer_of_artillery }
	skill = 2
}

create_corps_commander = {
	name = "Menad Nouba"
	picture = "generals/Portrait_Menad_Nouba.dds"
	traits = { urban_assault_specialist }
	skill = 2
}

create_navy_leader = {
		name = "Mohammed Larbi Haouli"
		picture = "admirals/Portrait_Mohammed_Larbi_Haouli.dds"
		traits = { superior_tactician }
		skill = 2
		}
	}
}