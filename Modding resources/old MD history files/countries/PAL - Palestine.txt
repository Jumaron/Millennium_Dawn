﻿capital = 811

oob = "PAL_2000"

set_convoys = 10
set_stability = 0.5

set_country_flag = country_language_arabic

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

set_country_flag = dominant_religion_islam
set_country_flag = sunni_islam

add_ideas = {
	population_growth_rapid
	arab_league_member
	partially_recognized_state
}

set_politics = {

	parties = {
		social_democrat = {
			popularity = 62
		}
		nationalist = {
			popularity = 11
		}
		conservative = {
			popularity = 6
		}
		islamist = {
			popularity = 21
		}
	}
	
	ruling_party = social_democrat
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Yasser Arafat"
	ideology = social_democrat_ideology
	picture = "Yasser_Arafat.dds"
}

create_country_leader = {
	name = "Ahmed Yassin"
	ideology = islamic_authoritarian
	picture = "Ahmed_Yassin.dds"
}

create_country_leader = {
	name = "Rami Hamdallah"
	ideology = centrist 
	picture = "Rami_Hamdallah.dds"
}

create_country_leader = {
	name = "Mustafa Barghouti"
	ideology = autocrat
	picture = "Mustafa_Barghouti.dds"
}

create_country_leader = {
	name = "Eyad el-Refa'ai"
	ideology = right_wing_conservative
	picture = "Eyad_el_Refaai.dds"
}

create_country_leader = {
	name = "Munib al-Masrii"
	ideology = counter_progressive_democrat
	picture = "Munib_al_Masrii.dds"
}

create_country_leader = {
	name = "Omar Hamad"
	ideology = libertarian
	picture = "Omar_Hamad.dds"
}

create_country_leader = {
	name = "Bassam as-Salhi"
	ideology = marxist
	picture = "Bassam_as-Salhi.dds"
}

create_country_leader = {
	name = "Ahmed Majdalani"
	ideology = democratic_socialist_ideology
	picture = "Ahmed_Majdalani.dds"
}

create_country_leader = {
	name = "Rania Al-Abdullah"
	ideology = absolute_monarchist
	picture = "Rania_Al-Abdullah.dds"
}

create_country_leader = {
	name = "Farhan Abu Al-Hayja"
	ideology = national_socialist
	picture = "Farhan_Abu_Al-Hayja.dds"
}
create_corps_commander = { 
	name = "Saeb al-Ajuz" 
	picture = "Saeb_al-Ajuz.dds"
	traits = { urban_assault_specialist }
	skill = 2
}
create_corps_commander = { 
	name = "Mohamed Talib Bouji" 
	picture = "Mohamed_Bouji.dds"
    traits = { commando }
	skill = 1
}
create_corps_commander = { 
	name = "Ziad Al-Atrash" 
	picture = "Ziad_Al-Atrash.dds"
	skill = 1
}
create_corps_commander = { 
	name = "Amjad Abu-Omar" 
	picture = "Amjad Abu-Omar.dds"
	skill = 1
}
2005.7.17 = {
    create_country_leader = {
	    name = "Mahmoud Abbas"
	    ideology = social_democrat_ideology
	    picture = "Mahmoud_Abbas.dds"
    }
	create_country_leader = {
		name = "Khaled Meshaal"
	    ideology = islamic_republican
	    picture = "Khaled_Meshaal.dds"
	}
}

2017.3.6 = {
	create_country_leader = {
		name = "Ismail Haniya"
	    ideology = islamic_republican
	    picture = "Ismail_Haniya.dds"
	}
}