﻿capital = 335

oob = "INS_2000"

set_convoys = 670
set_stability = 0.5

set_country_flag = country_language_indonesian

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	corvette1 = 1
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_2 = 1
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1

	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
	
}

add_ideas = {
	population_growth_rapid
}

set_country_flag = dominant_religion_islam
set_country_flag = sunni_islam

set_politics = {

	parties = {
		islamist = { popularity = 21 } 
		nationalist = { popularity = 12 }
		reactionary = { popularity = 2 } 
		conservative = { popularity = 22 } 
		market_liberal = { popularity = 1 } 
		social_liberal = { popularity = 4 }
		social_democrat = { popularity = 3 } 
		progressive = { popularity = 30	} 
		democratic_socialist = { popularity = 1	} 
		communist = { popularity = 4 }
	}
	
	ruling_party = nationalist
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Abdurrahman Wahid"
	picture = "Abdurrahman_Wahid.dds"
	ideology = national_democrat
}

create_country_leader = {
	name = "Megawati Sukarnoputri"
	picture = "Megawati_Sukarnoputri.dds"
	ideology = progressive_ideology
}

create_country_leader = {
	name = "Prabowo Subianto Djojohadikusumo"
	picture = "Prabowo_Subianto.dds"
	ideology = libertarian
}

create_country_leader = {
	name = "Wiranto"
	picture = "Wiranto.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Akbar Tandjung"
	picture = "Akbar_Tandjung.dds"
	ideology = fiscal_conservative
}

create_country_leader = {
	name = "Susilo Bambang Yudhoyono"
	picture = "Susilo_B_Yudhoyono.dds"
	ideology = liberalist
}

create_country_leader = {
	name = "Nur Mahmudi Ismail"
	picture = "NM_Ismail.dds"
	ideology = oligarchist
}

create_country_leader = {
	name = "Malem Sambat Kaban"
	picture = "Malem_S_Kaban.dds"
	ideology = islamic_republican
}

create_country_leader = {
	name = "HR Hartono"
	picture = "HR_Hartono.dds"
	ideology = rexist
}

create_country_leader = {
	name = "Grace Natalie"
	picture = "Grace_Natalie.dds"
	ideology = progressive_ideology 
}

create_country_leader = {
	name = "Budiman Sudjatmiko"
	picture = "Budiman_Sudjatmiko.dds"
	ideology = democratic_socialist_ideology  
}

create_country_leader = {
	name = "The Indonesian Politiburo"
	picture = "Indonesian_Politiburo.dds"
	ideology = marxist  
}

create_country_leader = {
	name = "Hamengkubuwono X"
	picture = "Hamengkubuwono_X.dds"
	ideology = absolute_monarchist   
}



create_corps_commander = {
	name = "Bambang Darmono"
	picture = "generals/Bambang_Darmono.dds"
	skill = 2
}

create_corps_commander = {
	name = "Gatot Numantyo"
	picture = "generals/Gatot_Nurmantyo.dds"
	skill = 1
}

create_corps_commander = {
	name = "Jenderal Mulyono"
	picture = "generals/Jenderal_Mulyono.dds"
	skill = 1
}

create_corps_commander = {
	name = "Moeldoko"
	picture = "generals/Moeldoko.dds"
	skill = 1
}

create_corps_commander = {
	name = "Muhammad Munir"
	picture = "generals/Muhammad_Munir.dds"
	skill = 1
}

create_navy_leader = {
	name = "Ade Supandi"
	picture = "admirals/Ade_Supandi.dds"
	skill = 2
}

create_navy_leader = {
	name = "Marsetio"
	picture = "admirals/Marsetio.dds"
	skill = 1
}

create_navy_leader = {
	name = "Soeparno"
	picture = "admirals/Soeparno.dds"
	skill = 1
}

2015.1.1 = {
	set_politics = {

		parties = {
			islamist = { popularity = 6	}
			nationalist = { popularity = 10	}
			reactionary = { popularity = 7 }
			conservative = { popularity = 15 }
			market_liberal = { popularity = 11 }
			social_liberal = { popularity = 8 }
			social_democrat = { popularity = 9 }
			progressive = { popularity = 25	}
			democratic_socialist = { popularity = 5 }
			communist = { popularity = 4 }
	}
		ruling_party = progressive
		last_election = "2014.4.9"
		election_frequency = 48
		elections_allowed = yes
	}	
}

2015.1.1 = {
create_country_leader = {
	name = "Joko Widodo"
	desc = "POLITICIAN_IDO_JOKO_WIDODO_DESC"
	picture = "Joko_Widodo.dds"
	expire = "2020.1.1"
	ideology = progressive_ideology
	
	traits = {
	
	}
}

create_country_leader = {
	name = "Aburizal Bakrie"
	desc = "POLITICIAN_IDO_ABURIZAL_BAKRIE_DESC"
	picture = "Aburizal_Bakrie.dds"
	expire = "2020.1.1"
	ideology = fiscal_conservative
	
	traits = {
	
	}
}

}