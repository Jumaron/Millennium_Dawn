﻿capital = 828

oob = "CHE_2000"

set_convoys = 10
set_stability = 0.5

set_country_flag = country_language_arabic

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1



	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}
set_politics = {

	parties = {
		islamist = {
			popularity = 60
		}
		reactionary = {
		    popularity = 16
		}
		conservative = {
		    popularity = 8
		}
		social_democrat = {
		    popularity = 8
		}
		social_liberal = {
		    popularity = 8
		}
	}
	
	ruling_party = islamist
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "Aslan Mashadov"
	picture = "Aslan_Mashadov.dds"
	ideology = islamic_authoritarian
}
2017.1.1 = {

oob = "SOV_2017"

# Starting tech
set_technology = {
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	#For templates
	infantry_weapons = 1
	combat_eng_equipment = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	gw_artillery = 1
	SP_arty_equipment_0 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	Early_APC = 1
	APC_1 = 1
	IFV_1 = 1
	MBT_1 = 1
	util_vehicle_equipment_0 = 1
	Rec_tank_0 = 1
}

add_ideas = {
	pop_050
	unrestrained_corruption
	gdp_4
	sufi_islam
		stagnation
		defence_03
	edu_01
	health_01
	social_01
	bureau_03
	police_03
	draft_army
	drafted_women
}
set_country_flag = gdp_4

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot

set_politics = {

	parties = {
		democratic = { 
			popularity = 0
		}

		fascism = {
			popularity = 20
		}
		
		communism = {
			popularity = 58
		}
		
		neutrality = {
			popularity = 10
		}
		
		nationalist = {
			popularity = 12
		}
	}
	
	ruling_party = communism
	last_election = "2011.3.5"
	election_frequency = 72
	elections_allowed = yes
}

create_country_leader = {
	name = "Ramzan Kadyrov"
	desc = "POLITICS_Ramzan_Kadyrov_DESC" 
	picture = "CHE_Ramzan_Kadyrov.dds"
	expire = "2050.1.1"
	ideology = Conservative
	traits = {
	emerging_Conservative
	sly
	}
}
}