﻿capital = 540

oob = "ANG_2000"

set_convoys = 20
set_stability = 0.5

add_namespace = {
	name = "ang_unit_leader"
	type = unit_leader
}

set_country_flag = country_language_portuguese

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	corvette1 = 1
	corvette2 = 1
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_2 = 1


	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}


add_ideas = {
	population_growth_explosion
	african_union_member
	limited_conscription
}

set_politics = {

	parties = {
		social_democrat = {
			popularity = 34
		}	
		conservative = {
			popularity = 30
		}
		social_liberal = {
			popularity = 15
		}
		nationalist = {
			popularity = 6
		}
		communist = {
			popularity = 15
		}
	}
	
	ruling_party = conservative
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "José Eduardo dos Santos"
	picture = "Jose_Eduardo_dos_Santos.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Jonas Savimbi"
	picture = "Jonas_Savimbi.dds"
	ideology = fiscal_conservative
}

create_country_leader = {
	name = "Holden Roberto"
	picture = "Holden_Roberto.dds"
	ideology = national_democrat
}

create_field_marshal = {
	name = "Geraldo Sachipengo Nunda"
	picture = "generals/Geraldo_Sachipengo_Nunda.dds"
	traits = { old_guard }
	skill = 2
}

create_field_marshal = {
	name = "Francisco Lopes Gonçalves Afonso"
	picture = "generals/Francisco_Goncalves.dds"
	skill = 1
}

create_field_marshal = {
	name = "Egídio Sousa Santos"
	picture = "generals/Egidio_Sosa_Santos.dds"
	skill = 1
}

create_corps_commander = {
	name = "Luis Manuel Domingos"
	picture = "generals/Luis_Domingos.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Baltazar Diogo"
	picture = "generals/Baltazar_Diogo.dds"
	skill = 1
}

create_corps_commander = {
	name = "Arlindo José Assis"
	picture = "generals/Arlindo_Assis.dds"
	skill = 1
}

create_corps_commander = {
	name = "Lúcio Goncalves do Amaral"
	picture = "generals/Lucio_Goncalves_Amaral.dds"
	skill = 1
}

create_corps_commander = {
	name = "Antonio dos Santos Neto"
	picture = "generals/Antonio_dos_Santos_Neto.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Simão Wala Carlitos"
	picture = "generals/Simao_Carlitos.dds"
	skill = 1
}

create_corps_commander = {
	name = "Peregrino Huambo"
	picture = "generals/Peregrino_Huambo.dds"
	traits = { hill_fighter }
	skill = 1
}

create_navy_leader = {
	name = "Francisco José"
	picture = "admirals/Francisco_Jose.dds"
	skill = 2
}

create_navy_leader = {
	name = "Augusto da Silva Cunha"
	picture = "admirals/Augusto_da_Silva.dds"
	traits = { old_guard_navy }
	skill = 2
}

2002.2.22 = {
	create_country_leader = {
		name = "Isaías Samakuva"
		picture = "Isaias_Samakuva.dds"
		ideology = fiscal_conservative
	}
}

2007.8.2 = {
	create_country_leader = {
		name = "Ngola Kabangu"
		picture = "Ngola_Kabangu.dds"
		ideology = national_democrat
	}
}
2017.1.1 = {
oob = "AGL_2017"

set_technology = { 
	legacy_doctrines = 1 
	modern_blitzkrieg = 1 
	forward_defense = 1 
	encourage_nco_iniative = 1 
	air_land_battle = 1
	#For Templates
	infantry_weapons = 1
	
	combat_eng_equipment = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	gw_artillery = 1
	SP_arty_equipment_0 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	Early_APC = 1
	APC_1 = 1
	IFV_1 = 1
	util_vehicle_equipment_0 = 1
	Rec_tank_0 = 1
	MBT_1 = 1

	landing_craft = 1
}

add_ideas = {
	pop_050
	crippling_corruption
	christian
	gdp_3
	rentier_state
    export_economy
    	stagnation
		defence_03
	edu_02
	health_01
	social_02
	bureau_04
	police_05
	draft_army
	volunteer_women
	industrial_conglomerates
	fossil_fuel_industry
	international_bankers
	civil_law
}
set_country_flag = gdp_3
set_country_flag = enthusiastic_industrial_conglomerates
set_country_flag = enthusiastic_fossil_fuel_industry

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot

	set_politics = {

	parties = {
		democratic = { 
			popularity = 15
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 60
			#banned = no #default is no
		}
		neutrality = { 
			popularity = 25
		}
	}
	
	ruling_party = communism
	last_election = "1932.11.8"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "Jose Eduardo dos Santos"
	desc = "POLITICS_Mohamed Cheikh Biadillah_DESC" #
	picture = "ANG_Jose_Eduardo_dos_Santos.dds"
	expire = "2050.1.1"
	ideology = Communist-State
	traits = {
		#
	}
}
create_field_marshal = {
	name = "Geraldo Sachipengo Nunda"
	picture = "Portrait_Geraldo_Sachipengo_Nunda.dds"
	traits = { old_guard inspirational_leader }
	skill = 4
}

create_field_marshal = {
	name = "Francisco Lopes Gonçalves Afonso"
	picture = "Portrait_Francisco_Goncalves.dds"
	traits = { old_guard fast_planner }
	skill = 4
}

create_field_marshal = {
	name = "Egídio Sousa Santos"
	picture = "Portrait_Egidio_Sosa_Santos.dds"
	traits = { offensive_doctrine }
	skill = 3
}

create_corps_commander = {
	name = "Luis Manuel Domingos"
	picture = "Portrait_Luis_Domingos.dds"
	traits = { commando }
	skill = 3
}

create_corps_commander = {
	name = "Baltazar Diogo"
	picture = "Portrait_Baltazar_Diogo.dds"
	traits = { ranger jungle_rat }
	skill = 3
}

create_corps_commander = {
	name = "Arlindo José Assis"
	picture = "Portrait_Arlindo_Assis.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Lúcio Goncalves do Amaral"
	picture = "Portrait_Lucio_Goncalves_Amaral.dds"
	traits = { fortress_buster }
	skill = 3
}

create_corps_commander = {
	name = "Antonio dos Santos Neto"
	picture = "Portrait_Antonio_dos_Santos_Neto.dds"
	traits = { trickster }
	skill = 3
}

create_corps_commander = {
	name = "Simão Wala Carlitos"
	picture = "Portrait_Simao_Carlitos.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "Peregrino Huambo"
	picture = "Portrait_Peregrino_Huambo.dds"
	traits = { hill_fighter }
	skill = 3
}

create_navy_leader = {
	name = "Francisco José"
	picture = "Portrait_Francisco_Jose.dds"
	traits = { spotter }
	skill = 4
}

create_navy_leader = {
	name = "Augusto da Silva Cunha"
	picture = "Portrait_Augusto_da_Silva.dds"
	traits = { old_guard blockade_runner }
	skill = 4
}

}