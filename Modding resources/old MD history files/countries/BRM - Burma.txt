capital = 505

oob = "BRM_2017"

set_convoys = 20

set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	#MAV-1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	util_vehicle_equipment_2 = 1
	
	#EMERK K-3
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	
	#Kyan Sittha-Class
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_3 = 1
	missile_frigate_4 = 1
	
	#Anawratha-class
	corvette_1 = 1
	corvette_2 = 1
	missile_corvette_1 = 1
	missile_corvette_2 = 1
	
	#For templates
	combat_eng_equipment = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	gw_artillery = 1
	SP_arty_equipment_0 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	Early_APC = 1
	APC_1 = 1
	IFV_1 = 1
	MBT_1 = 1
	Rec_tank_0 = 1
	ENG_MBT_1 = 1

	landing_craft = 1
	
}

2017.1.1 = {

add_ideas = {
	pop_050
	rampant_corruption
	buddism
	gdp_2
		economic_boom
		defence_04
	edu_01
	health_01
	social_01
	bureau_04
	police_04
	volunteer_army
	volunteer_women
	the_military
	farmers
	fossil_fuel_industry
	hybrid
}
set_country_flag = gdp_2
set_country_flag = Major_Importer_CHI_Arms
set_country_flag = positive_the_military
set_country_flag = positive_fossil_fuel_industry

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot

	set_politics = {

	parties = {
		democratic = { 
			popularity = 22
		}

		fascism = {
			popularity = 1
		}
		
		communism = {
			popularity = 22
			#banned = no #default is no
		}
		
		neutrality = { 
			popularity = 55
		}
	}
	
	ruling_party = neutrality
	last_election = "2015.11.8"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Aung San Suu Kyi"
	desc = ""
	picture = "Western_Aung_San_Suu_Kyi.dds"
	expire = "2050.1.1"
	ideology = Neutral_conservatism
	traits = {
		career_politician
		neutrality_Neutral_conservatism
		rational
		humble
		deceitful
		political_dancer
	}
}

create_country_leader = {
	name = "Thein Sein"
	desc = ""
	picture = "Thein_Sein.dds"
	ideology = Autocracy
	traits = {
		military_career
		emerging_Autocracy
		ruthless
		capable
		sly
	}
}

}