﻿capital = 16

oob = "FRA_2000"

set_convoys = 1000
set_stability = 0.5

set_war_support = 0.3

set_country_flag = country_language_french

set_technology = {
	legacy_doctrines = 1 
	grand_battleplan = 1 
	forward_defense = 1 
	encourage_nco_iniative = 1 
	air_land_battle = 1
	
	modern_carrier_0 = 1
	modern_carrier_1 = 1
	modern_carrier_2 = 1
	modern_carrier_3 = 1
	
	corvette_1 = 1
	corvette_2 = 1 #D'Estienne d'Orves-Class
	missile_corvette_1 = 1 
	missile_corvette_2 = 1 #Kership-Class
	
	frigate_1 = 1
	frigate_2 = 1 #Georges Leygues class
	missile_frigate_1 = 1 #Floréal class
	missile_frigate_2 = 1 #La Fayette class
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1 #Cassard class
	missile_destroyer_2 = 1
	missile_destroyer_3 = 1 #Horizon classHorizon class
	
	submarine_1 = 1
	missile_submarine_1 = 1
	missile_submarine_2 = 1
	missile_submarine_3 = 1 #Triomphant class
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1 #Rubis class
	attack_submarine_4 = 1 #Barracuda-class submarine
	
	#Scorpene class
	diesel_attack_submarine_1 = 1 
	diesel_attack_submarine_2 = 1 
	diesel_attack_submarine_3 = 1 
	diesel_attack_submarine_4 = 1 
	
	LPD_0 = 1 #Mistral class
	LPD_1 = 1 #Mistral class
	LPD_2 = 1 #Mistral class
	
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	
	body_armor = 1
	body_armor2 = 1
	
	camouflage = 1
	camouflage2 = 1
	camouflage3 = 1
	
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	land_Drone_equipment2 = 1
	
	Anti_tank_0 = 1
	Anti_tank_1 = 1
	Anti_tank_2 = 1
	AT_upgrade_1 = 1
	AT_upgrade_2 = 1

	Heavy_Anti_tank_0 = 1
	Heavy_Anti_tank_1 = 1
	Heavy_Anti_tank_2 = 1
	
	combat_eng_equipment = 1
	
	Anti_Air_0 = 1
	Anti_Air_1 = 1
	Anti_Air_2 = 1
	AA_upgrade_1 = 1
	AA_upgrade_3 = 1
	
	SP_Anti_Air_0 = 1
	SP_Anti_Air_1 = 1
	SP_Anti_Air_2 = 1
	
	Early_APC = 1
	MBT_1 = 1
	MBT_2 = 1
	MBT_3 = 1
	MBT_4 = 1
	
	ENG_MBT_1 = 1
	ENG_MBT_2 = 1
	ENG_MBT_3 = 1
	ENG_MBT_4 = 1
	
	Rec_tank_0 = 1
	Rec_tank_1 = 1
	Rec_tank_2 = 1
	
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	util_vehicle_equipment_2 = 1
	util_vehicle_equipment_3 = 1
	util_vehicle_equipment_4 = 1
	
	IFV_1 = 1
	IFV_2 = 1
	IFV_3 = 1
	IFV_4 = 1
	IFV_5 = 1
	Air_IFV_5 = 1
	
	APC_1 = 1
	APC_2 = 1
	APC_3 = 1
	APC_4 = 1
	APC_5 = 1
	
	SP_arty_equipment_0 = 1
	SP_arty_equipment_1 = 1
	SP_arty_equipment_2 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	artillery_2 = 1
	Arty_upgrade_1 = 1
	Arty_upgrade_2 = 1
	Arty_upgrade_3 = 1
	
	early_helicopter = 1
	attack_helicopter1 = 1
	attack_helicopter2 = 1
	attack_helicopter3 = 1
	
	early_fighter = 1
	CV_MR_Fighter1 = 1
	CV_MR_Fighter2 = 1
	CV_MR_Fighter3 = 1
	
	AS_Fighter1 = 1
	AS_Fighter2 = 1
	
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	
	Strike_fighter1 = 1
	Strike_fighter2 = 1
	Strike_upgrade_1 = 1
	Strike_fighter3 = 1
	
	night_vision_1 = 1
	night_vision_2 = 1
	night_vision_3 = 1
	
	landing_craft = 1
	amphibious_assault_ship = 1
	
	early_bomber = 1
	strategic_bomber1 = 1
	
	transport_plane1 = 1
	transport_plane2 = 1
	transport_plane3 = 1
	transport_plane4 = 1
	
	naval_plane1 = 1
	naval_plane2 = 1
	
	transport_helicopter1 = 1
	transport_helicopter2 = 1
	transport_helicopter3 = 1
	
	#Alpha Jet
	L_Strike_fighter1 = 1
	L_Strike_fighter2 = 1

}

create_equipment_variant = { name = "Mirage 2000N" type = jet_bomber_equipment_2 upgrades = { plane_engine_upgrade = 5 } }

add_ideas = {
	population_growth_stagnation
	free_trade
	idea_eu_member
	idea_united_nations_security_council_member
}

set_politics = {

	parties = {
		reactionary = {
			popularity = 7
		}
		conservative = {
			popularity = 23
		}
		market_liberal = {
			popularity = 20
		}
		social_liberal = {
			popularity = 3
		}
		social_democrat = {
			popularity = 40
		}
		progressive = {
			popularity = 3
		}
		communist = {
			popularity = 4
		}
	}
	
	ruling_party = conservative
	last_election = "1997.6.1"
	election_frequency = 60
	elections_allowed = yes
}

add_opinion_modifier = {
	target = ENG
	modifier = entente_cordiale
}

add_opinion_modifier = {
	target = GER
	modifier = franco_german_friendship
}

give_guarantee = DJI
give_guarantee = IVO
give_guarantee = MOC
give_guarantee = AND
give_guarantee = SEN
give_guarantee = GAB
give_military_access = DJI
give_military_access = IVO
give_military_access = MOC
give_military_access = AND
give_military_access = SEN
give_military_access = GAB

create_country_leader = {
	name = "Lionel Jospin"
	picture = "Lionel_Jospin.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Jacques Chirac"
	picture = "Jacques_Chirac.dds"
	ideology = gaullist
}

create_country_leader = {
	name = "Jean Marie Le Pen"
	picture = "Jean_Marie_Le_Pen.dds"
	ideology = counter_progressive_democrat
}

create_country_leader = {
	name = "Alain Soral"
	picture = "Alain_Soral.dds"
	ideology = national_socialist  
}

create_country_leader = {
	name = "Andre Gandillon"
	picture = "Andre_Gandillon.dds"
	ideology = national_democrat 
}

create_country_leader = {
	name = "Jean-Luc Melenchon"
	picture = "Jean_Luc_Melenchon.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Marie Buffet"
	picture = "Marie_Buffet.dds"
	ideology = leninist
}

create_country_leader = {
	name = "Francois Bayrou"
	picture = "Francois_Bayrou.dds"
	ideology = libertarian
}

create_country_leader = {
	name = "Noel Mamere"
	picture = "Noel_Mamere.dds"
	ideology = green
}
	
create_country_leader = {
	name = "Louis XX"
	picture = "Louis_XX.dds"
	ideology = absolute_monarchist
}

create_country_leader = {
	name = "Christiane Taubira"
	picture = "Christiane_Taubira.dds"
	ideology = liberalist
}

create_corps_commander = {
	name = "Jean-Pierre Bosser"
	picture = "generals/Jean_Pierre_Bosser.dds"
	skill = 3
}

create_corps_commander = {
	name = "Pierre Chavancy"
	picture = "generals/Pierre_Chavancy.dds"
	skill = 1
}

create_corps_commander = {
	name = "Bertrand Ract-Madoux"
	picture = "generals/Bertrand_Ract_Madoux.dds"
	skill = 1
}

2001.1.1 = {
	complete_national_focus = FRA_the_french_economy
	complete_national_focus = FRA_civilian_buildup
	complete_national_focus = FRA_northern_industry
	complete_national_focus = FRA_our_future
	complete_national_focus = FRA_liberte
	complete_national_focus = FRA_the_european_dream
	complete_national_focus = FRA_term_length_reform
	complete_national_focus = FRA_judicial_reforms
	complete_national_focus = FRA_executive_reforms
	complete_national_focus = FRA_reform_the_officer_school
	complete_national_focus = FRA_army_training_1
	complete_national_focus = FRA_renew_the_naval_presence
	complete_national_focus = FRA_naval_maneuvre_1
	complete_national_focus = FRA_brest_arsenal
	complete_national_focus = FRA_strengthen_toulon
	complete_national_focus = FRA_new_fighter_programs
	complete_national_focus = FRA_new_bomber_programs
	complete_national_focus = FRA_air_force_officer_school
}

2002.1.1 = { add_ideas = the_euro }

2002.6.16 = {
	set_politics = {
		parties = {
			reactionary = { popularity = 10 }
			conservative = { popularity = 37 }
			market_liberal = { popularity = 6 }
			social_liberal = { popularity = 3 }
			social_democrat = { popularity = 30 }
			progressive = { popularity = 6 }
			democratic_socialist = { popularity = 3 }
			communist = { popularity = 5 }
		}
		ruling_party = conservative
		last_election = "2002.6.16"
		election_frequency = 60
		elections_allowed = yes
	}
}

2003.1.1 = {
	create_country_leader = {
		name = "Segolene Royal"
		picture = "Segolene_Royal.dds"
		ideology = social_democrat_ideology
	}

	set_party_name = {
		ideology = conservative
		long_name = FRA_conservative_party_UMP_long
		name = FRA_conservative_party_UMP
	}
}

2007.6.16 = {
	
	set_politics = {

	parties = {
		reactionary = {
			popularity = 4
		}
		conservative = {
			popularity = 43
		}
		market_liberal = {
			popularity = 8
		}
		social_liberal = {
			popularity = 2
		}
		social_democrat = {
			popularity = 30
		}
		progressive = {
			popularity = 4
		}
		democratic_socialist = {
			popularity = 4
		}
		communist = {
			popularity = 5
		}
	}
	
	ruling_party = conservative
	last_election = "2007.6.16"
	election_frequency = 60
	elections_allowed = yes
	}

	create_country_leader = {
		name = "Nicolas Sarkozy"
		picture = "Nicolas_Sarkozy.dds"
		ideology = gaullist
	}

}

2008.1.1 = {
	
	set_party_name = {
		ideology = market_liberal
		long_name = FRA_market_liberal_party_MoDem_long
		name = FRA_market_liberal_party_MoDem
	}

}

2010.1.1 = {
	create_country_leader = {
		name = "Pierre Laurent"
		picture = "Pierre_Laurent.dds"
		ideology = leninist
	}
}

2011.1.16 = {
	create_country_leader = {
		name = "Francois Hollande"
		picture = "Francois_Hollande.dds"
		ideology = social_democrat_ideology
	}
	create_country_leader = {
		name = "Marine Le Pen"
		picture = "Marine_Le_Pen.dds"
		ideology = counter_progressive_democrat
	}
	create_country_leader = {
		name = "Eva Joly"
		picture = "Eva_Joly.dds"
		ideology = green
	}
	create_country_leader = {
		name = "Jean-Michel Baylet"
		picture = "Jean_Michel_Baylet.dds"
		ideology = liberalist
	}
}

2012.5.6 = {
	set_politics = {
		parties = {
			reactionary = { popularity = 14 }
			conservative = { popularity = 32 }
			market_liberal = { popularity = 2 }
			social_liberal = { popularity = 2 }
			social_democrat = { popularity = 34 }
			progressive = { popularity = 5 }
			democratic_socialist = { popularity = 8 }
			communist = { popularity = 3 }
		}
		ruling_party = social_democrat
		last_election = "2012.5.6"
		election_frequency = 60
		elections_allowed = yes
	}
}

2015.1.1 = {
	set_party_name = {
		ideology = conservative
		long_name = FRA_conservative_party_LR_long
		name = FRA_conservative_party_LR
	}
}

2016.1.1 = {
	set_party_name = {
		ideology = democratic_socialist
		long_name = FRA_democratic_socialist_party_FI_long
		name = FRA_democratic_socialist_party_FI
	}
	create_country_leader = {
		name = "Emmanuel Macron"
		picture = "Emmanuel_Macron.dds"
		ideology = liberalist
	}
	set_party_name = {
		ideology = social_liberal
		long_name = FRA_social_liberal_party_En_Marche_long
		name = FRA_social_liberal_party_En_Marche
	}
}