﻿capital = 692

oob = "BRB_2000"

set_convoys = 10
set_stability = 0.5

set_country_flag = country_language_english

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_steady
}

set_politics = {
	parties = {
		conservative = { popularity = 1 }
		social_democrat = { popularity = 36 }
		democratic_socialist = { popularity = 63 }
	}
	
	ruling_party = democratic_socialist
	last_election = "1999.1.20"
	election_frequency = 52
	elections_allowed = yes
}

create_country_leader = {
	name = "Freundel Stuart"
	picture = "Freundel_Stuart.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Owen Seymoure Arthur"
	picture = "Owen_Seymoure_Arthur.dds"
	ideology = democratic_socialist_ideology
}

2016.1.1 = {
	set_politics = {
		parties = {
			conservative = { popularity = 0.5 }
			social_democrat = { popularity = 51.5 }
			democratic_socialist = { popularity = 48 }
		}
		ruling_party = social_democrat
		last_election = "2013.2.21"
		election_frequency = 52
		elections_allowed = yes
	}
}
2017.1.1 = {

oob = "BAR_2017"

set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	#For templates
	infantry_weapons = 1
	
	command_control_equipment = 1
	Anti_tank_0 = 1
	Anti_Air_0 = 1
}

add_ideas = {
	pop_050
	systematic_corruption
	christian
	gdp_6
	    stable_growth
		defence_01
	edu_05
	health_04
	bureau_01
	police_04
	volunteer_army
	volunteer_women
	international_bankers
	small_medium_business_owners
	landowners
	common_law
	}
set_country_flag = gdp_6
set_country_flag = positive_international_bankers
set_country_flag = positive_small_medium_business_owners
set_country_flag = positive_landowners


#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_15K_GDPC_slot

	set_politics = {

	parties = {
		democratic = { 
			popularity = 99
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 1
			#banned = no #default is no
		}
	}
	
	ruling_party = democratic
	last_election = "2013.2.21"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Freundel Stuart"
	desc = "POLITICS_AGUSTIN_PEDRO_JUSTO_DESC"
	picture = "Barb_Freundel_Stuart.dds"
	expire = "2065.1.1"
	ideology = conservatism
	traits = {
		#
	}
}
create_field_marshal = {
	name = "Glyne Grannum"
	picture = "generals/Portrait_Glyne_Grannum.dds"
	traits = { organisational_leader }
	skill = 1
}

create_field_marshal = {
	name = "Alvin Quintyne"
	picture = "generals/Portrait_Alvin_Quintyne.dds"
	traits = { inspirational_leader }
	skill = 1
}

create_field_marshal = {
	name = "Aquinas Clarke"
	picture = "generals/Portrait_Aquinas_Clarke.dds"
	traits = { fast_planner }
	skill = 1
}

create_corps_commander = {
	name = "Wendy Yearwood"
	picture = "generals/Portrait_Wendy_Yearwood.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Errol Brathwaite"
	picture = "generals/Portrait_Errol_Brathwaite.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Tyrone Griffith"
	picture = "generals/Portrait_Tyrone_Griffith.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Carlos Lovell"
	picture = "generals/Portrait_Carlos_Lovell.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Trevor Browne"
	picture = "generals/Portrait_Trevor_Browne.dds"
	traits = { naval_invader }
	skill = 1
}

create_navy_leader = {
	name = "Aquinas Clarke"
	picture = "admirals/Portrait_Aquinas_Clarke.dds"
	traits = { blockade_runner }
	skill = 1
}

create_navy_leader = {
	name = "Errington Shurland"
	picture = "admirals/Portrait_Errington_Shurland.dds"
	traits = { spotter }
	skill = 1
}

create_navy_leader = {
	name = "Sean Reece"
	picture = "admirals/Portrait_Sean_Reece.dds"
	traits = { superior_tactician }
	skill = 1
}

create_navy_leader = {
	name = "David Harewood"
	picture = "admirals/Portrait_David_Harewood.dds"
	traits = { seawolf }
	skill = 1
}

}