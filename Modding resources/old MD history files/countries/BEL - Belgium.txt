﻿capital = 6

oob = "BEL_2000"

set_convoys = 40
set_stability = 0.5

set_country_flag = country_language_dutch
set_country_flag = country_language_french

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	corvette1 = 1
	corvette2 = 1
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1


	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_stagnation
	free_trade
	idea_eu_member
	belgian_political_system
}

set_politics = {

	parties = {
		nationalist = {
			popularity = 16
		}
		reactionary = {
			popularity = 2
		}
		conservative = {
			popularity = 21
		}
		market_liberal = {
			popularity = 25
		}
		social_liberal = {
			popularity = 2
		}
		social_democrat = {
			popularity = 20
		}
		progressive = {
			popularity = 14
		}
	}
	
	ruling_party = market_liberal
	last_election = "1999.6.13"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Abdelhay Bakkali Tahiri"
	picture = "Abdelhay_Bakkali_Tahiri.dds"
	ideology = islamic_authoritarian
}
create_country_leader = {
	name = "Herve Van Laethem"
	picture = "Herve_Van_Laethem.dds"
	ideology = fascist_ideology 
}
create_country_leader = {
	name = "Bart De Wever"
	picture = "Bart_De_Wever.dds"
	ideology = national_democrat
}
create_country_leader = {
	name = "Albert II of Belgium"
	picture = "Albert.dds"
	ideology = absolute_monarchist	
}
create_country_leader = {
	name = "Yves Leterme"
	picture = "Yves_Leterme.dds"
	ideology = christian_democrat
}
create_country_leader = {
	name = "Guy Verhofstadt"
	picture = "Guy_Verhofstadt.dds"
	ideology = libertarian 
}
create_country_leader = {
	name = "Elio Di Rupo"
	picture = "Elio_Di_Rupo.dds"
	ideology = social_democrat_ideology
}
create_country_leader = {
	name = "Peter Mertens"
	picture = "Peter_Mertens.dds"
	ideology = marxist
}

add_namespace = {
	name = "bel_unit_leader"
	type = unit_leader
}

create_field_marshal = {
	name = "Hubert de Vos"
	picture = "generals/Hubert_de_Vos.dds"
	skill = 1
}
create_field_marshal = {
	name = "Eddy Testelmans"
	picture = "generals/Eddy_Testelmans.dds"
	skill = 1
}
create_field_marshal = {
	name = "Jean-Marie Jockin"
	picture = "generals/Jean_Marie_Jockin.dds"
	skill = 2
}
create_corps_commander = {
	name = "Pierre Neirinckx"
	picture = "generals/Pierre_Neirinckx.dds"
	skill = 2
}
create_corps_commander = {
	name = "Frederick Vansina"
	picture = "generals/Frederick_Vansina.dds"
	traits = { ranger }
	skill = 1
}
create_corps_commander = {
	name = "Marc Thys"
	picture = "generals/Marc_Thys.dds"
	skill = 1
}
create_corps_commander = {
	name = "Bart Moerman"
	picture = "generals/Bart_Moerman.dds"
	skill = 1
}
create_corps_commander = {
	name = "Bruno Denis"
	picture = "generals/Bruno_Denis.dds"
	skill = 1
}
create_corps_commander = {
	name = "Bruno van Loo"
	picture = "generals/Bruno_van_Loo.dds"
	skill = 1
}
create_corps_commander = {
	name = "Michel Pihard"
	picture = "generals/Michel_Pihard.dds"
	skill = 1
}
create_corps_commander = {
	name = "Wim Denolf"
	picture = "generals/Wim_Denolf.dds"
	skill = 2
}
create_navy_leader = {
	name = "Georges Heerlen"
	picture = "generals/Georges_Heerlen.dds"
	traits = { blockade_runner }
	skill = 1
}

2008.3.20 = {
	set_politics = {
		ruling_party = conservative
		elections_allowed = yes
	}
}

2008.12.30 = {
	create_country_leader = {
		name = "Herman van Rompuy"
		picture = "Herman_Van_Rompuy.dds"
		ideology = christian_democrat
	}
}

2011.12.6 = {
	set_politics = { ruling_party = social_democrat elections_allowed = yes }
	create_navy_leader = {
		name = "Georges Heeren"
		picture = "admirals/Georges_Heeren.dds"
		skill = 2
	}
}

2013.1.1 = {
	create_country_leader = {
		name = "Philippe I of Belgium"
		picture = "Philippe.dds"
		ideology = absolute_monarchist
	}
	
	create_corps_commander = {
		name = "Philippe I of Belgium"
		picture = "generals/Philippe.dds"
		skill = 1
	}
	
	create_navy_leader = {
		name = "M. Hoffmans"
		picture = "M_Hoffmans.dds"
		skill = 1
	}
}

2014.1.1 = {
	create_country_leader = {
		name = "Charles Michel"
		picture = "Charles_Michel.dds"
		ideology = libertarian
	}
	
	set_politics = {
		ruling_party = market_liberal
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Tom Van Grieken"
		picture = "Tom_van_Grieken.dds"
		ideology = national_democrat
	}

	create_country_leader = {
		name = "Paul Magnette"
		picture = "Paul_Magnette.dds"
		ideology = social_democrat_ideology
	}
}

2015.1.1 = {
	create_country_leader = {
		name = "Patrick Dupriez"
		picture = "Patrick_Dupriez.dds"
		ideology = green
	}
	
	create_country_leader = {
		name = "John Crombez"
		picture = "John_Crombez.dds"
		ideology = democratic_socialist_ideology
	}
}

2016.1.1 = {
	
	set_politics = {
	
		parties = {
			nationalist = {
				popularity = 21
			}
			reactionary = {
				popularity = 3
			}
			conservative = {
				popularity = 17
			}
			market_liberal = {
				popularity = 21
			}
			social_liberal = {
				popularity = 1
			}
			social_democrat = {
				popularity = 20
			}
			progressive = {
				popularity = 15
			}
			democratic_socialist = {
				popularity = 1
			}
			communist = {
				popularity = 1
			}
		}
		
		ruling_party = market_liberal
		last_election = "2014.5.25"
		election_frequency = 48
		elections_allowed = yes
	
	}
	
	create_corps_commander = {
		name = "Jean-Paul Deconick"
		picture = "generals/Jean_Paul_Deconick.dds"
		skill = 2
		traits = { ranger }
	}
	create_corps_commander = {
		name = "Marc Compernol"
		picture = "generals/Marc_Compernol.dds"
		skill = 1
		traits = { commando }
	}
	create_navy_leader = {
		name = "Wim Robberecht"
		picture = "admirals/Wim_Robberecht.dds"
		skill = 1
	}
}
2017.1.1 = {

oob = "BEL_2017"

set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	night_vision_1 = 1
	night_vision_2 = 1
	night_vision_3 = 1
	
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1

	#FN SCAR
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	
	#Wielingen-Class
	frigate_1 = 1
	frigate_2 = 1
	
	#SIBMAS
	IFV_1 = 1
	IFV_2 = 1
	
	#For templates
	
	combat_eng_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	gw_artillery = 1
	Anti_Air_0 = 1
	Early_APC = 1
	APC_1 = 1
	util_vehicle_equipment_0 = 1
	
	landing_craft = 1
	
}

add_ideas = {
	pop_050
	#small_medium_business_owners
	#Labour_Unions
	#The_Clergy
	modest_corruption
	christian
	gdp_9
	EU_member
	    stable_growth
		defence_02
	edu_05
	health_05
	social_06
	bureau_03
	police_04
	volunteer_army
	volunteer_women
	intervention_limited_interventionism
	NATO_member
	western_country
	medium_far_right_movement
	small_medium_business_owners
	landowners
	industrial_conglomerates
	civil_law
	}
set_country_flag = gdp_9
set_country_flag = positive_small_medium_business_owners
set_country_flag = positive_landowners
set_country_flag = positive_industrial_conglomerates

#NATO military access
diplomatic_relation = {
	country = ALB
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = BUL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = CAN
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = CRO
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = CZH
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = DEN
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = EST
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = FRA
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = GER
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = GRE
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = HUN
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ICE
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ITA
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = LAT
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = LIT
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = LUX
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = HOL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = NOR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = POL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = POR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ROM
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = SLO
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = SLV
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = SPR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = TUR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ENG
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = USA
	relation = military_access
	active = yes
}

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_15K_GDPC_slot
	complete_national_focus = Generic_30K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot
	complete_national_focus = Generic_16_IC_slot

	set_politics = {

	parties = {
		democratic = { 
			popularity = 87
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 1
		}
		
		neutrality = {
			popularity = 10
		}
		
		nationalist = {
			popularity = 2
		}
	}
	
	ruling_party = democratic
	last_election = "2014.5.25"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Wouter Beke"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "wouter_beke.dds"
	expire = "2065.1.1"
	ideology = conservatism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Elio Di Rupo"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "elio_di_rupo.dds"
	expire = "2050.1.1"
	ideology = socialism
	traits = {
		#
	}
}

create_country_leader = {
	name = "John Crombez"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "john_crombez.dds"
	expire = "2060.1.1"
	ideology = socialism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Peter Mertens"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "peter_mertens.dds"
	expire = "2065.1.1"
	ideology = Communist-State
	traits = {
		#
	}
}

create_country_leader = {
	name = "Bart De Wever"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "BEL_Bart_De_Wever.dds"
	expire = "2065.1.1"
	ideology = Neutral_conservatism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Olivier Maingain"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "olivier_maingain.dds"
	expire = "2065.1.1"
	ideology = Nat_Populism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Charles Michel"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "BEL_Charles_Michel.dds"
	expire = "2065.1.1"
	ideology = liberalism
	traits = {
		#
	}
}

create_field_marshal = {
	name = "Marc Compernol"
	picture = "Portrait_Marc_Compernol.dds"
	traits = { old_guard thorough_planner }
	skill = 3
}

create_corps_commander = {
	name = "Pierre Neirinckx"
	picture = "Portrait_Pierre_Neirinckx.dds"
	traits = {  }
	skill = 2
}

create_corps_commander = {
	name = "Frederick Vansina"
	picture = "Portrait_Frederick_Vansina.dds"
	traits = { commando }
	skill = 2
}

create_corps_commander = {
	name = "Jean-Paul Deconinck"
	picture = "Portrait_Jean-Paul_Deconicnk.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Hubert de Vos"
	picture = "Portrait_Hubert_de_Vos.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Eddy Testelmans"
	picture = "Portrait_Eddy_Testelmans.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Marc Thys"
	picture = "Portrait_Marc_Thys.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Bart Moerman"
	picture = "Portrait_Bart_Moerman.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Bruno Denis"
	picture = "Portrait_Bruno_Denis.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Bruno van Loo"
	picture = "Portrait_Bruno_van_Loo.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Michel Pihard"
	picture = "Portrait_Michel_Pihard.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Wim Denolf"
	picture = "Portrait_Wim_Denolf.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Wim Robberecht"
	picture = "Portrait_Wim_Robberecht.dds"
	traits = { old_guard_navy superior_tactician }
	skill = 2
}

create_navy_leader = {
	name = "Georges Heerlen"
	picture = "Portrait_Georges_Heerlen.dds"
	traits = { blockade_runner }
	skill = 1
}

}