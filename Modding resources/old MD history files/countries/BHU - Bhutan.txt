﻿capital = 324

oob = "BHU_2000"

set_stability = 0.5

set_country_flag = country_language_dzongkha

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_steady
}

set_politics = {

	parties = {
		reactionary = {
			popularity = 20
		}
		conservative = {
			popularity = 50
		}
		monarchist = {
			popularity = 20
		}
		communist = {
			popularity = 10
		}
	}
	
	ruling_party = conservative
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "Sangay Ngedup"
	picture = "Sangay_Ngedup.dds"
	ideology = constitutional_monarchist
}

2016.1.1 = {
	create_country_leader = {
		name = "Tshering Tobgay"
		picture = "Tshering_Tobgay.dds"
		ideology = constitutional_monarchist
	}
}
2017.1.1 = {

oob = "BHU_2017"

set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	night_vision_1 = 1

	#For templates
	infantry_weapons = 1
	
	command_control_equipment = 1
	Anti_tank_0 = 1
	Anti_Air_0 = 1
}

add_ideas = {
	pop_050
    buddism
	medium_corruption
	gdp_3
	    economic_boom
		defence_01
	edu_03
	health_02
	social_02
	bureau_03
	police_03
	volunteer_army
	no_women_in_military
	farmers
	industrial_conglomerates
	small_medium_business_owners
	hybrid
}
set_country_flag = gdp_3
set_country_flag = negative_farmers
set_country_flag = negative_industrial_conglomerates
set_country_flag = negative_small_medium_business_owners

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot

	set_politics = {
	parties = {
		democratic = { 
			popularity = 0
		}
		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 0
		}
		
		neutrality = { 
			popularity = 100
		}
	}
	
	ruling_party = neutrality
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}
create_country_leader = {
	name = "Tshering Tobgay"
	desc = ""
	picture = "BHU_Tshering_Tobgay.dds"
	expire = "2065.1.1"
	ideology = Neutral_conservatism
	traits = {
		#
	}
}
create_field_marshal = {
	name = "Batoo Tshering"
	picture = "Portrait_Batoo_Tshering.dds"
	traits = { organisational_leader offensive_doctrine }
	skill = 2
}

create_corps_commander = {
	name = "Kipchu Namgyel"
	picture = "Portrait_Kipchu_Namgyel.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

}