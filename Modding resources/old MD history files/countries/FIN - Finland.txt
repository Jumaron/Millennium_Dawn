﻿capital = 111

oob = "FIN_2000"

set_convoys = 200
set_stability = 0.5

set_country_flag = country_language_finnish

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	AT_upgrade_1 = 1
	Heavy_Anti_tank_1 = 1
	Anti_tank_1 = 1 
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	corvette1 = 1
	corvette2 = 1
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_2 = 1
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1



	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_stagnation
	free_trade
	idea_eu_member
	limited_conscription
	finnish_neutrality
}

set_politics = {

	parties = {
		nationalist = {
			popularity = 1
		}
		reactionary = {
			popularity = 1
		}
		conservative = {
			popularity = 4
		}
		market_liberal = {
			popularity = 21
		}
		social_liberal = {
			popularity = 27
		}
		social_democrat = {
			popularity = 27
		}
		progressive = {
			popularity = 7
		}
		democratic_socialist = {
			popularity = 11
		}
		communist = {
			popularity = 1
		}
	}
	
	ruling_party = social_democrat
	last_election = "1999.3.21"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Paavo Lipponen"
	picture = "Paavo_Lipponen.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Juha Sipilä"
	picture = "Juha_Sipila.dds"
	ideology = liberalist
}

create_country_leader = {
	name = "Esa Henrik Holappa"
	picture = "Esa_Henrik_Holappa.dds"
	ideology = fascist_ideology
}

create_country_leader = {
	name = "Olavi Maenpaa"
	picture = "Olavi_Maenpaa.dds"
	ideology = proto_fascist
}

create_country_leader = {
	name = "Timo Soini"
	picture = "Timo_Soini.dds"
	ideology = counter_progressive_democrat
}

create_country_leader = {
	name = "Bjarne Kallis"
	picture = "Bjarne_Kallis.dds"
	ideology = christian_democrat
}

create_country_leader = {
	name = "Sauli Niinisto"
	picture = "Sauli_Niinisto.dds"
	ideology = libertarian
}

create_country_leader = {
	name = "Anneli Jaatteenmaki"
	picture = "Anneli_Jaatteenmaki.dds"
	ideology = liberalist
}

create_country_leader = {
	name = "Satu Hassi"
	picture = "Satu_Hassi.dds"
	ideology = green
}

create_country_leader = {
	name = "Suvi-Anne Siimesn"
	picture = "Suvi_Anne_Siimes.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Hannu Harju"
	picture = "Hannu_Harju.dds"
	ideology = leninist
}

2002.1.1 = {
	add_ideas = the_euro
}

2015.1.1 = {
	
	create_country_leader = {
		name = "Sari Essayah"
		picture = "Sari_Essayah.dds"
		ideology = christian_democrat
	}
		
	create_country_leader = {
		name = "Alexander Stubb"
		picture = "Alexander_Stubb.dds"
		ideology = libertarian
	}
		
	create_country_leader = {
		name = "Juha Sipila"
		picture = "Juha_Sipila.dds"
		ideology = moderate
	}
		
	create_country_leader = {
		name = "Antti Rinne"
		picture = "Antti_Rinne.dds"
		ideology = social_democrat_ideology
	}
		
	create_country_leader = {
		name = "Ville Niinisto"
		picture = "Ville_Niinisto.dds"
		ideology = progressive_ideology
	}
		
	create_country_leader = {
		name = "Li Andersson"
		picture = "Li_Andersson.dds"
		ideology = democratic_socialist_ideology
	}
}

2016.1.1 = {

	oob = "FIN_2016"
	
	set_politics = {
		parties = {
			reactionary = { popularity = 16.9 }			#True Finns
			conservative = { popularity = 4 }			#Christian Democrats
			market_liberal = { popularity = 18.5 }		#National Coalition
			social_liberal = { popularity = 22.3 }		#Centre
			social_democrat = { popularity = 16.1 }		#Social Democrats
			progressive = { popularity = 9.6 }			#Green League
			democratic_socialist = { popularity = 7.4 }	#Left Alliance
			communist = { popularity = 0.2 }
		}
		ruling_party = social_liberal
		last_election = "2015.4.19"
		election_frequency = 48
		elections_allowed = yes
	}
}