﻿capital = 275

oob = "SAF_2000"

set_convoys = 535
set_stability = 0.5

set_country_flag = country_language_afrikaans
set_country_flag = country_language_english
set_country_flag = country_language_southern_ndebele
set_country_flag = country_language_northern_sotho
set_country_flag = country_language_sotho
set_country_flag = country_language_swazi
set_country_flag = country_language_tsonga
set_country_flag = country_language_tswana
set_country_flag = country_language_venda
set_country_flag = country_language_xhosa
set_country_flag = country_language_zulu

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1


	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	
	
}

add_ideas = {
	population_growth_steady
	african_union_member
	commonwealth_of_nations_member
}

set_politics = {

	parties = {
		
		islamist = {
			popularity = 0
		}
		nationalist = {
			popularity = 0
		}
		reactionary = {
			popularity = 5
		}
		conservative = {
			popularity = 5
		}
		market_liberal = {
			popularity = 9
		}
		social_liberal = {
			popularity = 15
		}
		social_democrat = {
			popularity = 65
		}
		progressive = {
			popularity = 0
		}
		democratic_socialist = {
			popularity = 1
		}
		communist = {
			popularity = 0
		}
	}
	
	ruling_party = social_democrat
	last_election = "1999.6.2"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Thabo Mbeki"
	picture = "Thabo_Mbeki.dds"
	ideology = social_democrat_ideology
}
create_country_leader = {
	name = "Tony Leon"
	picture = "Tony_Leon.dds"
	ideology = liberalist
}
create_country_leader = {
	name = "Mangosuthu Buthelezi"
	picture = "Mangosuthu_Buthelezi.dds"
	ideology = libertarian
}
create_country_leader = {
	name = "Kenneth Meshoe"
	picture = "Kenneth_Meshoe.dds"
	ideology = christian_democrat
}
create_country_leader = {
	name = "Pieter Mulder"
	picture = "Pieter_Mulder.dds"
	ideology = counter_progressive_democrat
}
create_country_leader = {
	name = "Luthando Mbinda"
	picture = "Luthando_Mbinda.dds"
	ideology = democratic_socialist_ideology
}
create_country_leader = {
	name = "Judy Sole"
	picture = "Judy_Sole.dds"
	ideology = green
}
create_country_leader = {
	name = "Lybon Masapa"
	ideology = marxist
}
create_country_leader = {
	name = "Wasfie Hassiem"
	picture = "Wasfie_Hassiem.dds"
	ideology = islamic_republican
}

create_field_marshal = {
	name = "Lindile Yam"
	picture = "generals/Lindile_Yam.dds"
	skill = 2
}
create_field_marshal = {
	name = "Gerhard Kamffer"
	picture = "generals/Gerhard_Kamffer.dds"
	skill = 1
}
create_corps_commander = {
	name = "Godfrey Ngwenya"
	picture = "generals/Godfrey_Ngwenya.dds"
	skill = 2
}
create_corps_commander = {
	name = "Solly Shoke"
	picture = "generals/Solly_Shoke.dds"
	skill = 1
}
create_corps_commander = {
	name = "Lawrence Smith"
	picture = "generals/Lawrence_Smith.dds"
	skill = 1
}
create_corps_commander = {
	name = "Andre Retief"
	picture = "generals/Andre_Retief.dds"
	skill = 1
}
create_corps_commander = {
	name = "Willis Nkosi"
	picture = "generals/Willis_Nkosi.dds"
	skill = 1
}
create_corps_commander = {
	name = "Rudzani Maphwanya"
	picture = "generals/Rudzani_Maphwanya.dds"
	skill = 1
}

create_navy_leader = {
	name = "Samuel Hlongwane"
	picture = "admirals/Samuel_Hlongwane.dds"
	skill = 2
}
create_navy_leader = {
	name = "Bubele Mhlana"
	picture = "admirals/Bubele_Mhlana.dds"
	skill = 2
}
create_navy_leader = {
	name = "Hanno Teuteberg"
	picture = "admirals/Hanno_Teuteberg.dds"
	skill = 1
}

2008.1.1 = {
	set_party_name = {
		ideology = progressive
		long_name = SAF_progressive_party_COPE_long
		name = SAF_progressive_party_COPE
	}
}

2013.1.1 = {
	set_party_name = {
		ideology = communist
		long_name = SAF_communist_party_EFF_long
		name = SAF_communist_party_EFF
	}
}

2014.5.7 = {
	set_politics = {
		parties = {
			islamist = { popularity = 0 }
			nationalist = { popularity = 0 }
			reactionary = { popularity = 2 }
			conservative = { popularity = 1 }
			market_liberal = { popularity = 3 }
			social_liberal = { popularity = 25 }
			social_democrat = { popularity = 60 }
			progressive = { popularity = 1 }
			democratic_socialist = { popularity = 1 }
			communist = { popularity = 7 }
		}
		ruling_party = social_democrat
		last_election = "2014.5.7"
		election_frequency = 60
		elections_allowed = yes
	}
	create_country_leader = {
		name = "Jacob Zuma"
		picture = "Jacob_Zuma.dds"
		ideology = social_democrat_ideology
	}
	create_country_leader = {
		name = "Helen Zille"
		picture = "Helen_Zille.dds"
		ideology = liberalist
	}
	create_country_leader = {
		name = "Julius Malema"
		picture = "Julius_Malema.dds"
		ideology = marxist
	}
	create_country_leader = {
		name = "Mosiuoa_Lekota"
		picture = "Mosiuoa_Lekota.dds"
		ideology = progressive_ideology
	}
}