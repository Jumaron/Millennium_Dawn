﻿capital = 292

oob = "SAU_2000"

set_convoys = 370
set_stability = 0.5

set_country_flag = country_language_arabic

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	AT_upgrade_1 = 1
	Heavy_Anti_tank_1 = 1
	Anti_tank_1 = 1 
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

set_country_flag = dominant_religion_islam
set_country_flag = sunni_islam

add_ideas = {
	population_growth_rapid
	arab_league_member
	limited_conscription
}

set_politics = {

	parties = {
		
		islamist = {
			popularity = 25
		}
		nationalist = {
			popularity = 10
		}
		monarchist = {
			popularity = 30
		}
		conservative = {
			popularity = 18
		}
		market_liberal = {
			popularity = 10
		}
		social_liberal = {
			popularity = 2
		}
		communist = {
			popularity = 5
		}
		
	}
	
	ruling_party = monarchist
	last_election = "1982.1.1"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "King Abdullah"
	picture = "King_Abdullah.dds"
	ideology = absolute_monarchist
}

create_country_leader = {
	name = "Abdulrahman Al Banyan"
	picture = "Abdulrahman_Al_Banyan.dds" 
	ideology = national_democrat
}

create_country_leader = {
	name = "Saleh el-Mansour"
	picture = "Saleh_el-Mansour.dds"
	ideology = marxist
}

create_country_leader = {
    name = "Abdul-Aziz Al ash-Sheikh" 
	picture = "Abdulaziz_Al_Sheikh.dds"
	ideology = islamic_authoritarian
}

create_country_leader = {
    name = "Mohammad al-Massari" 
	picture = "Mohammad_al-Massari.dds"
	ideology = falangist
}

create_country_leader = {
    name = "Ahmed Mohammad al-Rebh"
	picture = "Ahmed_Alrebh.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
    name = "Mohammad Fahad Muflih al-Qahtani"
	picture = "Mohammad_F_Muflih.dds"
	ideology = centrist
}
	
create_country_leader = {
    name = "Ibrahim al-Mugaiteeb"
	picture = "Ibrahim_al-Mugaiteeb.dds"
	ideology = constitutionalist
}
	
create_country_leader = {
    name = "Khalid Bin Jamal" 
	picture = "Khalid_Bin_Jamal.dds"
	ideology = counter_progressive_democrat 
} 

create_country_leader = {
    name = "Badr Bin Abdul Aziz" 
	picture = "Badr_bin_Abdul_Aziz.dds"
	ideology = libertarian
}

2013.12.11 = {
	create_faction = peninsula_shield_force
	add_to_faction = SAU
	add_to_faction = KUW
	add_to_faction = OMA
	add_to_faction = BAH
	add_to_faction = UAE
	add_to_faction = QAT
}

2015.1.23 = {
	create_country_leader = {
		name = "King Salman"
		picture = "King_Salman.dds"
		ideology = absolute_monarchist
	}
}