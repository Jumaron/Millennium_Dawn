focus_tree = {
	id = Syria_Focus
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = SYR
		}
	}
	default = no
	focus = {
		id = SYR_civil_war_victory
		icon = GFX_goal_unknown
		cost = 10,00
		x = 8
		y = 1

	}
	focus = {
		id = SYR_greater_syria
		icon = GFX_goal_unknown
		cost = 10,00
		x = 6
		y = 9

	}
	focus = {
		id = SYR_reestablish_diplomacy
		icon = GFX_goal_unknown
		cost = 10,00
		x = 22
		y = 1

	}
	focus = {
		id = SYR_reform_navy
		icon = GFX_goal_unknown
		cost = 10,00
		x = 26
		y = 9

	}
	focus = {
		id = SYR_reinforce_airforce
		icon = GFX_goal_unknown
		cost = 10,00
		x = 26
		y = 5

	}
	focus = {
		id = SYR_restructure_military
		icon = GFX_goal_unknown
		cost = 10,00
		x = 16
		y = 9

	}
	focus = {
		id = SYR_rebuild_economy
		icon = GFX_goal_unknown
		cost = 10,00
		x = 38
		y = 1

	}
	focus = {
		id = SYR_kurdish_question
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_civil_war_victory
		}
		x = 0
		y = 1
		relative_position_id = SYR_civil_war_victory

	}
	focus = {
		id = SYR_free_syria
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_kurdish_question
		}
		x = 0
		y = 2
		relative_position_id = SYR_civil_war_victory

	}
	focus = {
		id = SYR_syrian_arab_army
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_free_syria
		}
		x = 0
		y = 3
		relative_position_id = SYR_civil_war_victory

	}
	focus = {
		id = SYR_political_plurality
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_syrian_arab_army
		}
		x = -1
		y = 4
		relative_position_id = SYR_civil_war_victory

	}
	focus = {
		id = SYR_form_of_government
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_syrian_arab_army
		}
		x = 1
		y = 4
		relative_position_id = SYR_civil_war_victory

	}
	focus = {
		id = SYR_ethnicity_status
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_political_plurality
		}
		prerequisite = {
			focus = SYR_form_of_government
		}
		x = 0
		y = 5
		relative_position_id = SYR_civil_war_victory

	}
	focus = {
		id = SYR_elections
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_ethnicity_status
		}
		x = 0
		y = 6
		relative_position_id = SYR_civil_war_victory

	}
	focus = {
		id = SYR_kurds_in_turkey
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_greater_syria
		}
		x = 1
		y = 1
		relative_position_id = SYR_greater_syria

	}
	focus = {
		id = SYR_taurus_mountains
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_kurds_in_turkey
		}
		x = 1
		y = 2
		relative_position_id = SYR_greater_syria

	}
	focus = {
		id = SYR_iraq_diplomacy
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_reestablish_diplomacy
		}
		x = 2
		y = 1
		relative_position_id = SYR_reestablish_diplomacy

	}
	focus = {
		id = SYR_iran_dues
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_iraq_diplomacy
		}
		x = 2
		y = 2
		relative_position_id = SYR_reestablish_diplomacy

	}
	focus = {
		id = SYR_deploy_spies_cyprus
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_greater_syria
		}
		x = 3
		y = 1
		relative_position_id = SYR_greater_syria

	}
	focus = {
		id = SYR_invade_cyprus
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_deploy_spies_cyprus
		}
		x = 3
		y = 2
		relative_position_id = SYR_greater_syria

	}
	focus = {
		id = SYR_islamic_emirate
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_kurdish_question
		}
		x = 4
		y = 2
		relative_position_id = SYR_civil_war_victory

	}
	focus = {
		id = SYR_tahrir_show_true_colors
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_islamic_emirate
		}
		mutually_exclusive = {
			focus = SYR_tahrir_go_alone
		}
		x = 3
		y = 3
		relative_position_id = SYR_civil_war_victory

	}
	focus = {
		id = SYR_tahrir_go_alone
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_islamic_emirate
		}
		mutually_exclusive = {
			focus = SYR_tahrir_show_true_colors
		}
		x = 5
		y = 3
		relative_position_id = SYR_civil_war_victory

	}
	focus = {
		id = SYR_true_caliphate
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_tahrir_show_true_colors
			focus = SYR_tahrir_go_alone
		}
		mutually_exclusive = {
			focus = SYR_align_with_isis
		}
		x = 3
		y = 4
		relative_position_id = SYR_civil_war_victory

	}
	focus = {
		id = SYR_hatay_question
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_reestablish_diplomacy
		}
		x = 4
		y = 1
		relative_position_id = SYR_reestablish_diplomacy

	}
	focus = {
		id = SYR_national_oil_production
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_rebuild_economy
		}
		mutually_exclusive = {
			focus = SYR_foreign_oil_fields
		}
		x = 5
		y = 1
		relative_position_id = SYR_rebuild_economy

	}
	focus = {
		id = SYR_foreign_oil_fields
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_rebuild_economy
		}
		mutually_exclusive = {
			focus = SYR_national_oil_production
		}
		x = 3
		y = 1
		relative_position_id = SYR_rebuild_economy

	}
	focus = {
		id = SYR_national_phosphate_expansion
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_foreign_oil_fields
			focus = SYR_national_oil_production
		}
		mutually_exclusive = {
			focus = SYR_foreign_phosphate_expansion
		}
		x = 5
		y = 2
		relative_position_id = SYR_rebuild_economy

	}
	focus = {
		id = SYR_align_with_isis
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_tahrir_go_alone
		}
		mutually_exclusive = {
			focus = SYR_true_caliphate
		}
		x = 5
		y = 4
		relative_position_id = SYR_civil_war_victory

	}
	focus = {
		id = SYR_stance_west
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_reestablish_diplomacy
		}
		x = 7
		y = 1
		relative_position_id = SYR_reestablish_diplomacy

	}
	focus = {
		id = SYR_development_USA
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_stance_west
		}
		x = 6
		y = 2
		relative_position_id = SYR_reestablish_diplomacy

	}
	focus = {
		id = SYR_rebuild_schools
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_rebuild_economy
		}
		x = 7
		y = 1
		relative_position_id = SYR_rebuild_economy

	}
	focus = {
		id = SYR_education_reform
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_rebuild_schools
		}
		x = 7
		y = 2
		relative_position_id = SYR_rebuild_economy

	}
	focus = {
		id = SYR_development_germany
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_stance_west
		}
		x = 7
		y = 3
		relative_position_id = SYR_reestablish_diplomacy

	}
	focus = {
		id = SYR_development_UK
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_stance_west
		}
		x = 8
		y = 2
		relative_position_id = SYR_reestablish_diplomacy

	}
	focus = {
		id = SYR_rebuild_hospitals
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_rebuild_economy
		}
		x = 9
		y = 1
		relative_position_id = SYR_rebuild_economy

	}
	focus = {
		id = SYR_housing_projects
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_rebuild_hospitals
		}
		x = 9
		y = 2
		relative_position_id = SYR_rebuild_economy

	}
	focus = {
		id = SYR_refugee_incentives
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_housing_projects
		}
		x = 9
		y = 3
		relative_position_id = SYR_rebuild_economy

	}
	focus = {
		id = SYR_support_hezbollah
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_reestablish_diplomacy
		}
		mutually_exclusive = {
			focus = SYR_against_hezbollah
		}
		x = -4
		y = 1
		relative_position_id = SYR_reestablish_diplomacy

	}
	focus = {
		id = SYR_against_hezbollah
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_reestablish_diplomacy
		}
		mutually_exclusive = {
			focus = SYR_support_hezbollah
		}
		x = -6
		y = 1
		relative_position_id = SYR_reestablish_diplomacy

	}
	focus = {
		id = SYR_golan_question
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_jordan_diplomacy
		}
		x = -1
		y = 2
		relative_position_id = SYR_reestablish_diplomacy

	}
	focus = {
		id = SYR_prepare_lebanon_coup
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_support_hezbollah
		}
		x = -4
		y = 2
		relative_position_id = SYR_reestablish_diplomacy

	}
	focus = {
		id = SYR_lebanese_coup
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_prepare_lebanon_coup
		}
		x = -4
		y = 3
		relative_position_id = SYR_reestablish_diplomacy

	}
	focus = {
		id = SYR_shadow_over_hezbollah
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_lebanese_coup
		}
		x = -4
		y = 4
		relative_position_id = SYR_reestablish_diplomacy

	}
	focus = {
		id = SYR_lebanese_support
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_against_hezbollah
		}
		x = -7
		y = 2
		relative_position_id = SYR_reestablish_diplomacy

	}
	focus = {
		id = SYR_strike_hezbollah
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_against_hezbollah
			focus = SYR_lebanese_support
		}
		x = -6
		y = 3
		relative_position_id = SYR_reestablish_diplomacy

	}
	focus = {
		id = SYR_shadow_over_lebanon
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_strike_hezbollah
		}
		x = -6
		y = 4
		relative_position_id = SYR_reestablish_diplomacy

	}
	focus = {
		id = SYR_assad_day
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_kurdish_question
		}
		x = -4
		y = 2
		relative_position_id = SYR_civil_war_victory

	}
	focus = {
		id = SYR_incorporate_ssnp
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_assad_day
		}
		mutually_exclusive = {
			focus = SYR_ssnp_traitors
		}
		x = -5
		y = 3
		relative_position_id = SYR_civil_war_victory

	}
	focus = {
		id = SYR_ssnp_traitors
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_assad_day
		}
		mutually_exclusive = {
			focus = SYR_incorporate_ssnp
		}
		x = -3
		y = 3
		relative_position_id = SYR_civil_war_victory

	}
	focus = {
		id = SYR_integrate_palestinians
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_incorporate_ssnp
			focus = SYR_ssnp_traitors
		}
		mutually_exclusive = {
			focus = SYR_expel_palestinians
		}
		x = -5
		y = 4
		relative_position_id = SYR_civil_war_victory

	}
	focus = {
		id = SYR_expel_palestinians
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_ssnp_traitors
			focus = SYR_incorporate_ssnp
		}
		mutually_exclusive = {
			focus = SYR_integrate_palestinians
		}
		x = -3
		y = 4
		relative_position_id = SYR_civil_war_victory

	}
	focus = {
		id = SYR_formalise_shia_militias
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_integrate_palestinians
			focus = SYR_expel_palestinians
		}
		mutually_exclusive = {
			focus = SYR_disarm_militias
		}
		x = -5
		y = 5
		relative_position_id = SYR_civil_war_victory

	}
	focus = {
		id = SYR_disarm_militias
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_expel_palestinians
			focus = SYR_integrate_palestinians
		}
		mutually_exclusive = {
			focus = SYR_formalise_shia_militias
		}
		x = -3
		y = 5
		relative_position_id = SYR_civil_war_victory

	}
	focus = {
		id = SYR_ssnp_coup
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_incorporate_ssnp
		}
		x = -7
		y = 4
		relative_position_id = SYR_civil_war_victory

	}
	focus = {
		id = SYR_russian_tool
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_formalise_shia_militias
			focus = SYR_disarm_militias
		}
		mutually_exclusive = {
			focus = SYR_iranian_puppet
		}
		x = -5
		y = 6
		relative_position_id = SYR_civil_war_victory

	}
	focus = {
		id = SYR_iranian_puppet
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_disarm_militias
			focus = SYR_formalise_shia_militias
		}
		mutually_exclusive = {
			focus = SYR_russian_tool
		}
		x = -3
		y = 6
		relative_position_id = SYR_civil_war_victory

	}
	focus = {
		id = SYR_global_recruitment
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_true_caliphate
			focus = SYR_align_with_isis
		}
		x = 4
		y = 5
		relative_position_id = SYR_civil_war_victory

	}
	focus = {
		id = SYR_campaign_of_terror
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_global_recruitment
		}
		x = 4
		y = 6
		relative_position_id = SYR_civil_war_victory

	}
	focus = {
		id = SYR_jordan_diplomacy
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_reestablish_diplomacy
		}
		x = -1
		y = 1
		relative_position_id = SYR_reestablish_diplomacy

	}
	focus = {
		id = SYR_two_state_solution
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_golan_question
		}
		mutually_exclusive = {
			focus = SYR_support_palestine
		}
		x = -2
		y = 3
		relative_position_id = SYR_reestablish_diplomacy

	}
	focus = {
		id = SYR_support_palestine
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_golan_question
		}
		mutually_exclusive = {
			focus = SYR_two_state_solution
		}
		x = 0
		y = 3
		relative_position_id = SYR_reestablish_diplomacy

	}
	focus = {
		id = SYR_russia_dues
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_hatay_question
		}
		x = 4
		y = 2
		relative_position_id = SYR_reestablish_diplomacy

	}
	focus = {
		id = SYR_lebanese_chaos
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_greater_syria
		}
		x = -3
		y = 1
		relative_position_id = SYR_greater_syria

	}
	focus = {
		id = SYR_deal_with_scraps
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_lebanese_chaos
		}
		x = -3
		y = 2
		relative_position_id = SYR_greater_syria

	}
	focus = {
		id = SYR_arm_palestinians
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_deal_with_scraps
		}
		x = -5
		y = 3
		relative_position_id = SYR_greater_syria

	}
	focus = {
		id = SYR_zionist_threat
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_deal_with_scraps
			focus = SYR_arm_palestinians
		}
		x = -4
		y = 4
		relative_position_id = SYR_greater_syria

	}
	focus = {
		id = SYR_assault_jordan
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_deal_with_scraps
		}
		x = -2
		y = 4
		relative_position_id = SYR_greater_syria

	}
	focus = {
		id = SYR_befriend_sudan
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_zionist_threat
			focus = SYR_assault_jordan
		}
		x = -5
		y = 5
		relative_position_id = SYR_greater_syria

	}
	focus = {
		id = SYR_demand_sinai
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_befriend_sudan
			focus = SYR_zionist_threat
			focus = SYR_assault_jordan
		}
		x = -3
		y = 6
		relative_position_id = SYR_greater_syria

	}
	focus = {
		id = SYR_iraq_divisions
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_greater_syria
		}
		x = -1
		y = 1
		relative_position_id = SYR_greater_syria

	}
	focus = {
		id = SYR_mop_up_iraq
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_iraq_divisions
		}
		x = -1
		y = 2
		relative_position_id = SYR_greater_syria

	}
	focus = {
		id = SYR_submission_kuwait
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_mop_up_iraq
		}
		x = -1
		y = 3
		relative_position_id = SYR_greater_syria

	}
	focus = {
		id = SYR_akrotiri
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_invade_cyprus
		}
		x = 3
		y = 3
		relative_position_id = SYR_greater_syria

	}
	focus = {
		id = SYR_2nd_agrarian_reform
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_rebuild_economy
		}
		x = 0
		y = 1
		relative_position_id = SYR_rebuild_economy

	}
	focus = {
		id = SYR_loan_money_iran
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_rebuild_economy
		}
		mutually_exclusive = {
			focus = SYR_loan_money_saudi
		}
		x = -5
		y = 1
		relative_position_id = SYR_rebuild_economy

	}
	focus = {
		id = SYR_loan_money_russia
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_loan_money_iran
			focus = SYR_loan_money_saudi
		}
		mutually_exclusive = {
			focus = SYR_loan_money_eu
		}
		x = -5
		y = 2
		relative_position_id = SYR_rebuild_economy

	}
	focus = {
		id = SYR_loan_money_china
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_loan_money_russia
			focus = SYR_loan_money_eu
		}
		mutually_exclusive = {
			focus = SYR_loan_money_us
		}
		x = -5
		y = 3
		relative_position_id = SYR_rebuild_economy

	}
	focus = {
		id = SYR_loan_money_us
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_loan_money_eu
			focus = SYR_loan_money_russia
		}
		mutually_exclusive = {
			focus = SYR_loan_money_china
		}
		x = -3
		y = 3
		relative_position_id = SYR_rebuild_economy

	}
	focus = {
		id = SYR_loan_money_eu
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_loan_money_iran
			focus = SYR_loan_money_saudi
		}
		mutually_exclusive = {
			focus = SYR_loan_money_russia
		}
		x = -3
		y = 2
		relative_position_id = SYR_rebuild_economy

	}
	focus = {
		id = SYR_loan_money_saudi
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_rebuild_economy
		}
		mutually_exclusive = {
			focus = SYR_loan_money_iran
		}
		x = -3
		y = 1
		relative_position_id = SYR_rebuild_economy

	}
	focus = {
		id = SYR_foreign_phosphate_expansion
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_foreign_oil_fields
			focus = SYR_national_oil_production
		}
		mutually_exclusive = {
			focus = SYR_national_phosphate_expansion
		}
		x = 3
		y = 2
		relative_position_id = SYR_rebuild_economy

	}
	focus = {
		id = SYR_foreign_weapon_deal
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_restructure_military
		}
		x = -3
		y = 1
		relative_position_id = SYR_restructure_military

	}
	focus = {
		id = SYR_purge_military
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_restructure_military
		}
		x = 0
		y = 1
		relative_position_id = SYR_restructure_military

	}
	focus = {
		id = SYR_new_officer_corps
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_purge_military
		}
		x = 0
		y = 2
		relative_position_id = SYR_restructure_military

	}
	focus = {
		id = SYR_anti_guerilla_army
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_restructure_military
		}
		mutually_exclusive = {
			focus = SYR_large_scale_army
		}
		x = 2
		y = 1
		relative_position_id = SYR_restructure_military

	}
	focus = {
		id = SYR_large_scale_army
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_restructure_military
		}
		mutually_exclusive = {
			focus = SYR_anti_guerilla_army
		}
		x = 4
		y = 1
		relative_position_id = SYR_restructure_military

	}
	focus = {
		id = SYR_conscription
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_anti_guerilla_army
			focus = SYR_large_scale_army
		}
		x = 3
		y = 2
		relative_position_id = SYR_restructure_military

	}
	focus = {
		id = SYR_loyal_elite
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_conscription
		}
		mutually_exclusive = {
			focus = SYR_people_army
		}
		x = 2
		y = 3
		relative_position_id = SYR_restructure_military

	}
	focus = {
		id = SYR_people_army
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_conscription
		}
		mutually_exclusive = {
			focus = SYR_loyal_elite
		}
		x = 4
		y = 3
		relative_position_id = SYR_restructure_military

	}
	focus = {
		id = SYR_tartus_base
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_reform_navy
		}
		x = -2
		y = 1
		relative_position_id = SYR_reform_navy

	}
	focus = {
		id = SYR_tartus_dockyards
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_tartus_base
		}
		x = -2
		y = 2
		relative_position_id = SYR_reform_navy

	}
	focus = {
		id = SYR_submarines
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_reform_navy
		}
		mutually_exclusive = {
			focus = SYR_surface_fleet
		}
		x = 0
		y = 1
		relative_position_id = SYR_reform_navy

	}
	focus = {
		id = SYR_surface_fleet
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_reform_navy
		}
		mutually_exclusive = {
			focus = SYR_submarines
		}
		x = 2
		y = 1
		relative_position_id = SYR_reform_navy

	}
	focus = {
		id = SYR_new_naval_generation
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_submarines
			focus = SYR_surface_fleet
		}
		x = 1
		y = 2
		relative_position_id = SYR_reform_navy

	}
	focus = {
		id = SYR_domestic_ship_production
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_tartus_dockyards
		}
		mutually_exclusive = {
			focus = SYR_russian_naval_school
		}
		x = -3
		y = 3
		relative_position_id = SYR_reform_navy

	}
	focus = {
		id = SYR_russian_naval_school
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_tartus_dockyards
		}
		mutually_exclusive = {
			focus = SYR_domestic_ship_production
		}
		x = -1
		y = 3
		relative_position_id = SYR_reform_navy

	}
	focus = {
		id = SYR_arms_factories
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_foreign_weapon_deal
		}
		x = -3
		y = 2
		relative_position_id = SYR_restructure_military

	}
	focus = {
		id = SYR_domestic_research
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_arms_factories
		}
		mutually_exclusive = {
			focus = SYR_iranian_military_cooperation
		}
		x = -4
		y = 3
		relative_position_id = SYR_restructure_military

	}
	focus = {
		id = SYR_iranian_military_cooperation
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_arms_factories
		}
		mutually_exclusive = {
			focus = SYR_domestic_research
		}
		x = -2
		y = 3
		relative_position_id = SYR_restructure_military

	}
	focus = {
		id = SYR_iranian_aircraft
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_reinforce_airforce
		}
		mutually_exclusive = {
			focus = SYR_western_procurement
			focus = SYR_russian_procurement
			focus = SYR_asian_procurement
		}
		x = -3
		y = 1
		relative_position_id = SYR_reinforce_airforce

	}
	focus = {
		id = SYR_russian_procurement
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_reinforce_airforce
		}
		mutually_exclusive = {
			focus = SYR_western_procurement
			focus = SYR_iranian_aircraft
			focus = SYR_asian_procurement
		}
		x = -1
		y = 1
		relative_position_id = SYR_reinforce_airforce

	}
	focus = {
		id = SYR_asian_procurement
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_reinforce_airforce
		}
		mutually_exclusive = {
			focus = SYR_western_procurement
			focus = SYR_russian_procurement
			focus = SYR_iranian_aircraft
		}
		x = 1
		y = 1
		relative_position_id = SYR_reinforce_airforce

	}
	focus = {
		id = SYR_western_procurement
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_reinforce_airforce
		}
		mutually_exclusive = {
			focus = SYR_asian_procurement
			focus = SYR_russian_procurement
			focus = SYR_iranian_aircraft
		}
		x = 3
		y = 1
		relative_position_id = SYR_reinforce_airforce

	}
	focus = {
		id = SYR_aerial_training
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_iranian_aircraft
			focus = SYR_russian_procurement
			focus = SYR_asian_procurement
			focus = SYR_western_procurement
		}
		x = 0
		y = 2
		relative_position_id = SYR_reinforce_airforce

	}
	focus = {
		id = SYR_telecom
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_2nd_agrarian_reform
		}
		x = 0
		y = 2
		relative_position_id = SYR_rebuild_economy

	}
	focus = {
		id = SYR_electronic_army
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_domestic_research
			focus = SYR_iranian_military_cooperation
		}
		x = -3
		y = 4
		relative_position_id = SYR_restructure_military

	}
	focus = {
		id = SYR_increase_nationalisation
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_telecom
		}
		mutually_exclusive = {
			focus = SYR_increase_economic_freedom
		}
		x = -1
		y = 3
		relative_position_id = SYR_rebuild_economy

	}
	focus = {
		id = SYR_increase_economic_freedom
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_telecom
		}
		mutually_exclusive = {
			focus = SYR_increase_nationalisation
		}
		x = 1
		y = 3
		relative_position_id = SYR_rebuild_economy

	}
	focus = {
		id = SYR_free_trade_partner
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_foreign_phosphate_expansion
			focus = SYR_national_phosphate_expansion
		}
		x = 4
		y = 3
		relative_position_id = SYR_rebuild_economy

	}
	focus = {
		id = SYR_invest_in_financial
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_increase_nationalisation
			focus = SYR_increase_economic_freedom
		}
		x = -1
		y = 4
		relative_position_id = SYR_rebuild_economy

	}
	focus = {
		id = SYR_invest_heavy_industry
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_increase_nationalisation
			focus = SYR_increase_economic_freedom
		}
		x = 1
		y = 4
		relative_position_id = SYR_rebuild_economy

	}
	focus = {
		id = SYR_invigorate_tourism
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_cooperate_syrair
		}
		prerequisite = {
			focus = SYR_invest_in_financial
		}
		x = 8
		y = 2
		relative_position_id = SYR_reinforce_airforce

	}
	focus = {
		id = SYR_cooperate_syrair
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_reinforce_airforce
		}
		x = 5
		y = 1
		relative_position_id = SYR_reinforce_airforce

	}
	focus = {
		id = SYR_syrian_railways
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_invest_heavy_industry
		}
		x = 1
		y = 5
		relative_position_id = SYR_rebuild_economy

	}
	focus = {
		id = SYR_renegotiate_loans
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_invest_in_financial
		}
		prerequisite = {
			focus = SYR_loan_money_china
		}
		prerequisite = {
			focus = SYR_loan_money_us
		}
		x = -4
		y = 5
		relative_position_id = SYR_rebuild_economy

	}
	focus = {
		id = SYR_fly_damas
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_cooperate_syrair
		}
		x = 4
		y = 2
		relative_position_id = SYR_reinforce_airforce

	}
	focus = {
		id = SYR_syrian_pearl
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_cooperate_syrair
		}
		x = 6
		y = 2
		relative_position_id = SYR_reinforce_airforce

	}
	focus = {
		id = SYR_new_universities
		icon = GFX_goal_unknown
		cost = 10,00
		prerequisite = {
			focus = SYR_education_reform
		}
		x = 7
		y = 3
		relative_position_id = SYR_rebuild_economy

	}
}
